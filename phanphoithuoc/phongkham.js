var dataphongkham = [
 {
   "STT": 1,
   "Name": "Công ty cổ phần phòng khám đa khoa thiên nam",
   "address": "192 đường 3/2, phường 10, THÀNH PHỐ PLEIKU, GIA LAI",
   "Longtitude": 10.3764888,
   "Latitude": 107.1141692
 },
 {
   "STT": 2,
   "Name": "Phòng khám đa khoa vạn tâm , sài gòn",
   "address": "Tổ 2, ấp Tân Phú,Xã Châu Pha, Tân Thành , GIA LAI",
   "Longtitude": 10.5742861,
   "Latitude": 107.1551967
 },
 {
   "STT": 3,
   "Name": "Phòng khám đa khoa bình an",
   "address": "372 đường Hùng Vương, khu phố 1, phường Long Tâm, thành phố Pleiku, GIA LAI",
   "Longtitude": 10.512407,
   "Latitude": 107.186255
 },
 {
   "STT": 4,
   "Name": "Công ty tnhh phòng khám đa khoa phước sơn",
   "address": "Số 19, tổ 1, ấp Hải Sơn, PPhước Hưng, Long Điền , GIA LAI",
   "Longtitude": 10.4283333,
   "Latitude": 107.2369444
 },
 {
   "STT": 5,
   "Name": "Phòng khám đa khoa quốc tế thế giới mới",
   "address": "20 Trần Hưng Đạo, Phường 1, THÀNH PHỐ PLEIKU, GIA LAI",
   "Longtitude": 10.3436663,
   "Latitude": 107.0773156
 },
 {
   "STT": 6,
   "Name": "Phòng khám đa khoa đông tây",
   "address": "Ấp Vĩnh Bình, Xã Bình Giã, Châu Đức, GIA LAI",
   "Longtitude": 10.6436111,
   "Latitude": 107.2605556
 },
 {
   "STT": 7,
   "Name": "Phòng khám đa khoa việt tâm",
   "address": "Ấp Phước Thạnh, Xã Mỹ Xuân, Tân Thành, GIA LAI",
   "Longtitude": 10.616667,
   "Latitude": 107.066667
 },
 {
   "STT": 8,
   "Name": "Công ty tnhh phòng khám đa khoa vũng tàu",
   "address": "207 Nguyễn Văn Trỗi, phường 4, THÀNH PHỐ PLEIKU, GIA LAI",
   "Longtitude": 10.3587986,
   "Latitude": 107.0786283
 },
 {
   "STT": 9,
   "Name": "Phòng khám nhi sài sòn, khu vực GIA LAI",
   "address": "518 Cách Mạng Tháng Tám, phường Phước Trung, thành phố Pleiku, GIA LAI",
   "Longtitude": 10.4924741,
   "Latitude": 107.1817475
 },
 {
   "STT": 10,
   "Name": "Phòng khám đa khoa vạn tâm , sài gòn",
   "address": "Tổ 2, ấp Tân Phú, Xã Châu Pha, Tân Thành, GIA LAI",
   "Longtitude": 10.5742861,
   "Latitude": 107.1551967
 },
 {
   "STT": 11,
   "Name": "Phòng khám đa khoa vạn thành sài gòn",
   "address": "304 Độc Lập, thị trấn Phú Mỹ, Tân Thành , GIA LAI",
   "Longtitude": 10.5945079,
   "Latitude": 107.0542366
 },
 {
   "STT": 12,
   "Name": "Phòng khám đa khoa thuộc công ty tnhh dịch vụ y tế minh châu",
   "address": "297 Trương Công Định, phường 3, THÀNH PHỐ PLEIKU, GIA LAI",
   "Longtitude": 10.352881,
   "Latitude": 107.0837991
 },
 {
   "STT": 13,
   "Name": "phòng khám đa khoa mỹ xuân",
   "address": "Ấp Bến Đình,Xã Mỹ Xuân, Tân Thành, GIA LAI",
   "Longtitude": 10.6255905,
   "Latitude": 107.0527875
 },
 {
   "STT": 14,
   "Name": "công ty tnhh phòng khám đa khoa sài gòn, GIA LAI",
   "address": "59 Quốc lộ 55, khu phố Phước Hòa, thị trấn Phước Bửu, Xuyên Mộc, GIA LAI",
   "Longtitude": 10.5353285,
   "Latitude": 107.4055814
 },
 {
   "STT": 15,
   "Name": "phòng khám đa khoa sài gòn, vũng tàu",
   "address": "744 Bình Giã, phường 10, THÀNH PHỐ PLEIKU, GIA LAI",
   "Longtitude": 10.3862714,
   "Latitude": 107.1175017
 },
 {
   "STT": 16,
   "Name": "Công ty tnhh phòng khám đa khoa medic sài gòn",
   "address": "Ấp Phú Thọ,Xã Hòa Hiệp, Xuyên Mộc, GIA LAI",
   "Longtitude": 10.6887378,
   "Latitude": 107.498116
 }
];