var mook = {
    sobn: function () {
        let raw = `
        Bệnh viện Điều dưỡng - Phục hồi chức năng TỈNH GIA LAI
        Trung tâm chăm sóc sức khỏe sinh sản
        Trung tâm Y tế thị xã Chư Păh
        Trung tâm Y tế thị xã Chư Prông
        Trung tâm Y tế thị xã Chư Pưh
        Trung tâm Y tế thị xã Chư Sê
        Trung tâm Y tế thị xã Đắk Đoa
        Trung tâm Y tế thị xã An Khê
        Trung tâm Y tế thị xã Ayun Pa`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    },

    tinhuy: function () {
        let raw = `
        Bệnh viện Điều dưỡng - Phục hồi chức năng TỈNH GIA LAI
        Trung tâm chăm sóc sức khỏe sinh sản
        Trung tâm Y tế huyện Chư Păh
        Trung tâm Y tế huyện Chư Prông
        Trung tâm Y tế huyện Chư Pưh
        Trung tâm Y tế huyện Chư Sê
        Trung tâm Y tế huyện Đắk Đoa
        Trung tâm Y tế thị xã An Khê
        Trung tâm Y tế thị xã Ayun Pa`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    },

    ubnd: function () {
        let raw = `TP. Pleiku‎                
                H. Chư Păh
                    H. Chư Prông
                H. Chư Pưh
                    H. Chư Sê
                    H. Đắk Đoa`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    },
    cdv: function () {
        let raw = `
        Bệnh viện Điều dưỡng - Phục hồi chức năng TỈNH GIA LAI
        Trung tâm chăm sóc sức khỏe sinh sản
        Trung tâm Y tế thị xã Chư Păh
        Trung tâm Y tế thị xã Chư Prông
        Trung tâm Y tế thị xã Chư Pưh
        Trung tâm Y tế thị xã Chư Sê
        Trung tâm Y tế thị xã Đắk Đoa
        Trung tâm Y tế thị xã An Khê
        Trung tâm Y tế thị xã Ayun Pa`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    },

    http: function () {
        let raw = `
        Bệnh viện Điều dưỡng - Phục hồi chức năng TỈNH GIA LAI
        Trung tâm chăm sóc sức khỏe sinh sản
        Trung tâm Y tế thị xã Chư Păh
        Trung tâm Y tế thị xã Chư Prông
        Trung tâm Y tế thị xã Chư Pưh
        Trung tâm Y tế thị xã Chư Sê
        Trung tâm Y tế thị xã Đắk Đoa
        Trung tâm Y tế thị xã An Khê
        Trung tâm Y tế thị xã Ayun Pa`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    }
}
