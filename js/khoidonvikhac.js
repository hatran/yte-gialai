var datakhoidonvikhac = [
 {
   "STT": 1,
   "Name": "Trung tâm Kiểm soát bệnh tật",
   "area": "Tỉnh",
   "address": "394 Lê Đại Hành, Phường Mỹ Phú, TP. Pleiku, GIA LAI, Việt Nam",
   "Longtitude": 10.4694181,
   "Latitude": 105.6447887
 },
 {
   "STT": 2,
   "Name": "Trung tâm Kiểm nghiệm",
   "area": "Tỉnh",
   "address": "Phường Mỹ Phú, TP. Pleiku, GIA LAI, Việt Nam",
   "Longtitude": 10.4697076,
   "Latitude": 105.6450797
 },
 {
   "STT": 3,
   "Name": "Trung tâm Pháp y",
   "area": "Tỉnh",
   "address": "392 Lê Đại Hành, Phường Mỹ Phú, TP. Pleiku, GIA LAI, Việt Nam",
   "Longtitude": 10.4688764,
   "Latitude": 105.6453242
 },
 {
   "STT": 4,
   "Name": "Trung tâm Giám định Y khoa",
   "area": "Tỉnh",
   "address": "Mỹ Tân Thị Xã Cao Lãnh, Mỹ Tân, TP. Pleiku, GIA LAI, Việt Nam",
   "Longtitude": 10.4812685,
   "Latitude": 105.5944205
 }
];