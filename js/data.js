var data = [
 {
   "STT": 1,
   "Name": "Bệnh viện Lê Lợi",
   "address": "22 đường Lê Lợi, Phường 7, THÀNH PHỐ PLEIKU, TỈNH GIA LAI",
   "Longtitude": 10.353059,
   "Latitude": 107.074865,
   "Rating": 3.98,
   "Number_of_beds": 600,
   "area": "Tỉnh"
 },
 {
   "STT": 2,
   "Name": "Ban Chăm sóc sức khoẻ Cán bộ",
   "address": "22 đường Lê Lợi, Phường 7, THÀNH PHỐ PLEIKU, TỈNH GIA LAI",
   "Longtitude": 10.358001,
   "Latitude": 107.074988,
   "Rating": 3.98,
   "Number_of_beds": 600,
   "area": "Tỉnh"
 },
 {
   "STT": 3,
   "Name": "Bệnh viện Tâm thần",
   "address": "ấp Bình Mỹ, Châu Đức, GIA LAI, Việt Nam",
   "Longtitude": 10.5909619,
   "Latitude": 107.2284272,
   "Rating": 3.98,
   "Number_of_beds": 600,
   "area": "Tỉnh"
 },
 {
   "STT": 4,
   "Name": "Bệnh viện Mắt",
   "address": "21 Phạm Ngọc Thạch, Phước Hưng, Pleiku, GIA LAI, Việt Nam",
   "Longtitude": 10.5073168,
   "Latitude": 107.1752089,
   "Rating": 3.98,
   "Number_of_beds": 600,
   "area": "Tỉnh"
 },
 {
   "STT": 5,
   "Name": "Bệnh viện Phổi Phạm Hữu Chí",
   "address": "ấp An Đồng, An Nhứt, Long Điền, GIA LAI, Việt Nam",
   "Longtitude": 10.4829744,
   "Latitude": 107.2402242,
   "Rating": 3.98,
   "Number_of_beds": 600,
   "area": "Tỉnh"
 },
 {
   "STT": 6,
   "Name": "Bệnh viện Pleiku",
   "address": "Khu Phè 5, Phước Hưng, Thành phố Pleiku, TỈNH GIA LAI",
   "Longtitude": 10.509607,
   "Latitude": 107.196725,
   "Rating": 3.98,
   "Number_of_beds": 600,
   "area": "Tỉnh"
 },
 {
   "STT": 7,
   "Name": "Trung tâm Y tế THÀNH PHỐ PLEIKU",
   "address": "Phường 6, THÀNH PHỐ PLEIKU, TỈNH GIA LAI",
   "Longtitude": 10.367515,
   "Latitude": 107.076392,
   "Rating": 3.98,
   "Number_of_beds": 600,
   "area": "Tỉnh"
 },
 {
   "STT": 8,
   "Name": "Trung tâm Y tế Long Điền",
   "address": "Xã Tam Phước, Huyện Long Điền, TỈNH GIA LAI",
   "Longtitude": 10.464606,
   "Latitude": 107.215427,
   "Rating": 3.98,
   "Number_of_beds": 600,
   "area": "Tỉnh"
 },
 {
   "STT": 9,
   "Name": "Trung tâm Y tế Xuyên Mộc",
   "address": "Thị Trấn Phước Bửu, Huyện Xuyên Mộc, TỈNH GIA LAI",
   "Longtitude": 10.5390052,
   "Latitude": 107.4108108,
   "Rating": 3.98,
   "Number_of_beds": 600,
   "area": "Tỉnh"
 },
 {
   "STT": 10,
   "Name": "Trung tâm Y tế Tân Thành",
   "address": "Thị Trấn Mỹ Xuân, Huyện Tân Thành, TỈNH GIA LAI",
   "Longtitude": 10.596877,
   "Latitude": 107.058933,
   "Rating": 3.98,
   "Number_of_beds": 600,
   "area": "Tỉnh"
 },
 {
   "STT": 11,
   "Name": "Trung tâm Y tế huyện Châu Đức",
   "address": "Thị Trấn Ngãi Giao, Huyện Châu Đức, TỈNH GIA LAI",
   "Longtitude": 10.647814,
   "Latitude": 107.240635,
   "Rating": 3.98,
   "Number_of_beds": 600,
   "area": "Tỉnh"
 },
 {
   "STT": 12,
   "Name": "Trung tâm Y tế Quân dân y Côn Đảo",
   "address": "Lê Hồng Phong, Huyện Côn Đảo, TỈNH GIA LAI",
   "Longtitude": 8.7120617,
   "Latitude": 106.6011811,
   "Rating": 3.98,
   "Number_of_beds": 600,
   "area": "Tỉnh"
 },
 {
   "STT": 13,
   "Name": "Trung tâm Y tế thành phố Pleiku",
   "address": "Phường Phước Hiệp,Thành phố Pleiku, TỈNH GIA LAI",
   "Longtitude": 10.498684,
   "Latitude": 107.172207,
   "Rating": 3.98,
   "Number_of_beds": 600,
   "area": "Tỉnh"
 },
 {
   "STT": 14,
   "Name": "Trung tâm Y tế huyện Đất Đỏ",
   "address": "Tỉnh Lộ 44, Phước Hội, Đất Đỏ, GIA LAI, Việt Nam",
   "Longtitude": 10.4648746,
   "Latitude": 107.2729932,
   "Rating": 3.98,
   "Number_of_beds": 600,
   "area": "Tỉnh"
 }
];