var datanhathuoc = [
 {
   "STT": 1,
   "Name": "Quầy thuốc Kim Khánh",
   "address": "02/4/A khóm 1, thị trấn Mỹ An, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5225428,
   "Latitude": 105.8402536
 },
 {
   "STT": 2,
   "Name": "Quầy thuốc Toàn Phúc",
   "address": "07-08 lô C, ấp Phú Thạnh, chợ xã Phú Long, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2306393,
   "Latitude": 105.8056247
 },
 {
   "STT": 3,
   "Name": "Quầy thuốc Phạm Oanh",
   "address": "13 khu dân cư mở rộng ấp An Thuận, xã Mỹ An Hưng B, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3831971,
   "Latitude": 105.6119301
 },
 {
   "STT": 4,
   "Name": "Quầy thuốc Hậu Bình",
   "address": "14/B Hùng Vương, khóm 2, thị trấn Mỹ An, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5196331,
   "Latitude": 105.8403271
 },
 // {
 //   "STT": 5,
 //   "Name": "Quầy thuốc Tây Tân",
 //   "address": "142 ấp Gò Bói, xã Tân Hộ Cơ, huyện Tân Hồng, TỈNH GIA LAI",
 //   "Longtitude": 15.9128998,
 //   "Latitude": 79.7399875
 // },
 {
   "STT": 6,
   "Name": "Quầy thuốc Quỳnh Nhung",
   "address": "16 lô A, CDC xã Phú Long, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2222167,
   "Latitude": 105.7879371
 },
 {
   "STT": 7,
   "Name": "Quầy thuốc Vân Khánh",
   "address": "18A, Nguyễn Sinh Sắc, ấp Phú Long, xã Tân Phú Đông, thị xã Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2879068,
   "Latitude": 105.7485419
 },
 {
   "STT": 8,
   "Name": "Quầy thuốc Duy Anh",
   "address": "19D Nguyễn Thị Minh Khai, khóm 4, thị trấn Mỹ An, huyện  Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5225555,
   "Latitude": 105.8427448
 },
 {
   "STT": 9,
   "Name": "Quầy thuốc Số 3",
   "address": "227 khóm 1, thị trấn Lai Vung, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2938059,
   "Latitude": 105.6588485
 },
 {
   "STT": 10,
   "Name": "Nhà thuốc Hằng Nghiệp",
   "address": "23 tổ 48, khóm 5, phường 6, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4288288,
   "Latitude": 105.6343903
 },
 {
   "STT": 11,
   "Name": "Quầy thuốc Thiện Nhân",
   "address": "256 khóm Bình Thạnh I, thị trấn Lấp Vò, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3607896,
   "Latitude": 105.518867
 },
 {
   "STT": 12,
   "Name": "Quầy thuốc Thiên Lộc",
   "address": "295 khóm 4, thị trấn  Lai Vung, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2938059,
   "Latitude": 105.6588485
 },
 {
   "STT": 13,
   "Name": "Quầy thuốc Thanh Tâm",
   "address": "3, thị trấn Lai Vung, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2899792,
   "Latitude": 105.6581148
 },
 {
   "STT": 14,
   "Name": "Quầy thuốc Cẩm Tö",
   "address": "3, xã Phong Mỹ, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.5318166,
   "Latitude": 105.5826123
 },
 {
   "STT": 15,
   "Name": "Nhà thuốc Kim Phụng",
   "address": "32 Lê Thánh Tôn, khóm 1, phường 2, thị xã Sa Đéc,  TỈNH GIA LAI",
   "Longtitude": 10.2922478,
   "Latitude": 105.7680249
 },
 {
   "STT": 16,
   "Name": "Cơ Sở thuốc Đông Y Vĩnh Khánh",
   "address": "320 tổ 4, ấp Phú Bình, xã Phú Hựu, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2535915,
   "Latitude": 105.8671954
 },
 {
   "STT": 17,
   "Name": "Quầy thuốc Gia Nghi",
   "address": "34 Lê Duẩn, xã Mỹ Trà, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4722983,
   "Latitude": 105.6355941
 },
 {
   "STT": 18,
   "Name": "Quầy thuốc Đức Tín",
   "address": "356 A/5, ấp Tân Thuận, xã Tân Phước, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.8390414,
   "Latitude": 105.5591615
 },
 {
   "STT": 19,
   "Name": "Quầy thuốc Thanh Liễu",
   "address": "4, thị trấn Lai Vung, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2938059,
   "Latitude": 105.6588485
 },
 {
   "STT": 20,
   "Name": "Quầy thuốc Số 4",
   "address": "422, khóm 1, thị trấn Lai Vung, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2938059,
   "Latitude": 105.6588485
 },
 {
   "STT": 21,
   "Name": "Quầy thuốc Kim Ngân",
   "address": "443B ấp An Lợi A, xã Định Yên, huyện  Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.309874,
   "Latitude": 105.5532993
 },
 // {
 //   "STT": 22,
 //   "Name": "Quầy thuốc Hồng Khanh",
 //   "address": "498 Đường 3/2, khóm Bình Thạnh 1, thị trấn Lấp Vò, TỈNH GIA LAI",
 //   "Longtitude": 10.7653413,
 //   "Latitude": 106.662725
 // },
 {
   "STT": 23,
   "Name": "Nhà thuốc Kim Hương",
   "address": "54 đường ThápMười, phường 2, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.457327,
   "Latitude": 105.6361996
 },
 {
   "STT": 24,
   "Name": "Quầy thuốc Số 7",
   "address": "565 khóm Bình Thạnh, thị trấn LấpVò, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3459748,
   "Latitude": 105.5386447
 },
 {
   "STT": 25,
   "Name": "Quầy thuốc Nguyên Huệ",
   "address": "565A ấp Bình Lợi, xã Bình Thành, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3409275,
   "Latitude": 105.5000447
 },
 {
   "STT": 26,
   "Name": "Quầy thuốc Hải Dương",
   "address": "593/2 ấp Hòa Định, xã Tân Hòa, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2074033,
   "Latitude": 105.6471178
 },
 {
   "STT": 27,
   "Name": "Quầy thuốc Bảo Ngọc",
   "address": "595 C/2 ấp Hòa Định, xã Tân Hòa, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2074033,
   "Latitude": 105.6471178
 },
 {
   "STT": 28,
   "Name": "Quầy thuốc Tuấn Dũng",
   "address": "598 khóm Bình Thạnh 1, thị trấn Lấp Vò, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3607896,
   "Latitude": 105.518867
 },
 {
   "STT": 29,
   "Name": "Quầy thuốc Bé Hai",
   "address": "67 ấp Vĩnh Bình A, xã Vĩnh Thạnh, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3447983,
   "Latitude": 105.6177942
 },
 {
   "STT": 30,
   "Name": "Nhà thuốc Mỹ Phát",
   "address": "71-73 Nguyễn VănCừ, khóm 4, phường An Thạnh, thị xã Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8094129,
   "Latitude": 105.3452093
 },
 // {
 //   "STT": 31,
 //   "Name": "Quầy thuốc Giang Chi 2",
 //   "address": "72A/4 ấp Hòa Bình, xã Tân Hòa, huyện Lai Vung, TỈNH GIA LAI",
 //   "Longtitude": 20.6861265,
 //   "Latitude": 105.3131185
 // },
 {
   "STT": 32,
   "Name": "Nhà thuốc Hoài Tân",
   "address": "74A Trần Hưng Đạo, khóm 1, phường 2, thị xã Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2925456,
   "Latitude": 105.7682345
 },
 {
   "STT": 33,
   "Name": "Quầy thuốc Minh Nhi",
   "address": "7A Điện Biên Phủ, xã Mỹ Trà, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4625669,
   "Latitude": 105.6443654
 },
 {
   "STT": 34,
   "Name": "Nhà thuốc Thanh Tùng",
   "address": "848, ấp Đông Khánh, xã Tân Khánh Đông, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.3634864,
   "Latitude": 105.723855
 },
 {
   "STT": 35,
   "Name": "Nhà thuốc Đức Thuận",
   "address": "91 Nguyễn Trãi,khóm 2, phường An Thạnh, thị xã Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8097454,
   "Latitude": 105.3416591
 },
 {
   "STT": 36,
   "Name": "Quầy thuốc Đông Bình",
   "address": "93 tổ 3, ấp Đông Bình, xã Hòa An, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.516667,
   "Latitude": 105.55
 },
 {
   "STT": 37,
   "Name": "Quầy thuốc Tám Dùng",
   "address": "A, xã Phú Thành A, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.6975735,
   "Latitude": 105.4360963
 },
 {
   "STT": 38,
   "Name": "Quầy thuốc Trí Loan",
   "address": "A, xã Phú Thành A, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.6975735,
   "Latitude": 105.4360963
 },
 {
   "STT": 39,
   "Name": "Quầy thuốc Thảo Uyên",
   "address": "A1/1 chợ Định Yên, ấp An Lợi B, xã Định Yên, huyện  Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3090167,
   "Latitude": 105.5360802
 },
 {
   "STT": 40,
   "Name": "Quầy thuốc Bình Dân",
   "address": "An Lợi B, xã Định Yên, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3239995,
   "Latitude": 105.5243971
 },
 {
   "STT": 41,
   "Name": "Quầy thuốc Chí Hải",
   "address": "An Lợi B, xã Định Yên, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3239995,
   "Latitude": 105.5243971
 },
 {
   "STT": 42,
   "Name": "Quầy thuốc Kim Bằng",
   "address": "An Lợi B, xã Định Yên, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3239995,
   "Latitude": 105.5243971
 },
 {
   "STT": 43,
   "Name": "Quầy thuốc Hõa Hiệp",
   "address": "An, xã LongThắng, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2428539,
   "Latitude": 105.6999122
 },
 {
   "STT": 44,
   "Name": "Quầy thuốc Kim Cẩm",
   "address": "An, xã LongThắng, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2428539,
   "Latitude": 105.6999122
 },
 {
   "STT": 45,
   "Name": "Quầy thuốc Kim Phượng 1",
   "address": "An, xã LongThắng, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2428539,
   "Latitude": 105.6999122
 },
 {
   "STT": 46,
   "Name": "Quầy thuốc Hiếu Duyên",
   "address": "An, xã Tân Khánh Trung, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3791189,
   "Latitude": 105.6940454
 },
 {
   "STT": 47,
   "Name": "Quầy thuốc Nhân Lợi",
   "address": "An, xã Tân Khánh Trung, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3791189,
   "Latitude": 105.6940454
 },
 {
   "STT": 48,
   "Name": "Quầy thuốc Như Ý",
   "address": "An, xã Tân Khánh Trung, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3791189,
   "Latitude": 105.6940454
 },
 {
   "STT": 49,
   "Name": "Quầy thuốc Nam Hải",
   "address": "Ấp 1, thị trấn Sa Rài, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8753437,
   "Latitude": 105.4620245
 },
 {
   "STT": 50,
   "Name": "Quầy thuốc Hoàng Thắng",
   "address": "Ấp 1, xã An Bình B, thị xã Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.7966565,
   "Latitude": 105.4243804
 },
 {
   "STT": 51,
   "Name": "Quầy thuốc Bảo Huyền",
   "address": "Ấp 1, xã An BìnhB, thị xã Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.7765871,
   "Latitude": 105.3554589
 },
 {
   "STT": 52,
   "Name": "Quầy thuốc Thúy An",
   "address": "Ấp 1, xã An Hòa, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.718078,
   "Latitude": 105.3737699
 },
 {
   "STT": 53,
   "Name": "Quầy thuốc Ngô VănThuận",
   "address": "Ấp 1, xã Mỹ Hiệp, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4047542,
   "Latitude": 105.8116263
 },
 {
   "STT": 54,
   "Name": "Quầy thuốc Ngọc Châu",
   "address": "Ấp 1, xã Mỹ Hòa, huyện  Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5179126,
   "Latitude": 105.8809053
 },
 {
   "STT": 55,
   "Name": "Tủ thuốcTrạm Y Tế Xã Mỹ Hõa",
   "address": "Ấp 1, xã Mỹ Hòa, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5179126,
   "Latitude": 105.8809053
 },
 {
   "STT": 56,
   "Name": "Quầy thuốc Nguyễn QuốcHậu",
   "address": "Ấp 1, xã Mỹ Long, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4025172,
   "Latitude": 105.8066225
 },
 {
   "STT": 57,
   "Name": "Quầy thuốc Thanh Vân",
   "address": "Ấp 1, xã Mỹ Long, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4025172,
   "Latitude": 105.8066225
 },
 {
   "STT": 58,
   "Name": "Quầy thuốc Hồng Đào",
   "address": "Ấp 1, xã Phong Mỹ, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.5333548,
   "Latitude": 105.5757339
 },
 {
   "STT": 59,
   "Name": "Quầy thuốc Kim Xuyến",
   "address": "Ấp 1, xã Phong Mỹ, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.5333548,
   "Latitude": 105.5757339
 },
 {
   "STT": 60,
   "Name": "Quầy thuốc Nguyễn Thị Thúy",
   "address": "Ấp 1, xã Phong Mỹ, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.5333548,
   "Latitude": 105.5757339
 },
 {
   "STT": 61,
   "Name": "Quầy thuốc Thiên Lan",
   "address": "Ấp 1, xã Phong Mỹ, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.5333548,
   "Latitude": 105.5757339
 },
 {
   "STT": 62,
   "Name": "Quầy thuốc Tuấn Tuyến",
   "address": "Ấp 1, xã Phú Lợi, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.6296243,
   "Latitude": 105.4829681
 },
 {
   "STT": 63,
   "Name": "Quầy thuốc Ba Giấy",
   "address": "Ấp 1, xã Tân Mỹ, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.6265289,
   "Latitude": 105.5261707
 },
 {
   "STT": 64,
   "Name": "Quầy thuốc Gia Bảo",
   "address": "Ấp 1, xã Tân Mỹ, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.6265289,
   "Latitude": 105.5261707
 },
 {
   "STT": 65,
   "Name": "Quầy thuốc Thiện Thöy",
   "address": "Ấp 1, xã Thạnh Lợi, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.7133216,
   "Latitude": 105.720319
 },
 {
   "STT": 66,
   "Name": "Quầy thuốc Trạm Y Tế Xã Thạnh Lợi",
   "address": "Ấp 1, xã Thạnh Lợi, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.7133216,
   "Latitude": 105.720319
 },
 {
   "STT": 67,
   "Name": "Quầy thuốc Kim Tiến",
   "address": "Ấp 1, xã Thường Phước 1, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8788463,
   "Latitude": 105.2077798
 },
 {
   "STT": 68,
   "Name": "Quầy thuốc Kim Tiến 2",
   "address": "Ấp 1, xã Thường Phước 1, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8788463,
   "Latitude": 105.2077798
 },
 {
   "STT": 69,
   "Name": "Quầy thuốc Ngọc Quý",
   "address": "Ấp 1, xã Thường Phước 1, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8788463,
   "Latitude": 105.2077798
 },
 {
   "STT": 70,
   "Name": "Quầy thuốc NguyễnCường",
   "address": "Ấp 1, xã Thường Phước 1, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8788463,
   "Latitude": 105.2077798
 },
 {
   "STT": 71,
   "Name": "Quầy thuốc Thöy Ngọc",
   "address": "Ấp 1, xã Thường Phước 1, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8788463,
   "Latitude": 105.2077798
 },
 {
   "STT": 72,
   "Name": "Quầy thuốc Đăng Khoa 1",
   "address": "Ấp 1, xã Thường Phước 2,  huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8788463,
   "Latitude": 105.2077798
 },
 {
   "STT": 73,
   "Name": "Quầy thuốc Minh Hằng",
   "address": "Ấp 1, xã Thường Phước 2,  huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8788463,
   "Latitude": 105.2077798
 },
 {
   "STT": 74,
   "Name": "Quầy thuốc Lê Nu",
   "address": "Ấp 1, xã Thường Thới Hậu B, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8567804,
   "Latitude": 105.3189726
 },
 {
   "STT": 75,
   "Name": "Quầy thuốc Lê Thương",
   "address": "Ấp 2, xã Ba Sao, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.5231596,
   "Latitude": 105.6601943
 },
 {
   "STT": 76,
   "Name": "Quầy thuốc Thanh Thuyên",
   "address": "Ấp 2, xã Gáo Giồng, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.6066717,
   "Latitude": 105.6293731
 },
 {
   "STT": 77,
   "Name": "Quầy thuốc Đông Nguyên",
   "address": "Ấp 2, xã Mỹ Long, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.3635488,
   "Latitude": 105.7879371
 },
 {
   "STT": 78,
   "Name": "Quầy thuốc Mộng Thu",
   "address": "Ấp 2, xã Tân Kiều, huyện  Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5175769,
   "Latitude": 105.8642593
 },
 {
   "STT": 79,
   "Name": "Quầy thuốc Lê Tấn Tài",
   "address": "Ấp 2, xã Tân Nghĩa, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.5294622,
   "Latitude": 105.629523
 },
 {
   "STT": 80,
   "Name": "Quầy thuốc Tiên Thảo",
   "address": "Ấp 2, xã Tân Nghĩa, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.5294622,
   "Latitude": 105.629523
 },
 {
   "STT": 81,
   "Name": "Quầy thuốc Mỹ Thi",
   "address": "ấp 2, xã Tân Thành B, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8697347,
   "Latitude": 105.494688
 },
 {
   "STT": 82,
   "Name": "Quầy thuốc Nguyễn Thảo",
   "address": "Ấp 2, xã Tân Thành B, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8697347,
   "Latitude": 105.494688
 },
 {
   "STT": 83,
   "Name": "Quầy thuốc Danh Nhân",
   "address": "Ấp 2, xã Thường Phước 1, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8788463,
   "Latitude": 105.2077798
 },
 // {
 //   "STT": 84,
 //   "Name": "Quầy thuốc Hùng Cường",
 //   "address": "Ấp 2, xã Thường Phước 1, huyện Hồng Ngự, TỈNH GIA LAI",
 //   "Longtitude": 10.8788463,
 //   "Latitude": 105.2077798
 // },
 {
   "STT": 85,
   "Name": "Quầy thuốc Nguyễn Trang",
   "address": "Ấp 2, xã Thường Phước 1, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8788463,
   "Latitude": 105.2077798
 },
 {
   "STT": 86,
   "Name": "Quầy thuốc Phước An",
   "address": "Ấp 2, xã Thường Phước 1, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8788463,
   "Latitude": 105.2077798
 },
 {
   "STT": 87,
   "Name": "Quầy thuốc Tröc Đào",
   "address": "Ấp 2, xã Thường Phước 1, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8788463,
   "Latitude": 105.2077798
 },
 {
   "STT": 88,
   "Name": "Quầy thuốc Ngọc Nghĩa",
   "address": "Ấp 2, xã Thường Phước 2, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8389934,
   "Latitude": 105.2370339
 },
 {
   "STT": 89,
   "Name": "Quầy thuốc Kim Ngân 2",
   "address": "Ấp 2A, xã Hưng Thạnh, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.6649367,
   "Latitude": 105.7019754
 },
 {
   "STT": 90,
   "Name": "Quầy thuốc An Nghi",
   "address": "Ấp 2A, xã Hưng Thạnh, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.6649367,
   "Latitude": 105.7019754
 },
 {
   "STT": 91,
   "Name": "Quầy thuốc Bệnh Viện Đa Khoa HuyệnTân Hồng",
   "address": "Ấp 3, thị trấn Sa Rài, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8741269,
   "Latitude": 105.4624601
 },
 {
   "STT": 92,
   "Name": "Quầy thuốc Đỗ Đạt",
   "address": "Ấp 3, thị trấn Sa Rài, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8741269,
   "Latitude": 105.4624601
 },
 {
   "STT": 93,
   "Name": "Quầy thuốc Thái Học",
   "address": "Ấp 3, xã Ba Sao, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.5477285,
   "Latitude": 105.6999122
 },
 {
   "STT": 94,
   "Name": "Quầy thuốc Đình Yến",
   "address": "Ấp 3, xã Bình Hàng Tây, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.3625893,
   "Latitude": 105.7606608
 },
 {
   "STT": 95,
   "Name": "Quầy thuốc Tiến Trung",
   "address": "Ấp 3, xã Bình Hàng Tây, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.3625893,
   "Latitude": 105.7606608
 },
 {
   "STT": 96,
   "Name": "Quầy thuốc Trung Tuấn",
   "address": "Ấp 3, xã Bình Hàng Tây, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.3625893,
   "Latitude": 105.7606608
 },
 {
   "STT": 97,
   "Name": "Quầy thuốc Ngọc Bích",
   "address": "Ấp 3, xã Bình Tấn, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.6191437,
   "Latitude": 105.5826123
 },
 {
   "STT": 98,
   "Name": "Quầy thuốc Gia Nghi",
   "address": "Ấp 3, xã Đốc Binh Kiều, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.507924,
   "Latitude": 105.8992666
 },
 {
   "STT": 99,
   "Name": "Quầy thuốc Phương Thảo",
   "address": "Ấp 3, xã Đốc Binh Kiều, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.507924,
   "Latitude": 105.8992666
 },
 {
   "STT": 100,
   "Name": "Quầy thuốc Thủy Trang 2",
   "address": "Ấp 3, xã Đốc Binh Kiều, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.507924,
   "Latitude": 105.8992666
 },
 {
   "STT": 101,
   "Name": "Quầy thuốc Ánh Tuyết",
   "address": "Ấp 3, xã HưngThạnh, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.507924,
   "Latitude": 105.8992666
 },
 {
   "STT": 102,
   "Name": "Quầy thuốc Trạm Y Tế Xã Hưng Thạnh",
   "address": "Ấp 3, xã HưngThạnh, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.507924,
   "Latitude": 105.8992666
 },
 {
   "STT": 103,
   "Name": "Quầy thuốc Nguyễn ThịTiểu Mi",
   "address": "Ấp 3, xã Mỹ Long, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.3505524,
   "Latitude": 105.7994986
 },
 {
   "STT": 104,
   "Name": "Quầy thuốc Minh Trí",
   "address": "Ấp 3, xã Mỹ Thọ, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4671629,
   "Latitude": 105.7395894
 },
 {
   "STT": 105,
   "Name": "Quầy thuốc Minh Trí",
   "address": "Ấp 3, xã Mỹ Thọ, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4671629,
   "Latitude": 105.7395894
 },
 {
   "STT": 106,
   "Name": "Quầy thuốc Đông Nghi",
   "address": "Ấp 3, xã Phong Mỹ, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.5332089,
   "Latitude": 105.5824406
 },
 {
   "STT": 107,
   "Name": "Quầy thuốc Lê Thị Hồng Mến",
   "address": "Ấp 3, xã Phong Mỹ, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.5332089,
   "Latitude": 105.5824406
 },
 {
   "STT": 108,
   "Name": "Quầy thuốc Kiều Diễm",
   "address": "ấp 3, xã Phương Trà, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.5069025,
   "Latitude": 105.654042
 },
 {
   "STT": 109,
   "Name": "Quầy thuốc Lan Phương",
   "address": "Ấp 3, xã Phương Trà, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.5069025,
   "Latitude": 105.654042
 },
 {
   "STT": 110,
   "Name": "Quầy thuốc Phương Cöc",
   "address": "Ấp 3, xã Tân Kiều, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.507924,
   "Latitude": 105.8992666
 },
 {
   "STT": 111,
   "Name": "Quầy thuốc Quốc Duy",
   "address": "Ấp 3, xã Tân Kiều, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.507924,
   "Latitude": 105.8992666
 },
 {
   "STT": 112,
   "Name": "Quầy thuốc Út Ngoan",
   "address": "Ấp 3, xã Tân Kiều, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.507924,
   "Latitude": 105.8992666
 },
 {
   "STT": 113,
   "Name": "Quầy thuốc Tấn Lợi",
   "address": "Ấp 3, xã Thường Phước 1, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.9056064,
   "Latitude": 105.2012587
 },
 {
   "STT": 114,
   "Name": "Cơ Sở thuốc Đông Y La Mãn Đường",
   "address": "Ấp 3, xã Thường Phước 2, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8389934,
   "Latitude": 105.2370339
 },
 {
   "STT": 115,
   "Name": "Quầy thuốc Ngọc Hiền",
   "address": "Ấp 3, xã Thường Phước 2, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8389934,
   "Latitude": 105.2370339
 },
 {
   "STT": 116,
   "Name": "Quầy thuốc Tài Lợi",
   "address": "Ấp 3, xã Thường Phước 2, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8389934,
   "Latitude": 105.2370339
 },
 {
   "STT": 117,
   "Name": "Quầy thuốc Tám Nghễ",
   "address": "Ấp 4, xã An Hòa, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.7170245,
   "Latitude": 105.3755724
 },
 {
   "STT": 118,
   "Name": "Quầy thuốc Thành Công",
   "address": "Ấp 4, xã Bình Hàng Trung, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4659577,
   "Latitude": 105.7059785
 },
 {
   "STT": 119,
   "Name": "Quầy thuốc Trạm Y Tế Đốc Binh Kiều",
   "address": "Ấp 4, xã Đốc Binh Kiều, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.4933918,
   "Latitude": 105.9112424
 },
 {
   "STT": 120,
   "Name": "Quầy thuốc Hiếu Quyên",
   "address": "Ấp 4, xã Hòa Bình, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.7633938,
   "Latitude": 105.6471178
 },
 {
   "STT": 121,
   "Name": "Quầy thuốc Hoài Thu",
   "address": "Ấp 4, xã Hòa Bình, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.7633938,
   "Latitude": 105.6471178
 },
 {
   "STT": 122,
   "Name": "Quầy thuốc Hữu Dư",
   "address": "Ấp 4, xã Hòa Bình, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.7633938,
   "Latitude": 105.6471178
 },
 {
   "STT": 123,
   "Name": "Quầy thuốc Tuyết Thu",
   "address": "Ấp 4, xã Hòa Bình, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.7633938,
   "Latitude": 105.6471178
 },
 {
   "STT": 124,
   "Name": "Quầy thuốc Gia Huy",
   "address": "Ấp 4, xã Mỹ Đông, huyện  Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5302706,
   "Latitude": 105.8290298
 },
 {
   "STT": 125,
   "Name": "Quầy thuốc Ngọc Khải",
   "address": "Ấp 4, xã Mỹ Đông, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5302706,
   "Latitude": 105.8290298
 },
 {
   "STT": 126,
   "Name": "Quầy thuốc Trà My 8",
   "address": "Ấp 4, xã Mỹ Đông, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5302706,
   "Latitude": 105.8290298
 },
 {
   "STT": 127,
   "Name": "Quầy thuốc Thúy Vân",
   "address": "Ấp 4, xã Phong Mỹ, huyện Cao Lãnh , TỈNH GIA LAI",
   "Longtitude": 10.5333548,
   "Latitude": 105.5757339
 },
 {
   "STT": 128,
   "Name": "Quầy thuốc Trần Thị Thu Trang",
   "address": "Ấp 4, xã PhúCường, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.6984007,
   "Latitude": 105.6353878
 },
 {
   "STT": 129,
   "Name": "Quầy thuốc Hà Nghi",
   "address": "Ấp 4, xã Phương Trà, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.509923,
   "Latitude": 105.6607103
 },
 {
   "STT": 130,
   "Name": "Quầy thuốc Lê Quý",
   "address": "Ấp 4, xã Trường Xuân, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5302706,
   "Latitude": 105.8290298
 },
 {
   "STT": 131,
   "Name": "Quầy thuốc Năm Hoa",
   "address": "Ấp 5 chợ xã Đốc Binh Kiều, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.4933918,
   "Latitude": 105.9112424
 },
 {
   "STT": 132,
   "Name": "Quầy thuốc Hà HồngThiện",
   "address": "Ấp 5, xã Gáo Giồng, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.606677,
   "Latitude": 105.6292551
 },
 {
   "STT": 133,
   "Name": "Quầy thuốc Minh Thư",
   "address": "Ấp 5, xã Gáo Giồng, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.606677,
   "Latitude": 105.6292551
 },
 {
   "STT": 134,
   "Name": "Quầy thuốc NguyễnTrọng",
   "address": "Ấp 5, xã Gáo Giồng, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.606677,
   "Latitude": 105.6292551
 },
 {
   "STT": 135,
   "Name": "Quầy thuốc Chung Hồ",
   "address": "Ấp 5, xã Mỹ Hòa, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.563575,
   "Latitude": 105.8172881
 },
 {
   "STT": 136,
   "Name": "Quầy thuốc Võ Thị Kim Hoa",
   "address": "Ấp 5, xã Phương Thịnh, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.6039289,
   "Latitude": 105.6615526
 },
 {
   "STT": 137,
   "Name": "Cơ Sở thuốc Đông Y Minh Phát Đường",
   "address": "Ấp 5, xã Trường Xuân, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.631385,
   "Latitude": 105.7694306
 },
 {
   "STT": 138,
   "Name": "Quầy thuốc Tuấn Hương",
   "address": "Ấp 5A, chợ Trường Xuân, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.631385,
   "Latitude": 105.7694306
 },
 {
   "STT": 139,
   "Name": "Quầy thuốc Bạch Yến",
   "address": "Ấp 5A, xã Trường Xuân, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.631385,
   "Latitude": 105.7694306
 },
 {
   "STT": 140,
   "Name": "Quầy thuốc Hồng Lệ",
   "address": "Ấp 5A, xã Trường Xuân, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.631385,
   "Latitude": 105.7694306
 },
 {
   "STT": 141,
   "Name": "Quầy thuốc Hương Giang",
   "address": "Ấp 5A, xã Trường Xuân, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.631385,
   "Latitude": 105.7694306
 },
 {
   "STT": 142,
   "Name": "Quầy thuốc Phố - Cương",
   "address": "Ấp 5A, xã Trường Xuân, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.631385,
   "Latitude": 105.7694306
 },
 {
   "STT": 143,
   "Name": "Quầy thuốc Trạm Y Tế Xã Trường Xuân",
   "address": "Ấp 5A, xã Trường Xuân, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.631385,
   "Latitude": 105.7694306
 },
 {
   "STT": 144,
   "Name": "Quầy thuốc Nguyễn Thái",
   "address": "Ấp 5B xã Trường Xuân, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.6292284,
   "Latitude": 105.7700157
 },
 {
   "STT": 145,
   "Name": "Quầy thuốc Anh Thi",
   "address": "Ấp 6 Kinh Hội, xã Trường Xuân, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5302706,
   "Latitude": 105.8290298
 },
 {
   "STT": 146,
   "Name": "Quầy thuốc Gia Hân",
   "address": "Ấp 6 Kinh Hội, xã Trường Xuân, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5302706,
   "Latitude": 105.8290298
 },
 {
   "STT": 147,
   "Name": "Quầy thuốc Hồng Gấm",
   "address": "Ấp 6 Kinh Hội, xã Trường Xuân, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5302706,
   "Latitude": 105.8290298
 },
 {
   "STT": 148,
   "Name": "Quầy thuốc Phương Lan",
   "address": "Ấp 6B, xã Trường Xuân, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.648797,
   "Latitude": 105.787514
 },
 {
   "STT": 149,
   "Name": "Quầy thuốc Hữu Nghĩa 1",
   "address": "Ấp A, xã PhúCường, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.662345,
   "Latitude": 105.6071175
 },
 {
   "STT": 150,
   "Name": "Quầy thuốc Thanh Phê",
   "address": "Ấp A, xã PhúCường, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.662345,
   "Latitude": 105.6071175
 },
 {
   "STT": 151,
   "Name": "Quầy thuốc Tư Dũng",
   "address": "Ấp A, xã PhúCường, huyện TamNông, TỈNH GIA LAI",
   "Longtitude": 10.662345,
   "Latitude": 105.6071175
 },
 {
   "STT": 152,
   "Name": "Quầy thuốc Ngọc Thảo Nguyên",
   "address": "Ấp An Định, xã An Bình, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4680204,
   "Latitude": 105.6588485
 },
 {
   "STT": 153,
   "Name": "Quầy thuốc Phan Viết Nam",
   "address": "Ấp An Định, xã An Bình, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4680204,
   "Latitude": 105.6588485
 },
 {
   "STT": 154,
   "Name": "Quầy thuốc Nha Khoa Thạch Sơn",
   "address": "Ấp An Hòa Nhì, xã Tân Bình, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2750458,
   "Latitude": 105.7908229
 },
 {
   "STT": 155,
   "Name": "Quầy thuốc Mậu Nhân",
   "address": "Ấp An Hòa, xã An Bình A, thị xã Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.7829013,
   "Latitude": 105.3716684
 },
 {
   "STT": 156,
   "Name": "Quầy thuốc Mỹ Huyền",
   "address": "Ấp An Hòa, xã An Bình A, thị xã Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.7829013,
   "Latitude": 105.3716684
 },
 {
   "STT": 157,
   "Name": "Quầy thuốc Văn Đức",
   "address": "Ấp An Hòa, xã An Bình A, thị xã Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.7829013,
   "Latitude": 105.3716684
 },
 {
   "STT": 158,
   "Name": "Quầy thuốc 5 Hiệp",
   "address": "Ấp An Hòa, xã An Hiệp, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2856663,
   "Latitude": 105.8231588
 },
 {
   "STT": 159,
   "Name": "Quầy thuốc Nguyễn Pho",
   "address": "Ấp An Hòa, xã An Hiệp, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2856663,
   "Latitude": 105.8231588
 },
 {
   "STT": 160,
   "Name": "Quầy thuốc Tường Vy",
   "address": "ấp An Hòa, xã Định An, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.325877,
   "Latitude": 105.502516
 },
 {
   "STT": 161,
   "Name": "Quầy thuốc Vân Hương",
   "address": "ấp An Hòa, xã Định An, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.325877,
   "Latitude": 105.502516
 },
 {
   "STT": 162,
   "Name": "Quầy thuốc Mỹ Hõa",
   "address": "Ấp An Hưng, xã An Khánh, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.1970145,
   "Latitude": 105.8583873
 },
 {
   "STT": 163,
   "Name": "Quầy thuốc Thanh Tâm",
   "address": "Ấp An Hưng, xã An Khánh, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.1970145,
   "Latitude": 105.8583873
 },
 {
   "STT": 164,
   "Name": "Quầy thuốc Minh Duy",
   "address": "Ấp An Lạc (cách cầu Öt Chấp 15m), xã Định An, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3219082,
   "Latitude": 105.5298525
 },
 {
   "STT": 165,
   "Name": "Quầy thuốc Thảo Nhi",
   "address": "Ấp An Lộc, xã An Phước, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.804624,
   "Latitude": 105.4829681
 },
 {
   "STT": 166,
   "Name": "Quầy thuốc Tuấn Diễm",
   "address": "Ấp An Lợi A, xã Định Yên, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3239995,
   "Latitude": 105.5243971
 },
 {
   "STT": 167,
   "Name": "Quầy thuốc Mỹ Tiên",
   "address": "Ấp An Lợi B, xã Định Yên, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3239995,
   "Latitude": 105.5243971
 },
 {
   "STT": 168,
   "Name": "Quầy thuốc Trạm Y Tế Xã Định Yên",
   "address": "Ấp An Lợi B, xã Định Yên, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3239995,
   "Latitude": 105.5243971
 },
 {
   "STT": 169,
   "Name": "Quầy thuốc Thanh Hiền",
   "address": "Ấp An Nghiệp, xã An Bình, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4680204,
   "Latitude": 105.6588485
 },
 {
   "STT": 170,
   "Name": "Quầy thuốc Nguyễn Thị Lý",
   "address": "Ấp An Ninh, xã An Khánh, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2024309,
   "Latitude": 105.8600348
 },
 {
   "STT": 171,
   "Name": "Quầy thuốc Thanh Hùng",
   "address": "Ấp An Phú, xã An Nhơn, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2730435,
   "Latitude": 105.8583873
 },
 {
   "STT": 172,
   "Name": "Quầy thuốc Hoàng Nam 1",
   "address": "Ấp An Phú, xã Hội An Đông, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.4078862,
   "Latitude": 105.5532993
 },
 {
   "STT": 173,
   "Name": "Quầy thuốc Vĩnh Linh",
   "address": "Ấp An Phước, xã An Bình A, thị xã Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.7765871,
   "Latitude": 105.3554589
 },
 {
   "STT": 174,
   "Name": "Quầy thuốc Thùy Linh",
   "address": "Ấp An Thọ, xã An Phước, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8154761,
   "Latitude": 105.4588221
 },
 {
   "STT": 175,
   "Name": "Quầy thuốc Trường Hải",
   "address": "Ấp An Thọ, xã An Phước, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8154761,
   "Latitude": 105.4588221
 },
 {
   "STT": 176,
   "Name": "Quầy thuốc Minh Châu",
   "address": "Ấp An Thuận, xã An Hiệp, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2856663,
   "Latitude": 105.8231588
 },
 {
   "STT": 177,
   "Name": "Quầy thuốc Đăng Khoa",
   "address": "Ấp Anh Dũng, xã Tân Thành A, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8858566,
   "Latitude": 105.5480637
 },
 {
   "STT": 178,
   "Name": "Quầy thuốc Ngọc Loan",
   "address": "Ấp Anh Dũng, xã Tân Thành A, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8858566,
   "Latitude": 105.5480637
 },
 {
   "STT": 179,
   "Name": "Quầy thuốc Thu Thủy",
   "address": "Ấp Anh Dũng, xã Tân Thành A, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8858566,
   "Latitude": 105.5480637
 },
 {
   "STT": 180,
   "Name": "Quầy thuốc Tuyết Hồng",
   "address": "Ấp Bình An, xã Bình Thành, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.383333,
   "Latitude": 105.533333
 },
 {
   "STT": 181,
   "Name": "Cơ Sở thuốc Đông Y Minh Đức",
   "address": "Ấp Bình Chánh, xã Bình Thành, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5786733,
   "Latitude": 105.5564149
 },
 {
   "STT": 182,
   "Name": "Quầy thuốc Bảo Phöc",
   "address": "Ấp Bình Chánh, xã Bình Thành, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5786733,
   "Latitude": 105.5564149
 },
 {
   "STT": 183,
   "Name": "Quầy thuốc Khánh Linh",
   "address": "Ấp Bình Chánh, xã Bình Thành, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5786733,
   "Latitude": 105.5564149
 },
 {
   "STT": 184,
   "Name": "Quầy thuốc PhươngHuyền",
   "address": "Ấp Bình Hòa Trung, xã Thường Thới Hậu A, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8208572,
   "Latitude": 105.2814555
 },
 {
   "STT": 185,
   "Name": "Quầy thuốc Hải Duyên",
   "address": "Ấp Bình Hòa, xã Bình Thạnh Trung, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.380888,
   "Latitude": 105.5587323
 },
 {
   "STT": 186,
   "Name": "Cơ Sở thuốc Đông Y Lê Huy",
   "address": "Ấp Bình Hòa, xã Bình Thạnh, thị xã Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8362459,
   "Latitude": 105.3642725
 },
 {
   "STT": 187,
   "Name": "Quầy thuốc Hồng Hạo",
   "address": "Ấp Bình Hòa, xã Bình Thạnh, thị xã Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8362459,
   "Latitude": 105.3642725
 },
 {
   "STT": 188,
   "Name": "Quầy thuốc Bích Trâm",
   "address": "Ấp Bình Hưng, xã Bình Thạnh, thị xã Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8255238,
   "Latitude": 105.3950939
 },
 {
   "STT": 189,
   "Name": "Quầy thuốc Đạt Linh",
   "address": "Ấp Bình Lợi, xã Bình Thành, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.335945,
   "Latitude": 105.4985332
 },
 {
   "STT": 190,
   "Name": "Quầy thuốc Kim Ngân",
   "address": "Ấp Bình Lợi, xã Bình Thành, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.335945,
   "Latitude": 105.4985332
 },
 {
   "STT": 191,
   "Name": "Quầy thuốc Lan Phương 1",
   "address": "Ấp Bình Lợi, xã Bình Thành, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.335945,
   "Latitude": 105.4985332
 },
 {
   "STT": 192,
   "Name": "Quầy thuốc Thanh Hương",
   "address": "Ấp Bình Lợi, xã Bình Thành, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.335945,
   "Latitude": 105.4985332
 },
 {
   "STT": 193,
   "Name": "Quầy thuốc Triết Hương",
   "address": "Ấp Bình Lợi, xã Bình Thành, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.335945,
   "Latitude": 105.4985332
 },
 {
   "STT": 194,
   "Name": "Quầy thuốc Xuân Trinh",
   "address": "ấp Bình Lợi, xã Bình Thành, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.335945,
   "Latitude": 105.4985332
 },
 {
   "STT": 195,
   "Name": "Quầy thuốc Ngọc Anh",
   "address": "Ấp Bình Mỹ B, xã Bình Thạnh, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.3200374,
   "Latitude": 105.7879371
 },
 {
   "STT": 196,
   "Name": "Quầy thuốc Nhật Nguyên",
   "address": "Ấp Bình Mỹ B, xã Bình Thạnh, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.3200374,
   "Latitude": 105.7879371
 },
 {
   "STT": 197,
   "Name": "Quầy thuốc Hữu Khá",
   "address": "Ấp Bình Tân, xã Bình Thạnh, huyệnCao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.3200374,
   "Latitude": 105.7879371
 },
 {
   "STT": 198,
   "Name": "Quầy thuốc Tuyết Trinh",
   "address": "Ấp Bình Tân, xã Bình Thạnh, huyệnCao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.3200374,
   "Latitude": 105.7879371
 },
 {
   "STT": 199,
   "Name": "Quầy thuốc Số 2",
   "address": "Ấp Bình Thạnh 1, thị trấn Lấp Vò, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.360093,
   "Latitude": 105.5210607
 },
 {
   "STT": 200,
   "Name": "Quầy thuốc Tấn Phöc",
   "address": "Ấp Bình Thành A, xã Bình Thành, phường An Thạnh, thị xã Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8255238,
   "Latitude": 105.3950939
 },
 {
   "STT": 201,
   "Name": "Quầy thuốc An Minh",
   "address": "Ấp Bình Trung, xã Bình Thành, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.380888,
   "Latitude": 105.5587323
 },
 {
   "STT": 202,
   "Name": "Quầy thuốc Như Ý",
   "address": "Ấp Bình Trung, xã Bình Thành, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.380888,
   "Latitude": 105.5587323
 },
 {
   "STT": 203,
   "Name": "Quầy thuốc Phi Giao",
   "address": "Ấp Cả Răng, xã Bình Phú, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8900758,
   "Latitude": 105.4185227
 },
 {
   "STT": 204,
   "Name": "Quầy thuốc Phước Lộc",
   "address": "Ấp Cả Răng, xã Bình Phú, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8900758,
   "Latitude": 105.4185227
 },
 {
   "STT": 205,
   "Name": "Quầy thuốc Thúy Ái",
   "address": "Ấp Cái Cái, xã Tân Thành A, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8688713,
   "Latitude": 105.550907
 },
 {
   "STT": 206,
   "Name": "Quầy thuốc Anh Thư",
   "address": "Ấp Chiến Thắng, xã Tân Hộ Cơ, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.9330264,
   "Latitude": 105.442711
 },
 {
   "STT": 207,
   "Name": "Quầy thuốc Kim Tuyền",
   "address": "Ấp Chiến Thắng, xã Tân Hộ Cơ, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.9330264,
   "Latitude": 105.442711
 },
 {
   "STT": 208,
   "Name": "Quầy thuốc Việt Nhi",
   "address": "Ấp Chòi Mòi, xã Thông Bình, huyệnTân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.9354906,
   "Latitude": 105.494688
 },
 {
   "STT": 209,
   "Name": "Quầy thuốc Thứ Dung",
   "address": "Ấp Công Tạo, xã Bình Phú, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.9163678,
   "Latitude": 105.4184478
 },
 {
   "STT": 210,
   "Name": "Quầy thuốc Trung Hiếu",
   "address": "Ấp Công Tạo, xã Bình Phú, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.9163678,
   "Latitude": 105.4184478
 },
 {
   "STT": 211,
   "Name": "Quầy thuốc Uyên Quyên",
   "address": "Ấp Công Tạo, xã Bình Phú, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.9163678,
   "Latitude": 105.4184478
 },
 {
   "STT": 212,
   "Name": "Quầy thuốc Yến Vy",
   "address": "Ấp Công Tạo, xã Bình Phú, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.9163678,
   "Latitude": 105.4184478
 },
 {
   "STT": 213,
   "Name": "Tủ thuốc Trạm Y Tế Bình Phú",
   "address": "Ấp Công Tạo, xã Bình Phú, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.9163678,
   "Latitude": 105.4184478
 },
 {
   "STT": 214,
   "Name": "Quầy thuốc Bảy Thủ",
   "address": "Ấp Dinh Bà, xã Tân Hộ Cơ, huyệnTân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.9335268,
   "Latitude": 105.442255
 },
 {
   "STT": 215,
   "Name": "Quầy thuốc Cát Tường",
   "address": "Ấp Dinh Bà, xã Tân Hộ Cơ, huyệnTân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.9335268,
   "Latitude": 105.442255
 },
 {
   "STT": 216,
   "Name": "Quầy thuốc Đặng KimNgọc",
   "address": "Ấp Dinh Bà, xã Tân Hộ Cơ, huyệnTân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.9335268,
   "Latitude": 105.442255
 },
 {
   "STT": 217,
   "Name": "Quầy thuốc Hữu Tuấn",
   "address": "Ấp Dinh Bà, xã Tân Hộ Cơ, huyệnTân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.9335268,
   "Latitude": 105.442255
 },
 {
   "STT": 218,
   "Name": "Quầy thuốc Ngọc Tâm",
   "address": "Ấp Dinh Bà, xã Tân Hộ Cơ, huyệnTân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.9335268,
   "Latitude": 105.442255
 },
 {
   "STT": 219,
   "Name": "Quầy thuốc Nguyễn Thị Huyền",
   "address": "Ấp Dinh Bà, xã Tân Hộ Cơ, huyệnTân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.9335268,
   "Latitude": 105.442255
 },
 {
   "STT": 220,
   "Name": "Quầy thuốc Duy Thịnh",
   "address": "Ấp Đông Mỹ, xã Mỹ Hội, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4154679,
   "Latitude": 105.7315389
 },
 {
   "STT": 221,
   "Name": "Quầy thuốc Ngọc Dung",
   "address": "Ấp Đông Mỹ, xã Mỹ Hội, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4154679,
   "Latitude": 105.7315389
 },
 {
   "STT": 222,
   "Name": "Quầy thuốc Thanh Tuấn",
   "address": "Ấp Đông Mỹ, xã Mỹ Hội, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4154679,
   "Latitude": 105.7315389
 },
 {
   "STT": 223,
   "Name": "Quầy thuốc Thanh Tâm",
   "address": "Ấp Đông Quới, xã Tân Khánh Đông, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.3440331,
   "Latitude": 105.7378309
 },
 {
   "STT": 224,
   "Name": "Quầy thuốc Bạch Huệ",
   "address": "Ấp Đông, xã Tân Bình, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2553925,
   "Latitude": 105.776198
 },
 {
   "STT": 225,
   "Name": "Quầy thuốc Kim Hường",
   "address": "Ấp Gò Da, xã Bình Phú, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8900758,
   "Latitude": 105.4185227
 },
 {
   "STT": 226,
   "Name": "Quầy thuốc Thúy An",
   "address": "Ấp Gò Da, xã Bình Phú, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8900758,
   "Latitude": 105.4185227
 },
 {
   "STT": 227,
   "Name": "Quầy thuốc Ngọc Thủy",
   "address": "Ấp Hòa Hiệp, xã Hòa Tân, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.1716085,
   "Latitude": 105.8171379
 },
 {
   "STT": 228,
   "Name": "Quầy thuốc Bảo Phöc",
   "address": "Ấp Hòa Quới, xã Hòa Tân, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.1715029,
   "Latitude": 105.8172237
 },
 {
   "STT": 229,
   "Name": "Quầy thuốc Kim Tiền",
   "address": "Ấp Hòa Vân, xã Nhị Mỹ, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4880535,
   "Latitude": 105.6940454
 },
 {
   "STT": 230,
   "Name": "Quầy thuốc Cẩm Như",
   "address": "Ấp Hoàng Việt, xã Tân Phước, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8390414,
   "Latitude": 105.5591615
 },
 {
   "STT": 231,
   "Name": "Quầy thuốc Huỳnh Giao",
   "address": "Ấp Hoàng Việt, xã Tân Phước, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8390414,
   "Latitude": 105.5591615
 },
 {
   "STT": 232,
   "Name": "Quầy thuốc Ngọc Hà",
   "address": "Ấp Hoàng Việt, xã Tân Phước, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8390414,
   "Latitude": 105.5591615
 },
 {
   "STT": 233,
   "Name": "Quầy thuốc Ngọc Hóa",
   "address": "Ấp Hoàng Việt, xã Tân Phước, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8390414,
   "Latitude": 105.5591615
 },
 {
   "STT": 234,
   "Name": "Quầy thuốc Lê Khoa",
   "address": "Ấp Hưng Hòa, xã Tân Khánh Trung, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3791189,
   "Latitude": 105.6940454
 },
 {
   "STT": 235,
   "Name": "Cơ Sở thuốc Đông Y Vạn Phát Đường",
   "address": "Ấp Hưng Lợi, xã Thanh Mỹ, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.4057199,
   "Latitude": 105.8581524
 },
 {
   "STT": 236,
   "Name": "Quầy thuốc Trạm Y Tế Thanh Mỹ",
   "address": "Ấp Hưng Lợi, xã Thanh Mỹ, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.4057199,
   "Latitude": 105.8581524
 },
 {
   "STT": 237,
   "Name": "Quầy thuốc Sơn Tinh",
   "address": "Ấp K10, xã Phú Hiệp, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.7757656,
   "Latitude": 105.5122694
 },
 {
   "STT": 238,
   "Name": "Quầy thuốc Thu Ân",
   "address": "Ấp K10, xã Phú Hiệp, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.7757656,
   "Latitude": 105.5122694
 },
 {
   "STT": 239,
   "Name": "Quầy thuốc Mềm Mõng",
   "address": "Ấp K9, xã Phú Đức, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.7125522,
   "Latitude": 105.5391526
 },
 {
   "STT": 240,
   "Name": "Quầy thuốc Xuân Hoàng",
   "address": "ấp Khánh Nhơn, xã Tân Khánh Đông, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.3443488,
   "Latitude": 105.7363114
 },
 {
   "STT": 241,
   "Name": "Quầy thuốc Vũ Linh",
   "address": "Ấp Long Bình, xã Long Khánh B, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8115016,
   "Latitude": 105.3276638
 },
 // {
 //   "STT": 242,
 //   "Name": "Quầy thuốc Nhựt Nam 2",
 //   "address": "Ấp Long Định, xã Long Thắng, huyện Lai Vung, TỈNH GIA LAI",
 //   "Longtitude": 10.2415342,
 //   "Latitude": 105.697348
 // },
 {
   "STT": 243,
   "Name": "Quầy thuốc Hiền Nhân",
   "address": "Ấp Long Hưng, xã Long Thuận, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.7710854,
   "Latitude": 105.283978
 },
 {
   "STT": 244,
   "Name": "Quầy thuốc Hoàng Quân",
   "address": "Ấp Long Hưng, xã Long Thuận, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.7710854,
   "Latitude": 105.283978
 },
 {
   "STT": 245,
   "Name": "Quầy thuốc Thanh Mai 2",
   "address": "Ấp Long Hưng, xã Long Thuận, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.7710854,
   "Latitude": 105.283978
 },
 // {
 //   "STT": 246,
 //   "Name": "Quầy thuốc Hùng Cường",
 //   "address": "Ấp Long Phú A (thửa đất số 3610, TBĐ số 04), xã Phú Thành A, huyện Tam Nông, TỈNH GIA LAI",
 //   "Longtitude": 9.6289421,
 //   "Latitude": 106.0875326
 // },
 {
   "STT": 247,
   "Name": "Quầy thuốc Kim Vũ",
   "address": "Ấp Long Phú, xã Phú Thành A, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.6866371,
   "Latitude": 105.4360963
 },
 {
   "STT": 248,
   "Name": "Quầy thuốc Tiến Đạt",
   "address": "Ấp Long Phước, xã Long Khánh A, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8060563,
   "Latitude": 105.2926308
 },
 {
   "STT": 249,
   "Name": "Quầy thuốc Thanh Trúc",
   "address": "Ấp Long Phước, xã Long Khánh A, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8060563,
   "Latitude": 105.2926308
 },
 {
   "STT": 250,
   "Name": "Quầy thuốc Bích Kiều",
   "address": "Ấp Long Sơn, xã Thông Bình, huyệnTân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.9051057,
   "Latitude": 105.5234111
 },
 {
   "STT": 251,
   "Name": "Quầy thuốc Hoàng Thủy",
   "address": "Ấp Long Sơn, xã Thông Bình, huyệnTân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.9051057,
   "Latitude": 105.5234111
 },
 {
   "STT": 252,
   "Name": "Quầy thuốc Ngọc Linh",
   "address": "Ấp Long Sơn, xã Thông Bình, huyệnTân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.9051057,
   "Latitude": 105.5234111
 },
 {
   "STT": 253,
   "Name": "Quầy thuốc Ngọc Loan 2",
   "address": "Ấp Long Sơn, xã Thông Bình, huyệnTân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.9051057,
   "Latitude": 105.5234111
 },
 {
   "STT": 254,
   "Name": "Quầy thuốc Ngọc Trâm",
   "address": "Ấp Long Sơn, xã Thông Bình, huyệnTân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.9051057,
   "Latitude": 105.5234111
 },
 {
   "STT": 255,
   "Name": "Quầy thuốc Quốc Hùng",
   "address": "Ấp Long Sơn, xã Thông Bình, huyệnTân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.9051057,
   "Latitude": 105.5234111
 },
 {
   "STT": 256,
   "Name": "Quầy thuốc Văn Vũ",
   "address": "Ấp Long Sơn, xã Thông Bình, huyệnTân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.9051057,
   "Latitude": 105.5234111
 },
 {
   "STT": 257,
   "Name": "Cơ Sở thuốc Đông Y Dân Sanh Đường",
   "address": "Ấp Long Thái, xã Long Khánh B, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.7999562,
   "Latitude": 105.332145
 },
 {
   "STT": 258,
   "Name": "Quầy thuốc Bảy Á",
   "address": "Ấp Long Thái, xã Long Khánh B, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.7999562,
   "Latitude": 105.332145
 },
 {
   "STT": 259,
   "Name": "Quầy thuốc Thanh Thùy",
   "address": "Ấp Long Thành A, xã Long Hậu, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2987902,
   "Latitude": 105.6371713
 },
 {
   "STT": 260,
   "Name": "Quầy thuốc Thúy Nga",
   "address": "Ấp Long Thành A, xã Long Hậu, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2987902,
   "Latitude": 105.6371713
 },
 {
   "STT": 261,
   "Name": "Quầy thuốc Toàn Bánthuốc Tây",
   "address": "Ấp Long Thạnh B, xã Long Khánh A, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8031699,
   "Latitude": 105.2955575
 },
 {
   "STT": 262,
   "Name": "Quầy thuốc Lê Phong",
   "address": "Ấp Long Thới A, xã Long Thuận, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.7708956,
   "Latitude": 105.2838511
 },
 {
   "STT": 263,
   "Name": "Quầy thuốc Yến Thanh",
   "address": "Ấp Mỹ Điền, xã Phú Điền, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.4682585,
   "Latitude": 105.8701316
 },
 {
   "STT": 264,
   "Name": "Quầy thuốc Phong Linh",
   "address": "Ấp Mỹ Điền, xã Phú Điền, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.4682585,
   "Latitude": 105.8701316
 },
 {
   "STT": 265,
   "Name": "Quầy thuốc Ánh Nguyễn",
   "address": "Ấp Mỹ Phú B, xã Mỹ An, huyệnTháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5175769,
   "Latitude": 105.8642593
 },
 {
   "STT": 266,
   "Name": "Quầy thuốc Diễm Phöc",
   "address": "Ấp Mỹ Phú, xã Phú Điền, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.4498007,
   "Latitude": 105.8961593
 },
 {
   "STT": 267,
   "Name": "Quầy thuốc Ngọc Quỳnh",
   "address": "Ấp Mỹ Phước 1, xã Mỹ Quý, huyệnTháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5273347,
   "Latitude": 105.7565205
 },
 {
   "STT": 268,
   "Name": "Quầy thuốc Thúy Duyên",
   "address": "Ấp Mỹ Phước 1, xã Mỹ Quý, huyệnTháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5273347,
   "Latitude": 105.7565205
 },
 {
   "STT": 269,
   "Name": "Quầy thuốc Trạm Y Tế Xã Mỹ Quý",
   "address": "Ấp Mỹ Phước 1, xã Mỹ Quý, huyệnTháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5273347,
   "Latitude": 105.7565205
 },
 {
   "STT": 270,
   "Name": "Quầy thuốc Tuyết Hùng",
   "address": "Ấp Mỹ Tân, xã Phú Điền, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.4498007,
   "Latitude": 105.8961593
 },
 {
   "STT": 271,
   "Name": "Quầy thuốc Thống Nhiều",
   "address": "Ấp Mỹ Tây 1, xã Mỹ Quí, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5453505,
   "Latitude": 105.7468535
 },
 {
   "STT": 272,
   "Name": "Quầy thuốc Anh Khoa",
   "address": "Ấp Mỹ Tây 1, xã Mỹ Quý, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5453505,
   "Latitude": 105.7468535
 },
 {
   "STT": 273,
   "Name": "Quầy thuốc Kiều Oanh",
   "address": "Ấp Mỹ Tây 1, xã Mỹ Quý, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5453505,
   "Latitude": 105.7468535
 },
 {
   "STT": 274,
   "Name": "Quầy thuốc Kim Sông",
   "address": "Ấp Mỹ Tây 2, xã Mỹ Quí, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5247517,
   "Latitude": 105.7312531
 },
 {
   "STT": 275,
   "Name": "Cơ Sở thuốc Đông YThành NgọcĐường 1",
   "address": "Ấp Mỹ Tây 2, xã Mỹ Quý, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5247517,
   "Latitude": 105.7312531
 },
 {
   "STT": 276,
   "Name": "Quầy thuốc Kim Mai",
   "address": "Ấp Mỹ Tây 2, xã Mỹ Quý, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5247517,
   "Latitude": 105.7312531
 },
 {
   "STT": 277,
   "Name": "Quầy thuốc Lê Diễm An",
   "address": "Ấp Mỹ Tây 2, xã Mỹ Quý, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5247517,
   "Latitude": 105.7312531
 },
 {
   "STT": 278,
   "Name": "Quầy thuốc Nguyễn Bảo Trân",
   "address": "Ấp Mỹ Thạnh, xã Phú Điền, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.4498007,
   "Latitude": 105.8961593
 },
 {
   "STT": 279,
   "Name": "Quầy thuốc Nhất Chánh",
   "address": "Ấp Mỹ Thạnh, xã Phú Điền, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.4498007,
   "Latitude": 105.8961593
 },
 {
   "STT": 280,
   "Name": "Quầy thuốc Thùy Dung",
   "address": "Ấp Mỹ Thạnh, xã Phú Điền, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.4498007,
   "Latitude": 105.8961593
 },
 {
   "STT": 281,
   "Name": "Tủ thuốcTrạm Y Tế Xã Phö Điền",
   "address": "Ấp Mỹ Thạnh, xã Phú Điền, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.4498007,
   "Latitude": 105.8961593
 },
 {
   "STT": 282,
   "Name": "Quầy thuốc Kim Cương",
   "address": "Ấp Mỹ Thị B, xã Mỹ An, huyệnTháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5175769,
   "Latitude": 105.8642593
 },
 {
   "STT": 283,
   "Name": "Quầy thuốc Kim Liên",
   "address": "Ấp Mỹ Thị B, xã Mỹ An, huyệnTháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5175769,
   "Latitude": 105.8642593
 },
 {
   "STT": 284,
   "Name": "Quầy thuốc Kim Liên",
   "address": "Ấp Mỹ Thị B, xã Mỹ An, huyệnTháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5175769,
   "Latitude": 105.8642593
 },
 {
   "STT": 285,
   "Name": "Quầy thuốc Trạm Y Tế Xã Mỹ An",
   "address": "Ấp Mỹ Thị B, xã Mỹ An, huyệnTháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5175769,
   "Latitude": 105.8642593
 },
 {
   "STT": 286,
   "Name": "Quầy thuốc Dũ - Tuyền",
   "address": "Ấp Nhứt, xã An Phong, huyệnThanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.6333203,
   "Latitude": 105.4235221
 },
 {
   "STT": 287,
   "Name": "Quầy thuốc Thu Loan",
   "address": "Ấp Phú An, xã Phú Ninh, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.7205853,
   "Latitude": 105.4291848
 },
 {
   "STT": 288,
   "Name": "Quầy thuốc Tuấn Anh",
   "address": "ấp Phú An, xã Tân Phú Đông, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2788747,
   "Latitude": 105.7409852
 },
 {
   "STT": 289,
   "Name": "Quầy thuốc Mỹ Tín",
   "address": "Ấp Phú Bình, xã Phú Hựu, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2340764,
   "Latitude": 105.8615327
 },
 {
   "STT": 290,
   "Name": "Quầy thuốc Thế An",
   "address": "Ấp Phú Bình, xã Phú Hựu, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2340764,
   "Latitude": 105.8615327
 },
 {
   "STT": 291,
   "Name": "Quầy thuốc Ngọc Tö",
   "address": "Ấp Phú Hòa A, xã Phú Thuận A, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.722479,
   "Latitude": 105.2940941
 },
 {
   "STT": 292,
   "Name": "Quầy thuốc Thanh Tú",
   "address": "Ấp Phú Hòa A, xã Phú Thuận A, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.722479,
   "Latitude": 105.2940941
 },
 {
   "STT": 293,
   "Name": "Quầy thuốc Khoa Lợi",
   "address": "Ấp Phú Hội Xuân, xã Phú Long, huyện  Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2361256,
   "Latitude": 105.801996
 },
 {
   "STT": 294,
   "Name": "Quầy thuốc Nguyễn Tuấn",
   "address": "Ấp Phú Lợi A, xã Phú Thuận B, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.7570042,
   "Latitude": 105.3423909
 },
 {
   "STT": 295,
   "Name": "Quầy thuốc Quốc Huy",
   "address": "Ấp Phú Lợi A, xã Phú Thuận B, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.7570042,
   "Latitude": 105.3423909
 },
 {
   "STT": 296,
   "Name": "Quầy thuốc Tiếng Hát",
   "address": "Ấp Phú Lợi A, xã Phú Thuận B, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.7570042,
   "Latitude": 105.3423909
 },
 {
   "STT": 297,
   "Name": "Quầy thuốc Tính Phượng",
   "address": "Ấp Phú Lợi A, xã Phú Thuận B, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.7570042,
   "Latitude": 105.3423909
 },
 {
   "STT": 298,
   "Name": "Quầy thuốc Thùy Trang",
   "address": "Ấp Phú Lợi A, xã Phú Thuận B, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.7570042,
   "Latitude": 105.3423909
 },
 {
   "STT": 299,
   "Name": "Quầy thuốc Dạ Lan",
   "address": "Ấp Phú Lợi B, xã Phú Thuận B, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.7570042,
   "Latitude": 105.3423909
 },
 {
   "STT": 300,
   "Name": "Quầy thuốc Ngọc Cẩm",
   "address": "Ấp Phú Lợi B, xã Phú Thuận B, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.7570042,
   "Latitude": 105.3423909
 },
 {
   "STT": 301,
   "Name": "Quầy thuốc Phương Khánh",
   "address": "Ấp Phú Lợi B, xã Phú Thuận B, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.7570042,
   "Latitude": 105.3423909
 },
 {
   "STT": 302,
   "Name": "Quầy thuốc Quang Bình",
   "address": "Ấp Phú Lợi B, xã Phú Thuận B, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.7570042,
   "Latitude": 105.3423909
 },
 {
   "STT": 303,
   "Name": "Quầy thuốc Vĩnh Phöc",
   "address": "Ấp Phú Lợi B, xã Phú Thuận B, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.7570042,
   "Latitude": 105.3423909
 },
 {
   "STT": 304,
   "Name": "Quầy thuốc Minh Vy",
   "address": "Ấp Phú Long, xã Phú Hựu, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2404514,
   "Latitude": 105.8583873
 },
 {
   "STT": 305,
   "Name": "Quầy thuốc Kim Yến",
   "address": "Ấp Phú Long, xã Phú Thành B, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.7562633,
   "Latitude": 105.4653897
 },
 {
   "STT": 306,
   "Name": "Quầy thuốc Trâm Anh",
   "address": "Ấp Phú Long, xã Phú Thành B, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.7562633,
   "Latitude": 105.4653897
 },
 {
   "STT": 307,
   "Name": "Quầy thuốc Hiếu Nghĩa",
   "address": "Ấp Phú Nhuận, xã Tân Nhuận Đông, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2422003,
   "Latitude": 105.8231588
 },
 {
   "STT": 308,
   "Name": "Quầy thuốc Ánh Đào",
   "address": "Ấp Phú Thạnh B, xã Phú Thuận A, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.7403076,
   "Latitude": 105.2902865
 },
 {
   "STT": 309,
   "Name": "Quầy thuốc Anh Khoa",
   "address": "Ấp Phú Thạnh B, xã Phú Thuận A, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.7403076,
   "Latitude": 105.2902865
 },
 {
   "STT": 310,
   "Name": "Quầy thuốc Cẩm Tö",
   "address": "Ấp Phú Thạnh B, xã Phú Thuận A, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.7403076,
   "Latitude": 105.2902865
 },
 {
   "STT": 311,
   "Name": "Quầy thuốc Hiền Đức",
   "address": "Ấp Phú Thạnh, xã An Phú Thuận, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2169808,
   "Latitude": 105.8936224
 },
 {
   "STT": 312,
   "Name": "Quầy thuốc Thảo",
   "address": "Ấp Phú Thạnh, xã An Phú Thuận, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2169808,
   "Latitude": 105.8936224
 },
 {
   "STT": 313,
   "Name": "Quầy thuốc Mỹ Hoa",
   "address": "Ấp Phú Thạnh, xã Phú Long, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2306479,
   "Latitude": 105.8044244
 },
 {
   "STT": 314,
   "Name": "Quầy thuốc Tiến Anh",
   "address": "Ấp Phú Thạnh, xã Phú Long, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2306479,
   "Latitude": 105.8044244
 },
 {
   "STT": 315,
   "Name": "Quầy thuốc Thiên Lộc",
   "address": "Ấp Phú Thạnh, xã Phú Long, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2306479,
   "Latitude": 105.8044244
 },
 {
   "STT": 316,
   "Name": "Quầy thuốc Phú Quý",
   "address": "Ấp Phú Thọ B, xã Phú Thọ, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.681937,
   "Latitude": 105.4927557
 },
 {
   "STT": 317,
   "Name": "Quầy thuốc Mộng Linh",
   "address": "Ấp Phú Thọ, xã An Long, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.718436,
   "Latitude": 105.547094
 },
 {
   "STT": 318,
   "Name": "Quầy thuốc Phương Đại",
   "address": "Ấp Phú Thọ, xã An Long, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.718436,
   "Latitude": 105.547094
 },
 {
   "STT": 319,
   "Name": "Quầy thuốc Thái Hòa",
   "address": "Ấp Phú Thọ, xã An Long, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.718436,
   "Latitude": 105.547094
 },
 {
   "STT": 320,
   "Name": "Quầy thuốc Diễm Phöc",
   "address": "Ấp Phú Xuân, xã Phú Đức, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.760107,
   "Latitude": 105.5566537
 },
 {
   "STT": 321,
   "Name": "Cơ Sở thuốc Đông Y Bích Liên",
   "address": "Ấp Phước Tiên, xã Thông Bình, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.9447475,
   "Latitude": 105.4831451
 },
 {
   "STT": 322,
   "Name": "Quầy thuốc Duyên Anh",
   "address": "Ấp Phước Tiên, xã Thông Bình, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.9447475,
   "Latitude": 105.4831451
 },
 {
   "STT": 323,
   "Name": "Quầy thuốc Hồng Nhan",
   "address": "Ấp Phước Tiên, xã Thông Bình, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.9447475,
   "Latitude": 105.4831451
 },
 {
   "STT": 324,
   "Name": "Quầy thuốc Mười Hải",
   "address": "Ấp Phước Tiên, xã Thông Bình, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.9447475,
   "Latitude": 105.4831451
 },
 {
   "STT": 325,
   "Name": "Quầy thuốc Nhật Thanh",
   "address": "Ấp Phước Tiên, xã Thông Bình, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.9447475,
   "Latitude": 105.4831451
 },
 {
   "STT": 326,
   "Name": "Quầy thuốc Như Ngọc",
   "address": "Ấp Phước Tiên, xã Thông Bình, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.9447475,
   "Latitude": 105.4831451
 },
 {
   "STT": 327,
   "Name": "Quầy thuốc Trí Linh",
   "address": "Ấp Tân An, xã Tân Bình, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2553925,
   "Latitude": 105.776198
 },
 {
   "STT": 328,
   "Name": "Quầy thuốc Thạnh Phö",
   "address": "Ấp Tân An, xã Tân Huề, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.6134548,
   "Latitude": 105.365319
 },
 {
   "STT": 329,
   "Name": "Quầy thuốc Phú Long",
   "address": "Ấp Tân An, xã Tân Thành, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2482267,
   "Latitude": 105.6035329
 },
 {
   "STT": 330,
   "Name": "Quầy thuốc Hiển Dương",
   "address": "Ấp Tân Bảnh, xã Tân Phước, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8390414,
   "Latitude": 105.5591615
 },
 {
   "STT": 331,
   "Name": "Quầy thuốc Hoàng Sang",
   "address": "Ấp Tân Bảnh, xã Tân Phước, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8390414,
   "Latitude": 105.5591615
 },
 {
   "STT": 332,
   "Name": "Quầy thuốc Tài",
   "address": "Ấp Tân Bảnh, xã Tân Phước, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8390414,
   "Latitude": 105.5591615
 },
 {
   "STT": 333,
   "Name": "Quầy thuốc Tâm Nhi",
   "address": "Ấp Tân Bảnh, xã Tân Phước, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8390414,
   "Latitude": 105.5591615
 },
 {
   "STT": 334,
   "Name": "Quầy thuốc Thiên Kim",
   "address": "Ấp Tân Bảnh, xã Tân Phước, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8390414,
   "Latitude": 105.5591615
 },
 {
   "STT": 335,
   "Name": "Quầy thuốc Văn Lộc",
   "address": "Ấp Tân Bảnh, xã Tân Phước, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8390414,
   "Latitude": 105.5591615
 },
 {
   "STT": 336,
   "Name": "Quầy thuốc Lê Thủy",
   "address": "Ấp Tân Cường (nhà ông Nguyễn Chí Linh), xã Phú Cường, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.7179597,
   "Latitude": 105.5531043
 },
 {
   "STT": 337,
   "Name": "Quầy thuốc Thiên Kỳ",
   "address": "Ấp Tân Cường, xã Phú Cường, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.7179597,
   "Latitude": 105.5531043
 },
 {
   "STT": 338,
   "Name": "Quầy thuốc Hương Minh",
   "address": "Ấp Tân Định, xã Tân Thành, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.1563791,
   "Latitude": 105.7737064
 },
 {
   "STT": 339,
   "Name": "Quầy thuốc Bích Liễu",
   "address": "Ấp Tân Hòa B, xã Tân Phú, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.668875,
   "Latitude": 105.3541013
 },
 {
   "STT": 340,
   "Name": "Quầy thuốc Gia Hòa",
   "address": "Ấp Tân Hòa B, xã Tân Phú, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.668875,
   "Latitude": 105.3541013
 },
 {
   "STT": 341,
   "Name": "Quầy thuốc Hữu Nhân",
   "address": "Ấp Tân Hòa Trung, xã Tân Hội, thị xã Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8350068,
   "Latitude": 105.3428709
 },
 {
   "STT": 342,
   "Name": "Quầy thuốc Ngân Bình",
   "address": "Ấp Tân Hòa, xã An Hiệp, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2459214,
   "Latitude": 105.7377648
 },
 {
   "STT": 343,
   "Name": "Quầy thuốc Tín Nhung",
   "address": "Ấp Tân Hòa, xã Tân Hội, thị xã HồngNgự, TỈNH GIA LAI",
   "Longtitude": 10.8350068,
   "Latitude": 105.3428709
 },
 {
   "STT": 344,
   "Name": "Quầy thuốc Long Châu",
   "address": "Ấp Tân Hòa, xã Tân Phú, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2335256,
   "Latitude": 105.7602262
 },
 {
   "STT": 345,
   "Name": "Quầy thuốc Ngọc Lan",
   "address": "Ấp Tân Hòa, xã Tân Phú, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2335256,
   "Latitude": 105.7602262
 },
 {
   "STT": 346,
   "Name": "Quầy thuốc Ngọc Lan",
   "address": "Ấp Tân Hòa, xã Tân Phú, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2335256,
   "Latitude": 105.7602262
 },
 {
   "STT": 347,
   "Name": "Quầy thuốc Kim Thúy",
   "address": "Ấp Tân Hội, xã Tân Long, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5790949,
   "Latitude": 105.4009508
 },
 {
   "STT": 348,
   "Name": "Quầy thuốc Đức Tài",
   "address": "Ấp Tân Hưng, xã Tân Công Sính, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.7010395,
   "Latitude": 105.6093717
 },
 {
   "STT": 349,
   "Name": "Quầy thuốc Phương Tâm",
   "address": "Ấp Tân Hưng, xã Tân Công Sính, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.7010395,
   "Latitude": 105.6093717
 },
 {
   "STT": 350,
   "Name": "Quầy thuốc Trọng Tiên",
   "address": "Ấp Tân Hưng, xã Tân Công Sính, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.7010395,
   "Latitude": 105.6093717
 },
 {
   "STT": 351,
   "Name": "Quầy thuốc Minh Khôi",
   "address": "Ấp Tân Lập, xã Tân Nhuận Đông, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2422003,
   "Latitude": 105.8231588
 },
 {
   "STT": 352,
   "Name": "Quầy thuốc Nguyễn Bá Tước",
   "address": "Ấp Tân Lập, xã Tân Nhuận Đông, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2422003,
   "Latitude": 105.8231588
 },
 {
   "STT": 353,
   "Name": "Quầy thuốc Vân Khánh",
   "address": "Ấp Tân Lập, xã Tân Nhuận Đông, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2422003,
   "Latitude": 105.8231588
 },
 {
   "STT": 354,
   "Name": "Quầy thuốc Thanh Hùng",
   "address": "Ấp Tân Lộc, xã Tân Thành, huyệnLai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2560702,
   "Latitude": 105.592886
 },
 {
   "STT": 355,
   "Name": "Quầy thuốc Nguyễn Minh Trí",
   "address": "Ấp Tân Mỹ, xã Tân Phú Trung, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2462649,
   "Latitude": 105.7409852
 },
 {
   "STT": 356,
   "Name": "Quầy thuốc Thanh Thanh",
   "address": "Ấp Tân Phong, xã Phong Hòa, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.1833808,
   "Latitude": 105.6940454
 },
 {
   "STT": 357,
   "Name": "Quầy thuốc Hương Giang",
   "address": "Ấp Tân Phong, xã Tân Huề, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.6134548,
   "Latitude": 105.365319
 },
 {
   "STT": 358,
   "Name": "Quầy thuốc Cẩm Vân",
   "address": "Ấp Tân Phú, xã An Nhơn, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2335256,
   "Latitude": 105.7602262
 },
 {
   "STT": 359,
   "Name": "Quầy thuốc Huỳnh Duy",
   "address": "Ấp Tân Phú, xã An Nhơn, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2335256,
   "Latitude": 105.7602262
 },
 {
   "STT": 360,
   "Name": "Quầy thuốc Tâm An",
   "address": "Ấp Tân Phú, xã An Nhơn, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2335256,
   "Latitude": 105.7602262
 },
 {
   "STT": 361,
   "Name": "Quầy thuốc Minh Cảnh",
   "address": "Ấp Tân Phú, xã Tân Phú Trung, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2462649,
   "Latitude": 105.7409852
 },
 {
   "STT": 362,
   "Name": "Quầy thuốc Nguyễn Thị Thắm",
   "address": "Ấp Tân Phú, xã Tân Phú Trung, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2462649,
   "Latitude": 105.7409852
 },
 {
   "STT": 363,
   "Name": "Quầy thuốc Trường An",
   "address": "Ấp Tân Phú, xã Tân Phú Trung, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2462649,
   "Latitude": 105.7409852
 },
 {
   "STT": 364,
   "Name": "Quầy thuốc Bình An",
   "address": "Ấp Tân Quới, xã Tân Phú Trung, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2509947,
   "Latitude": 105.7409852
 },
 {
   "STT": 365,
   "Name": "Quầy thuốc Nguyễn Nhân",
   "address": "Ấp Tân Thạnh, xã An Hiệp, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.286241,
   "Latitude": 105.8231878
 },
 {
   "STT": 366,
   "Name": "Quầy thuốc Phú Vinh",
   "address": "Ấp Tân Thạnh, xã An Hiệp, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.286241,
   "Latitude": 105.8231878
 },
 {
   "STT": 367,
   "Name": "Cơ Sở thuốc Đông Y Phước HảiĐường",
   "address": "Ấp Tân Thạnh, xã Phong Hòa, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.1833808,
   "Latitude": 105.6940454
 },
 {
   "STT": 368,
   "Name": "Quầy thuốc Hồng Nhung",
   "address": "Ấp Tân Thạnh, xã Phong Hòa, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.1833808,
   "Latitude": 105.6940454
 },
 {
   "STT": 369,
   "Name": "Quầy thuốc Mai Anh",
   "address": "ấp Tân Thuận B, xã Tân Mỹ, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.4081902,
   "Latitude": 105.6435715
 },
 {
   "STT": 370,
   "Name": "Quầy thuốc Khánh Vy",
   "address": "Ấp Tân Thuận, xã An Phú Thuận, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2169808,
   "Latitude": 105.8936224
 },
 {
   "STT": 371,
   "Name": "Quầy thuốc Ngọc Hoa",
   "address": "Ấp Tân Thuận, xã Tân Hòa, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2074033,
   "Latitude": 105.6471178
 },
 {
   "STT": 372,
   "Name": "Quầy thuốc Kim Thoa",
   "address": "Ấp Tân Thuận, xã Tân Phú Trung, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2502428,
   "Latitude": 105.7616834
 },
 {
   "STT": 373,
   "Name": "Quầy thuốc Thiên Vinh",
   "address": "Ấp Tân Thuận, xã Tân Phú Trung, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2502428,
   "Latitude": 105.7616834
 },
 {
   "STT": 374,
   "Name": "Quầy thuốc Huy Phượng",
   "address": "ấp Tân Trong, xã Tân Mỹ, huyện LấpVò, TỈNH GIA LAI",
   "Longtitude": 10.3919408,
   "Latitude": 105.6454656
 },
 {
   "STT": 375,
   "Name": "Quầy thuốc Diệp Phương",
   "address": "Ấp Thạnh An, xã Tân Long, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5790949,
   "Latitude": 105.4009508
 },
 {
   "STT": 376,
   "Name": "Quầy thuốc Kim Ngoan",
   "address": "Ấp Thạnh An, xã Tân Long, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5790949,
   "Latitude": 105.4009508
 },
 {
   "STT": 377,
   "Name": "Quầy thuốc Tö Như",
   "address": "Ấp Thành Lập, xã Tân Công Chí, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.85,
   "Latitude": 105.5
 },
 {
   "STT": 378,
   "Name": "Quầy thuốc Ngọc Liên",
   "address": "Ấp Thạnh Phú, xã Tân Bình, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2553925,
   "Latitude": 105.776198
 },
 {
   "STT": 379,
   "Name": "Quầy thuốc Đặng Văn Guôl",
   "address": "Ấp Thị, xã An Phong, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.6310843,
   "Latitude": 105.4171228
 },
 {
   "STT": 380,
   "Name": "Cơ Sở thuốc Đông Y Dũng - Hạnh",
   "address": "Ấp Thị, xã An Phong, huyệnThanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.6310843,
   "Latitude": 105.4171228
 },
 {
   "STT": 381,
   "Name": "Quầy thuốc Linh Loan",
   "address": "Ấp Thị, xã Thông Bình, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.9447475,
   "Latitude": 105.4831451
 },
 {
   "STT": 382,
   "Name": "Quầy thuốc Bé Bảy",
   "address": "Ấp Thống Nhất 1, xã Tân Công Chí, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8439338,
   "Latitude": 105.4663768
 },
 {
   "STT": 383,
   "Name": "Quầy thuốc Hoàng Kim",
   "address": "Ấp Thống Nhất 1, xã Tân Công Chí, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8439338,
   "Latitude": 105.4663768
 },
 {
   "STT": 384,
   "Name": "Quầy thuốc Tý Tiến",
   "address": "Ấp Thống Nhất 1, xã Tân Công Chí, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8439338,
   "Latitude": 105.4663768
 },
 {
   "STT": 385,
   "Name": "Quầy thuốc Ngọc Hân",
   "address": "Ấp Thống Nhất, xã Tân Công Chí, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8439338,
   "Latitude": 105.4663768
 },
 {
   "STT": 386,
   "Name": "Quầy thuốc Lê Ngô",
   "address": "Ấp Thượng 1, xã Thường Thới Tiền, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8208572,
   "Latitude": 105.2814555
 },
 {
   "STT": 387,
   "Name": "Quầy thuốc Quí Phương",
   "address": "Ấp Thượng 1, xã Thường Thới Tiền, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8208572,
   "Latitude": 105.2814555
 },
 {
   "STT": 388,
   "Name": "Quầy thuốc Öt Kiển",
   "address": "Ấp Thượng, xã Tân Quới, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.6796067,
   "Latitude": 105.374894
 },
 {
   "STT": 389,
   "Name": "Quầy thuốc Bệnh Viện Huyện Hồng Ngự",
   "address": "Ấp Thượng, xã Thường Thới Tiền, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8368466,
   "Latitude": 105.2715969
 },
 {
   "STT": 390,
   "Name": "Quầy thuốc Kiều Phương",
   "address": "Ấp Thượng, xã Thường Thới Tiền, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8368466,
   "Latitude": 105.2715969
 },
 {
   "STT": 391,
   "Name": "Quầy thuốc Kim Duyên",
   "address": "Ấp Thượng, xã Thường Thới Tiền, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8368466,
   "Latitude": 105.2715969
 },
 {
   "STT": 392,
   "Name": "Quầy thuốc Mỹ Duyên",
   "address": "Ấp Thượng, xã Thường Thới Tiền, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8368466,
   "Latitude": 105.2715969
 },
 {
   "STT": 393,
   "Name": "Quầy thuốc Cẩm Hồng",
   "address": "Ấp Trung 1, xã Thường Thới Tiền, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.837225,
   "Latitude": 105.2721456
 },
 {
   "STT": 394,
   "Name": "Quầy thuốc Bảo Anh",
   "address": "Ấp Trung, xã Tân Thạnh, huyệnThanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5717356,
   "Latitude": 105.4504695
 },
 {
   "STT": 395,
   "Name": "Quầy thuốc Mỹ Xuân",
   "address": "Ấp Trung, xã Tân Thạnh, huyệnThanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5717356,
   "Latitude": 105.4504695
 },
 {
   "STT": 396,
   "Name": "Quầy thuốc Phương Điền",
   "address": "Ấp Trung, xã Tân Thạnh, huyệnThanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5717356,
   "Latitude": 105.4504695
 },
 {
   "STT": 397,
   "Name": "Quầy thuốc Mai Thanh",
   "address": "Ấp Trung, xãThường Thới Tiền, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.837225,
   "Latitude": 105.2721456
 },
 {
   "STT": 398,
   "Name": "Quầy thuốc Hùng Vĩ",
   "address": "Bà Nương, ấp Thới Mỹ 1, xã VĩnhThới, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2405852,
   "Latitude": 105.6353878
 },
 {
   "STT": 399,
   "Name": "Quầy thuốc Phượng Hằng",
   "address": "Bảnh, xã Tân Phước, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8196644,
   "Latitude": 105.5245111
 },
 {
   "STT": 400,
   "Name": "Nhà thuốc Hồng Tiếp",
   "address": "Biên Phủ, phường Mỹ Phú, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4681883,
   "Latitude": 105.6451373
 },
 {
   "STT": 401,
   "Name": "Quầy thuốc Châu Pha",
   "address": "Biển, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.4936192,
   "Latitude": 105.7996769
 },
 {
   "STT": 402,
   "Name": "Quầy thuốc Duy Thanh",
   "address": "Biển, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.4936192,
   "Latitude": 105.7996769
 },
 {
   "STT": 403,
   "Name": "Quầy thuốc Huỳnh Trang",
   "address": "Biển, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.4936192,
   "Latitude": 105.7996769
 },
 {
   "STT": 404,
   "Name": "Quầy thuốc Thanh Tuyền",
   "address": "Biển, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.4936192,
   "Latitude": 105.7996769
 },
 {
   "STT": 405,
   "Name": "Quầy thuốc Thu Hát",
   "address": "Biển, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.4936192,
   "Latitude": 105.7996769
 },
 {
   "STT": 406,
   "Name": "Cơ Sở thuốc Đông Y Thái Hưng",
   "address": "Bình A, xã Vĩnh Thạnh, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3460997,
   "Latitude": 105.6165914
 },
 {
   "STT": 407,
   "Name": "Quầy thuốc Bé Sáu",
   "address": "Bình A, xã Vĩnh Thạnh, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3460997,
   "Latitude": 105.6165914
 },
 {
   "STT": 408,
   "Name": "Quầy thuốc Kim Loan",
   "address": "Bình A, xã Vĩnh Thạnh, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3460997,
   "Latitude": 105.6165914
 },
 {
   "STT": 409,
   "Name": "Quầy thuốc Kim Trinh",
   "address": "Bình A, xã Vĩnh Thạnh, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3460997,
   "Latitude": 105.6165914
 },
 {
   "STT": 410,
   "Name": "Quầy thuốc Liễu",
   "address": "Bình A, xã Vĩnh Thạnh, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3460997,
   "Latitude": 105.6165914
 },
 {
   "STT": 411,
   "Name": "Quầy thuốc Thiện Thảo",
   "address": "Bình A, xã Vĩnh Thạnh, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3460997,
   "Latitude": 105.6165914
 },
 {
   "STT": 412,
   "Name": "Quầy thuốc Thái Hương",
   "address": "Bình An, xã Bình Thành, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3803603,
   "Latitude": 105.5591615
 },
 {
   "STT": 413,
   "Name": "Quầy thuốc Thanh Tiền",
   "address": "Bình Chánh, xã Bình Thành, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5786733,
   "Latitude": 105.5564149
 },
 {
   "STT": 414,
   "Name": "Cơ Sở thuốc Đông Y Hồng Hoa Đường",
   "address": "Bình Lợi, xã Bình Thành, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.335945,
   "Latitude": 105.4985332
 },
 {
   "STT": 415,
   "Name": "Quầy thuốc Bích Vân",
   "address": "Bình Lợi, xã Bình Thành, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.335945,
   "Latitude": 105.4985332
 },
 {
   "STT": 416,
   "Name": "Quầy thuốc Quốc Phong",
   "address": "Bình Lợi, xã Bình Thành, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.335945,
   "Latitude": 105.4985332
 },
 {
   "STT": 417,
   "Name": "Quầy thuốc Quốc Minh",
   "address": "Bình Thạnh 1, thị trấn Lấp Vò, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3578031,
   "Latitude": 105.5440664
 },
 {
   "STT": 418,
   "Name": "Quầy thuốc Thúy Anh 1",
   "address": "Bình Thạnh 1, thị trấn Lấp Vò, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3578031,
   "Latitude": 105.5440664
 },
 {
   "STT": 419,
   "Name": "Quầy thuốc Phúc Xuân",
   "address": "Bình Thạnh 2, thị trấn Lấp Vò, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3577289,
   "Latitude": 105.5210607
 },
 {
   "STT": 420,
   "Name": "Quầy thuốc Phúc Minh",
   "address": "Bình Tiên, ấp Tân Mỹ, xã Tân PhúTrung, huyện Châu",
   "Longtitude": 10.2462649,
   "Latitude": 105.7409852
 },
 {
   "STT": 421,
   "Name": "Quầy thuốc Bích Ngân",
   "address": "Bình, xã HòaThành, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2914895,
   "Latitude": 105.7057792
 },
 {
   "STT": 422,
   "Name": "Quầy thuốc Nguyễn Mừng",
   "address": "Bình, xã HòaThành, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2914895,
   "Latitude": 105.7057792
 },
 {
   "STT": 423,
   "Name": "Quầy thuốc Vẹn Phan",
   "address": "Bình, xã Long Thắng, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2428539,
   "Latitude": 105.6999122
 },
 {
   "STT": 424,
   "Name": "Quầy thuốc Võ Thị Hồng Thảo",
   "address": "Bình, xã Tân Khánh Trung, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3791189,
   "Latitude": 105.6940454
 },
 {
   "STT": 425,
   "Name": "Quầy thuốc Đặng ThịHồng",
   "address": "Bình, xã TânThành, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.255455,
   "Latitude": 105.6647142
 },
 {
   "STT": 426,
   "Name": "Quầy thuốc Trần Quang Thấm",
   "address": "Bình, xã TânThành, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.255455,
   "Latitude": 105.6647142
 },
 {
   "STT": 427,
   "Name": "Quầy thuốc Vĩnh Bảo",
   "address": "Bình, xã TânThành, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.255455,
   "Latitude": 105.6647142
 },
 {
   "STT": 428,
   "Name": "Quầy thuốc Hoàng Anh",
   "address": "Cao Lãnh-Vàm Cống, ấp TânTrong, xã Tân Mỹ,",
   "Longtitude": 10.3923418,
   "Latitude": 105.6471178
 },
 {
   "STT": 429,
   "Name": "Quầy thuốc Kim Tuyến",
   "address": "Cầu ván KDC, ấp An Hòa, xã Định An, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3219082,
   "Latitude": 105.5298525
 },
 {
   "STT": 430,
   "Name": "Quầy thuốc Việt An",
   "address": "CDC Ao Sen (thửa đất số 349, TBĐ số 03), khóm 2, thị trấn .Tràm Chim, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.6691946,
   "Latitude": 105.5650239
 },
 {
   "STT": 431,
   "Name": "Quầy thuốc Xuân Thành",
   "address": "Châu, xã Tịnh Thới, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4339621,
   "Latitude": 105.661869
 },
 {
   "STT": 432,
   "Name": "Quầy thuốc Thu Vân",
   "address": "Chợ An Hòa ấp 1, xã An Hòa, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.7459248,
   "Latitude": 105.3652405
 },
 {
   "STT": 433,
   "Name": "Quầy thuốc Quang Tuyến",
   "address": "Chợ An Long, ấp Phú Thọ, xã An Long, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.720632,
   "Latitude": 105.4126652
 },
 {
   "STT": 434,
   "Name": "Quầy thuốc Quốc Tuấn",
   "address": "Chợ Ba Sao, ấp 3, xã Ba Sao, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.5301379,
   "Latitude": 105.6846128
 },
 {
   "STT": 435,
   "Name": "Quầy thuốc Trung Nguyên",
   "address": "Chợ Ba Sao, ấp 3, xã Ba Sao, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.5301379,
   "Latitude": 105.6846128
 },
 {
   "STT": 436,
   "Name": "Cơ Sở thuốc Đông Y Sinh Phúc",
   "address": "Chợ Bình Thành, ấp Bình Chánh, xã Bình Thành, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5769859,
   "Latitude": 105.5592259
 },
 {
   "STT": 437,
   "Name": "Quầy thuốc Thanh Lâm",
   "address": "Chợ Bình Thành, ấp Bình Chánh, xã Bình Thành, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5769859,
   "Latitude": 105.5592259
 },
 {
   "STT": 438,
   "Name": "Quầy thuốc Thanh Nha",
   "address": "Chợ Bình Thành, ấp Bình Chánh, xã Bình Thành, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5769859,
   "Latitude": 105.5592259
 },
 {
   "STT": 439,
   "Name": "Quầy thuốc Cảnh Tö",
   "address": "Chợ Bình Thuận, ấp Bình Thuận, xã Bình Thành, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5766484,
   "Latitude": 105.5591615
 },
 {
   "STT": 440,
   "Name": "Quầy thuốc Trần Hồ Đắc Linh",
   "address": "Chợ Cái Đôi, ấp Tân Định, xã Tân Thành, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2640559,
   "Latitude": 105.6002024
 },
 {
   "STT": 441,
   "Name": "Quầy thuốc Cẩm Chi",
   "address": "Chợ Cái Dứa, ấp Hòa Bình, xã Tân Hòa, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.1911153,
   "Latitude": 105.6324318
 },
 {
   "STT": 442,
   "Name": "Quầy thuốc Thanh Hùng",
   "address": "Chợ Cái Sơn ấp Tân Lợi, xã Tân Thành, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.255455,
   "Latitude": 105.6647142
 },
 {
   "STT": 443,
   "Name": "Quầy thuốc Mỹ Hoa",
   "address": "Chợ Cái Tre, ấp Bình Định, xã BìnhThành, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5492454,
   "Latitude": 105.5132787
 },
 {
   "STT": 444,
   "Name": "Quầy thuốc Chí Linh",
   "address": "Chợ chiếu Định Yên, ấp An Bình, xã Định Yên, huyện  Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.307928,
   "Latitude": 105.536078
 },
 {
   "STT": 445,
   "Name": "Quầy thuốc Phương Thảo",
   "address": "Chợ đầu mối trái cây, xã Mỹ Hiệp, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.3308156,
   "Latitude": 105.8103168
 },
 {
   "STT": 446,
   "Name": "Quầy thuốc Đoàn Văn Lít",
   "address": "Chợ Định Hòa, ấp Định Thành, xã Định Hòa, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.1850999,
   "Latitude": 105.6588485
 },
 {
   "STT": 447,
   "Name": "Quầy thuốc Hoàng Giang",
   "address": "Chợ Định Hòa, ấp Định Thành, xã Định Hòa, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.1850999,
   "Latitude": 105.6588485
 },
 {
   "STT": 448,
   "Name": "Quầy thuốc Châu Thanh Giàu",
   "address": "chợ Định Yên, ấp An Lợi B, xã Định Yên, huyện Lấp",
   "Longtitude": 10.3239995,
   "Latitude": 105.5243971
 },
 {
   "STT": 449,
   "Name": "Quầy thuốc Hồng Thương",
   "address": "Chợ đô thị Phong Hòa, xã Phong Hòa, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.1706664,
   "Latitude": 105.673473
 },
 {
   "STT": 450,
   "Name": "Quầy thuốc Minh Ý",
   "address": "Chợ Đốc Binh Kiều, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5129025,
   "Latitude": 105.9243834
 },
 {
   "STT": 451,
   "Name": "Quầy thuốc Kim Luyến",
   "address": "Chợ Đường Thét, xã Mỹ Quí, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5269358,
   "Latitude": 105.7253131
 },
 {
   "STT": 452,
   "Name": "Quầy thuốc Hồng Thái",
   "address": "Chợ Giồng Găng, ấp Tân Bảnh, xã Tân Phước, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8396315,
   "Latitude": 105.5593332
 },
 {
   "STT": 453,
   "Name": "Quầy thuốc Cẩm An",
   "address": "Chợ Giồng Găng, xã Tân Phước, huyện Tân Hồng , TỈNH GIA LAI",
   "Longtitude": 10.8202769,
   "Latitude": 105.5144287
 },
 {
   "STT": 454,
   "Name": "Quầy thuốc Tuyết Trinh",
   "address": "Chợ Hậu Thành, ấp Hậu Thành, xã Tân Dương, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.3107215,
   "Latitude": 105.6863828
 },
 {
   "STT": 455,
   "Name": "Quầy thuốc Huy Hoàng 2",
   "address": "Chợ Lai Vung, khóm 3, thị trấn Lai Vung, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2884478,
   "Latitude": 105.6588858
 },
 {
   "STT": 456,
   "Name": "Quầy thuốc Nhã Quyên",
   "address": "Chợ Lai Vung, khóm 3, thị trấn Lai Vung, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2884478,
   "Latitude": 105.6588858
 },
 {
   "STT": 457,
   "Name": "Quầy thuốc Việt Tài",
   "address": "Chợ Lai Vung, khóm 3, thị trấn Lai Vung, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2884478,
   "Latitude": 105.6588858
 },
 {
   "STT": 458,
   "Name": "Quầy thuốc Kim Thủy",
   "address": "Chợ Long Thành, ấp Long Thành, xã Long Hậu, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2848557,
   "Latitude": 105.6235887
 },
 {
   "STT": 459,
   "Name": "Quầy thuốc Ánh Duy",
   "address": "chợ mới Tân Thạnh, xã Tân Thạnh, huyệnThanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5717356,
   "Latitude": 105.4504695
 },
 {
   "STT": 460,
   "Name": "Quầy thuốc Lệ Xuân",
   "address": "Chợ Mương Chùa, ấp An Quới, xã Hội An Đông, huyệnLấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.4078862,
   "Latitude": 105.5532993
 },
 {
   "STT": 461,
   "Name": "Quầy thuốc Thùy Dung",
   "address": "Chợ Mỹ Hiệp, ấp 1, xã Mỹ Hiệp, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.340617,
   "Latitude": 105.8114175
 },
 {
   "STT": 462,
   "Name": "Quầy thuốc Kim Cương 1",
   "address": "Chợ Mỹ Xương ấp Mỹ Thới, xã Mỹ Xương, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.410333,
   "Latitude": 105.718453
 },
 {
   "STT": 463,
   "Name": "Quầy thuốc Quốc Tuấn",
   "address": "Chợ Ngã Ba Tháp, ấp An Hòa, xã Mỹ An Hưng B, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3989179,
   "Latitude": 105.6007324
 },
 {
   "STT": 464,
   "Name": "Quầy thuốc Anh Tuấn 2",
   "address": "Chợ Nước Xoáy, ấp Hưng Mỹ Tây, xã Long Hưng A, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3449461,
   "Latitude": 105.6808476
 },
 {
   "STT": 465,
   "Name": "Quầy thuốc Trâm Anh",
   "address": "Chợ Phú Điền, xã Phú Điền, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.4510549,
   "Latitude": 105.8992553
 },
 {
   "STT": 466,
   "Name": "Quầy thuốc Thanh Hồng",
   "address": "Chợ Phú Hiệp, ấp K10, xã Phú Hiệp, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.7757656,
   "Latitude": 105.5122694
 },
 {
   "STT": 467,
   "Name": "Quầy thuốc Mộng Thắm",
   "address": "Chợ Phú Thành A, ấp Long Phú A, xã Phú Thành A, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.6975735,
   "Latitude": 105.4360963
 },
 {
   "STT": 468,
   "Name": "Quầy thuốc Nguyễn Giàu",
   "address": "Chợ Phương Thịnh, ấp 5, xã Phương Thịnh, huyện  Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.6092297,
   "Latitude": 105.6705801
 },
 {
   "STT": 469,
   "Name": "Quầy thuốc Thiên Kim",
   "address": "Chợ Rạch Miễu, ấp Tân Thuận, xã Tân Phước, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2943724,
   "Latitude": 105.5906564
 },
 {
   "STT": 470,
   "Name": "Cơ Sở thuốc Đông Y Tế Sanh Đường",
   "address": "Chợ Tân Thạnh, xã Tân Thạnh, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5705534,
   "Latitude": 105.4526212
 },
 {
   "STT": 471,
   "Name": "Cơ Sở thuốc Đông Y Vạn Phát Đường",
   "address": "Chợ Thanh Mỹ, ấp Hưng Lợi, xãThanh Mỹ, huyệnTháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.4072144,
   "Latitude": 105.8579285
 },
 {
   "STT": 472,
   "Name": "Quầy thuốc Bích Trâm",
   "address": "Chợ Thanh Mỹ, ấp Hưng Lợi, xãThanh Mỹ, huyệnTháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.4072144,
   "Latitude": 105.8579285
 },
 {
   "STT": 473,
   "Name": "Cơ Sở thuốc Đông Y Vĩnh Phát Đường",
   "address": "Chợ Thống Linh, xã Phương Trà, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.5302472,
   "Latitude": 105.6461097
 },
 {
   "STT": 474,
   "Name": "Quầy thuốc Nguyễn Nhân",
   "address": "Chợ Thống Linh, xã Phương Trà, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.5302472,
   "Latitude": 105.6461097
 },
 {
   "STT": 475,
   "Name": "Quầy thuốc Tho Thu",
   "address": "Chợ xã Hòa Tân, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.1626729,
   "Latitude": 105.8271813
 },
 {
   "STT": 476,
   "Name": "Cơ Sở thuốc Đông Y Tö Mỹ",
   "address": "Chợ xã Tân Thành, xã Tân Thành, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2506691,
   "Latitude": 105.6000525
 },
 {
   "STT": 477,
   "Name": "Quầy thuốc Nguyễn Thị Đẹp",
   "address": "Chợ Xẻo Quýt, ấp 3, xã Mỹ Long, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.3978506,
   "Latitude": 105.8046936
 },
 {
   "STT": 478,
   "Name": "Quầy thuốc Nhựt Linh",
   "address": "Cụm CN Bình Thành, ấp Bình Chánh, xã Bình Thành, huyệnThanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5449116,
   "Latitude": 105.5306017
 },
 {
   "STT": 479,
   "Name": "Quầy thuốc Phi Châu",
   "address": "Cụm CN Bình Thành, ấp Bình Chánh, xã Bình Thành, huyệnThanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5449116,
   "Latitude": 105.5306017
 },
 {
   "STT": 480,
   "Name": "Quầy thuốc Mai Trinh 2",
   "address": "Cụm Công Nghiệp Bình Thành, ấpBình Định, xã Bình Thành, huyệnThanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5768804,
   "Latitude": 105.5592259
 },
 {
   "STT": 481,
   "Name": "Quầy thuốc Lê Thanh Hiền",
   "address": "Cụm dân cư ấp 4, xã Mỹ Đông, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5211623,
   "Latitude": 105.7938069
 },
 {
   "STT": 482,
   "Name": "Quầy thuốc Kiều Anh",
   "address": "Cụm dân cư bờ Long Sơn Ngọc, ấp Chòi Mòi, xãThông Bình, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.9051057,
   "Latitude": 105.5234111
 },
 // {
 //   "STT": 483,
 //   "Name": "Nhà thuốc Bảo Trí",
 //   "address": "dân cư và nhà ở công vụ đường Điện Biên Phủ,",
 //   "Longtitude": 21.4063898,
 //   "Latitude": 103.0321549
 // },
 {
   "STT": 484,
   "Name": "Quầy thuốc Ngọc Hạnh",
   "address": "đầu mối trái cây Mỹ Hiệp, xã Mỹ Hiệp, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.3308156,
   "Latitude": 105.8103168
 },
 {
   "STT": 485,
   "Name": "Quầy thuốc Hùng Hương",
   "address": "Điền, ấp Mỹ Thạnh, xã PhúĐiền, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.4498007,
   "Latitude": 105.8961593
 },
 {
   "STT": 486,
   "Name": "Quầy thuốc Mai Xuân",
   "address": "Định An, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3219082,
   "Latitude": 105.5298525
 },
 {
   "STT": 487,
   "Name": "Quầy thuốc Tröc Hằng",
   "address": "Định An, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3219082,
   "Latitude": 105.5298525
 },
 {
   "STT": 488,
   "Name": "Quầy thuốc Huệ Thi",
   "address": "Định Yên, ấp An Lợi B, xã Định Yên, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3239995,
   "Latitude": 105.5243971
 },
 {
   "STT": 489,
   "Name": "Quầy thuốc Tân Duyên",
   "address": "Định, xã Bình Thành, huyệnThanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5675894,
   "Latitude": 105.4126652
 },
 {
   "STT": 490,
   "Name": "Quầy thuốc Thọ Thu",
   "address": "Định, xã Long Thắng, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.1850999,
   "Latitude": 105.6588485
 },
 {
   "STT": 491,
   "Name": "Quầy thuốc Văn Öt",
   "address": "Định, xã TânThành, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2506691,
   "Latitude": 105.6000525
 },
 {
   "STT": 492,
   "Name": "Quầy thuốc Huyền Anh",
   "address": "Định, xã Vĩnh Thới, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2405852,
   "Latitude": 105.6353878
 },
 {
   "STT": 493,
   "Name": "Quầy thuốc Huyền Anh",
   "address": "Định, xã Vĩnh Thới, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2405852,
   "Latitude": 105.6353878
 },
 {
   "STT": 494,
   "Name": "Quầy thuốc Huỳnh Đấu",
   "address": "Định, xã Vĩnh Thới, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2405852,
   "Latitude": 105.6353878
 },
 {
   "STT": 495,
   "Name": "Quầy thuốc Mạnh Khương",
   "address": "Định, xã Vĩnh Thới, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2405852,
   "Latitude": 105.6353878
 },
 {
   "STT": 496,
   "Name": "Quầy thuốc Ngọc Hiền",
   "address": "Đô thị Ngã Ba, ấp Tân Lợi, xã Phong Hòa, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.1833808,
   "Latitude": 105.6940454
 },
 {
   "STT": 497,
   "Name": "Quầy thuốc Nguyễn Lâm",
   "address": "Đô thị Phong Hòa, ấp Tân Lợi, xã Phong Hòa, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.1686134,
   "Latitude": 105.6732853
 },
 {
   "STT": 498,
   "Name": "Quầy thuốc Đức Hưng",
   "address": "Đông A, thị trấn .Thanh Bình, huyện",
   "Longtitude": 10.5554601,
   "Latitude": 105.491758
 },
 {
   "STT": 499,
   "Name": "Quầy thuốc Duy Tuyến",
   "address": "Đông Hòa, xã Tân Thuận Đông, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4659127,
   "Latitude": 105.6192603
 },
 {
   "STT": 500,
   "Name": "Quầy thuốc Kiều Oanh",
   "address": "Đông Nhì, xã Mỹ Thọ, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4501101,
   "Latitude": 105.6970266
 },
 {
   "STT": 501,
   "Name": "Quầy thuốc Diễm Chi",
   "address": "ĐT 854, ấp Hòa Hiệp, xã Hòa Tân, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.1814439,
   "Latitude": 105.8374827
 },
 {
   "STT": 502,
   "Name": "Quầy thuốc Minh Ngọc",
   "address": "ĐT852, ấp Tân Lập, xã Tân Qui Tây, thành phố Sa Đéc,",
   "Longtitude": 10.3120792,
   "Latitude": 105.7292491
 },
 {
   "STT": 503,
   "Name": "Nhà thuốc Bệnh Viện Quân Dân YGIA LAI",
   "address": "Đức Thắng, phường 1, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4670693,
   "Latitude": 105.6368545
 },
 {
   "STT": 504,
   "Name": "Nhà thuốc Hoàng Thuận",
   "address": "Đức Thắng, phường 1, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4670693,
   "Latitude": 105.6368545
 },
 {
   "STT": 505,
   "Name": "Quầy thuốc Ngọc Giàu",
   "address": "Dũng, xã Tân Thành A, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8857828,
   "Latitude": 105.5480744
 },
 // {
 //   "STT": 506,
 //   "Name": "Quầy thuốc Mai Quyên",
 //   "address": "Đường 1/5, khóm 2, thị trấn Tràm Chim, huyện Tam Nông, TỈNH GIA LAI",
 //   "Longtitude": 13.8006854,
 //   "Latitude": 109.1474137
 // },
 // {
 //   "STT": 507,
 //   "Name": "Quầy thuốc Dương Đông",
 //   "address": "Đường 1/5, thị trấn Tràm Chim, huyệnTam Nông, TỈNH GIA LAI",
 //   "Longtitude": 13.8006854,
 //   "Latitude": 109.1474137
 // },
 {
   "STT": 508,
   "Name": "Quầy thuốc Anh Hào",
   "address": "Đường 30 tháng 4, khóm Mỹ Tây, thị trấn Mỹ Thọ, huyệnCao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4453693,
   "Latitude": 105.6995299
 },
 {
   "STT": 509,
   "Name": "Quầy thuốc Cao Nguyên",
   "address": "Đường 852B, ấp Bình Hiệp B, xãBình Thạnh Trung, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3803603,
   "Latitude": 105.5591615
 },
 {
   "STT": 510,
   "Name": "Quầy thuốc Minh Trí",
   "address": "Đường 853, ấp Tân Mỹ, xã Tân PhúTrung, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2395342,
   "Latitude": 105.7296531
 },
 {
   "STT": 511,
   "Name": "Quầy thuốc Khánh Ngân",
   "address": "Đường ĐH 64, ấp Bình Hiệp A, xãBình Thạnh Trung, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3702023,
   "Latitude": 105.5236195
 },
 {
   "STT": 512,
   "Name": "Quầy thuốc Trường An",
   "address": "Đường ĐH 64, ấp Bình Hiệp A, xãBình Thạnh Trung, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3702023,
   "Latitude": 105.5236195
 },
 {
   "STT": 513,
   "Name": "Quầy thuốc Hải Vân",
   "address": "Đường ĐH64, ấp Bình Hiệp A, xãBình Thạnh Trung, huyện  Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3702023,
   "Latitude": 105.5236195
 },
 {
   "STT": 514,
   "Name": "Nhà thuốc Gia Huy",
   "address": "Đường ĐT 841 khóm Sở Thượng, phường An Lạc, thị xã Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8262668,
   "Latitude": 105.3647017
 },
 {
   "STT": 515,
   "Name": "Nhà thuốc Trung Tâm Y Tế Huyện Lấp Vò",
   "address": "Đường ĐT852B, ấp Bình Hiệp A, xã Bình Thạnh Trung, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3702023,
   "Latitude": 105.5236195
 },
 {
   "STT": 516,
   "Name": "Quầy thuốc Anh Thư",
   "address": "Đường Gò Tháp, khóm 1, thị trấn Mỹ An, huyệnTháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5243845,
   "Latitude": 105.8494889
 },
 {
   "STT": 517,
   "Name": "Quầy thuốc Thanh Hiệp",
   "address": "Đường Gò Tháp, khóm 3, thị trấn Mỹ An, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5259357,
   "Latitude": 105.8466153
 },
 {
   "STT": 518,
   "Name": "Cơ Sở thuốc Đông Y Thiện Hữu",
   "address": "Đường Hà Huy Tập, khóm 4, thị trấn Mỹ An, huyệnTháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5243845,
   "Latitude": 105.8494889
 },
 {
   "STT": 519,
   "Name": "Quầy thuốc Út Kim Liên",
   "address": "Đường Hùng Vương, khóm 2,thị trấn Mỹ An, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5203588,
   "Latitude": 105.8372589
 },
 {
   "STT": 520,
   "Name": "Quầy thuốc Phan Hiền",
   "address": "Đường Lê Đức Thọ, khóm 1, thị trấn .Mỹ An, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5225428,
   "Latitude": 105.8402536
 },
 {
   "STT": 521,
   "Name": "Nhà thuốc Đức Thiện",
   "address": "Đường Lê Hồng Phong, khóm 2, phường An Thạnh, thị xã Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8094634,
   "Latitude": 105.3474251
 },
 {
   "STT": 522,
   "Name": "Quầy thuốc Bội Bội",
   "address": "Đường Lê Lợi, khóm 3, thị trấn Mỹ An, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5251319,
   "Latitude": 105.8462096
 },
 {
   "STT": 523,
   "Name": "Quầy thuốc 7 Hòa",
   "address": "Đường Lê Lợi, khóm 3, thị trấn Sa Rài, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8783141,
   "Latitude": 105.4619104
 },
 {
   "STT": 524,
   "Name": "Quầy thuốc Trung Tâm Y Tế HuyệnTháp Mười",
   "address": "Đường Nguyễn Văn Cừ, Khóm 4, Thị Trấn Mỹ An, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5216969,
   "Latitude": 105.8409562
 },
 {
   "STT": 525,
   "Name": "Quầy thuốc Khánh Nguyên",
   "address": "Đường Nguyễn Văn Tre, khóm 4, thị trấn Mỹ An, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5190862,
   "Latitude": 105.8450996
 },
 {
   "STT": 526,
   "Name": "Quầy thuốc Phương Ngọc",
   "address": "Đường Phạm Hữu Lầu, khóm Mỹ Thuận, thị trấn MỹThọ, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4659577,
   "Latitude": 105.7059785
 },
 {
   "STT": 527,
   "Name": "Quầy thuốc Phong Hương",
   "address": "Đường số 2, ấp Dinh Bà, xã Tân Hộ Cơ, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.9327262,
   "Latitude": 105.4419546
 },
 {
   "STT": 528,
   "Name": "Quầy thuốc Thoại Hương",
   "address": "Đường Thiên Hộ Dương, khóm 4,thị trấn Mỹ An, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5208816,
   "Latitude": 105.8390084
 },
 {
   "STT": 529,
   "Name": "Quầy thuốc Hồng Trà",
   "address": "Đường Trần Hưng Đạo, khóm 2, thị trấn  Tràm Chim, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.6691946,
   "Latitude": 105.5650239
 },
 {
   "STT": 530,
   "Name": "Quầy thuốc Lê Tuấn",
   "address": "Đường Trần Hưng Đạo, khóm 2, thị trấn  Tràm Chim, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.6691946,
   "Latitude": 105.5650239
 },
 {
   "STT": 531,
   "Name": "Nhà thuốc Thuận Thảo",
   "address": "Đường Trần Hưng Đạo, khóm 4, phường An Thạnh, thị xã Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8088434,
   "Latitude": 105.3437335
 },
 {
   "STT": 532,
   "Name": "Nhà thuốc Quốc Huy",
   "address": "Đường Trần Hưng Đạo, phường An Thạnh, thị xã Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8093392,
   "Latitude": 105.3437549
 },
 {
   "STT": 533,
   "Name": "Nhà thuốc Lan Thanh",
   "address": "Đường Trần Phú, khóm An Lợi, phường An Lộc, thị xã Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8046782,
   "Latitude": 105.353116
 },
 {
   "STT": 534,
   "Name": "Nhà thuốc Tư Thanh",
   "address": "Đường Trần Phú, khóm An Lợi, phường An Lộc, thị xã Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8046782,
   "Latitude": 105.353116
 },
 {
   "STT": 535,
   "Name": "Nhà thuốc Chìa ty",
   "address": "Dương, khóm 2, phường An Thạnh, thị xã Hồng Ngự,",
   "Longtitude": 10.8103952,
   "Latitude": 105.3432131
 },
 {
   "STT": 536,
   "Name": "Cơ Sở thuốc Đông Y Quãng Thái Tường",
   "address": "Hàm Nghi, ấp Bình Lợi, xã BìnhThành, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3409275,
   "Latitude": 105.5000447
 },
 {
   "STT": 537,
   "Name": "Quầy thuốc Diễm Phượng",
   "address": "Hiệp A, xã Bình Thạnh Trung, huyện  Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.380888,
   "Latitude": 105.5587323
 },
 {
   "STT": 538,
   "Name": "Quầy thuốc Thu Hà",
   "address": "Hiệp A, xã Bình Thạnh Trung, huyện Lấp Vò,",
   "Longtitude": 10.3803603,
   "Latitude": 105.5591615
 },
 {
   "STT": 539,
   "Name": "Quầy thuốc Tuấn Huy",
   "address": "Hòa Định, xã Vĩnh Thới, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2405852,
   "Latitude": 105.6353878
 },
 {
   "STT": 540,
   "Name": "Quầy thuốc Xuân Trường",
   "address": "Hòa Định, xã Vĩnh Thới, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2405852,
   "Latitude": 105.6353878
 },
 {
   "STT": 541,
   "Name": "Quầy thuốc Dương Gia Huy",
   "address": "Hòa Lợi, xã Hòa An, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4622676,
   "Latitude": 105.6086621
 },
 {
   "STT": 542,
   "Name": "Quầy thuốc Thanh Tâm 1",
   "address": "Hòa Quới, xã Hòa Tân, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.1715029,
   "Latitude": 105.8172237
 },
 {
   "STT": 543,
   "Name": "Quầy thuốc Bích Trâm",
   "address": "Hòa, xã BìnhThành, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3577289,
   "Latitude": 105.5210607
 },
 {
   "STT": 544,
   "Name": "Quầy thuốc Phương Châm",
   "address": "Hòa, xã BìnhThành, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3577289,
   "Latitude": 105.5210607
 },
 {
   "STT": 545,
   "Name": "Quầy thuốc Cẩm Bình",
   "address": "Hòa, xã Tân Khánh Trung, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3791189,
   "Latitude": 105.6940454
 },
 {
   "STT": 546,
   "Name": "Quầy thuốc Nguyễn Hường",
   "address": "Hòa, xã Tân Khánh Trung, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3791189,
   "Latitude": 105.6940454
 },
 {
   "STT": 547,
   "Name": "Quầy thuốc Phöc Thịnh",
   "address": "Hòa, xã Tân Khánh Trung, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3791189,
   "Latitude": 105.6940454
 },
 {
   "STT": 548,
   "Name": "Quầy thuốc Thöy Hồng",
   "address": "Hòa, xã Tân Khánh Trung, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3791189,
   "Latitude": 105.6940454
 },
 {
   "STT": 549,
   "Name": "Quầy thuốc Trần Thị Thúy",
   "address": "Hòa, xã Tân Khánh Trung, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3791189,
   "Latitude": 105.6940454
 },
 {
   "STT": 550,
   "Name": "Nhà thuốc Hữu Duy",
   "address": "Hồng Gấm, khóm 3, phường AnThạnh, thị xã Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8059302,
   "Latitude": 105.3425283
 },
 {
   "STT": 551,
   "Name": "Quầy thuốc Ngọc Tuyền",
   "address": "Huệ (ngang chợ đầu mối) khómBình Thạnh 1, thị trấn Lấp Vò, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3577289,
   "Latitude": 105.5210607
 },
 {
   "STT": 552,
   "Name": "Quầy thuốc Vĩnh Tường",
   "address": "Huệ, khóm 2, thị trấn Sa Rài, huyện Tân Hồng , TỈNH GIA LAI",
   "Longtitude": 10.8797812,
   "Latitude": 105.450975
 },
 {
   "STT": 553,
   "Name": "Quầy thuốc Võ VănThắng",
   "address": "Hưng Lợi, xã Thanh Mỹ, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.4057199,
   "Latitude": 105.8581524
 },
 {
   "STT": 554,
   "Name": "Quầy thuốc Xuân Thu",
   "address": "Hưng Mỹ Tây, xã Long Hưng A, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3440543,
   "Latitude": 105.6808367
 },
 {
   "STT": 555,
   "Name": "Quầy thuốc Thu Ngân",
   "address": "Hùng Vương, khóm 1, thị trấn Sa Rài, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8878552,
   "Latitude": 105.4599241
 },
 {
   "STT": 556,
   "Name": "Nhà thuốc Hồng Khanh",
   "address": "Hùng Vương, khóm 2, phường 2, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2965443,
   "Latitude": 105.7645953
 },
 {
   "STT": 557,
   "Name": "Quầy thuốc Sơn Phöc",
   "address": "Hùng, xã Tân Thuận Tây, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4599597,
   "Latitude": 105.5868235
 },
 {
   "STT": 558,
   "Name": "Quầy thuốc Cẩm Cuống",
   "address": "Hùng, xã Tân Thuận Tây, thànhphố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4599597,
   "Latitude": 105.5868235
 },
 {
   "STT": 559,
   "Name": "Quầy thuốc Huỳnh Sen Ý",
   "address": "Hưng, xã Vĩnh Thạnh, huyện LấpVò, TỈNH GIA LAI",
   "Longtitude": 10.3357677,
   "Latitude": 105.6189708
 },
 {
   "STT": 560,
   "Name": "Quầy thuốc Khánh My",
   "address": "KDC An Bình 2, ấp An Định, xã An Bình, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4596591,
   "Latitude": 105.6557945
 },
 {
   "STT": 561,
   "Name": "Quầy thuốc Ngọc Trinh",
   "address": "KDC ấp 4, xã Bình Hàng Trung, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4088492,
   "Latitude": 105.7527221
 },
 {
   "STT": 562,
   "Name": "Quầy thuốc Nguyễn Thị Ngọc Trinh",
   "address": "KDC ấp 4, xã Bình Hàng Trung, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4088492,
   "Latitude": 105.7527221
 },
 {
   "STT": 563,
   "Name": "Quầy thuốc Kim Định",
   "address": "KDC ấp An Hòa, xã Định An, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.7436984,
   "Latitude": 105.3892372
 },
 {
   "STT": 564,
   "Name": "Quầy thuốc Song Hòa",
   "address": "KDC ấp An Lợi B, xã Định Yên, huyện  Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.309874,
   "Latitude": 105.5532993
 },
 {
   "STT": 565,
   "Name": "Quầy thuốc Lệ Hoa",
   "address": "KDC ấp Vĩnh Bình A, xã Vĩnh Thạnh, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3447983,
   "Latitude": 105.6177942
 },
 {
   "STT": 566,
   "Name": "Quầy thuốc Bảo Long",
   "address": "Kế số 114/8, ấp Phú Thuận, xã Tân Phú Đông, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.30076,
   "Latitude": 105.765701
 },
 {
   "STT": 567,
   "Name": "Quầy thuốc Vân Khánh",
   "address": "Kế số 213, ấp Khánh Nghĩa, xã Tân Khánh Đông, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.355597,
   "Latitude": 105.7292491
 },
 {
   "STT": 568,
   "Name": "Nhà thuốc Thái Hòa",
   "address": "Kế số 377 Nguyễn Sinh Sắc, khóm 2, phường 2, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2902212,
   "Latitude": 105.755742
 },
 {
   "STT": 569,
   "Name": "Quầy thuốc Quốc Ngọc",
   "address": "Kế số 639A Lê Hồng Phong, ấp Phú Hòa, xã Tân Phú Đông, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.30076,
   "Latitude": 105.765701
 },
 {
   "STT": 570,
   "Name": "Quầy thuốc Vạn An",
   "address": "Kế số 86, quốc lộ 80, ấp Phú Long, xã Tân Phú Đông, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2853117,
   "Latitude": 105.7738892
 },
 {
   "STT": 571,
   "Name": "Quầy thuốc Yến Quỳnh",
   "address": "Khánh, xã Tân Thành, huyện Lai",
   "Longtitude": 10.255455,
   "Latitude": 105.6647142
 },
 {
   "STT": 572,
   "Name": "Quầy thuốc Thùy Trang",
   "address": "Khánh, xã Tân Thành, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.255455,
   "Latitude": 105.6647142
 },
 {
   "STT": 573,
   "Name": "Quầy thuốc Xuân Hương",
   "address": "Khánh, xã Tân Thành, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.255455,
   "Latitude": 105.6647142
 },
 {
   "STT": 574,
   "Name": "Quầy thuốc Kim Phụng",
   "address": "Khóm 1, thị trấn Mỹ An, huyệnTháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5302706,
   "Latitude": 105.8290298
 },
 {
   "STT": 575,
   "Name": "Quầy thuốc Hoàng Chinh",
   "address": "Khóm 1, thị trấn  Sa Rài, huyện Tân Hồng , TỈNH GIA LAI",
   "Longtitude": 10.8797812,
   "Latitude": 105.450975
 },
 {
   "STT": 576,
   "Name": "Quầy thuốc Ngọc Dung",
   "address": "Khóm 1, thị trấn Lai Vung, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2818474,
   "Latitude": 105.6534576
 },
 {
   "STT": 577,
   "Name": "Quầy thuốc Nguyễn Hằng",
   "address": "Khóm 1, thị trấn Lai Vung, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2818474,
   "Latitude": 105.6534576
 },
 {
   "STT": 578,
   "Name": "Quầy thuốc Tống Gia",
   "address": "Khóm 1, thị trấn Lai Vung, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2818474,
   "Latitude": 105.6534576
 },
 {
   "STT": 579,
   "Name": "Quầy thuốc Ngô Nguyễn",
   "address": "Khóm 1, thị trấn Mỹ An, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5302706,
   "Latitude": 105.8290298
 },
 {
   "STT": 580,
   "Name": "Quầy thuốc Như Quỳnh 1",
   "address": "Khóm 1, thị trấn Mỹ An, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5302706,
   "Latitude": 105.8290298
 },
 {
   "STT": 581,
   "Name": "Quầy thuốc Thanh Nga",
   "address": "Khóm 1, thị trấn Mỹ An, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5302706,
   "Latitude": 105.8290298
 },
 {
   "STT": 582,
   "Name": "Quầy thuốc Dương An",
   "address": "Khóm 1, thị trấn Sa Rài, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8797812,
   "Latitude": 105.450975
 },
 {
   "STT": 583,
   "Name": "Quầy thuốc Hải Đăng",
   "address": "Khóm 1, thị trấn Sa Rài, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8797812,
   "Latitude": 105.450975
 },
 {
   "STT": 584,
   "Name": "Quầy thuốc Hoàng Phúc",
   "address": "Khóm 1, thị trấn Sa Rài, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8797812,
   "Latitude": 105.450975
 },
 {
   "STT": 585,
   "Name": "Quầy thuốc Kim Thu",
   "address": "Khóm 1, thị trấn Sa Rài, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8797812,
   "Latitude": 105.450975
 },
 {
   "STT": 586,
   "Name": "Quầy thuốc Ngọc Linh 3",
   "address": "Khóm 1, thị trấn Sa Rài, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8797812,
   "Latitude": 105.450975
 },
 {
   "STT": 587,
   "Name": "Quầy thuốc Thùy Dương",
   "address": "khóm 1, thị trấn Sa Rài, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8797812,
   "Latitude": 105.450975
 },
 {
   "STT": 588,
   "Name": "Quầy thuốc Dương Phát",
   "address": "Khóm 2 (thửa đất số 140, TBĐ số 60), thị trấn Tràm Chim, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.6691946,
   "Latitude": 105.5650239
 },
 {
   "STT": 589,
   "Name": "Quầy thuốc Bệnh Viện Đkkv ThápMười",
   "address": "Khóm 2, thị trấn Mỹ An, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5208515,
   "Latitude": 105.8358479
 },
 {
   "STT": 590,
   "Name": "Quầy thuốc Ba Thiện",
   "address": "Khóm 2, thị trấn Sa Rài, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.879998,
   "Latitude": 105.451251
 },
 {
   "STT": 591,
   "Name": "Quầy thuốc Tố Anh",
   "address": "Khóm 3, thị trấn Mỹ An, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5302706,
   "Latitude": 105.8784683
 },
 {
   "STT": 592,
   "Name": "Quầy thuốc Hồng Phương",
   "address": "Khóm 3, thị trấn Sa Rài, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8741269,
   "Latitude": 105.4624601
 },
 {
   "STT": 593,
   "Name": "Quầy thuốc Mai Thanh Duy",
   "address": "Khóm 3, thị trấn Sa Rài, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8741269,
   "Latitude": 105.4624601
 },
 {
   "STT": 594,
   "Name": "Quầy thuốc Ngọc Ánh",
   "address": "khóm 3, thị trấn Sa Rài, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8741269,
   "Latitude": 105.4624601
 },
 {
   "STT": 595,
   "Name": "Quầy thuốc Yến Uyên",
   "address": "Khóm 3, thị trấn Sa Rài, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8741269,
   "Latitude": 105.4624601
 },
 {
   "STT": 596,
   "Name": "Quầy thuốc Ngân Tuyết",
   "address": "Khóm 4, thị trấn Mỹ An, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5116754,
   "Latitude": 105.8522149
 },
 {
   "STT": 597,
   "Name": "Quầy thuốc Ngọc Khoa",
   "address": "Khóm 4, thị trấn Mỹ An, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5116754,
   "Latitude": 105.8522149
 },
 {
   "STT": 598,
   "Name": "Quầy thuốc Nhật Anh",
   "address": "khóm 4, thị trấn Mỹ An, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5116754,
   "Latitude": 105.8522149
 },
 {
   "STT": 599,
   "Name": "Quầy thuốc Thái Liên",
   "address": "Khóm 4, thị trấn Mỹ An, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5116754,
   "Latitude": 105.8522149
 },
 {
   "STT": 600,
   "Name": "Quầy thuốc Trung Tâm Y Tế Huyện Tam Nông",
   "address": "Khóm 4, thị trấn Tràm Chim, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.719562,
   "Latitude": 105.526428
 },
 {
   "STT": 601,
   "Name": "Quầy thuốc Thảo Nguyên",
   "address": "Khóm 5 (nhà bà Nguyễn Thị Thà), thị trấn Tràm Chim, huyện Tam Nông, TỈNH GIA LAI)",
   "Longtitude": 10.6691946,
   "Latitude": 105.5650239
 },
 {
   "STT": 602,
   "Name": "Quầy thuốc Hải Hà",
   "address": "Khóm 5, thị trấn Tràm Chim, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.6691946,
   "Latitude": 105.5650239
 },
 {
   "STT": 603,
   "Name": "Nhà thuốc Khải Hoàn",
   "address": "Khóm An Lợi, phường An Lộc, thị xã Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.7893213,
   "Latitude": 105.3476456
 },
 {
   "STT": 604,
   "Name": "Quầy thuốc Bình An 1",
   "address": "Khóm Bình Thạnh 1, thị trấn Lấp Vò, huyện  Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3607896,
   "Latitude": 105.518867
 },
 {
   "STT": 605,
   "Name": "Quầy thuốc Kim Hiền",
   "address": "Khóm Bình Thạnh 1, thị trấn Lấp Vò, huyện  Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3607896,
   "Latitude": 105.518867
 },
 {
   "STT": 606,
   "Name": "Cơ Sở thuốc Đông Y Phương Mai",
   "address": "Khóm Cây Da, phường An Lạc, thị xã Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8342679,
   "Latitude": 105.3306814
 },
 {
   "STT": 607,
   "Name": "Quầy thuốc Vân Anh",
   "address": "Khóm Mỹ Tây, thị trấn Mỹ Thọ, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.441625,
   "Latitude": 105.7019615
 },
 {
   "STT": 608,
   "Name": "Quầy thuốc Hồng Ngọc",
   "address": "Khóm Mỹ Thuận, thị trấn Mỹ Thọ, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4471594,
   "Latitude": 105.6928784
 },
 {
   "STT": 609,
   "Name": "Quầy thuốc Bệnh Viện Đa Khoa HuyệnChâu Thành",
   "address": "Khóm Phú Bình, thị trấn Cái Tàu Hạ, huyện  Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2523351,
   "Latitude": 105.864298
 },
 {
   "STT": 610,
   "Name": "Quầy thuốc Minh Hữu",
   "address": "Khóm Phú Hòa, thị trấn Cái Tàu Hạ, huyện  Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2577173,
   "Latitude": 105.8730678
 },
 {
   "STT": 611,
   "Name": "Quầy thuốc Vĩnh Khang",
   "address": "Khóm Phú Hòa, thị trấn Cái Tàu Hạ, huyện  Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2577173,
   "Latitude": 105.8730678
 },
 {
   "STT": 612,
   "Name": "Cơ Sở thuốc Đông Y Vạn Phát Đường Ii",
   "address": "Khóm Phú Mỹ Hiệp, thị trấn Cái Tàu Hạ, huyện ChâuThành, TỈNH GIA LAI",
   "Longtitude": 10.257936,
   "Latitude": 105.870189
 },
 {
   "STT": 613,
   "Name": "Quầy thuốc Hạnh Đào",
   "address": "Khóm Phú Mỹ Lương, thị trấn Cái Tàu Hạ, huyệnChâu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2583054,
   "Latitude": 105.8729053
 },
 {
   "STT": 614,
   "Name": "Quầy thuốc Lê Kim Anh",
   "address": "Khóm Phú Mỹ, thị trấn Cái Tàu Hạ, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2588575,
   "Latitude": 105.8725314
 },
 {
   "STT": 615,
   "Name": "Nhà thuốc Bạch Huệ",
   "address": "khóm Sở Thượng, phường An Lạc, thị xã Hồng Ngự,",
   "Longtitude": 10.8120978,
   "Latitude": 105.3359403
 },
 {
   "STT": 616,
   "Name": "Quầy thuốc Y Dược",
   "address": "khóm Tân Đông B, thị trấn Thanh Bình, huyện Thanh Bình,",
   "Longtitude": 10.5570358,
   "Latitude": 105.4886358
 },
 {
   "STT": 617,
   "Name": "Quầy thuốc Nhật Lam",
   "address": "Khóm Tân Đông B, thị trấn Thanh Bình, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5570633,
   "Latitude": 105.4921657
 },
 {
   "STT": 618,
   "Name": "Quầy thuốc Sĩ Hiền",
   "address": "Khóm Tân Đông B, thị trấn Thanh Bình, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5570633,
   "Latitude": 105.4921657
 },
 {
   "STT": 619,
   "Name": "Quầy thuốc Khỏe",
   "address": "Khóm Tân Mỹ, xã Tân Phú Trung, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2335223,
   "Latitude": 105.7606399
 },
 {
   "STT": 620,
   "Name": "Quầy thuốc Gia Mỹ",
   "address": "Khóm Tân Thuận, thị trấn Thanh Bình, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5554601,
   "Latitude": 105.491758
 },
 {
   "STT": 621,
   "Name": "Quầy thuốc Thùy Oanh",
   "address": "Khu chợ đêm, đường Lê Quý Đôn, khóm 1, thị trấn Mỹ An, huyệnTháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5213664,
   "Latitude": 105.847549
 },
 {
   "STT": 622,
   "Name": "Quầy thuốc Hồng Anh",
   "address": "Khu dân cư ấp 6B, xã Trường Xuân, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.648797,
   "Latitude": 105.787514
 },
 {
   "STT": 623,
   "Name": "Quầy thuốc Gia Hân",
   "address": "Khu đô thị Ngã ba Phong Hòa, xã Phong Hòa, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.1706664,
   "Latitude": 105.673473
 },
 {
   "STT": 624,
   "Name": "Quầy thuốc Mai Phương",
   "address": "Khu vực 1, ấp Thượng, xãThường Thới Tiền, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8208572,
   "Latitude": 105.2814555
 },
 {
   "STT": 625,
   "Name": "Quầy thuốc Thu Trang",
   "address": "Khu vực 1, ấp Thượng, xãThường Thới Tiền, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8208572,
   "Latitude": 105.2814555
 },
 {
   "STT": 626,
   "Name": "Quầy thuốc Quốc Thịnh",
   "address": "Khu vực chợ Hưng Thạnh, xã HưngThạnh, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.6568692,
   "Latitude": 105.6999122
 },
 {
   "STT": 627,
   "Name": "Cơ Sở thuốc Đông Y Khang Thịnh",
   "address": "Khương, xã Định Yên, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.309874,
   "Latitude": 105.5532993
 },
 {
   "STT": 628,
   "Name": "Quầy thuốc Khang Thịnh",
   "address": "Khương, xã Định Yên, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.309874,
   "Latitude": 105.5532993
 },
 {
   "STT": 629,
   "Name": "Quầy thuốc Thành Lợi",
   "address": "Kios 01, ấp Hạ, chợ Tân Bình, xã Tân Bình, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.6159048,
   "Latitude": 105.4003859
 },
 {
   "STT": 630,
   "Name": "Quầy thuốc Hòa Bình",
   "address": "Kios 03, khóm Tân Đông B, thị trấn Thanh Bình, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5554601,
   "Latitude": 105.491758
 },
 {
   "STT": 631,
   "Name": "Quầy thuốc Thanh Nga",
   "address": "Kios 12B, chợ Cái Tàu Hạ, huyệnChâu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2594423,
   "Latitude": 105.8703997
 },
 {
   "STT": 632,
   "Name": "Quầy thuốc Dung",
   "address": "Kios 13 chợ Tân Nghĩa, ấp 2, xã Tân Nghĩa, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.5321187,
   "Latitude": 105.6120363
 },
 {
   "STT": 633,
   "Name": "Quầy thuốc Văn Trung",
   "address": "Kios 3 chợ Thường Phước 1, ấp 1, xãThường Phước 1, huyện  Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.9056064,
   "Latitude": 105.2012587
 },
 {
   "STT": 634,
   "Name": "Quầy thuốc Ngọc Sánh",
   "address": "Kios 32 chợ Xẻo Quýt, ấp 3, xã Mỹ Long, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.3978506,
   "Latitude": 105.8046936
 },
 {
   "STT": 635,
   "Name": "Quầy thuốc Ngọc Tuyền",
   "address": "Kios 35, chợ Tân Nghĩa, xã Tân Nghĩa, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.5321187,
   "Latitude": 105.6120363
 },
 {
   "STT": 636,
   "Name": "Quầy thuốc Hữu Tiến",
   "address": "Kios chợ Long Thắng, ấp Hòa Ninh, xã LongThắng, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2539557,
   "Latitude": 105.6928539
 },
 {
   "STT": 637,
   "Name": "Quầy thuốc Mỹ Châu",
   "address": "Kios chợ Tân Dương, ấp Tân Lộc A, xã Tân Dương, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.3281344,
   "Latitude": 105.7033199
 },
 {
   "STT": 638,
   "Name": "Cơ Sở thuốc Đông Y Phước Mãn Đường",
   "address": "Kios chợ Thới Hòa, ấp Thới Hòa, xã Vĩnh Thới, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2342341,
   "Latitude": 105.6261994
 },
 {
   "STT": 639,
   "Name": "Quầy thuốc Thùy Trang",
   "address": "Kios số 01 - 02 lô B, chợ Thầy Lâm, ấp An Phú, xã Mỹ An Hưng B, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3742694,
   "Latitude": 105.6442522
 },
 {
   "STT": 640,
   "Name": "Quầy thuốc Tài Lộc 1",
   "address": "Kios số 01 chợ Cai Châu, ấp TânTrong, xã Tân Mỹ, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.4049865,
   "Latitude": 105.6604636
 },
 {
   "STT": 641,
   "Name": "Quầy thuốc Nguyễn Kim Thúy",
   "address": "Kios số 01 chợ xã Nhị Mỹ, ấp Nguyễn Cữ, xã Nhị Mỹ, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4880535,
   "Latitude": 105.6940454
 },
 {
   "STT": 642,
   "Name": "Quầy thuốc Thu Sương",
   "address": "Kios số 01, ấp 3, xã Bình Tấn, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.6191437,
   "Latitude": 105.5826123
 },
 {
   "STT": 643,
   "Name": "Quầy thuốc Hạnh Nhi",
   "address": "Kios số 01, lô B, xã Thanh Mỹ, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.4098655,
   "Latitude": 105.8407722
 },
 {
   "STT": 644,
   "Name": "Quầy thuốc Huỳnh Cung",
   "address": "Kios số 08, chợ Đốc Binh Kiều, xã Đốc Binh Kiều, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5129025,
   "Latitude": 105.9243834
 },
 {
   "STT": 645,
   "Name": "Quầy thuốc Thanh Tuyến",
   "address": "Kios số 1 chợ Ngã Năm, ấp Long An, xã Long Thắng, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2231147,
   "Latitude": 105.6761185
 },
 {
   "STT": 646,
   "Name": "Quầy thuốc Huệ Long",
   "address": "Kios số 1, ấp 1, xã Tân Hội Trung, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4569711,
   "Latitude": 105.7703287
 },
 {
   "STT": 647,
   "Name": "Quầy thuốc Mỹ Châu 2",
   "address": "Kios số 1, ấp Hậu Thành, xã Tân Dương, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.335001,
   "Latitude": 105.7057792
 },
 {
   "STT": 648,
   "Name": "Nhà thuốc Nga Sơn",
   "address": "Kios số 1, chợ Nàng Hai, khóm Tân Hòa, phường An Hòa, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.314286,
   "Latitude": 105.7457685
 },
 {
   "STT": 649,
   "Name": "Quầy thuốc Hướng Dương",
   "address": "Kios số 1, khóm Tân Đông B, thị trấn Thanh Bình, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5554601,
   "Latitude": 105.491758
 },
 {
   "STT": 650,
   "Name": "Quầy thuốc Kim Em",
   "address": "Kios số 10 chợ Cái Tàu Hạ, thị trấn Cái Tàu Hạ, huyệnChâu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2594423,
   "Latitude": 105.8703997
 },
 {
   "STT": 651,
   "Name": "Quầy thuốc Nhật Anh",
   "address": "Kios số 10, dãy A, chợ Tân Phú, huyện  Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2131397,
   "Latitude": 105.7588391
 },
 {
   "STT": 652,
   "Name": "Quầy thuốc Nguyễn Thị Chính",
   "address": "Kios số 11 và 12 dãy D chợ Phú Long, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2306479,
   "Latitude": 105.8044244
 },
 {
   "STT": 653,
   "Name": "Quầy thuốc Thöy Quỳnh",
   "address": "Kios số 12, ấp Hưng Lợi, xãThanh Mỹ, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.4098655,
   "Latitude": 105.8407722
 },
 // {
 //   "STT": 654,
 //   "Name": "Quầy thuốc Hoàng Vũ",
 //   "address": "Kios số 1-4, ấp Bình Chánh, xãBình Thành, huyện Thanh Bình, TỈNH GIA LAI",
 //   "Longtitude": 10.687392,
 //   "Latitude": 106.5938538
 // },
 {
   "STT": 655,
   "Name": "Quầy thuốc Bích Thủy",
   "address": "Kios số 3, ấp 4, xã Phong Mỹ, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.5318166,
   "Latitude": 105.5826123
 },
 {
   "STT": 656,
   "Name": "Quầy thuốc Bảo Anh",
   "address": "Kios số 31, chợ Đốc Binh Kiều, xã Đốc Binh Kiều, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5129025,
   "Latitude": 105.9243834
 },
 {
   "STT": 657,
   "Name": "Quầy thuốc Phương Tín",
   "address": "Kios số 4, ấp Thới Hòa, xã Vĩnh Thới, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2405852,
   "Latitude": 105.6353878
 },
 {
   "STT": 658,
   "Name": "Quầy thuốc Kim Cúc",
   "address": "Kios số 45, ấp 2, xã Mỹ Hiệp, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4059017,
   "Latitude": 105.8114175
 },
 {
   "STT": 659,
   "Name": "Quầy thuốc Trung Vĩnh",
   "address": "Kios số 7 chợ Gáo Giồng, xã Gáo Giồng, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.6055423,
   "Latitude": 105.635172
 },
 {
   "STT": 660,
   "Name": "Cơ Sở thuốc Đông Y Phước ThiệnĐường",
   "address": "Kios số 9, ấp Mỹ Tây 2, xã Mỹ Quý, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5453505,
   "Latitude": 105.7468535
 },
 {
   "STT": 661,
   "Name": "Quầy thuốc Ngọc Lan",
   "address": "Kiot 12, Chợ đầu mối trái cây, xã Mỹ Hiệp, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.3313748,
   "Latitude": 105.8092886
 },
 {
   "STT": 662,
   "Name": "Quầy thuốc Nguyễn Thành Nam",
   "address": "Kiot lô 13Aa, chợ Nha Mân, ấp Tân Lập, xã Tân Nhuận Đông, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2698298,
   "Latitude": 105.8291222
 },
 {
   "STT": 663,
   "Name": "Quầy thuốc Mỹ Tiên",
   "address": "Lâm, ấp An Phú, xã Mỹ An Hưng B, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.4050821,
   "Latitude": 105.6043964
 },
 {
   "STT": 664,
   "Name": "Quầy thuốc Kiều Ngân",
   "address": "lập, ấp An Phong, xã Định An, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3057867,
   "Latitude": 105.5388294
 },
 {
   "STT": 665,
   "Name": "Quầy thuốc Tuyền Lâm",
   "address": "Lô 27A, chợ Nha Mân, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2698298,
   "Latitude": 105.8291222
 },
 {
   "STT": 666,
   "Name": "Quầy thuốc Cẩm Linh",
   "address": "Lô B, ấp Tân Hòa, xã Tân Bình, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2553925,
   "Latitude": 105.776198
 },
 {
   "STT": 667,
   "Name": "Quầy thuốc Võ Trí Thông",
   "address": "Lô B, cụm dân cư xã Tân Phú, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2130899,
   "Latitude": 105.7527221
 },
 {
   "STT": 668,
   "Name": "Quầy thuốc Mộng Thu",
   "address": "Lô H, CDCthị trấn  chợ Bình Thành, ấp Bình Chánh, xã Bình Thành, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5349965,
   "Latitude": 105.5411161
 },
 {
   "STT": 669,
   "Name": "Quầy thuốc Kim Xoàn",
   "address": "Lô K, CDC thị trấn Bình Thành, xã Bình Thành, huyệnThanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5766484,
   "Latitude": 105.5591615
 },
 {
   "STT": 670,
   "Name": "Quầy thuốc Ngọc Trang",
   "address": "Lô K01, ấp Phú Thành, xã Tân Phú Đông, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.30076,
   "Latitude": 105.765701
 },
 {
   "STT": 671,
   "Name": "Quầy thuốc Thiên Thanh",
   "address": "Lợi A, xã Định Yên, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.309874,
   "Latitude": 105.5532993
 },
 {
   "STT": 672,
   "Name": "Cơ Sở thuốc Đông Y Thái Hưng",
   "address": "Lợi B, xã Định Yên, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.309874,
   "Latitude": 105.5532993
 },
 {
   "STT": 673,
   "Name": "Quầy thuốc Mai Nương",
   "address": "Lợi B, xã Định Yên, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.309874,
   "Latitude": 105.5532993
 },
 {
   "STT": 674,
   "Name": "Quầy thuốc Nguyễn ThịThoa",
   "address": "Lợi, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.7005628,
   "Latitude": 105.6999122
 },
 {
   "STT": 675,
   "Name": "Quầy thuốc Ngọc Hạnh",
   "address": "Lợi, xã BìnhThạnh, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.3200374,
   "Latitude": 105.7879371
 },
 {
   "STT": 676,
   "Name": "Quầy thuốc Ngọc Yến",
   "address": "Lợi, xã BìnhThạnh, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.3200374,
   "Latitude": 105.7879371
 },
 {
   "STT": 677,
   "Name": "Quầy thuốc Kim Bích",
   "address": "Lợi, xã BìnhThành, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3413789,
   "Latitude": 105.5767493
 },
 {
   "STT": 678,
   "Name": "Quầy thuốc Quỳnh Anh",
   "address": "Lợi, xã BìnhThành, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3413789,
   "Latitude": 105.5767493
 },
 {
   "STT": 679,
   "Name": "Quầy thuốc Tùng Bách",
   "address": "Lợi, xã BìnhThành, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3413789,
   "Latitude": 105.5767493
 },
 {
   "STT": 680,
   "Name": "Quầy thuốc Thảo Dư",
   "address": "Long, xã Tịnh Thới, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4364387,
   "Latitude": 105.664511
 },
 {
   "STT": 681,
   "Name": "Nhà thuốc Thái Thy",
   "address": "Lý Thường Kiệt, khóm 4, phường 1, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2929464,
   "Latitude": 105.7666716
 },
 {
   "STT": 682,
   "Name": "Nhà thuốc Ngọc Sang",
   "address": "Minh Khai, khóm 4, phường AnThạnh, thị xã Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8077564,
   "Latitude": 105.3471816
 },
 {
   "STT": 683,
   "Name": "Quầy thuốc Bích Ngọc",
   "address": "Mỹ 1, xã Vĩnh Thới, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2446333,
   "Latitude": 105.6412267
 },
 {
   "STT": 684,
   "Name": "Quầy thuốc Tố Nhung",
   "address": "Mỹ 1, xã Vĩnh Thới, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2446333,
   "Latitude": 105.6412267
 },
 {
   "STT": 685,
   "Name": "Quầy thuốc Thanh Tuấn",
   "address": "Mỹ Đông, xã Long Hưng A, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3440543,
   "Latitude": 105.6808367
 },
 {
   "STT": 686,
   "Name": "Quầy thuốc Gia Phúc",
   "address": "Mỹ Tây, xã Long Hưng A, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3444058,
   "Latitude": 105.6807179
 },
 {
   "STT": 687,
   "Name": "Quầy thuốc Nguyễn Quang Huy",
   "address": "Mỹ Tây, xã Long Hưng A, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3444058,
   "Latitude": 105.6807179
 },
 {
   "STT": 688,
   "Name": "Quầy thuốc Xinh Phong",
   "address": "Mỹ Tây, xã Long Hưng A, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3444058,
   "Latitude": 105.6807179
 },
 {
   "STT": 689,
   "Name": "Quầy thuốc Kim Anh",
   "address": "Mỹ, thị trấn Cái Tàu Hạ, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.257936,
   "Latitude": 105.870189
 },
 {
   "STT": 690,
   "Name": "Quầy thuốc Trí Nhân",
   "address": "Nền nhà số 02, TDC phía Đông tỉnh lộ 855, ấp 4, xã Hòa Bình, huyện  Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.7471804,
   "Latitude": 105.6232011
 },
 {
   "STT": 691,
   "Name": "Quầy thuốc Minh Triết",
   "address": "Nền số 4, lô 3, đường Lê Quý Đôn, khóm 1, thị trấn Mỹ An, huyệnTháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.525278,
   "Latitude": 105.8470868
 },
 {
   "STT": 692,
   "Name": "Quầy thuốc Xuân Khoa",
   "address": "Nghiệp, xã An Bình, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4528822,
   "Latitude": 105.6624846
 },
 {
   "STT": 693,
   "Name": "Nhà thuốc Ngọc Minh",
   "address": "Nguyễn Huệ, phường An Thạnh, thị xã Hồng Ngự,",
   "Longtitude": 10.8129342,
   "Latitude": 105.3493004
 },
 {
   "STT": 694,
   "Name": "Nhà thuốc Tâm Phúc",
   "address": "Nguyễn Sinh Sắc, khóm Hòa Khánh, phường 2, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2901261,
   "Latitude": 105.7517453
 },
 {
   "STT": 695,
   "Name": "Nhà thuốc Nguyễn Ngọc Gia Hân",
   "address": "Nguyễn Thái Học, phường 4, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4520535,
   "Latitude": 105.6336139
 },
 {
   "STT": 696,
   "Name": "Nhà thuốc Khải Hoàng",
   "address": "Nguyễn Thái Học, phường HòaThuận, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4607609,
   "Latitude": 105.6291315
 },
 {
   "STT": 697,
   "Name": "Quầy thuốc Thiên Hương",
   "address": "Nhơn, xã Long Hưng B, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3264445,
   "Latitude": 105.6588485
 },
 {
   "STT": 698,
   "Name": "Quầy thuốc Trung Hiếu",
   "address": "Nhuận Đông, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2422003,
   "Latitude": 105.8231588
 },
 {
   "STT": 699,
   "Name": "Quầy thuốc An Bình",
   "address": "Ninh, xã An Khánh, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.1970145,
   "Latitude": 105.8583873
 },
 {
   "STT": 700,
   "Name": "Quầy thuốc Đăng Khoa",
   "address": "Ninh, xã Mỹ An Hưng A, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.4067257,
   "Latitude": 105.5767493
 },
 {
   "STT": 701,
   "Name": "Quầy thuốc Gia Thịnh",
   "address": "Ninh, xã Mỹ An Hưng A, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.4067257,
   "Latitude": 105.5767493
 },
 {
   "STT": 702,
   "Name": "Quầy thuốc Lệ Huyền",
   "address": "Ninh, xã Mỹ An Hưng A, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.4067257,
   "Latitude": 105.5767493
 },
 {
   "STT": 703,
   "Name": "Quầy thuốc Mai Ly",
   "address": "Ninh, xã Mỹ An Hưng A, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.4067257,
   "Latitude": 105.5767493
 },
 {
   "STT": 704,
   "Name": "Quầy thuốc Trúc Mai",
   "address": "Ninh, xã Mỹ An Hưng A, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.4067257,
   "Latitude": 105.5767493
 },
 {
   "STT": 705,
   "Name": "Quầy thuốc Trí Quang",
   "address": "Phà Vàm Cống mới, ấp An Thạnh, xã Bình Thành, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3383672,
   "Latitude": 105.4973904
 },
 {
   "STT": 706,
   "Name": "Nhà thuốc Thiên Vạn Lợi",
   "address": "Phạm Hữu Lầu, phường 6, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4304989,
   "Latitude": 105.6338385
 },
 {
   "STT": 707,
   "Name": "Quầy thuốc Võ Ngọc Thanh",
   "address": "Phạm Hữu Lầu, thị trấn Mỹ Thọ, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4449416,
   "Latitude": 105.6962628
 },
 {
   "STT": 708,
   "Name": "Quầy thuốc Nguyên Dũng",
   "address": "Phong Hòa, ấp Tân Lợi, xã Phong Hòa, huyện Lai Vung,",
   "Longtitude": 10.1833808,
   "Latitude": 105.6940454
 },
 {
   "STT": 709,
   "Name": "Quầy thuốc Minh Ngọc",
   "address": "Phong, xã Phong Hòa, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.1589532,
   "Latitude": 105.6640202
 },
 {
   "STT": 710,
   "Name": "Quầy thuốc Ba Phát",
   "address": "Phong, xã Tân Huề, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.6136235,
   "Latitude": 105.3658125
 },
 {
   "STT": 711,
   "Name": "Quầy thuốc Hoàng Tường",
   "address": "Phong, xã Tân Huề, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.6136235,
   "Latitude": 105.3658125
 },
 {
   "STT": 712,
   "Name": "Quầy thuốc Huy Vũ",
   "address": "Phong, xã Tân Huề, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.6136235,
   "Latitude": 105.3658125
 },
 {
   "STT": 713,
   "Name": "Quầy thuốc Trung Cang",
   "address": "Phong, xã Tân Huề, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.6136235,
   "Latitude": 105.3658125
 },
 {
   "STT": 714,
   "Name": "Quầy thuốc Thùy Trang",
   "address": "Phú A, xã TânBình, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.668875,
   "Latitude": 105.3541013
 },
 {
   "STT": 715,
   "Name": "Quầy thuốc Xuân Nguyệt",
   "address": "Phú Hòa, thị trấn Cái Tàu Hạ, huyệnChâu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2591109,
   "Latitude": 105.8728103
 },
 {
   "STT": 716,
   "Name": "Quầy thuốc Minh Trang",
   "address": "Phú Long, ấp Phú Thành, xã Tân Phú Đông, thành phố Sa Đéc,",
   "Longtitude": 10.2782937,
   "Latitude": 105.7527221
 },
 {
   "STT": 717,
   "Name": "Quầy thuốc Hảo Lê",
   "address": "Phú Quới, xã Bình Thành, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3559181,
   "Latitude": 105.5454646
 },
 {
   "STT": 718,
   "Name": "Nhà thuốc Mỹ Lệ",
   "address": "Phủ, khóm 4, phường An Thạnh,",
   "Longtitude": 10.8114054,
   "Latitude": 105.3465217
 },
 {
   "STT": 719,
   "Name": "Quầy thuốc An Phú",
   "address": "Phú, xã Mỹ An Hưng B, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3831971,
   "Latitude": 105.6119301
 },
 {
   "STT": 720,
   "Name": "Quầy thuốc Hoa Mười",
   "address": "Phước, ấp An Thọ, xã An Phước, huyện Tân Hồng,",
   "Longtitude": 10.804624,
   "Latitude": 105.4829681
 },
 {
   "STT": 721,
   "Name": "Quầy thuốc Y Dược",
   "address": "Quãng Khánh, xã Mỹ Trà, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4899262,
   "Latitude": 105.6385471
 },
 {
   "STT": 722,
   "Name": "Quầy thuốc Nguyễn Thành Nam",
   "address": "Quầy số 13, lô Aa13, chợ Nha Mân, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2698298,
   "Latitude": 105.8291222
 },
 {
   "STT": 723,
   "Name": "Quầy thuốc Trí Thanh",
   "address": "Quầy số 2, chợ Long Hồi, xã Tịnh Thới, thành phố CaoLãnh, TỈNH GIA LAI",
   "Longtitude": 10.416122,
   "Latitude": 105.6710842
 },
 {
   "STT": 724,
   "Name": "Quầy thuốc Kiều Nga",
   "address": "Quầy số 5, chợ Tịnh Thới, xã TịnhThới, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4238487,
   "Latitude": 105.6705801
 },
 {
   "STT": 725,
   "Name": "Quầy thuốc Trí Nghị",
   "address": "Quầy số 65, chợ Bà Học, xã Mỹ Tân,thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4837745,
   "Latitude": 105.61987
 },
 {
   "STT": 726,
   "Name": "Nhà thuốc Ngọc Đức",
   "address": "Quầy số 80-81 chợ Mỹ Phú, phường Mỹ Phú, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.475252,
   "Latitude": 105.6463528
 },
 {
   "STT": 727,
   "Name": "Quầy thuốc Nguyễn Phượng",
   "address": "Quầy số 9 chợ Thống Linh, ấp 3, xã Phương Trà, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.5302472,
   "Latitude": 105.6461097
 },
 {
   "STT": 728,
   "Name": "Quầy thuốc Sinh Phúc",
   "address": "Quầy số 9-10, nhà lồng chợ Phú Long, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2306479,
   "Latitude": 105.8044244
 },
 {
   "STT": 729,
   "Name": "Quầy thuốc Mỹ Duyên",
   "address": "Quốc lộ 30, ấp 3, xã Phong Mỹ, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.509589,
   "Latitude": 105.562924
 },
 {
   "STT": 730,
   "Name": "Quầy thuốc Khánh Ny",
   "address": "Quốc lộ 30, ấp An Lợi, xã An Bình A, thị xã Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.7765871,
   "Latitude": 105.3554589
 },
 {
   "STT": 731,
   "Name": "Quầy thuốc Quế Anh",
   "address": "Quốc lộ 30, chợ đầu mối trái cây, ấp 2, xã Mỹ Hiệp, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.3308156,
   "Latitude": 105.8103168
 },
 {
   "STT": 732,
   "Name": "Quầy thuốc Tấn Yến",
   "address": "Quốc lộ 30, khóm Tân Đông B, thị trấn Thanh Bình, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.557999,
   "Latitude": 105.490358
 },
 {
   "STT": 733,
   "Name": "Quầy thuốc Ngọc Nhung",
   "address": "Quốc lộ 30, tổ 22, ấp An Hòa, xã An Bình A, thị xã Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.7747892,
   "Latitude": 105.3557572
 },
 {
   "STT": 734,
   "Name": "Quầy thuốc Thiên Phú",
   "address": "Quốc lộ 30, xã An Bình A, thị xã Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.7747892,
   "Latitude": 105.3557572
 },
 {
   "STT": 735,
   "Name": "Quầy thuốc Thu Vân",
   "address": "Quốc lộ 54, ấp Hòa Khánh, xã Vĩnh Thới, huyện LaiVung, TỈNH GIA LAI",
   "Longtitude": 10.2444808,
   "Latitude": 105.607772
 },
 {
   "STT": 736,
   "Name": "Quầy thuốc Nhân Hậu",
   "address": "Quốc lộ 54, ấp Tân Lợi, xã Phong Hòa, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.1723499,
   "Latitude": 105.6694373
 },
 {
   "STT": 737,
   "Name": "Quầy thuốc Thanh Thư",
   "address": "Quốc lộ 54, ấp Tân Lợi, xã Tân Thành, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.1723499,
   "Latitude": 105.6694373
 },
 {
   "STT": 738,
   "Name": "Quầy thuốc Phúc An",
   "address": "Quốc lộ 54, ấp Tân Phong, xã Phong Hòa, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.1723499,
   "Latitude": 105.6694373
 },
 {
   "STT": 739,
   "Name": "Quầy thuốc Lộc Phöc",
   "address": "Quốc lộ 80, khóm 1, thị trấn Lai Vung, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2903253,
   "Latitude": 105.6585429
 },
 {
   "STT": 740,
   "Name": "Quầy thuốc Trung Tâm Y Tế Huyện Lai Vung",
   "address": "Quốc lộ 80, khóm 1, thị trấn Lai Vung, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2903253,
   "Latitude": 105.6585429
 },
 {
   "STT": 741,
   "Name": "Quầy thuốc Thanh Tâm",
   "address": "Quốc lộ 80, khóm Phú Mỹ, thị trấn Cái Tàu Hạ, huyệnChâu Thành, TỈNH GIA LAI",
   "Longtitude": 10.257936,
   "Latitude": 105.870189
 },
 {
   "STT": 742,
   "Name": "Quầy thuốc Thanh Thủy",
   "address": "Quới 1, xã Long Hưng A, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3527801,
   "Latitude": 105.6764461
 },
 {
   "STT": 743,
   "Name": "Quầy thuốc Ngọc Lan",
   "address": "Quới, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.6567686,
   "Latitude": 105.3775245
 },
 {
   "STT": 744,
   "Name": "Quầy thuốc Lê Trân",
   "address": "Quới, xã Vĩnh Thạnh, huyện LấpVò, TỈNH GIA LAI",
   "Longtitude": 10.3447983,
   "Latitude": 105.6177942
 },
 {
   "STT": 745,
   "Name": "Quầy thuốc Xuân Hoa",
   "address": "Quới, xã Vĩnh Thạnh, huyện LấpVò, TỈNH GIA LAI",
   "Longtitude": 10.3447983,
   "Latitude": 105.6177942
 },
 {
   "STT": 746,
   "Name": "Quầy thuốc Thu Anh",
   "address": "Quý, ấp Mỹ Tây 1, xã Mỹ Quý, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5453505,
   "Latitude": 105.7468535
 },
 {
   "STT": 747,
   "Name": "Nhà thuốc Tuyết Nghi",
   "address": "Số 01 đường Lê Hồng Phong, phường 1, TP Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4587225,
   "Latitude": 105.6358903
 },
 {
   "STT": 748,
   "Name": "Nhà thuốc Bệnh ViệnQuốc Tế TháiHòa",
   "address": "Số 01, đường Lê Thị Riêng, phường 1, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4649299,
   "Latitude": 105.6292135
 },
 {
   "STT": 749,
   "Name": "Quầy thuốc Hải Hà",
   "address": "Số 016, khóm Mỹ Tây, thị trấn Mỹ Thọ, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4453693,
   "Latitude": 105.6995299
 },
 {
   "STT": 750,
   "Name": "Quầy thuốc Hải Yến",
   "address": "Số 02, ấp Vĩnh Lợi, xã Vĩnh Thạnh, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.334938,
   "Latitude": 105.619322
 },
 {
   "STT": 751,
   "Name": "Quầy thuốc Thu Hoài 1",
   "address": "Số 02, khóm 1, thị trấn Lai Vung, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2954488,
   "Latitude": 105.7664771
 },
 {
   "STT": 752,
   "Name": "Nhà thuốc Cao Lãnh",
   "address": "Số 02, lô B, KDCphường 3, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4549723,
   "Latitude": 105.6340352
 },
 {
   "STT": 753,
   "Name": "Nhà thuốc Tiếp Hồng Ngân",
   "address": "Số 02, Phạm Hữu Lầu, phường 4, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4532686,
   "Latitude": 105.6314475
 },
 {
   "STT": 754,
   "Name": "Quầy thuốc Huỳnh Kim Khánh",
   "address": "Số 02/4/A, khóm 1, thị trấn Mỹ An, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5225428,
   "Latitude": 105.8402536
 },
 {
   "STT": 755,
   "Name": "Quầy thuốc Thu Hoài",
   "address": "Số 02/K1, thị trấn LaiVung, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2938059,
   "Latitude": 105.6588485
 },
 {
   "STT": 756,
   "Name": "Cơ Sở thuốc Đông Y Hưng Thịnh",
   "address": "Số 022, đường Phạm Hữu Lầu, khóm Mỹ Tây, thị trấn Mỹ Thọ, huyệnCao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4449416,
   "Latitude": 105.6962628
 },
 // {
 //   "STT": 757,
 //   "Name": "Quầy thuốc Thành Ngọc",
 //   "address": "Số 026, đường 26/3, khóm MỹTây, thị trấn Mỹ Thọ, huyện Cao Lãnh, TỈNH GIA LAI",
 //   "Longtitude": 10.7995423,
 //   "Latitude": 106.6066564
 // },
 {
   "STT": 758,
   "Name": "Quầy thuốc Trần Mẫn",
   "address": "Số 027,  Xẻo Quýt, khóm Mỹ Tây, thị trấn Mỹ Thọ, huyệnCao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4446844,
   "Latitude": 105.6957348
 },
 {
   "STT": 759,
   "Name": "Quầy thuốc Định An",
   "address": "Số 03 KDC ấp An Hòa, xã Định An, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.7436984,
   "Latitude": 105.3892372
 },
 {
   "STT": 760,
   "Name": "Quầy thuốc Bé Sáu",
   "address": "Số 03 Nguyễn Huệ, khóm 1, thị trấn  Sa Rài, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8732659,
   "Latitude": 105.4625849
 },
 {
   "STT": 761,
   "Name": "Quầy thuốc Thiện Tâm",
   "address": "Số 03, lô C, KDCPhú Thuận, xã Tân Phú Đông, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2788747,
   "Latitude": 105.7409852
 },
 {
   "STT": 762,
   "Name": "Nhà thuốc Quý Trọng",
   "address": "Số 03, tổ 32, khóm4, phường 6, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4288288,
   "Latitude": 105.6343903
 },
 {
   "STT": 763,
   "Name": "Quầy thuốc Kim Tảo",
   "address": "Số 038, ấp Nguyễn Cử, xã Nhị Mỹ, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4880535,
   "Latitude": 105.6940454
 },
 {
   "STT": 764,
   "Name": "Quầy thuốc Lộc Lợi",
   "address": "Số 03A, khóm 4, thị trấn Lai Vung, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2938059,
   "Latitude": 105.6588485
 },
 {
   "STT": 765,
   "Name": "Quầy thuốc Tấn Mỹ",
   "address": "Số 03B, ấp Bình Chánh, xã Bình Thành, huyệnThanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5449116,
   "Latitude": 105.5306017
 },
 {
   "STT": 766,
   "Name": "Quầy thuốc Duy Âu",
   "address": "Số 04 lô A, chợ Mỹ Hòa, ấp 1, xã Mỹ Hòa, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5890785,
   "Latitude": 105.8137711
 },
 {
   "STT": 767,
   "Name": "Quầy thuốc Anh Tuấn",
   "address": "Số 04 lô E chợ Ngã Năm Cây Trâm, ấp Long An, xã Long Thắng, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2231147,
   "Latitude": 105.6761185
 },
 {
   "STT": 768,
   "Name": "Quầy thuốc Nguyễn Thị Thanh Lan",
   "address": "Số 04, ấp Hòa Quới, xã Hòa Tân, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.1719042,
   "Latitude": 105.8172881
 },
 {
   "STT": 769,
   "Name": "Chi Nhánh Công ty Cp Dược Phẩm Imexpharm -GIA LAI",
   "address": "Số 04, đường 30/4, phường 1, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4606016,
   "Latitude": 105.6429871
 },
 {
   "STT": 770,
   "Name": "Quầy thuốc Hiển Vinh",
   "address": "Số 045, tổ 2, ấp 3, xã Bình HàngTrung, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4088492,
   "Latitude": 105.7527221
 },
 {
   "STT": 771,
   "Name": "Quầy thuốc Tuyết Anh",
   "address": "Số 051, tổ 3, ấp 4, xã Bình HàngTrung, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4088492,
   "Latitude": 105.7527221
 },
 {
   "STT": 772,
   "Name": "Quầy thuốc Phổ Tế",
   "address": "Số 053, ấp Mỹ Thới, xã MỹXương, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4111988,
   "Latitude": 105.7057792
 },
 {
   "STT": 773,
   "Name": "Quầy thuốc Cẩm Vân",
   "address": "Số 054, ấp 4, xã Phong Mỹ, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.5318166,
   "Latitude": 105.5826123
 },
 {
   "STT": 774,
   "Name": "Cơ Sở thuốc Đông Y Vạn Hưng",
   "address": "Số 06 Lê Hồng Phong, phường 1, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4697284,
   "Latitude": 105.6246343
 },
 {
   "STT": 775,
   "Name": "Quầy thuốc Hồng Hạnh",
   "address": "Số 06, ấp Tân Thành, xã Tân Quy Tây, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.3214874,
   "Latitude": 105.7303619
 },
 {
   "STT": 776,
   "Name": "Quầy thuốc Phương Duyên",
   "address": "Số 06, đường Nguyễn Trãi, khóm 2, thị trấn Tràm Chim, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.6721942,
   "Latitude": 105.5611437
 },
 {
   "STT": 777,
   "Name": "Quầy thuốc Thanh Trúc",
   "address": "Số 06, khóm Tân Đông B, thị trấnThanh Bình, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.557999,
   "Latitude": 105.490358
 },
 {
   "STT": 778,
   "Name": "Quầy thuốc Kim Ngọc",
   "address": "Số 07 KDC ấp Long Định, xã Long Thắng, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2475292,
   "Latitude": 105.6773627
 },
 {
   "STT": 779,
   "Name": "Quầy thuốc An Thịnh",
   "address": "Số 07, ấp AnThịnh, xã An Long, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.720632,
   "Latitude": 105.4126652
 },
 {
   "STT": 780,
   "Name": "Quầy thuốc Đặng Thị Phượng",
   "address": "Số 07, lô D, khóm 4, thị trấn Mỹ An, huyện  Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5116754,
   "Latitude": 105.8522149
 },
 {
   "STT": 781,
   "Name": "Quầy thuốc Khánh Trinh",
   "address": "Số 07, tổ 1, ấp 4, xã Phong Mỹ, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.5318166,
   "Latitude": 105.5826123
 },
 {
   "STT": 782,
   "Name": "Quầy thuốc Thắng Hạnh",
   "address": "Số 08 lô N KDC chợ Nha Mân, huyện  Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2698298,
   "Latitude": 105.8291222
 },
 {
   "STT": 783,
   "Name": "Quầy thuốc Kim Dung",
   "address": "Số 089, tổ 1, ấp 1, xã Mỹ Hiệp, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4059017,
   "Latitude": 105.8114175
 },
 {
   "STT": 784,
   "Name": "Quầy thuốc Trúc Yên",
   "address": "Số 089, tổ 1, ấp Bình Mỹ B, xãBình Thạnh, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.31316,
   "Latitude": 105.7750516
 },
 {
   "STT": 785,
   "Name": "Nhà thuốc Thu Tuyết",
   "address": "Số 09 đường Cách Mạng Tháng 8,phường 2, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4447999,
   "Latitude": 105.6490442
 },
 {
   "STT": 786,
   "Name": "Quầy thuốc Ánh Tuyết",
   "address": "Số 09, ấp Bình Chánh, xã Bình Thành, huyệnThanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5449116,
   "Latitude": 105.5306017
 },
 {
   "STT": 787,
   "Name": "Quầy thuốc Lê Vi",
   "address": "Số 09, ấp Tân An, xã Tân Huề, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.6136235,
   "Latitude": 105.3658125
 },
 // {
 //   "STT": 788,
 //   "Name": "Quầy thuốc Kim Soàn",
 //   "address": "Số 09, lô B, ấp Bình Thuận, xãBình Thành, huyện Thanh Bình, TỈNH GIA LAI",
 //   "Longtitude": 11.0903703,
 //   "Latitude": 108.0720781
 // },
 {
   "STT": 789,
   "Name": "Nhà thuốc Bảo Minh",
   "address": "Số 09, ven rạch Cái Sơn, khóm 2, phường 1, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2948726,
   "Latitude": 105.7664303
 },
 {
   "STT": 790,
   "Name": "Quầy thuốc Số 39",
   "address": "Số 09/53, tổ 7, ấp 3, xã Mỹ Trà, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4826015,
   "Latitude": 105.6426665
 },
 {
   "STT": 791,
   "Name": "Quầy thuốc Minh Đức",
   "address": "Số 1, đường Điện Biên Phủ, xã Mỹ Trà, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4779601,
   "Latitude": 105.6464638
 },
 {
   "STT": 792,
   "Name": "Quầy thuốc Võ Minh Đức",
   "address": "Số 1, đường Điện Biên Phủ, xã Mỹ Trà, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4779601,
   "Latitude": 105.6464638
 },
 {
   "STT": 793,
   "Name": "Quầy thuốc Ngọc Điệp",
   "address": "Số 1/1F, ấp Long Thuận, xã Long Hậu, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2846597,
   "Latitude": 105.6236585
 },
 {
   "STT": 794,
   "Name": "Quầy thuốc Thùy Loan",
   "address": "Số 1/5, ấp Tân Bình, xã HòaThành, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.335001,
   "Latitude": 105.7057792
 },
 {
   "STT": 795,
   "Name": "Quầy thuốc Hạnh Xuân",
   "address": "Số 10, ấp Bình Trung, xã Bình Thành, huyệnThanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5766484,
   "Latitude": 105.5591615
 },
 {
   "STT": 796,
   "Name": "Quầy thuốc Vân Sơn",
   "address": "Số 10, ấp Mỹ Thới, xã Mỹ Xương, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4111988,
   "Latitude": 105.7057792
 },
 {
   "STT": 797,
   "Name": "Quầy thuốc Châu Thảo",
   "address": "Số 10, ấp Trung, xã Tân Quới, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.6567686,
   "Latitude": 105.3775245
 },
 // {
 //   "STT": 798,
 //   "Name": "Quầy thuốc Trung Sơn",
 //   "address": "Số 10, đường 3/2, khóm Mỹ Tây, thị trấn Mỹ Thọ, huyện Cao Lãnh, TỈNH GIA LAI",
 //   "Longtitude": 10.7778648,
 //   "Latitude": 106.6810764
 // },
 {
   "STT": 799,
   "Name": "Quầy thuốc Ngọc Điền",
   "address": "Số 10/B Nguyễn Văn Cừ, khóm 2, thị trấn Mỹ An, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5202534,
   "Latitude": 105.8435547
 },
 {
   "STT": 800,
   "Name": "Cơ Sở thuốc Đông Y Phước Nguyên Hưng",
   "address": "Số 100, tổ 11, ấp 4, xã Bình Hàng Trung, huyện CaoLãnh, TỈNH GIA LAI",
   "Longtitude": 10.4088492,
   "Latitude": 105.7527221
 },
 {
   "STT": 801,
   "Name": "Quầy thuốc Đường Hưng",
   "address": "Số 1005/TL, ấp Tân Lợi, xã Tân Thành, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2640559,
   "Latitude": 105.6002024
 },
 {
   "STT": 802,
   "Name": "Nhà thuốc Thoại Vân",
   "address": "Số 101, đường Hòa Đông, phường Hòa Thuận, thành phố CaoLãnh, TỈNH GIA LAI",
   "Longtitude": 10.4592314,
   "Latitude": 105.6241524
 },
 {
   "STT": 803,
   "Name": "Quầy thuốc Mai Thị Sen",
   "address": "Số 10-11, lô C, ấp Phú Thạnh, chợ xã Phú Long, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2306393,
   "Latitude": 105.8056247
 },
 {
   "STT": 804,
   "Name": "Nhà thuốc Huỳnh Anh",
   "address": "Số 102 Tôn Đức Thắng, phường Mỹ Phú, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4654082,
   "Latitude": 105.634653
 },
 {
   "STT": 805,
   "Name": "Quầy thuốc Thọ Bình",
   "address": "Số 102, tổ 1, ấp An Định, xã An Bình, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4596806,
   "Latitude": 105.6617588
 },
 {
   "STT": 806,
   "Name": "Quầy thuốc Hoài Anh",
   "address": "Số 1025, ấp Tân Hội, xã Tân Bình, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5790949,
   "Latitude": 105.4009508
 },
 {
   "STT": 807,
   "Name": "Quầy thuốc Lê Trí",
   "address": "Số 103, QL 80, ấpThạnh Phú, xã Tân Bình, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2726736,
   "Latitude": 105.806821
 },
 {
   "STT": 808,
   "Name": "Quầy thuốc Thảo Phong",
   "address": "Số 103A, TỈNH GIA LAI lộ 848, ấp An Thạnh, xã Mỹ An Hưng B, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.4062672,
   "Latitude": 105.6138279
 },
 {
   "STT": 809,
   "Name": "Quầy thuốc Hồng Thịnh",
   "address": "Số 104, ấp Khánh Hòa, xã Tân Khánh Đông, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.355597,
   "Latitude": 105.7292491
 },
 {
   "STT": 810,
   "Name": "Quầy thuốc Kim Cúc",
   "address": "Số 104, ấp Tân Thới, xã Tân Quới, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.6989054,
   "Latitude": 105.3689504
 },
 {
   "STT": 811,
   "Name": "Quầy thuốc Diễm Phöc Anh",
   "address": "Số 104, tổ 4, ấp Đông Bình, xã Hòa An, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.516667,
   "Latitude": 105.55
 },
 {
   "STT": 812,
   "Name": "Quầy thuốc Tuyết Trinh",
   "address": "Số 104/2, chợ Long Định, xã LongThắng, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2475292,
   "Latitude": 105.6773627
 },
 // {
 //   "STT": 813,
 //   "Name": "Quầy thuốc Thi Dung",
 //   "address": "Số 104/7/1/1/4A, ấp Bình Định, xã Bình Thành, huyện Thanh Bình, TỈNH GIA LAI",
 //   "Longtitude": 14.1665324,
 //   "Latitude": 108.902683
 // },
 {
   "STT": 814,
   "Name": "Quầy thuốc Thiên An",
   "address": "Số 1044/TL, ấp Tân Lộc, xã Tân Thành, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.335001,
   "Latitude": 105.7057792
 },
 {
   "STT": 815,
   "Name": "Quầy thuốc Oanh Trường",
   "address": "Số 1046B, ấp Tân Hội, xã Tân Bình, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5790949,
   "Latitude": 105.4009508
 },
 {
   "STT": 816,
   "Name": "Quầy thuốc Tâm Ngọc",
   "address": "Số 1049B, QL80,ấp An Hòa Nhì, xã Tân Bình, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2678479,
   "Latitude": 105.837814
 },
 {
   "STT": 817,
   "Name": "Quầy thuốc Mỹ Kiều",
   "address": "Số 106, ấp Thị, xã An Phong, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.6325611,
   "Latitude": 105.4243804
 },
 {
   "STT": 818,
   "Name": "Quầy thuốc Anh Đào",
   "address": "Số 1069, ấp 3, xã Hưng Thạnh, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.6649367,
   "Latitude": 105.7019754
 },
 {
   "STT": 819,
   "Name": "Cơ Sở thuốc Đông Y TếLương Đường 3",
   "address": "Số 107 NguyễnTrãi, khóm 4, phường An Thạnh, thị xã Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.4567426,
   "Latitude": 105.6392428
 },
 {
   "STT": 820,
   "Name": "Quầy thuốc Thanh Hiền",
   "address": "Số 107A, ấp Tân Lộc A, xã Tân Dương, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.335001,
   "Latitude": 105.7057792
 },
 {
   "STT": 821,
   "Name": "Nhà thuốc Hồng Phước",
   "address": "Số 108 HùngVương, phường 2, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4550284,
   "Latitude": 105.635637
 },
 {
   "STT": 822,
   "Name": "Quầy thuốc Kiều Hạnh",
   "address": "Số 1082, ấp 1, xã Tân Mỹ, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.6215091,
   "Latitude": 105.5357139
 },
 {
   "STT": 823,
   "Name": "Quầy thuốc Thanh Tùng",
   "address": "Số 1085A/TL, ấp Tân Lộc, xã Tân Thành, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.251179,
   "Latitude": 105.5876588
 },
 {
   "STT": 824,
   "Name": "Quầy thuốc Kim Dung",
   "address": "Số 109, khóm Phú Mỹ Hiệp, thị trấn Cái Tàu Hạ, huyệnChâu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2587308,
   "Latitude": 105.8730678
 },
 {
   "STT": 825,
   "Name": "Quầy thuốc Thanh Vân",
   "address": "Số 10A, ấp Long Phú A, xã PhúThành A, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.6866371,
   "Latitude": 105.4360963
 },
 {
   "STT": 826,
   "Name": "Quầy thuốc Ba Lộc",
   "address": "Số 10E chợ Trường Xuân, xã Trường Xuân, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.656971,
   "Latitude": 105.7832675
 },
 {
   "STT": 827,
   "Name": "Nhà thuốc Minh Luân",
   "address": "Số 11 An Dương Vương, khóm 1, phường 2, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2954488,
   "Latitude": 105.7664771
 },
 {
   "STT": 828,
   "Name": "Quầy thuốc Hai Phước",
   "address": "Số 11, lô G, chợ Trường Xuân, xã Trường Xuân, huyện  Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.656971,
   "Latitude": 105.7832675
 },
 {
   "STT": 829,
   "Name": "Nhà thuốc Thanh Hương",
   "address": "Số 11, tổ 11, khóm2, phường 6, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4288288,
   "Latitude": 105.6343903
 },
 {
   "STT": 830,
   "Name": "Quầy thuốc Tư On",
   "address": "Số 11/D, chợ Tháp Mười, khóm 4, thị trấn Mỹ An, huyệnTháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5116754,
   "Latitude": 105.8522149
 },
 {
   "STT": 831,
   "Name": "Quầy thuốc Tuyết Linh",
   "address": "Số 1100A/1, ấp Long Khánh A, xã Long Hậu, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2846597,
   "Latitude": 105.6236585
 },
 {
   "STT": 832,
   "Name": "Quầy thuốc Minh Trung",
   "address": "Số 1101/D1, ấp 5,xã Đốc Binh Kiều, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.4933918,
   "Latitude": 105.9112424
 },
 {
   "STT": 833,
   "Name": "Cơ Sở thuốc Đông Y Hiệp Sanh Đường",
   "address": "Số 111, ấp 1, xã Phong Mỹ, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.5318166,
   "Latitude": 105.5826123
 },
 {
   "STT": 834,
   "Name": "Quầy thuốc Hồng Kim Chi",
   "address": "Số 111, ấp Tân Lộc A, xã Tân Dương, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.335001,
   "Latitude": 105.7057792
 },
 {
   "STT": 835,
   "Name": "Quầy thuốc Châu Lê Cử Nhân",
   "address": "Số 111, tổ 1, ấp Bình Mỹ B, xãBình Thạnh, huyệnCao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.31316,
   "Latitude": 105.7750516
 },
 {
   "STT": 836,
   "Name": "Quầy thuốc Phước Thành",
   "address": "Số 112A, ấp An Bình, xã Định Yên, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.383333,
   "Latitude": 105.533333
 },
 {
   "STT": 837,
   "Name": "Quầy thuốc Thùy Trang 1",
   "address": "Số 113, ấp Nhứt, xã An Phong, huyện  Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.6325611,
   "Latitude": 105.4243804
 },
 {
   "STT": 838,
   "Name": "Nhà thuốc Cẩm Hường",
   "address": "Số 113, tổ 42,khóm 5, phường 6, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4288288,
   "Latitude": 105.6343903
 },
 {
   "STT": 839,
   "Name": "Nhà thuốc Quang Hào",
   "address": "Số 114, đường30/4, phường 1, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4606016,
   "Latitude": 105.6429871
 },
 // {
 //   "STT": 840,
 //   "Name": "Quầy thuốc Nhựt Nam",
 //   "address": "Số 114E/1, ấp Long An, xã Long Thắng, huyện Lai Vung, TỈNH GIA LAI",
 //   "Longtitude": 10.695572,
 //   "Latitude": 106.2431205
 // },
 {
   "STT": 841,
   "Name": "Nhà thuốc Hữu Nghị 115",
   "address": "Số 115, đườngCMT8, phường 2, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4548522,
   "Latitude": 105.6361209
 },
 {
   "STT": 842,
   "Name": "Quầy thuốc Số 1",
   "address": "Số 115/LB, ấp Long Bình, xã Hòa Long, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2611796,
   "Latitude": 105.6588485
 },
 {
   "STT": 843,
   "Name": "Quầy thuốc Vy Đạt(Hạnh)",
   "address": "Số 1168, tổ 39, ấp Phú Lợi A, xã Phú Thuận B, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.7570042,
   "Latitude": 105.3423909
 },
 {
   "STT": 844,
   "Name": "Nhà thuốc Trần Hào",
   "address": "Số 117 NguyễnHuệ, khóm 1, phường 2, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2907956,
   "Latitude": 105.7686998
 },
 {
   "STT": 845,
   "Name": "Quầy thuốc Bích Phượng",
   "address": "Số 117/6, ấp Tân Thạnh, xã Phong Hòa, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2640559,
   "Latitude": 105.6002024
 },
 {
   "STT": 846,
   "Name": "Nhà thuốc Duy Anh",
   "address": "Số 118 Cách Mạng Tháng 8, phường 2, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4518451,
   "Latitude": 105.6361871
 },
 {
   "STT": 847,
   "Name": "Quầy thuốc Ngọc Đáng",
   "address": "Số 118 HùngVương, khóm 1, thị trấn Sa Rài, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8810218,
   "Latitude": 105.4496526
 },
 {
   "STT": 848,
   "Name": "Quầy thuốc Tân Nương",
   "address": "Số 118, ấp AnThịnh, xã An Long, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.720632,
   "Latitude": 105.4126652
 },
 {
   "STT": 849,
   "Name": "Quầy thuốc Thanh Tùng",
   "address": "Số 118B, chợ An Hòa, ấp 1, xã An Hòa, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.7459248,
   "Latitude": 105.3652405
 },
 {
   "STT": 850,
   "Name": "Quầy thuốc Hồng Hạnh",
   "address": "Số 118B, quốc lộ 80, ấp Phú Long, xã Tân Phú Đông, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2788292,
   "Latitude": 105.7240215
 },
 {
   "STT": 851,
   "Name": "Chi Nhánh Công ty Cpdp Imexpharm - Cửu Long 1",
   "address": "Số 119, đường Nguyễn Văn Voi, thị trấn Cái Tàu Hạ, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2588302,
   "Latitude": 105.8757745
 },
 {
   "STT": 852,
   "Name": "Quầy thuốc Hạnh - Dung",
   "address": "Số 11A, khóm 4, thị trấn  Mỹ An, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5116754,
   "Latitude": 105.8522149
 },
 {
   "STT": 853,
   "Name": "Nhà thuốc Ngô Phước Lợi",
   "address": "Số 12 Lê Hồng Phong, phường 1, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4697926,
   "Latitude": 105.6248867
 },
 {
   "STT": 854,
   "Name": "Quầy thuốc Minh Thư",
   "address": "Số 12, ấp AnThuận, xã An Hiệp, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2856663,
   "Latitude": 105.8231588
 },
 {
   "STT": 855,
   "Name": "Quầy thuốc Kim Tuyền",
   "address": "Số 12/5, ấp Long Định, xã LongThắng, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2475292,
   "Latitude": 105.6773627
 },
 {
   "STT": 856,
   "Name": "Quầy thuốc Kim Chi",
   "address": "Số 121, ấp Khánh Nhơn, xã Tân Khánh Trung, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3791189,
   "Latitude": 105.6940454
 },
 {
   "STT": 857,
   "Name": "Nhà thuốc Muỗi Lẽn",
   "address": "Số 122, Thiên Hộ Dương, khóm 2, phường An Thạnh, thị xã Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8096026,
   "Latitude": 105.3385654
 },
 {
   "STT": 858,
   "Name": "Quầy thuốc Minh Mẫn",
   "address": "Số 122/B, QL 30,ấp Thị, xã An Phong, huyệnThanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.6191158,
   "Latitude": 105.4167044
 },
 {
   "STT": 859,
   "Name": "Nhà thuốc Võ Minh Nhựt",
   "address": "Số 123 HùngVương, phường 2, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4551328,
   "Latitude": 105.6359911
 },
 {
   "STT": 860,
   "Name": "Quầy thuốc Đời Thu",
   "address": "Số 123, ấp 1, xãThường Phước 1, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8788463,
   "Latitude": 105.2077798
 },
 {
   "STT": 861,
   "Name": "Quầy thuốc Kiều Mai",
   "address": "Số 123, ấp Mỹ Thới, xã MỹXương, huyện CaoLãnh, TỈNH GIA LAI",
   "Longtitude": 10.4111988,
   "Latitude": 105.7057792
 },
 {
   "STT": 862,
   "Name": "Quầy thuốc Hoàng Kim",
   "address": "Số 123, đường Huỳnh Công Sính, khóm 2, thị trấn Tràm Chim, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.6720776,
   "Latitude": 105.5566074
 },
 {
   "STT": 863,
   "Name": "Quầy thuốc Hoàng Kim",
   "address": "Số 123, đường Huỳnh Công Sính, khóm 2, thị trấn Tràm Chim, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.6720776,
   "Latitude": 105.5566074
 },
 {
   "STT": 864,
   "Name": "Quầy thuốc Quang Thọ",
   "address": "Số 124 HùngVương, khóm 1, thị trấn Sa Rài, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8810031,
   "Latitude": 105.4496603
 },
 {
   "STT": 865,
   "Name": "Quầy thuốc Thanh Thy",
   "address": "Số 124, ấp An Lạc, xã Định An, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3219082,
   "Latitude": 105.5298525
 },
 {
   "STT": 866,
   "Name": "Quầy thuốc Thùy Linh",
   "address": "Số 124, ấp Thị, xã An Phong, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.6325611,
   "Latitude": 105.4243804
 },
 {
   "STT": 867,
   "Name": "Quầy thuốc Chánh Mai Thanh",
   "address": "Số 124, đường Trần Hữu Trang, xã Hòa An, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4556871,
   "Latitude": 105.6238619
 },
 {
   "STT": 868,
   "Name": "Quầy thuốc Quốc Huy",
   "address": "Số 124A, ấp Tân Thuận A, xã Tân Mỹ, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.335001,
   "Latitude": 105.7057792
 },
 {
   "STT": 869,
   "Name": "Quầy thuốc Vương Nguyễn Tấn",
   "address": "Số 125, ấp Tân Lộc A, xã Tân Dương, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.335001,
   "Latitude": 105.7057792
 },
 {
   "STT": 870,
   "Name": "Quầy thuốc Bảo Ngọc",
   "address": "Số 1251/TL, ấp Tân Lộc, xã Tân Thành, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.251179,
   "Latitude": 105.5876588
 },
 // {
 //   "STT": 871,
 //   "Name": "Quầy thuốc Song Thịnh Ii",
 //   "address": "Số 126 Nguyễn Văn Voi, tổ 1, khóm Phú Hưng, thị trấn Cái Tàu Hạ, huyện  Châu Thành, TỈNH GIA LAI",
 //   "Longtitude": 10.2583556,
 //   "Latitude": 105.8742316
 // },
 {
   "STT": 872,
   "Name": "Quầy thuốc Nguyễn Thị Kim Thúy",
   "address": "Số 127 Mai Văn Khải, ấp 3, xã Mỹ Tân, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4854179,
   "Latitude": 105.6088859
 },
 {
   "STT": 873,
   "Name": "Quầy thuốc Đặng Minh Thức",
   "address": "Số 127, ấp Tân Quới, xã Tân Phú Trung, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2462649,
   "Latitude": 105.7409852
 },
 {
   "STT": 874,
   "Name": "Quầy thuốc Hồng Giang",
   "address": "Số 1279 tuyến dân cư Tứ Tân, ấp 2, xã Tân Thành B, huyện  Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8697347,
   "Latitude": 105.494688
 },
 {
   "STT": 875,
   "Name": "Cơ Sở thuốc Đông Y Kế Tám",
   "address": "Số 128, ấp An Lợi B, xã Định Yên, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.309874,
   "Latitude": 105.5532993
 },
 {
   "STT": 876,
   "Name": "Quầy thuốc Tường Giao",
   "address": "Số 128, tổ 4, ấp 1, xã Mỹ Ngãi, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.5034079,
   "Latitude": 105.590733
 },
 {
   "STT": 877,
   "Name": "Quầy thuốc Ngân Thắng",
   "address": "Số 128/Đ1 đường Phan Thị Bảy, xã Hòa An, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.460912,
   "Latitude": 105.6094073
 },
 {
   "STT": 878,
   "Name": "Quầy thuốc Ngọc Yến",
   "address": "Số 129, khóm 3, thị trấn Lai Vung, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2938059,
   "Latitude": 105.6588485
 },
 {
   "STT": 879,
   "Name": "Quầy thuốc Trúc Giang",
   "address": "Số 1291/D, ấp 5, xã Đốc Binh Kiều, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.4933918,
   "Latitude": 105.9112424
 },
 {
   "STT": 880,
   "Name": "Quầy thuốc Lộc Mai",
   "address": "Số 12A, ấp Lợi An, xã Thanh Mỹ, huyện  Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.4098655,
   "Latitude": 105.8407722
 },
 {
   "STT": 881,
   "Name": "Quầy thuốc Minh Phát",
   "address": "Số 12A1, tổ 9, ấp Hòa Mỹ, xã Hòa An, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4622676,
   "Latitude": 105.6086621
 },
 {
   "STT": 882,
   "Name": "Quầy thuốc Trung Hậu",
   "address": "Số 13 Nguyễn Trãi, khóm II, thị trấn Tràm Chim, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.672501,
   "Latitude": 105.5612057
 },
 {
   "STT": 883,
   "Name": "Nhà thuốc Duy Khoa",
   "address": "Số 13, đườmg 30/4, phường 1, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4606016,
   "Latitude": 105.6429871
 },
 {
   "STT": 884,
   "Name": "Nhà thuốc Trung Tín",
   "address": "Số 13, đường Nguyễn Trãi, khóm 1, phường AnThạnh, thị xã HồngNgự, TỈNH GIA LAI",
   "Longtitude": 10.80853,
   "Latitude": 105.3421987
 },
 {
   "STT": 885,
   "Name": "Nhà thuốc Quỳnh Liêm",
   "address": "Số 13, khóm Mỹ Phú, phường Mỹ Phú, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4582842,
   "Latitude": 105.646167
 },
 {
   "STT": 886,
   "Name": "Nhà thuốc Ngọc Sơn",
   "address": "Số 13, tổ 54, khóm5, phường 11, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4878813,
   "Latitude": 105.5839265
 },
 {
   "STT": 887,
   "Name": "Quầy thuốc Quốc Sĩ",
   "address": "Số 130, ấp An Lợi B, xã Định Yên, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.309874,
   "Latitude": 105.5532993
 },
 {
   "STT": 888,
   "Name": "Quầy thuốc Thanh Dân",
   "address": "Số 1300/C, ấp Mỹ Tây 2, xã Mỹ Quý, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.4453693,
   "Latitude": 105.6995299
 },
 {
   "STT": 889,
   "Name": "Quầy thuốc Trường Phước",
   "address": "Số 132, ấp Phú Hòa, xã Tân Phú Đông, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.30076,
   "Latitude": 105.765701
 },
 {
   "STT": 890,
   "Name": "Nhà thuốc Hải Lâm",
   "address": "Số 1329, tổ 51,khóm 5, phường 11, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4878813,
   "Latitude": 105.5839265
 },
 {
   "STT": 891,
   "Name": "Quầy thuốc Ánh Ngọc",
   "address": "Số 135, ấp Thị, xã An Phong, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.6325611,
   "Latitude": 105.4243804
 },
 {
   "STT": 892,
   "Name": "Quầy thuốc Kiều Anh",
   "address": "Số 135, đường Mai Văn Khải, xã Mỹ Tân, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4799668,
   "Latitude": 105.6188059
 },
 {
   "STT": 893,
   "Name": "Quầy thuốc Liên - Vũ",
   "address": "Số 135A, ấp Bình Hiệp A, xã Bình Thạnh Trung, huyện  Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3803603,
   "Latitude": 105.5591615
 },
 {
   "STT": 894,
   "Name": "Quầy thuốc Thanh Thủy",
   "address": "Số 135A, khóm Phú Hòa, thị trấn Cái Tàu Hạ, huyệnChâu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2587308,
   "Latitude": 105.8730678
 },
 {
   "STT": 895,
   "Name": "Quầy thuốc Quốc Phục",
   "address": "Số 135A/LB, ấp Long Bửu, xã Hòa Long, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2611796,
   "Latitude": 105.6588485
 },
 {
   "STT": 896,
   "Name": "Nhà thuốc Sơn Tùng",
   "address": "Số 137 Trần Hưng Đạo, khóm 1, phường 1, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2962456,
   "Latitude": 105.766078
 },
 {
   "STT": 897,
   "Name": "Quầy thuốc Xuân Mai",
   "address": "Số 137B/1, ấp Tân Lợi, xã Phong Hòa, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.335001,
   "Latitude": 105.7057792
 },
 {
   "STT": 898,
   "Name": "Quầy thuốc Thanh Tuyền",
   "address": "Số 138 Đốc Binh Vàng, khóm Tân Đông B, thị trấn Thanh Bình, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5556085,
   "Latitude": 105.4858514
 },
 {
   "STT": 899,
   "Name": "Nhà thuốc Ngọc Tuyền",
   "address": "Số 139 Cao Thắng, khóm Tân Huề, phường Tân Qui Đông, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.3259527,
   "Latitude": 105.7282487
 },
 {
   "STT": 900,
   "Name": "Quầy thuốc Thùy Linh",
   "address": "Số 139, ấp Khánh Nhơn, xã Tân Khánh Đông, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.3791189,
   "Latitude": 105.6940454
 },
 {
   "STT": 901,
   "Name": "Quầy thuốc An Trinh",
   "address": "Số 139, khóm Tân Đông B, thị trấn Thanh Bình, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5554601,
   "Latitude": 105.491758
 },
 {
   "STT": 902,
   "Name": "Quầy thuốc Phö Nghĩa",
   "address": "Số 139/1, quốc lộ 80, ấp Phú Thuận, xã Tân Phú Đông, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2794286,
   "Latitude": 105.7186961
 },
 {
   "STT": 903,
   "Name": "Quầy thuốc Quỳnh Mai",
   "address": "Số 14 lô H, ấp Tân Lập, xã Tân Nhuận Đông, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2422003,
   "Latitude": 105.8231588
 },
 {
   "STT": 904,
   "Name": "Quầy thuốc Phương Ngân",
   "address": "Số 14 Nguyễn Huệ, khóm 1, thị trấn Sa Rài, huyện TânHồng, TỈNH GIA LAI",
   "Longtitude": 10.8797812,
   "Latitude": 105.450975
 },
 {
   "STT": 905,
   "Name": "Cơ Sở thuốc Đông Y Phát Xương Long",
   "address": "Số 14, tổ 5, khu phố Mỹ Tây, thị trấn Mỹ Thọ, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4453693,
   "Latitude": 105.6995299
 },
 {
   "STT": 906,
   "Name": "Quầy thuốc Thảo Quyên",
   "address": "Số 140 Nguyễn Trãi, khóm MỹTây, thị trấn Mỹ Thọ, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4461118,
   "Latitude": 105.6961852
 },
 {
   "STT": 907,
   "Name": "Quầy thuốc Mương Điều",
   "address": "Số 140A chợ Mương Điều, ấp Khánh An, xã Tân Khánh Trung, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3787201,
   "Latitude": 105.7071695
 },
 {
   "STT": 908,
   "Name": "Quầy thuốc Ngọc Nghĩa",
   "address": "Số 141 ấp Tân Lộc A, xã Tân Dương, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.335001,
   "Latitude": 105.7057792
 },
 {
   "STT": 909,
   "Name": "Quầy thuốc Thanh Đường",
   "address": "Số 141, ấp An Lợi B, xã Định Yên, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.309874,
   "Latitude": 105.5532993
 },
 // {
 //   "STT": 910,
 //   "Name": "Quầy thuốc Vạn Phước- Trung Hậu",
 //   "address": "Số 141, đường 3/2, khóm Bình Thạnh 1, thị trấn Lấp Vò, huyện  Lấp Vò, TỈNH GIA LAI",
 //   "Longtitude": 10.7727694,
 //   "Latitude": 106.6763088
 // },
 {
   "STT": 911,
   "Name": "Quầy thuốc Trâm Anh",
   "address": "Số 142 lô A chợ Lai Vung, khóm 3, thị trấn Lai Vung, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2884478,
   "Latitude": 105.6588858
 },
 {
   "STT": 912,
   "Name": "Quầy thuốc Thanh Nhàn",
   "address": "Số 1425/C, ấp Mỹ Tây 2, xã Mỹ Quí, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5453505,
   "Latitude": 105.7468535
 },
 {
   "STT": 913,
   "Name": "Quầy thuốc Hữu Nghị",
   "address": "Số 143, ấp Bình Mỹ B, xã BìnhThạnh, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.31316,
   "Latitude": 105.7750516
 },
 {
   "STT": 914,
   "Name": "Quầy thuốc Nhật Liên",
   "address": "Số 144, ấp Tân Lộc A, xã Tân Dương, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.335001,
   "Latitude": 105.7057792
 },
 {
   "STT": 915,
   "Name": "Nhà thuốc Bệnh Viện Đa Khoa ĐồngTháp",
   "address": "Số 144, đường Mai Văn Khải, ấp 3, xã Mỹ Tân, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4720372,
   "Latitude": 105.6262242
 },
 {
   "STT": 916,
   "Name": "Quầy thuốc Tuyết Ngân",
   "address": "Số 144, tổ 20, ấp Hòa Khánh, xã Hòa An, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4622676,
   "Latitude": 105.6086621
 },
 {
   "STT": 917,
   "Name": "Quầy thuốc 9 Trung",
   "address": "Số 147, ấp Phú Bình, xã Phú Long, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2222167,
   "Latitude": 105.7879371
 },
 {
   "STT": 918,
   "Name": "Nhà thuốc Tâm Ngọc 2",
   "address": "Số 148, đường Trần Phú, khóm Tân Bình, phường An Hòa, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2946176,
   "Latitude": 105.7583089
 },
 {
   "STT": 919,
   "Name": "Quầy thuốc Hải Châu",
   "address": "Số 149 HùngVương, khóm 1, thị trấn Sa Rài, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8810016,
   "Latitude": 105.449657
 },
 {
   "STT": 920,
   "Name": "Quầy thuốc Anh Kiệt",
   "address": "Số 149, tổ 7, ấp Hòa Dân, xã Nhị Mỹ, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4880535,
   "Latitude": 105.6940454
 },
 {
   "STT": 921,
   "Name": "Quầy thuốc Kim Ngân",
   "address": "Số 149, tổ 7, ấp Hòa Dân, xã Nhị Mỹ, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4880535,
   "Latitude": 105.6940454
 },
 {
   "STT": 922,
   "Name": "Quầy thuốc Trường An",
   "address": "Số 149, tổ 7, ấp Hòa Dân, xã Nhị Mỹ, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4880535,
   "Latitude": 105.6940454
 },
 {
   "STT": 923,
   "Name": "Nhà thuốc Số 26",
   "address": "Số 149B Trần Hưng Đạo, khóm 1, phường 1, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2956239,
   "Latitude": 105.7666267
 },
 {
   "STT": 924,
   "Name": "Quầy thuốc Thanh Tuyền",
   "address": "Số 14B, ấp Bình Hiệp A, xã Bình Thạnh Trung, huyện  Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3803603,
   "Latitude": 105.5591615
 },
 {
   "STT": 925,
   "Name": "Chi Nhánh Công ty Cổ Phần Dược Hậu Giang Tại ĐồngTháp",
   "address": "Số 14C Nguyễn Văn Trỗi, phường 2, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.456505,
   "Latitude": 105.6390556
 },
 {
   "STT": 926,
   "Name": "Nhà thuốc Kim Sang",
   "address": "Số 15 đường Lê Hồng Phong, phường 1, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4769245,
   "Latitude": 105.5987406
 },
 {
   "STT": 927,
   "Name": "Quầy thuốc Phúc An Khang",
   "address": "Số 15 Huỳnh Công Chí, khóm 1, thị trấn Sa Rài, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8811325,
   "Latitude": 105.4525226
 },
 {
   "STT": 928,
   "Name": "Cơ Sở thuốc Đông Y Sáu Châu",
   "address": "Số 15, ấp 5, xã Tân Hội Trung, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4569711,
   "Latitude": 105.7703287
 },
 {
   "STT": 929,
   "Name": "Quầy thuốc Huỳnh Yến 2",
   "address": "Số 15, ấp Bình Hiệp A, xã Bình Thạnh Trung, huyện  Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3803603,
   "Latitude": 105.5591615
 },
 {
   "STT": 930,
   "Name": "Cơ Sở thuốc Đông Y Thanh XuânĐường",
   "address": "Số 15, xã Đốc Binh Kiều, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.4933918,
   "Latitude": 105.9112424
 },
 {
   "STT": 931,
   "Name": "Cơ Sở thuốc Đông Y Minh Tú",
   "address": "Số 1500, QL 30, tổ20, khóm 3,phường 11, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4861855,
   "Latitude": 105.5804369
 },
 {
   "STT": 932,
   "Name": "Quầy thuốc Ngọc Nhanh",
   "address": "Số 151, Quốc lộ 30, ấp Gò Da, xã Bình Phú, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8612459,
   "Latitude": 105.4033623
 },
 {
   "STT": 933,
   "Name": "Nhà thuốc Thiện Phöc",
   "address": "Số 152A, đường Nguyễn Sinh Sắc, khóm Hòa Khánh, phường 2, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2858446,
   "Latitude": 105.7741866
 },
 {
   "STT": 934,
   "Name": "Nhà thuốc Bệnh Viện Đa Khoa Sa Đéc",
   "address": "Số 153 Nguyễn Sinh Sắc, khóm Hòa Khánh, phường 2, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2885385,
   "Latitude": 105.7712039
 },
 {
   "STT": 935,
   "Name": "Nhà thuốc Phương Châu Sa Đéc",
   "address": "Số 153 Nguyễn Sinh Sắc, phường 2, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2869166,
   "Latitude": 105.7728457
 },
 {
   "STT": 936,
   "Name": "Quầy thuốc Thái Bảo",
   "address": "Số 153/D Hùng Vương, khóm 4,thị trấn Mỹ An, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.519119,
   "Latitude": 105.842282
 },
 {
   "STT": 937,
   "Name": "Quầy thuốc Hữu Phước",
   "address": "Số 154A/TLA, ấp Tân Lộc A, xã Tân Dương, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.335001,
   "Latitude": 105.7057792
 },
 {
   "STT": 938,
   "Name": "Quầy thuốc Đại Hải",
   "address": "Số 155, ấp Thị, xã An Phong, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.6325611,
   "Latitude": 105.4243804
 },
 {
   "STT": 939,
   "Name": "Cơ Sở thuốc Đông Y Đại Đồng",
   "address": "Số 155B, ấp Thị, xã An Phong, huyện  Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.6325611,
   "Latitude": 105.4243804
 },
 {
   "STT": 940,
   "Name": "Quầy thuốc Thủy Linh",
   "address": "Số 156, ấp Phú Thọ B, xã Phú Thọ, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.6958011,
   "Latitude": 105.471249
 },
 {
   "STT": 941,
   "Name": "Quầy thuốc Tuấn Anh",
   "address": "Số 158 HùngVương, khóm 1, thị trấn Sa Rài, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8818278,
   "Latitude": 105.4509401
 },
 {
   "STT": 942,
   "Name": "Nhà thuốc Hoàng Quân",
   "address": "Số 158, đường30/4, phường 1, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4606016,
   "Latitude": 105.6429871
 },
 {
   "STT": 943,
   "Name": "Quầy thuốc Kiều Oanh",
   "address": "Số 159, ấp An Phú, xã Mỹ An Hưng B, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3831971,
   "Latitude": 105.6119301
 },
 {
   "STT": 944,
   "Name": "Quầy thuốc Sử Ánh",
   "address": "Số 159/MH, ấp 1, xã Mỹ Hòa, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5179126,
   "Latitude": 105.8809053
 },
 {
   "STT": 945,
   "Name": "Nhà thuốc Chính Loan",
   "address": "Số 16 Hai Bà Trưng, khóm 2, phường 3, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.3063257,
   "Latitude": 105.7629924
 },
 // {
 //   "STT": 946,
 //   "Name": "Cơ Sở thuốc Đông Y Quang Minh",
 //   "address": "Số 16 Ngô Thời Nhiệm, khóm 1, phường 1, thành phố Sa Đéc, TỈNH GIA LAI",
 //   "Longtitude": 10.7790477,
 //   "Latitude": 106.6883889
 // },
 {
   "STT": 947,
   "Name": "Quầy thuốc Minh Đức 1",
   "address": "Số 16, ấp Bình Nhứt, xã Nhị Mỹ, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4880535,
   "Latitude": 105.6940454
 },
 // {
 //   "STT": 948,
 //   "Name": "Quầy thuốc Thanh Vân",
 //   "address": "Số 16, lô D2, khu DC ấp 2, xã Mỹ Long, huyện Cao Lãnh, TỈNH GIA LAI",
 //   "Longtitude": 38.9071923,
 //   "Latitude": -77.0368707
 // },
 {
   "STT": 949,
   "Name": "Quầy thuốc Trúc Linh",
   "address": "Số 160, ấp 3, xã Mỹ Hiệp, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4059017,
   "Latitude": 105.8114175
 },
 {
   "STT": 950,
   "Name": "Cơ Sở thuốc Đông Y Hạnh Lâm Xuân",
   "address": "Số 161, đường Nguyễn Huệ, khóm 1, phường 2, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2921589,
   "Latitude": 105.7691017
 },
 // {
 //   "STT": 951,
 //   "Name": "Nhà thuốc Cai Dao",
 //   "address": "Số 161A, khóm Tân Hiệp, phường Tân Quy Đông, thành phố Sa Đéc, TỈNH GIA LAI",
 //   "Longtitude": 10.1154358,
 //   "Latitude": 105.2834404
 // },
 // {
 //   "STT": 952,
 //   "Name": "Nhà thuốc Ngọc Trâm",
 //   "address": "Số 161B, khóm Tân Hiệp, phường Tân Quy Đông, thành phố Sa Đéc, TỈNH GIA LAI",
 //   "Longtitude": 10.1154358,
 //   "Latitude": 105.2834404
 // },
 {
   "STT": 953,
   "Name": "Quầy thuốc Từ Tiến",
   "address": "Số 162 đường Quãng Khánh, xã Mỹ Trà, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4899262,
   "Latitude": 105.6385471
 },
 {
   "STT": 954,
   "Name": "Quầy thuốc Thanh Bình",
   "address": "Số 162/3, đường ĐT 848, ấp Khánh Hòa, xã Tân Khánh Đông, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.3513067,
   "Latitude": 105.729024
 },
 {
   "STT": 955,
   "Name": "Quầy thuốc Thúy Vân",
   "address": "Số 163, đường ĐT 848, ấp Khánh Hòa, xã Tân Khánh Đông, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.3513067,
   "Latitude": 105.729024
 },
 {
   "STT": 956,
   "Name": "Quầy thuốc Thanh Quý",
   "address": "Số 164, ấp 1, xã Mỹ Hiệp, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4059017,
   "Latitude": 105.8114175
 },
 {
   "STT": 957,
   "Name": "Quầy thuốc Thiện Phöc Iii",
   "address": "Số 165, đường Hùng Vương, ấp Phú Hòa, xã Tân Phú Đông, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2846549,
   "Latitude": 105.7676649
 },
 {
   "STT": 958,
   "Name": "Quầy thuốc Thúy Vân",
   "address": "Số 167, ấp An Hòa Nhì, xã Tân Bình, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.7436984,
   "Latitude": 105.3892372
 },
 {
   "STT": 959,
   "Name": "Nhà thuốc Tâm Đức (Bệnh Viện Phcn GIA LAI)",
   "address": "Số 167, đường Tôn Đức Thắng, phường 1, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4670693,
   "Latitude": 105.6368545
 },
 {
   "STT": 960,
   "Name": "Quầy thuốc Nhân Hồ I",
   "address": "Số 168, khóm Bình Thạnh 1, thị trấn Lấp Vò, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3577289,
   "Latitude": 105.5210607
 },
 {
   "STT": 961,
   "Name": "Quầy thuốc Minh Huệ",
   "address": "Số 169, ấp 4, xã Bình Hàng Trung, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4088492,
   "Latitude": 105.7527221
 },
 {
   "STT": 962,
   "Name": "Quầy thuốc Hồng Thái",
   "address": "Số 169, ấp 5, xã Gáo Giồng, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.6066717,
   "Latitude": 105.6293731
 },
 {
   "STT": 963,
   "Name": "Quầy thuốc Cöc Phương",
   "address": "Số 169, đường 2/9, khóm 2, thị trấn Tràm Chim, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.6691946,
   "Latitude": 105.5650239
 },
 {
   "STT": 964,
   "Name": "Quầy thuốc Thành Tín",
   "address": "Số 17 KP 06, khóm Mỹ Tây, thị trấn Mỹ Thọ, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4453693,
   "Latitude": 105.6995299
 },
 {
   "STT": 965,
   "Name": "Nhà thuốc Hoài Nhân 2",
   "address": "Số 17 Lê Thánh Tôn, khóm 1, phường 2, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2920996,
   "Latitude": 105.7671407
 },
 {
   "STT": 966,
   "Name": "Quầy thuốc Ngọc Châu 1",
   "address": "Số 17 lô A chợ Mỹ Hòa, ấp 1, xã Mỹ Hòa, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5890785,
   "Latitude": 105.8137711
 },
 {
   "STT": 967,
   "Name": "Nhà thuốc Quỳnh Chi",
   "address": "Số 17 Nguyễn Du, phường 2, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4544666,
   "Latitude": 105.6366184
 },
 {
   "STT": 968,
   "Name": "Quầy thuốc Khoa Thi",
   "address": "Số 170/TH, ấp Tân Hòa, xã HòaThành, huyện LaiVung, TỈNH GIA LAI",
   "Longtitude": 10.2984155,
   "Latitude": 105.7118286
 },
 {
   "STT": 969,
   "Name": "Quầy thuốc Thu Anh",
   "address": "Số 171, ấp 1, xã Mỹ Hiệp, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4059017,
   "Latitude": 105.8114175
 },
 {
   "STT": 970,
   "Name": "Quầy thuốc Thành Nghiệp",
   "address": "Số 171, tổ 5, ấp Tịnh Châu, xã TịnhThới, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4324174,
   "Latitude": 105.6488796
 },
 {
   "STT": 971,
   "Name": "Cơ Sở thuốc Đông Y Dũ An Hòa 1",
   "address": "Số 172 Lý Thường Kiệt, phường An Thạnh, thị xã Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8113606,
   "Latitude": 105.3391403
 },
 {
   "STT": 972,
   "Name": "Quầy thuốc Nguyễn Thị Kim Thanh",
   "address": "Số 172, ấp 3, xã Phương Trà, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.5116284,
   "Latitude": 105.6588485
 },
 {
   "STT": 973,
   "Name": "Quầy thuốc Minh Tú 2",
   "address": "Số 174, ấp 3, xã Phong Mỹ, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.5318166,
   "Latitude": 105.5826123
 },
 {
   "STT": 974,
   "Name": "Nhà thuốc Ngọc Hải 175",
   "address": "Số 175 HùngVương, phường 2, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4564525,
   "Latitude": 105.6377804
 },
 {
   "STT": 975,
   "Name": "Nhà thuốc Bảo Uyên",
   "address": "Số 175 Trần Phú, khóm 5, phường 1, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2932088,
   "Latitude": 105.7563038
 },
 {
   "STT": 976,
   "Name": "Quầy thuốc Thành Thảo",
   "address": "Số 175, khu phố 6, khóm Mỹ Thuận,thị trấn Mỹ Thọ, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4501101,
   "Latitude": 105.6970266
 },
 // {
 //   "STT": 977,
 //   "Name": "Quầy thuốc Tân Khánh Đông",
 //   "address": "Số 175/1 đườngtỉnh 848, ấp Khánh Hòa, xã Tân Khánh Đông, thành phố Sa Đéc, TỈNH GIA LAI",
 //   "Longtitude": 12.2585098,
 //   "Latitude": 109.0526076
 // },
 {
   "STT": 978,
   "Name": "Quầy thuốc Tấn Hưng",
   "address": "Số 178, TL 854,khóm Phú Mỹ Hiệp, thị trấn Cái Tàu Hạ, huyện ChâuThành, TỈNH GIA LAI",
   "Longtitude": 10.2587308,
   "Latitude": 105.8730678
 },
 {
   "STT": 979,
   "Name": "Cơ Sở thuốc Đông Y Hồng Phát Đường",
   "address": "Số 18, đường 3/2, khóm Mỹ Tây, thị trấn Mỹ Thọ, huyệnCao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4453693,
   "Latitude": 105.6995299
 },
 {
   "STT": 980,
   "Name": "Quầy thuốc Hoàng Châu",
   "address": "Số 18, khóm Mỹ Tây, thị trấn Mỹ Thọ, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4453693,
   "Latitude": 105.6995299
 },
 {
   "STT": 981,
   "Name": "Quầy thuốc Hoài Dung",
   "address": "Số 18, TỈNH GIA LAI lộ 853, chợ Giao Thông, ấp Tân Thạnh, xã Phong Hòa, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.1955141,
   "Latitude": 105.7040604
 },
 {
   "STT": 982,
   "Name": "Quầy thuốc Vân Anh 1",
   "address": "Số 180, ấp An Hòa, xã Mỹ An Hưng B, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3831971,
   "Latitude": 105.6119301
 },
 {
   "STT": 983,
   "Name": "Quầy thuốc Ngọc Diệp",
   "address": "Số 180, tổ 3, ấp 3, xã Mỹ Trà, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4826015,
   "Latitude": 105.6426665
 },
 {
   "STT": 984,
   "Name": "Quầy thuốc Trần Thị Ngân Trúc",
   "address": "Số 180, tổ 3, ấp 3, xã Mỹ Trà, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4826015,
   "Latitude": 105.6426665
 },
 {
   "STT": 985,
   "Name": "Quầy thuốc Kim Cương",
   "address": "Số 181, ấp Tân Phú, xã Tân Long, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5790949,
   "Latitude": 105.4009508
 },
 {
   "STT": 986,
   "Name": "Nhà thuốc An Hòa",
   "address": "Số 181A Tỉnh lộ 852, khóm Tân Hòa, phường An Hòa, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.3174737,
   "Latitude": 105.7397175
 },
 {
   "STT": 987,
   "Name": "Quầy thuốc Minh Bạch",
   "address": "Số 182 Hùng Vương, ấp 1, thị trấn Sa Rài, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8809797,
   "Latitude": 105.4517179
 },
 {
   "STT": 988,
   "Name": "Quầy thuốc Lan Thanh",
   "address": "Số 182, tổ 11, ấp Tân Chủ, xã Tân Thuận Tây, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4738847,
   "Latitude": 105.5858819
 },
 {
   "STT": 989,
   "Name": "Quầy thuốc Diễm Phương",
   "address": "Số 182/B, ấp Bình Hòa, xã BìnhThành, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5766484,
   "Latitude": 105.5591615
 },
 {
   "STT": 990,
   "Name": "Quầy thuốc Quốc Đạt",
   "address": "Số 182A, ấp Bình Hiệp A, xã Bình Thạnh Trung, huyện  Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3803603,
   "Latitude": 105.5591615
 },
 {
   "STT": 991,
   "Name": "Nhà thuốc Phước Ngọc Mai",
   "address": "Số 183 Mai Văn Khải, xã Mỹ Tân, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4723518,
   "Latitude": 105.6243436
 },
 {
   "STT": 992,
   "Name": "Quầy thuốc Phương Thảo",
   "address": "Số 183, ấp Hưng Thạnh Đông, xã Long Hưng B, huyện  Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3264445,
   "Latitude": 105.6588485
 },
 {
   "STT": 993,
   "Name": "Quầy thuốc Hữu Trí",
   "address": "Số 185 lô B, KDCxã Hưng Thạnh, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.6626295,
   "Latitude": 105.6940454
 },
 {
   "STT": 994,
   "Name": "Quầy thuốc Tú Xuân",
   "address": "Số 185, ấp 1, xã Gáo Giồng, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.6051812,
   "Latitude": 105.6369188
 },
 {
   "STT": 995,
   "Name": "Quầy thuốc Xuân Diễm",
   "address": "Số 185A, khóm 2, thị trấn Lai Vung, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2938059,
   "Latitude": 105.6588485
 },
 {
   "STT": 996,
   "Name": "Nhà thuốc Vĩnh Phöc",
   "address": "Số 186 Trần Hưng Đạo, khóm 1, phường 1, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2956713,
   "Latitude": 105.7665921
 },
 {
   "STT": 997,
   "Name": "Quầy thuốc Mai Trăm",
   "address": "Số 188, đường ĐT 843, ấp 2, xã Tân Thành B, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8697347,
   "Latitude": 105.494688
 },
 {
   "STT": 998,
   "Name": "Quầy thuốc Nhân Tâm",
   "address": "Số 188, khóm Tân Đông B, thị trấn Thanh Bình, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5554601,
   "Latitude": 105.491758
 },
 {
   "STT": 999,
   "Name": "Cơ Sở thuốc Đông YThạnh Phát",
   "address": "Số 19 Nguyễn Du, phường 2, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4544666,
   "Latitude": 105.6366184
 },
 {
   "STT": 1000,
   "Name": "Quầy thuốc Thu Thủy",
   "address": "Số 19, khóm Mỹ Tây, thị trấn Mỹ Thọ, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4453693,
   "Latitude": 105.6995299
 },
 {
   "STT": 1001,
   "Name": "Nhà thuốc Trực Loan",
   "address": "Số 190, đường Ngô Thời Nhậm, phường 2, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4571912,
   "Latitude": 105.6414503
 },
 {
   "STT": 1002,
   "Name": "Quầy thuốc Thanh Vân",
   "address": "Số 191, ấp 1, xã Phong Mỹ, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.5318166,
   "Latitude": 105.5826123
 },
 {
   "STT": 1003,
   "Name": "Quầy thuốc Cao Hằng",
   "address": "Số 191, ấp Tân Hòa, xã Tân Nhuận Đông, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2422003,
   "Latitude": 105.8231588
 },
 {
   "STT": 1004,
   "Name": "Quầy thuốc Lê Thanh",
   "address": "Số 192, khóm 2, thị trấn Lai Vung, huyện Lai Vung , TỈNH GIA LAI",
   "Longtitude": 10.2938059,
   "Latitude": 105.6588485
 },
 {
   "STT": 1005,
   "Name": "Quầy thuốc Minh Phúc",
   "address": "Số 193, tổ 5, ấp Tân Mỹ, xã Tân Phú Trung, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2462649,
   "Latitude": 105.7409852
 },
 {
   "STT": 1006,
   "Name": "Nhà thuốc Ngân Khoa",
   "address": "Số 193A, đường 30/4, phường 1,thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4606016,
   "Latitude": 105.6429871
 },
 {
   "STT": 1007,
   "Name": "Quầy thuốc Mỹ Hạnh",
   "address": "Số 193E, ấp Khánh Nhơn, xã Tân Khánh Trung, huyện  Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3791189,
   "Latitude": 105.6940454
 },
 {
   "STT": 1008,
   "Name": "Cơ Sở thuốc Đông Y Lâm Tế Sanh",
   "address": "Số 194, tổ 5, ấp Mỹ Hưng Hòa, xã Mỹ Xương, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.394795,
   "Latitude": 105.7255132
 },
 {
   "STT": 1009,
   "Name": "Quầy thuốc Mỹ Duyên",
   "address": "Số 194/4, ấp Hòa Tân, xã Tân Hòa, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2074033,
   "Latitude": 105.6471178
 },
 // {
 //   "STT": 1010,
 //   "Name": "Quầy thuốc Mỹ Anh",
 //   "address": "Số 197, đường 1/5, khóm 2, thị trấn Tràm Chim, huyện Tam Nông, TỈNH GIA LAI",
 //   "Longtitude": 13.8006854,
 //   "Latitude": 109.1474137
 // },
 {
   "STT": 1011,
   "Name": "Nhà thuốc Phượng Hoàng",
   "address": "Số 197, đường30/4, phường 1, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4606016,
   "Latitude": 105.6429871
 },
 {
   "STT": 1012,
   "Name": "Quầy thuốc Bích Ngọc",
   "address": "Số 199, ấp Trung, xã Tân Quới, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.6567686,
   "Latitude": 105.3775245
 },
 {
   "STT": 1013,
   "Name": "Quầy thuốc Khánh Linh",
   "address": "Số 199A, ấp Tân Thành, xã HòaThành, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2914895,
   "Latitude": 105.7057792
 },
 {
   "STT": 1014,
   "Name": "Cơ Sở thuốc Đông Y Nhơn Hõa Đường",
   "address": "Số 19C, đường Trần Hưng Đạo, thị trấn Thanh Bình, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5554601,
   "Latitude": 105.491758
 },
 {
   "STT": 1015,
   "Name": "Quầy thuốc Gia Mẫn",
   "address": "Số 2, ấp Long Định, xã LongThắng, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2475292,
   "Latitude": 105.6773627
 },
 {
   "STT": 1016,
   "Name": "Nhà thuốc Hồng Diễm",
   "address": "Số 2, đường Lê Lợi, phường An Thạnh, thị xã Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8079445,
   "Latitude": 105.3419365
 },
 // {
 //   "STT": 1017,
 //   "Name": "Quầy thuốc Huy Thảo",
 //   "address": "Số 2, tổ 18, ấp Hòa Khánh, xã Hòa An, thành phố Cao Lãnh, TỈNH GIA LAI",
 //   "Longtitude": 15.9128998,
 //   "Latitude": 79.7399875
 // },
 {
   "STT": 1018,
   "Name": "Quầy thuốc Ngọc An",
   "address": "Số 20 lô G, KDCchợ Nha Mân, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2547739,
   "Latitude": 105.8155181
 },
 {
   "STT": 1019,
   "Name": "Quầy thuốc Bệnh Viện Đk Huyện Cao Lãnh",
   "address": "Số 20, đường 30/4, khóm Mỹ Tây, thị trấn Mỹ Thọ, huyệnCao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4606016,
   "Latitude": 105.6429871
 },
 {
   "STT": 1020,
   "Name": "Nhà thuốc Phước Bền",
   "address": "Số 2000, tổ 36,khóm 4, phường 11, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4878813,
   "Latitude": 105.5839265
 },
 {
   "STT": 1021,
   "Name": "Quầy thuốc Ngọc Hà 1",
   "address": "Số 200B, ấp 1, xã An Hòa, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.7436984,
   "Latitude": 105.3892372
 },
 {
   "STT": 1022,
   "Name": "Quầy thuốc Hồng Điều",
   "address": "Số 201/6, ấp Tân Mỹ, xã Tân Phước, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.616667,
   "Latitude": 105.516667
 },
 {
   "STT": 1023,
   "Name": "Nhà thuốc Nam Hương",
   "address": "Số 201A, đường Lý Thường Kiệt, khóm 5, phường 1, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2929464,
   "Latitude": 105.7666716
 },
 {
   "STT": 1024,
   "Name": "Quầy thuốc Thạnh Phát",
   "address": "Số 202, ấp Khánh Nhơn, xã Tân Khánh Đông, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.3791189,
   "Latitude": 105.6940454
 },
 {
   "STT": 1025,
   "Name": "Quầy thuốc Hồng Đức",
   "address": "Số 202, ấp Tân Mỹ, xã Tân Phú Trung, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2462649,
   "Latitude": 105.7409852
 },
 {
   "STT": 1026,
   "Name": "Quầy thuốc Ngọc Öt",
   "address": "Số 202, đường 847, tổ 4, ấp Mỹ Đông Bốn, xã Mỹ Thọ, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4453778,
   "Latitude": 105.7076956
 },
 {
   "STT": 1027,
   "Name": "Quầy thuốc Đoàn Đa",
   "address": "Số 202, lô G chợ Lai Vung, khóm 3, thị trấn Lai Vung, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2884478,
   "Latitude": 105.6588858
 },
 {
   "STT": 1028,
   "Name": "Quầy thuốc Hải Âu",
   "address": "Số 204 đường Hòa Tây, xã Hòa An,thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4682639,
   "Latitude": 105.611352
 },
 {
   "STT": 1029,
   "Name": "Nhà thuốc Trần A Dõn",
   "address": "Số 205, đường Mai Văn Khải, xã Mỹ Tân, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4720372,
   "Latitude": 105.6262242
 },
 {
   "STT": 1030,
   "Name": "Nhà thuốc Nam Huy",
   "address": "Số 206, đường Điện Biên Phủ, phường Mỹ Phú,thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4681883,
   "Latitude": 105.6451373
 },
 {
   "STT": 1031,
   "Name": "Quầy thuốc Như Ý",
   "address": "Số 207 Nguyễn Huệ, khóm 2, thị trấn  Sa Rài, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8752042,
   "Latitude": 105.4600605
 },
 {
   "STT": 1032,
   "Name": "Quầy thuốc Ngọc Mai 1",
   "address": "Số 207, ấp Tân Mỹ, xã Tân Phú Trung, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2462649,
   "Latitude": 105.7409852
 },
 {
   "STT": 1033,
   "Name": "Quầy thuốc Mỹ Linh",
   "address": "Sô 207A/1, ấp Tân Lợi, xã Phong Hòa, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.335001,
   "Latitude": 105.7057792
 },
 {
   "STT": 1034,
   "Name": "Quầy thuốc Nguyễn Chơn",
   "address": "Số 208B/5, ấp Tân Thuận, xã Tân Phước, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.8390414,
   "Latitude": 105.5591615
 },
 {
   "STT": 1035,
   "Name": "Cơ Sở thuốc Đông Y Hạnh Xuân Hòa",
   "address": "Số 209 NguyễnHuệ, khóm 1, phường 2, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2929053,
   "Latitude": 105.7690066
 },
 {
   "STT": 1036,
   "Name": "Cơ Sở thuốc Đông YThuận Phước",
   "address": "Số 21 Nguyễn Du, phường 2, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4544666,
   "Latitude": 105.6366184
 },
 {
   "STT": 1037,
   "Name": "Cơ Sở thuốc Đông Y Thịnh Phát Đường",
   "address": "Số 21, ấp 1, xã Tân Hội Trung, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4569711,
   "Latitude": 105.7703287
 },
 {
   "STT": 1038,
   "Name": "Quầy thuốc Ngọc Thöy",
   "address": "Số 210, ấp Bình Chánh, xã Bình Thành, huyệnThanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.3409275,
   "Latitude": 105.5000447
 },
 {
   "STT": 1039,
   "Name": "Nhà thuốc Lai Vung",
   "address": "Số 210, Nguyễn Sinh Sắc, khóm Hòa Khánh, phường 2, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2891552,
   "Latitude": 105.7712806
 },
 {
   "STT": 1040,
   "Name": "Nhà thuốc Thiên Trang",
   "address": "Số 211 Mai Văn Khải, tổ 7, ấp 3, xã Mỹ Tân, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4715756,
   "Latitude": 105.6253052
 },
 {
   "STT": 1041,
   "Name": "Công ty TNHH Thương Mại Dược Phẩm Chân ThiênPhúc",
   "address": "Số 211 Phạm Ngọc Thạch, khóm Hòa Khánh, phường 2, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2878298,
   "Latitude": 105.7696067
 },
 {
   "STT": 1042,
   "Name": "Quầy thuốc Phạm Huỳnh Khánh Ngọc",
   "address": "Số 211A, tổ 4, ấp Tân Dân, xã Tân Thuận Tây, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4738847,
   "Latitude": 105.5858819
 },
 {
   "STT": 1043,
   "Name": "Quầy thuốc Minh Nguyệt",
   "address": "Số 213 A1, ấp Tân Lợi, xã Phong Hòa, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.1833808,
   "Latitude": 105.6940454
 },
 {
   "STT": 1044,
   "Name": "Quầy thuốc Thanh Tùng",
   "address": "Số 213, ấp Khánh Nhơn, xã Tân Khánh Đông,thành phốSa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.3791189,
   "Latitude": 105.6940454
 },
 {
   "STT": 1045,
   "Name": "Quầy thuốc Thành Phát",
   "address": "Số 214, ấp Bình Hiệp A, xã Bình Thạnh Trung, huyện  Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3803603,
   "Latitude": 105.5591615
 },
 {
   "STT": 1046,
   "Name": "Nhà thuốc Minh Huy",
   "address": "Số 214, QL 30, tổ36, khóm 4,phường 11, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4608398,
   "Latitude": 105.6304415
 },
 {
   "STT": 1047,
   "Name": "Quầy thuốc Tiến Đạt",
   "address": "Số 217, ấp Khánh Nhơn, xã Tân Khánh Đông, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.3791189,
   "Latitude": 105.6940454
 },
 {
   "STT": 1048,
   "Name": "Quầy thuốc Nguyễn Phöc Thịnh",
   "address": "Số 217A/1, ấp Tân Lợi, xã Phong Hòa, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.335001,
   "Latitude": 105.7057792
 },
 {
   "STT": 1049,
   "Name": "Nhà thuốc Vĩnh Lộc",
   "address": "Số 218 Trần Phú, khóm Tân Bình, phường An Hòa, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2933135,
   "Latitude": 105.7560846
 },
 {
   "STT": 1050,
   "Name": "Nhà thuốc Đức Hồng",
   "address": "Số 22, đường Lê Lợi, khóm 2, phường An Thạnh, thị xã Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8081832,
   "Latitude": 105.3461308
 },
 {
   "STT": 1051,
   "Name": "Quầy thuốc Nguyễn Phương",
   "address": "Số 221B, ấp An Hòa Nhì, xã Tân Bình, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.7436984,
   "Latitude": 105.3892372
 },
 {
   "STT": 1052,
   "Name": "Quầy thuốc Thiên Lộc",
   "address": "Số 223A, Nguyễn Huệ, thị trấn Sa Rài, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8741628,
   "Latitude": 105.4615295
 },
 {
   "STT": 1053,
   "Name": "Nhà thuốc Hải Chinh",
   "address": "Số 224 đường 30/4, phường 1, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4606016,
   "Latitude": 105.6429871
 },
 // {
 //   "STT": 1054,
 //   "Name": "Nhà thuốc Cẩm Lìl",
 //   "address": "Số 228/6, đường 1/6, khóm 2, phường An Thạnh, thị xã Hồng Ngự, TỈNH GIA LAI",
 //   "Longtitude": 10.9680413,
 //   "Latitude": 106.9072509
 // },
 {
   "STT": 1055,
   "Name": "Quầy thuốc Kim Nhiệm",
   "address": "Số 229, ấp Tây, xã Tân Thạnh, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5761773,
   "Latitude": 105.4595306
 },
 {
   "STT": 1056,
   "Name": "Quầy thuốc Minh Hiếu",
   "address": "Số 229A/4, ấp Thới Hòa, xã Vĩnh Thới, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2405852,
   "Latitude": 105.6353878
 },
 {
   "STT": 1057,
   "Name": "Quầy thuốc Hiểu Hiểu",
   "address": "Số 229C, ấp Tân Thới, xã Tân Quới, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.6989054,
   "Latitude": 105.3689504
 },
 {
   "STT": 1058,
   "Name": "Quầy thuốc Minh Quang",
   "address": "Số 23 Nguyễn Văn Voi, tổ 12, khóm Phú Mỹ Hiệp, thị trấn Cái Tàu Hạ, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2585778,
   "Latitude": 105.8707367
 },
 {
   "STT": 1059,
   "Name": "Quầy thuốc Hồng Dung",
   "address": "Số 23, tổ 1, ấp Bình Nhứt, xã NhịMỹ, huyện CaoLãnh, TỈNH GIA LAI",
   "Longtitude": 10.4823086,
   "Latitude": 105.6999122
 },
 {
   "STT": 1060,
   "Name": "Nhà thuốc Tuyết Nga",
   "address": "Số 230 HùngVương, khóm 1, phường 1, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2982561,
   "Latitude": 105.763274
 },
 {
   "STT": 1061,
   "Name": "Quầy thuốc Loan Anh",
   "address": "Số 230/5, ấp Tân An, xã Phong Hòa, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.1833808,
   "Latitude": 105.6940454
 },
 {
   "STT": 1062,
   "Name": "Quầy thuốc Nguyễn Thị Định",
   "address": "Số 231, ấp Mỹ Thới, xã MỹXương, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4111988,
   "Latitude": 105.7057792
 },
 {
   "STT": 1063,
   "Name": "Quầy thuốc Ngọc Hiển",
   "address": "Số 231, ấp Phú Thuận, xã Tân Phú Đông, thị xã Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2788747,
   "Latitude": 105.7409852
 },
 {
   "STT": 1064,
   "Name": "Quầy thuốc Tuyết Trinh",
   "address": "Số 235, ấp An Ninh, xã Định An, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3219082,
   "Latitude": 105.5298525
 },
 {
   "STT": 1065,
   "Name": "Quầy thuốc Công Đức",
   "address": "Số 235, ấp Tân Hội, xã Tân Long, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5790949,
   "Latitude": 105.4009508
 },
 {
   "STT": 1066,
   "Name": "Quầy thuốc Ý Nhi 1",
   "address": "Số 235, ấp Tân Trong, xã Tân Mỹ, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3923418,
   "Latitude": 105.6471178
 },
 // {
 //   "STT": 1067,
 //   "Name": "Quầy thuốc Ngọc Thành",
 //   "address": "Số 237, ấp Phú Yên, xã An Long, huyện Tam Nông, TỈNH GIA LAI",
 //   "Longtitude": 13.0881861,
 //   "Latitude": 109.0928764
 // },
 // {
 //   "STT": 1068,
 //   "Name": "Quầy thuốc Hữu Khá",
 //   "address": "Số 238, ấp Bình Tân, xã BìnhThạnh, huyện Cao Lãnh, TỈNH GIA LAI",
 //   "Longtitude": 10.7652581,
 //   "Latitude": 106.6038535
 // },
 {
   "STT": 1069,
   "Name": "Quầy thuốc Ngọc Tình",
   "address": "Số 238, ấp Long Bửu, xã Hòa Long, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2611796,
   "Latitude": 105.6588485
 },
 {
   "STT": 1070,
   "Name": "Quầy thuốc Diệp Thanh Ngân",
   "address": "Số 24 Đường số 02 ấp Tân Bình, xãTân Thành, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2595462,
   "Latitude": 105.5896933
 },
 {
   "STT": 1071,
   "Name": "Quầy thuốc Nguyễn Phi Hùng",
   "address": "Số 242, ấp Khánh Mỹ A, xã Tân Khánh Trung, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.385244,
   "Latitude": 105.6735354
 },
 {
   "STT": 1072,
   "Name": "Quầy thuốc Nhân Ái 242",
   "address": "Số 242, tổ 8, ấp Đông Bình, xã Hòa An, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.516667,
   "Latitude": 105.55
 },
 {
   "STT": 1073,
   "Name": "Quầy thuốc Thanh Nhi",
   "address": "Số 243/5, ấp Tân An, xã Phong Hòa, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.1833808,
   "Latitude": 105.6940454
 },
 {
   "STT": 1074,
   "Name": "Quầy thuốc Diễm Trinh",
   "address": "Số 243A/6, ấp Tân Mỹ, xã Tân Phước, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.616667,
   "Latitude": 105.516667
 },
 {
   "STT": 1075,
   "Name": "Quầy thuốc Hồ Văn Tình",
   "address": "Số 244, tổ 10, xã Thường Thới Hậu A, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8810796,
   "Latitude": 105.2721456
 },
 {
   "STT": 1076,
   "Name": "Nhà thuốc Hòa Khánh",
   "address": "Số 244A Nguyễn Sinh Sắc, khóm Hòa Khánh, phường 2, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.289707,
   "Latitude": 105.7707051
 },
 {
   "STT": 1077,
   "Name": "Quầy thuốc Huy Trang",
   "address": "Số 246A, đường Mai Văn Khải, xã Mỹ Tân, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4720372,
   "Latitude": 105.6262242
 },
 {
   "STT": 1078,
   "Name": "Quầy thuốc Tám Hạnh",
   "address": "Số 247, ấp Tân Bình, xã Tân Nhuận Đông, huyện  Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2553925,
   "Latitude": 105.776198
 },
 {
   "STT": 1079,
   "Name": "Quầy thuốc Số 49",
   "address": "Số 247, khóm Tân Đông B, thị trấn Thanh Bình, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5554601,
   "Latitude": 105.491758
 },
 {
   "STT": 1080,
   "Name": "Nhà thuốc Phạm Quốc Anh",
   "address": "Số 247A Nguyễn Thái Học, khóm 3, phường 4, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4520583,
   "Latitude": 105.6336107
 },
 {
   "STT": 1081,
   "Name": "Nhà thuốc Thiên Thảo",
   "address": "Số 247B, TỈNH GIA LAI lộ 848, khóm Tân An, phường An Hòa,thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2904838,
   "Latitude": 105.7513093
 },
 {
   "STT": 1082,
   "Name": "Cơ Sở thuốc Đông Y Vạn Đức",
   "address": "Số 249 NguyễnHuệ, khóm 1, phường 2, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2938061,
   "Latitude": 105.7686714
 },
 {
   "STT": 1083,
   "Name": "Quầy thuốc Hồng Duyên",
   "address": "Số 24A, khóm 3, thị trấn Lai Vung, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2938059,
   "Latitude": 105.6588485
 },
 {
   "STT": 1084,
   "Name": "Quầy thuốc Yến Phương",
   "address": "Số 24A1, tổ 9, ấp Hòa Mỹ, xã Hòa An, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4622676,
   "Latitude": 105.6086621
 },
 {
   "STT": 1085,
   "Name": "Cơ Sở thuốc Đông Y Vĩnh Đức Đường",
   "address": "Số 25, đường Nguyễn Du, phường 2, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4544259,
   "Latitude": 105.6366455
 },
 {
   "STT": 1086,
   "Name": "Quầy thuốc Quang Hiền",
   "address": "Số 25/D Nguyễn Văn Tre, khóm 4, thị trấn Mỹ An, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5188472,
   "Latitude": 105.844785
 },
 {
   "STT": 1087,
   "Name": "Quầy thuốc Văn Tình",
   "address": "Số 250B/2, ấp Long Khánh, xã Long Hậu, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2846597,
   "Latitude": 105.6236585
 },
 {
   "STT": 1088,
   "Name": "Quầy thuốc Hoàng Trí",
   "address": "Số 252/1, đường ĐT 848, ấp Khánh Nhơn, xã Tân Khánh Đông, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.3443488,
   "Latitude": 105.7363114
 },
 {
   "STT": 1089,
   "Name": "Quầy thuốc Tây Trung Vĩnh",
   "address": "Số 255, ấp 5, xã Phương Thịnh, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.5983142,
   "Latitude": 105.6705801
 },
 {
   "STT": 1090,
   "Name": "Quầy thuốc Thái Nghị",
   "address": "Số 255, ấp Bưng Sấm, xã Tân Công Sính, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.7272256,
   "Latitude": 105.6060661
 },
 {
   "STT": 1091,
   "Name": "Quầy thuốc Gia Huy",
   "address": "Số 255A/1, ấp Tân Lợi, xã Phong Hòa, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.335001,
   "Latitude": 105.7057792
 },
 {
   "STT": 1092,
   "Name": "Quầy thuốc Trúc Linh",
   "address": "Số 257, QL30, ấpBình Chánh, xã Bình Thành, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5315952,
   "Latitude": 105.5454561
 },
 {
   "STT": 1093,
   "Name": "Quầy thuốc Nam Hiệp",
   "address": "Số 258 Mai Văn Khải, xã Mỹ Tân, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4723533,
   "Latitude": 105.624342
 },
 {
   "STT": 1094,
   "Name": "Nhà thuốc Minh Phúc",
   "address": "Số 258, đường Trần Hưng Đạo, khóm 1, phường 1, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2970333,
   "Latitude": 105.7656528
 },
 {
   "STT": 1095,
   "Name": "Quầy thuốc Cao Phương Anh",
   "address": "Số 258, tổ 7, ấp Đông Thạnh, xã Tân Thuận Đông,thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4549723,
   "Latitude": 105.6340352
 },
 {
   "STT": 1096,
   "Name": "Quầy thuốc Bảo Nhi",
   "address": "Số 259, tổ 11, ấp 2, xã Phong Mỹ, huyện  Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.5318166,
   "Latitude": 105.5826123
 },
 {
   "STT": 1097,
   "Name": "Quầy thuốc Nhật Anh",
   "address": "Số 26, tổ 1, ấp An Nghiệp, xã AnBình, huyện CaoLãnh, TỈNH GIA LAI",
   "Longtitude": 10.4659577,
   "Latitude": 105.7059785
 },
 {
   "STT": 1098,
   "Name": "Cơ Sở thuốc Đông Y Vạn Xuân Đường",
   "address": "Số 26, tổ 11, khóm2, phường 6, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4288288,
   "Latitude": 105.6343903
 },
 {
   "STT": 1099,
   "Name": "Quầy thuốc Nguyễn Nhàn",
   "address": "Số 26/3B-E5 ấp An Lợi, xã Định Yên, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.309874,
   "Latitude": 105.5532993
 },
 {
   "STT": 1100,
   "Name": "Quầy thuốc Phöc Triết",
   "address": "Số 26/D đường Nguyễn Văn Tre, khóm 4, thị trấn Mỹ An, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5187487,
   "Latitude": 105.8449414
 },
 {
   "STT": 1101,
   "Name": "Quầy thuốc Xuân Mai",
   "address": "Số 260, ấp Tân Thuận B, xã TânMỹ, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3923418,
   "Latitude": 105.6471178
 },
 {
   "STT": 1102,
   "Name": "Quầy thuốc Diễm Phương",
   "address": "Số 260/TH, ấp Tân Hưng, xã TânThành, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2536944,
   "Latitude": 105.6196806
 },
 // {
 //   "STT": 1103,
 //   "Name": "Quầy thuốc Thúy Duyên",
 //   "address": "Số 261, tổ 1, ấp Tân Hòa, xã Tân Nhuận Đông, huyện  Châu Thành, TỈNH GIA LAI",
 //   "Longtitude": 10.2422003,
 //   "Latitude": 105.8231588
 // },
 {
   "STT": 1104,
   "Name": "Quầy thuốc Phương Hà",
   "address": "Số 261, tổ 7, ấp Đông Thạnh, xã Tân Thuận Đông,thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4549723,
   "Latitude": 105.6340352
 },
 {
   "STT": 1105,
   "Name": "Quầy thuốc Duy Kha",
   "address": "Số 261, xã An Bình, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4596806,
   "Latitude": 105.6617588
 },
 {
   "STT": 1106,
   "Name": "Quầy thuốc Phong Ngọc",
   "address": "Số 265, ấp Phú Hòa B, xã Phú Thuận A, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.7570042,
   "Latitude": 105.3423909
 },
 // {
 //   "STT": 1107,
 //   "Name": "Quầy thuốc Huỳnh Hạnh",
 //   "address": "Số 268, tổ 2, khóm Phú Mỹ Thành,thị trấn Cái Tàu Hạ, huyện Châu Thành, TỈNH GIA LAI",
 //   "Longtitude": 10.7161298,
 //   "Latitude": 106.7411559
 // },
 {
   "STT": 1108,
   "Name": "Nhà thuốc Gia Phát 269",
   "address": "Số 269, tổ 18,khóm 2, phường 6, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4288288,
   "Latitude": 105.6343903
 },
 {
   "STT": 1109,
   "Name": "Nhà thuốc Thiên Ân",
   "address": "Số 269, tổ 9, khóm Mỹ Thượng, phường Mỹ Phú,thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4615518,
   "Latitude": 105.6444979
 },
 {
   "STT": 1110,
   "Name": "Cơ Sở thuốc Đông Y TếLương Đường6",
   "address": "Số 27 lô A, khóm 3, thị trấn Lai Vung, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2938059,
   "Latitude": 105.6588485
 },
 {
   "STT": 1111,
   "Name": "Nhà thuốc Thảo Hương",
   "address": "Số 27, đường Lê Hồng Phong, phường 1, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4587225,
   "Latitude": 105.6358903
 },
 {
   "STT": 1112,
   "Name": "Nhà thuốc Thanh Tân",
   "address": "Số 27/5, tổ 54, khóm 5, phường 11, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4878813,
   "Latitude": 105.5839265
 },
 {
   "STT": 1113,
   "Name": "Quầy thuốc Hiến Phụng",
   "address": "Số 27/D, Nguyễn Văn Tre, khóm 4, thị trấn Mỹ An, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5188736,
   "Latitude": 105.8448279
 },
 {
   "STT": 1114,
   "Name": "Quầy thuốc Đông Hõa",
   "address": "Số 271, tổ 9, ấp Đông Hòa, xã Tân Thuận Đông, thành phốCao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.516667,
   "Latitude": 105.55
 },
 {
   "STT": 1115,
   "Name": "Quầy thuốc Tuyết Đào",
   "address": "Số 271/B, tổ 12, ấp 2, xã Mỹ Ngãi, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.5034079,
   "Latitude": 105.590733
 },
 {
   "STT": 1116,
   "Name": "Quầy thuốc Cường Thịnh",
   "address": "Số 271E/2, ấp Hòa Ninh, xã LongThắng, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2428539,
   "Latitude": 105.6999122
 },
 {
   "STT": 1117,
   "Name": "Quầy thuốc Châu Anh",
   "address": "Số 272 Nguyễn Trãi, khóm MỹTây, thị trấn Mỹ Thọ, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4460063,
   "Latitude": 105.6961101
 },
 {
   "STT": 1118,
   "Name": "Quầy thuốc Hòa Chi",
   "address": "Số 272/A, tổ 6, ấp Tịnh Châu, xã Tịnh Thới, thành phố CaoLãnh, TỈNH GIA LAI",
   "Longtitude": 10.4324174,
   "Latitude": 105.6488796
 },
 {
   "STT": 1119,
   "Name": "Quầy thuốc Thanh Thúy",
   "address": "Số 274/C, ấp Hòa Quới, xã Hòa Tân, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.1719042,
   "Latitude": 105.8172881
 },
 {
   "STT": 1120,
   "Name": "Quầy thuốc Thành Đạt",
   "address": "Số 275, ấp Bắc, xã Tân Thạnh, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5761773,
   "Latitude": 105.4595306
 },
 {
   "STT": 1121,
   "Name": "Quầy thuốc Mai Trinh",
   "address": "Số 275, ấp Bình Chánh, xã Bình Thành, huyệnThanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.3409275,
   "Latitude": 105.5000447
 },
 {
   "STT": 1122,
   "Name": "Quầy thuốc Lương Đình",
   "address": "Số 276/B, ấp Tân An, xã Phong Hòa, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.1833808,
   "Latitude": 105.6940454
 },
 {
   "STT": 1123,
   "Name": "Nhà thuốc Quốc Chi",
   "address": "Số 276A Lê Lợi, khóm 3, phường 3, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.3082341,
   "Latitude": 105.7581765
 },
 {
   "STT": 1124,
   "Name": "Quầy thuốc Kim Huệ",
   "address": "Số 277, ấp Mỹ Thới, xã MỹXương, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4111988,
   "Latitude": 105.7057792
 },
 {
   "STT": 1125,
   "Name": "Nhà thuốc Quang Huy",
   "address": "Số 28, đường Lê Lợi, phường An Thạnh, thị xã Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8079461,
   "Latitude": 105.3424555
 },
 {
   "STT": 1126,
   "Name": "Nhà thuốc Ngọc Hà",
   "address": "Số 28, đường Lê Thị Hồng Gấm,phường 2, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4587225,
   "Latitude": 105.6358903
 },
 // {
 //   "STT": 1127,
 //   "Name": "Cơ Sở thuốc Đông Y Nguyên Lợi Hòa",
 //   "address": "Số 28, tổ 8, khóm Phú Mỹ  Hiệp, thị trấn Cái Tàu Hạ, huyện Châu Thành, TỈNH GIA LAI",
 //   "Longtitude": 10.7161298,
 //   "Latitude": 106.7411559
 // },
 {
   "STT": 1128,
   "Name": "Quầy thuốc Nam Phượng",
   "address": "Số 280 tổ 6, ấp Tân Dân, xã Tân Thuận Tây, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4738847,
   "Latitude": 105.5858819
 },
 {
   "STT": 1129,
   "Name": "Quầy thuốc Đặng Ngọc Khánh Linh",
   "address": "Số 285, đường Lê Duẩn, xã Mỹ Trà, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4722822,
   "Latitude": 105.6357042
 },
 {
   "STT": 1130,
   "Name": "Quầy thuốc Giao Linh",
   "address": "Số 286A/4, ấp Long Hưng 2, xã Long Hậu, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2846597,
   "Latitude": 105.6236585
 },
 {
   "STT": 1131,
   "Name": "Quầy thuốc Trung Hiếu",
   "address": "Số 289, đường 30/4, thị trấn ThanhBình, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.8113762,
   "Latitude": 105.3423943
 },
 {
   "STT": 1132,
   "Name": "Quầy thuốc Thành Phát",
   "address": "Số 29/4/A, đường Trần Phú, khóm I, thị trấn Mỹ An, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.518376,
   "Latitude": 105.843719
 },
 {
   "STT": 1133,
   "Name": "Quầy thuốc Nam Hy",
   "address": "Số 29/8A, đường Lê Quý Đôn, khóm 1, thị trấn Mỹ An, huyện  Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5237048,
   "Latitude": 105.8467751
 },
 {
   "STT": 1134,
   "Name": "Quầy thuốc Minh Đăng",
   "address": "Số 290, ấp An Thọ, xã An Phước, huyện  Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8154761,
   "Latitude": 105.4588221
 },
 {
   "STT": 1135,
   "Name": "Quầy thuốc Phượng",
   "address": "Số 292, ấp Tân Hội, xã Tân Long, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5790949,
   "Latitude": 105.4009508
 },
 {
   "STT": 1136,
   "Name": "Quầy thuốc Lê Thị Định",
   "address": "Số 295, ấp An Hưng, xã An Khánh, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.1970145,
   "Latitude": 105.8583873
 },
 {
   "STT": 1137,
   "Name": "Quầy thuốc Thùy Trang",
   "address": "Số 295, ấp An Phú, xã Hội An Đông, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.4078862,
   "Latitude": 105.5532993
 },
 // {
 //   "STT": 1138,
 //   "Name": "Quầy thuốc Ngọc Mai",
 //   "address": "Số 295/A, ấp Hòa Bình, xã Hòa Tân, huyện Châu Thành, TỈNH GIA LAI",
 //   "Longtitude": 20.6861265,
 //   "Latitude": 105.3131185
 // },
 // {
 //   "STT": 1139,
 //   "Name": "Quầy thuốc Hoàng Phát",
 //   "address": "Số 297, ấp Phú Yên, xã An Long, huyện Tam Nông, TỈNH GIA LAI",
 //   "Longtitude": 13.0881861,
 //   "Latitude": 109.0928764
 // },
 {
   "STT": 1140,
   "Name": "Quầy thuốc Minh Nhựt",
   "address": "Số 297, ấp Tân Thuận B, xã TânMỹ, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3923418,
   "Latitude": 105.6471178
 },
 {
   "STT": 1141,
   "Name": "Quầy thuốc Nguyễn Nga",
   "address": "Số 29A, tổ 13, ấp 4, xã Mỹ Tân, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4885898,
   "Latitude": 105.6196744
 },
 {
   "STT": 1142,
   "Name": "Nhà thuốc Nam Lợi",
   "address": "Số 2A, đường Lý Thường Kiệt, khóm1, phường 2, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2935491,
   "Latitude": 105.7684721
 },
 {
   "STT": 1143,
   "Name": "Quầy thuốc Hồng Ngoan - Cẩm Hõa",
   "address": "Số 3, ấp Hạ, xã Tân Bình, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.6009459,
   "Latitude": 105.4009508
 },
 {
   "STT": 1144,
   "Name": "Cơ Sở thuốc Đông Y Vạn Lợi Đường",
   "address": "Số 3, đường 3/2, thị trấn Mỹ Thọ, huyệnCao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4501101,
   "Latitude": 105.6970266
 },
 {
   "STT": 1145,
   "Name": "Quầy thuốc Tấn Lộc",
   "address": "Số 3, lô A, chợ Thanh Mỹ, xãThanh Mỹ, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.4072144,
   "Latitude": 105.8579285
 },
 {
   "STT": 1146,
   "Name": "Quầy thuốc Đức Thuận",
   "address": "Số 3, lô E, chợ Nha Mân, xã Tân Nhuận Đông, huyện  Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2698298,
   "Latitude": 105.8291222
 },
 {
   "STT": 1147,
   "Name": "Quầy thuốc Sơn Phương",
   "address": "Số 3, lô G, ấp 5A, xã Trường Xuân, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.6314323,
   "Latitude": 105.7703287
 },
 {
   "STT": 1148,
   "Name": "Cơ Sở thuốc Đông Y Dân Sanh Đường",
   "address": "Số 30 Thiên Hộ Dương, phường An Thạnh, thị xã Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8086256,
   "Latitude": 105.3407815
 },
 {
   "STT": 1149,
   "Name": "Quầy thuốc Khánh Ngọc",
   "address": "Số 30/D Nguyễn Văn Cừ, khóm 4, thị trấn Mỹ An, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5202291,
   "Latitude": 105.8434814
 },
 {
   "STT": 1150,
   "Name": "Quầy thuốc Kim Loan",
   "address": "Số 302, khóm 1, thị trấn Lai Vung, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2876504,
   "Latitude": 105.6604743
 },
 {
   "STT": 1151,
   "Name": "Quầy thuốc Hoàng Linh",
   "address": "Số 303, quốc lộ 30, ấp Mỹ Thới, xã Mỹ Xương, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4127043,
   "Latitude": 105.7183584
 },
 {
   "STT": 1152,
   "Name": "Quầy thuốc Kim Yến",
   "address": "Số 303/2, ấp Hòa Ninh, xã LongThắng, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2428539,
   "Latitude": 105.6999122
 },
 {
   "STT": 1153,
   "Name": "Quầy thuốc Thiên Dung",
   "address": "Số 304, QL 54, ấpAn Hòa, xã Định An, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3168696,
   "Latitude": 105.5302352
 },
 // {
 //   "STT": 1154,
 //   "Name": "Quầy thuốc Kim Quỳnh",
 //   "address": "Số 306, khóm Phú Mỹ Thành, thị trấn Cái Tàu Hạ, huyện Châu Thành, TỈNH GIA LAI",
 //   "Longtitude": 10.7161298,
 //   "Latitude": 106.7411559
 // },
 {
   "STT": 1155,
   "Name": "Quầy thuốc Đặng Vũ Ngân Khánh",
   "address": "Số 307 khu vực 3, ấp Trung, xãThường Thới Tiền, huyện  Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.837225,
   "Latitude": 105.2721456
 },
 {
   "STT": 1156,
   "Name": "Quầy thuốc Toàn Thịnh",
   "address": "Số 307, ấp An Hòa, xã Định An, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3219082,
   "Latitude": 105.5298525
 },
 {
   "STT": 1157,
   "Name": "Nhà thuốc Thiện Phöc 2",
   "address": "Số 307, đường Nguyễn Sinh Sắc, khóm 2, phường 2, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2902697,
   "Latitude": 105.7626829
 },
 // {
 //   "STT": 1158,
 //   "Name": "Quầy thuốc Y Dược",
 //   "address": "Số 307/TX, ấp 5A,xã Trường Xuân, huyện Tháp Mười, TỈNH GIA LAI",
 //   "Longtitude": 31.9685988,
 //   "Latitude": -99.9018131
 // },
 {
   "STT": 1159,
   "Name": "Quầy thuốc Thái Bảo",
   "address": "Số 307A/2, ấp Hòa Ninh, xã LongThắng, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2428539,
   "Latitude": 105.6999122
 },
 {
   "STT": 1160,
   "Name": "Quầy thuốc Đình Chi",
   "address": "Số 308A/2, ấp Long Khánh, xã Long Hậu, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2846597,
   "Latitude": 105.6236585
 },
 {
   "STT": 1161,
   "Name": "Quầy thuốc Mai Khôi",
   "address": "Số 309, khóm Bình Thạnh 1, thị trấn Lấp Vò, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3577289,
   "Latitude": 105.5210607
 },
 {
   "STT": 1162,
   "Name": "Nhà thuốc Bệnh Viện Mắt QuangĐức",
   "address": "Số 31 HùngVương, phường 2, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2883786,
   "Latitude": 105.766856
 },
 {
   "STT": 1163,
   "Name": "Quầy thuốc Đắc Khoa",
   "address": "Số 31, đường Xẻo Quýt, thị trấn Mỹ Thọ, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4452756,
   "Latitude": 105.6958258
 },
 {
   "STT": 1164,
   "Name": "Nhà thuốc Hoàng Ngọc",
   "address": "Số 310/3, đường Nguyễn Thị Minh Khai, phường 1,thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4666728,
   "Latitude": 105.6284646
 },
 {
   "STT": 1165,
   "Name": "Quầy thuốc Thanh Tuấn",
   "address": "Số 310A, ấp Tân Thành, xã HòaThành, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2914895,
   "Latitude": 105.7057792
 },
 {
   "STT": 1166,
   "Name": "Cơ Sở thuốc Đông Y Quãng Triều Thái",
   "address": "Số 311 Nguyễn Huệ, khóm 1, phường 1, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2954092,
   "Latitude": 105.7675994
 },
 {
   "STT": 1167,
   "Name": "Quầy thuốc Như Phương",
   "address": "Số 311, ấp Tân Hòa Đông, xã Tân Mỹ, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3923418,
   "Latitude": 105.6471178
 },
 {
   "STT": 1168,
   "Name": "Quầy thuốc 312",
   "address": "Số 312 đường Lê Duẩn, xã Mỹ Trà, TP Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4674473,
   "Latitude": 105.6395797
 },
 {
   "STT": 1169,
   "Name": "Quầy thuốc Tröc Phương",
   "address": "Số 312, tổ 9, ấp Hòa Long, xã Hòa An, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4622676,
   "Latitude": 105.6086621
 },
 {
   "STT": 1170,
   "Name": "Quầy thuốc Bích Ngọc",
   "address": "Số 314, ấp Tân Dinh, xã Tân Hòa, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.668875,
   "Latitude": 105.3541013
 },
 {
   "STT": 1171,
   "Name": "Quầy thuốc Ba Thuận",
   "address": "Số 316/B, ấp 3, xã Tân Kiều, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.507924,
   "Latitude": 105.8992666
 },
 {
   "STT": 1172,
   "Name": "Quầy thuốc Thiên Phú",
   "address": "Số 319, ấp A, xã Phú Cường, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.6616491,
   "Latitude": 105.6060661
 },
 {
   "STT": 1173,
   "Name": "Quầy thuốc Mỹ Tiên",
   "address": "Số 319A, khóm 2, thị trấn Lai Vung, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2938059,
   "Latitude": 105.6588485
 },
 {
   "STT": 1174,
   "Name": "Chi Nhánh GIA LAI - Công ty Cp Xnk Y TếDomesco",
   "address": "Số 32 Lê Anh Xuân, khóm 2, phường 2, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4554888,
   "Latitude": 105.6387844
 },
 {
   "STT": 1175,
   "Name": "Quầy thuốc Phương Lan",
   "address": "Số 320, ấp Tân An, xã Tân Bình, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2185915,
   "Latitude": 105.7506635
 },
 {
   "STT": 1176,
   "Name": "Quầy thuốc Nhân Tâm",
   "address": "Số 320/1, tổ 6, ấp Tân Phú, xã An Nhơn, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2462649,
   "Latitude": 105.7409852
 },
 {
   "STT": 1177,
   "Name": "Quầy thuốc Thu Hoài",
   "address": "Số 323, ấp An Lợi A, xã Định Yên, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.309874,
   "Latitude": 105.5532993
 },
 {
   "STT": 1178,
   "Name": "Quầy thuốc Chí Thiện",
   "address": "Số 323, ấp Long Thạnh B, xã Long Khánh A, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.7982508,
   "Latitude": 105.311655
 },
 {
   "STT": 1179,
   "Name": "Quầy thuốc Trường An 2",
   "address": "Số 323A, đường Nguyễn Huệ, khóm Bình Hòa, thị trấn Lấp Vò, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3605105,
   "Latitude": 105.5231936
 },
 {
   "STT": 1180,
   "Name": "Nhà thuốc Hoa Lài",
   "address": "Số 326, đường30/4, phường 1, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4606016,
   "Latitude": 105.6429871
 },
 {
   "STT": 1181,
   "Name": "Quầy thuốc Kim Loan Kim",
   "address": "Số 326/J, khóm 5, thị trấn Tràm Chim, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.6691946,
   "Latitude": 105.5650239
 },
 {
   "STT": 1182,
   "Name": "Nhà thuốc Phạm Huỳnh",
   "address": "Số 328 Nguyễn Sinh Sắc, khóm 4, phường 1, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2904171,
   "Latitude": 105.7656059
 },
 {
   "STT": 1183,
   "Name": "Nhà thuốc Huỳnh Ngân",
   "address": "Số 329, đường Phạm Hữu Lầu, phường 6, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4304989,
   "Latitude": 105.6338385
 },
 {
   "STT": 1184,
   "Name": "Nhà thuốc Đức Thành",
   "address": "Số 329, HùngVương, khóm 4, phường 1, thị xã Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2932441,
   "Latitude": 105.7665253
 },
 {
   "STT": 1185,
   "Name": "Quầy thuốc Minh Anh",
   "address": "Số 329, khóm Phú Hòa, thị trấn Cái Tàu Hạ, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2587308,
   "Latitude": 105.8730678
 },
 {
   "STT": 1186,
   "Name": "Quầy thuốc Phượng Thanh",
   "address": "Số 329B, ấp Bình Hiệp A, xã Bình Thạnh Trung, huyện  Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3803603,
   "Latitude": 105.5591615
 },
 {
   "STT": 1187,
   "Name": "Quầy thuốc Minh Tài",
   "address": "Số 32B/3, ấp Định Thành, xã Định Hòa, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.1953911,
   "Latitude": 105.6705801
 },
 {
   "STT": 1188,
   "Name": "Nhà thuốc Vạn Lộc",
   "address": "Số 33 HùngVương, khóm 2, phường An Thạnh, thị xã Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8086206,
   "Latitude": 105.3414684
 },
 {
   "STT": 1189,
   "Name": "Nhà thuốc NguyễnThanh Thuận",
   "address": "Số 33 TrươngĐịnh, phường 1, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4575771,
   "Latitude": 105.6352566
 },
 {
   "STT": 1190,
   "Name": "Quầy thuốc Thắng Lợi",
   "address": "Số 33, chợ Thanh Mỹ, xã Thanh Mỹ, huyện Tháp Mười, TỈNH GIA LAI  Đồng Thap",
   "Longtitude": 10.4072144,
   "Latitude": 105.8579285
 },
 {
   "STT": 1191,
   "Name": "Quầy thuốc Bình Tiên",
   "address": "Số 33, tổ 13, ấp 4, xã Mỹ Tân, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4885898,
   "Latitude": 105.6196744
 },
 {
   "STT": 1192,
   "Name": "Quầy thuốc Bảo Ngọc",
   "address": "Số 330, ấp Tân Thành, xã Tân Qui Tây, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.3120792,
   "Latitude": 105.7292491
 },
 {
   "STT": 1193,
   "Name": "Quầy thuốc Thùy Trang",
   "address": "Số 330/3, ấp Hòa Bình, xã LongThắng, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2428539,
   "Latitude": 105.6999122
 },
 {
   "STT": 1194,
   "Name": "Quầy thuốc Mai Hoàng Duy",
   "address": "Số 331, đường Hòa Tây, xã Hòa An,thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4682639,
   "Latitude": 105.611352
 },
 {
   "STT": 1195,
   "Name": "Quầy thuốc Tâm Đức",
   "address": "Số 333C, ấp Tân Lộc A, xã Tân Dương, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.335001,
   "Latitude": 105.7057792
 },
 {
   "STT": 1196,
   "Name": "Quầy thuốc Phước Đẹp",
   "address": "Số 334, ấp Thị, xã An Phong, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.6325611,
   "Latitude": 105.4243804
 },
 {
   "STT": 1197,
   "Name": "Quầy thuốc Nguyễn Huệ",
   "address": "Số 334,khóm Bình Thạnh 1, thị trấn Lấp Vò, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3577289,
   "Latitude": 105.5210607
 },
 {
   "STT": 1198,
   "Name": "Quầy thuốc Phụng Thư",
   "address": "Số 335, tổ 11, ấp Đông Hòa, xã Tân Thuận Đông, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4250752,
   "Latitude": 105.5893817
 },
 {
   "STT": 1199,
   "Name": "Quầy thuốc Mai Trung",
   "address": "Số 336, ấp Thị, xã An Phong, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.6325611,
   "Latitude": 105.4243804
 },
 {
   "STT": 1200,
   "Name": "Quầy thuốc Nhã Uyên",
   "address": "Số 336, đường Bà Huyện Thanh Quan, xã Hòa An, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4469449,
   "Latitude": 105.6050235
 },
 {
   "STT": 1201,
   "Name": "Quầy thuốc Tuyết Nga",
   "address": "Số 338, ấp An Lợi, xã An Bình A, thị xã Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.7829013,
   "Latitude": 105.3716684
 },
 {
   "STT": 1202,
   "Name": "Quầy thuốc Ngọc An",
   "address": "Số 338, ấp An Phú, xã Hội An Đông, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.4078862,
   "Latitude": 105.5532993
 },
 {
   "STT": 1203,
   "Name": "Quầy thuốc Nguyễn Hằng",
   "address": "Số 338, đường Mai Văn Khải, ấp 3, xã Mỹ Tân, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4799668,
   "Latitude": 105.6188059
 },
 {
   "STT": 1204,
   "Name": "Quầy thuốc Phương Nhi",
   "address": "Số 339, tổ 8, ấp Tân Chủ, xã Tân Thuận Tây, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4738847,
   "Latitude": 105.5858819
 },
 {
   "STT": 1205,
   "Name": "Nhà thuốc Xuân Lộc",
   "address": "Số 34 Hoàng Diệu, khóm 1, phường 1, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2954488,
   "Latitude": 105.7664771
 },
 {
   "STT": 1206,
   "Name": "Quầy thuốc Nguyễn Phượng",
   "address": "Số 34, ấp 1, xã An Bình B, thị xã Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.7966565,
   "Latitude": 105.4243804
 },
 {
   "STT": 1207,
   "Name": "Quầy thuốc Phạm Thị Thùy Trang",
   "address": "Số 341, ấp Tân Thuận B, xã Tân Dương, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.335001,
   "Latitude": 105.7057792
 },
 {
   "STT": 1208,
   "Name": "Quầy thuốc Phan Lý",
   "address": "Số 342/B, ấp Thị, xã An Phong, huyện  Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.6325611,
   "Latitude": 105.4243804
 },
 {
   "STT": 1209,
   "Name": "Quầy thuốc Mỹ Lan",
   "address": "Số 343, ấp 2, xã Phú Lợi, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.6250429,
   "Latitude": 105.4653897
 },
 {
   "STT": 1210,
   "Name": "Quầy thuốc Tuơi Lâm",
   "address": "Số 344, tổ 05, ấp Phú Bình, xã Phú Hựu, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2535915,
   "Latitude": 105.8671954
 },
 {
   "STT": 1211,
   "Name": "Quầy thuốc Trần Phạm Huy Vũ",
   "address": "Số 345, ấp Phú Bình, xã Phú Hựu, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2535915,
   "Latitude": 105.8671954
 },
 {
   "STT": 1212,
   "Name": "Quầy thuốc Thành Vinh",
   "address": "Số 346, ấp Bình An, xã Bình Thành, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3409275,
   "Latitude": 105.5000447
 },
 // {
 //   "STT": 1213,
 //   "Name": "Quầy thuốc Yến Vy",
 //   "address": "Số 347A/4, ấp Hòa Bình, xã Tân Hòa, huyện Lai Vung, TỈNH GIA LAI",
 //   "Longtitude": 20.6861265,
 //   "Latitude": 105.3131185
 // },
 {
   "STT": 1214,
   "Name": "Quầy thuốc Anh Thy",
   "address": "Số 347C/3, ấp Định Thành, xã Định Hòa, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.1953911,
   "Latitude": 105.6705801
 },
 {
   "STT": 1215,
   "Name": "Quầy thuốc Thu Trang",
   "address": "Số 348, ấp An Phong, xã Định An, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3219082,
   "Latitude": 105.5298525
 },
 {
   "STT": 1216,
   "Name": "Quầy thuốc Hường Mai Ii",
   "address": "Số 348, khóm 1, thị trấn Lai Vung, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2954488,
   "Latitude": 105.7664771
 },
 {
   "STT": 1217,
   "Name": "Nhà thuốc Hồ Huỳnh Trân",
   "address": "Số 349 Tôn Đức Thắng, phường Mỹ Phú, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4714976,
   "Latitude": 105.645363
 },
 {
   "STT": 1218,
   "Name": "Quầy thuốc Trần Kim Dung",
   "address": "Số 35, ấp Phú Thạnh, xã PhúLong, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2222167,
   "Latitude": 105.7879371
 },
 {
   "STT": 1219,
   "Name": "Quầy thuốc Vy Đạt (A)",
   "address": "Số 351, tổ 12, ấp Phú Lợi A, xã Phú Thuận B, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.7570042,
   "Latitude": 105.3423909
 },
 // {
 //   "STT": 1220,
 //   "Name": "Quầy thuốc Anh Mỹ",
 //   "address": "Số 353, ấp Bình Định, xã Bình Thành, huyệnThanh Bình, TỈNH GIA LAI",
 //   "Longtitude": 14.1665324,
 //   "Latitude": 108.902683
 // },
 {
   "STT": 1221,
   "Name": "Quầy thuốc Hồng Lâm",
   "address": "Số 353, ấp Hưng Thạnh Đông, xã Long Hưng B, huyện  Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3264445,
   "Latitude": 105.6588485
 },
 {
   "STT": 1222,
   "Name": "Quầy thuốc Huy Hoàng",
   "address": "Số 354, khóm 1, thị trấn Lai Vung, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2938059,
   "Latitude": 105.6588485
 },
 {
   "STT": 1223,
   "Name": "Quầy thuốc Thái Mai",
   "address": "Số 356, ấp Hưng Thạnh Đông, xã Long Hưng B, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3264445,
   "Latitude": 105.6588485
 },
 {
   "STT": 1224,
   "Name": "Quầy thuốc Minh Kha",
   "address": "Số 356A, ấp Khánh An, xã Tân Khánh Trung, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3791189,
   "Latitude": 105.6940454
 },
 {
   "STT": 1225,
   "Name": "Nhà thuốc Mai Lan",
   "address": "Số 357, đường Trần Hưng Đạo, khóm 3, phường 1, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.3056167,
   "Latitude": 105.7575303
 },
 {
   "STT": 1226,
   "Name": "Quầy thuốc Gia Khánh",
   "address": "Số 357/TK, ấp Tân Khánh, xã TânThành, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2598706,
   "Latitude": 105.5924872
 },
 {
   "STT": 1227,
   "Name": "Quầy thuốc Huỳnh Hiếu",
   "address": "Số 363 QL842, ấpTân Bảnh, xã Tân Phước, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8390414,
   "Latitude": 105.5591615
 },
 {
   "STT": 1228,
   "Name": "Cơ Sở thuốc Đông Y Phước LậpĐường",
   "address": "Số 363, khóm 1, thị trấn Lai Vung, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2938059,
   "Latitude": 105.6588485
 },
 {
   "STT": 1229,
   "Name": "Quầy thuốc Ngọc Phượng",
   "address": "Số 363A/5, ấp Tân Thuận, xã Tân Phước, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2972665,
   "Latitude": 105.5884754
 },
 {
   "STT": 1230,
   "Name": "Quầy thuốc Phước An",
   "address": "Số 364, đường Lê Duẩn, xã Mỹ Trà, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4722822,
   "Latitude": 105.6357042
 },
 {
   "STT": 1231,
   "Name": "Quầy thuốc Lê Trung Nguyên",
   "address": "Số 367H/5, ấp Tân Thuận, xã Tân Phước, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.8390414,
   "Latitude": 105.5591615
 },
 {
   "STT": 1232,
   "Name": "Quầy thuốc Út Lan",
   "address": "Số 36D Nguyễn Văn Tre, khóm 4, thị trấn Mỹ An, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5189481,
   "Latitude": 105.845131
 },
 {
   "STT": 1233,
   "Name": "Quầy thuốc Thanh Thảo",
   "address": "Số 371, ấp Tân Dinh, xã Tân Hòa, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.668875,
   "Latitude": 105.3541013
 },
 {
   "STT": 1234,
   "Name": "Quầy thuốc Hữu Dự",
   "address": "Số 377, ấp Bình Trung, xã Bình Thạnh Trung, huyện  Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3803603,
   "Latitude": 105.5591615
 },
 {
   "STT": 1235,
   "Name": "Cơ Sở thuốc Đông Y Phạm Thị Khen",
   "address": "Số 381/K1, khóm 1, thị trấn Lai Vung, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2938059,
   "Latitude": 105.6588485
 },
 {
   "STT": 1236,
   "Name": "Quầy thuốc Phan Nho",
   "address": "Số 382 Lê Duẩn, xã Mỹ Trà, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4632812,
   "Latitude": 105.6411856
 },
 {
   "STT": 1237,
   "Name": "Quầy thuốc Lộc Thọ",
   "address": "Số 383, ấp 3, xãThường Phước 2, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8389934,
   "Latitude": 105.2370339
 },
 {
   "STT": 1238,
   "Name": "Quầy thuốc Minh Hòa",
   "address": "Số 383, ấp Bình Trung, xã Bình Thạnh Trung, huyện  Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3803603,
   "Latitude": 105.5591615
 },
 {
   "STT": 1239,
   "Name": "Quầy thuốc Mỹ Lan",
   "address": "Số 383, ấp Bình Trung, xã Bình Thạnh Trung, huyện  Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3803603,
   "Latitude": 105.5591615
 },
 {
   "STT": 1240,
   "Name": "Quầy thuốc Hữu Thịnh",
   "address": "Số 383, khóm Mỹ Tây, thị trấn Mỹ Thọ, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4453693,
   "Latitude": 105.6995299
 },
 {
   "STT": 1241,
   "Name": "Quầy thuốc Phöc Tuyền",
   "address": "Số 39, ấp Nguyễn Cử, xã Nhị Mỹ, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4880535,
   "Latitude": 105.6940454
 },
 {
   "STT": 1242,
   "Name": "Nhà thuốc Thủy Uyên",
   "address": "Số 39, đường Lê Lợi, phường 2, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4552412,
   "Latitude": 105.6392586
 },
 {
   "STT": 1243,
   "Name": "Nhà thuốc Công Anh",
   "address": "Số 390, đường Lê Lợi, khóm 3, phường 3, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.3082341,
   "Latitude": 105.7581765
 },
 {
   "STT": 1244,
   "Name": "Quầy thuốc Thanh Phong",
   "address": "Số 394, ấp Trung, xã Thường Thới Tiền, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.837225,
   "Latitude": 105.2721456
 },
 {
   "STT": 1245,
   "Name": "Quầy thuốc Minh Trang",
   "address": "Số 395, ấp Hưng Thạnh Đông, xã Long Hưng B, huyện  Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3264445,
   "Latitude": 105.6588485
 },
 {
   "STT": 1246,
   "Name": "Quầy thuốc Kim Cương",
   "address": "Số 395, ấp Thượng, xã Thường ThớiTiền, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8314496,
   "Latitude": 105.2779983
 },
 {
   "STT": 1247,
   "Name": "Nhà thuốc Bệnh Viện DaLiễu GIA LAI",
   "address": "Số 396, đường Lê Đại Hành, phường Mỹ Phú, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4643955,
   "Latitude": 105.6474429
 },
 {
   "STT": 1248,
   "Name": "Quầy thuốc Phạm Thị Bé Năm",
   "address": "Số 399B/6, ấp Tân Thạnh, xã Phong Hòa, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2640559,
   "Latitude": 105.6002024
 },
 {
   "STT": 1249,
   "Name": "Nhà thuốc Anh Hào",
   "address": "Số 4 Nguyễn Trãi, phường 1, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4609627,
   "Latitude": 105.6365145
 },
 {
   "STT": 1250,
   "Name": "Nhà thuốc Minh Trà",
   "address": "Số 4, đường Nguyễn Trung Trực, khóm 4,phường An Thạnh, thị xã Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8067208,
   "Latitude": 105.3445651
 },
 {
   "STT": 1251,
   "Name": "Nhà thuốc Hồng Gấm",
   "address": "Số 40 Lê Lợi, khóm 2, phường An Thạnh, thị xã Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8097454,
   "Latitude": 105.3416591
 },
 {
   "STT": 1252,
   "Name": "Công ty TNHH Thương Mại - Dịch Vụ - Dược Phẩm Hoàng Thuận",
   "address": "Số 40 Lê Quý Đôn, phường 1, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4578908,
   "Latitude": 105.6346106
 },
 // {
 //   "STT": 1253,
 //   "Name": "Quầy thuốc Song Thịnh",
 //   "address": "Số 40, khóm Phú Mỹ, thị trấn Cái Tàu Hạ, huyện Châu Thành, TỈNH GIA LAI",
 //   "Longtitude": 10.7161298,
 //   "Latitude": 106.7411559
 // },
 {
   "STT": 1254,
   "Name": "Quầy thuốc Trường Hiệp",
   "address": "Số 405 khóm Bình Hòa, thị trấn Lấp Vò, huyện Lấp Vò, TỈNH GIA LAI.",
   "Longtitude": 10.3594127,
   "Latitude": 105.521427
 },
 {
   "STT": 1255,
   "Name": "Quầy thuốc Ngọc Hoa",
   "address": "Số 406, ấp Hưng Thạnh Đông, xã Long Hưng B, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3264445,
   "Latitude": 105.6588485
 },
 {
   "STT": 1256,
   "Name": "Quầy thuốc Đỗ Giàu",
   "address": "Số 409, ấp TânThạnh, xã An Hiệp, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2856663,
   "Latitude": 105.8231588
 },
 {
   "STT": 1257,
   "Name": "Quầy thuốc Dũng Hạnh",
   "address": "Số 409, ấp Thị, xã An Phong, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.6325611,
   "Latitude": 105.4243804
 },
 {
   "STT": 1258,
   "Name": "Quầy thuốc Đỗ Ân",
   "address": "Số 409, tổ 12, ấp 4, xã Bình HàngTrung, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4088492,
   "Latitude": 105.7527221
 },
 {
   "STT": 1259,
   "Name": "Nhà thuốc Minh Đức",
   "address": "Số 40A Nguyễn Tất Thành, khóm 2, phường 1, thành phố SaĐéc, TỈNH GIA LAI",
   "Longtitude": 10.2998597,
   "Latitude": 105.7584854
 },
 {
   "STT": 1260,
   "Name": "Quầy thuốc Tröc Phương",
   "address": "Số 41 Đốc Binh Vàng, khóm Tân Đông B, thị trấn Thanh Bình, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5547869,
   "Latitude": 105.4760236
 },
 {
   "STT": 1261,
   "Name": "Quầy thuốc Nguyễn Thông",
   "address": "Số 41, ấp Bình Hiệp A, xã Bình Thạnh Trung, huyện Lấp Vò , TỈNH GIA LAI",
   "Longtitude": 10.3803603,
   "Latitude": 105.5591615
 },
 {
   "STT": 1262,
   "Name": "Nhà thuốc Phúc Vinh",
   "address": "Số 411, đường 848, khóm Tân Hòa, phường An Hòa, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2904838,
   "Latitude": 105.7513093
 },
 {
   "STT": 1263,
   "Name": "Cơ Sở thuốc Đông Y Di Thái Hòa",
   "address": "Số 411, đường Nguyễn Huệ, khóm 1, phường 1, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.297149,
   "Latitude": 105.766171
 },
 {
   "STT": 1264,
   "Name": "Quầy thuốc Hiếu Nghĩa",
   "address": "Số 414, ấp Tân An, xã Tân Thành, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2671402,
   "Latitude": 105.597998
 },
 {
   "STT": 1265,
   "Name": "Quầy thuốc Nguyễn Văn Quí",
   "address": "Số 414, xã Bình Hàng Trung, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4088492,
   "Latitude": 105.7527221
 },
 {
   "STT": 1266,
   "Name": "Quầy thuốc Tiến Phát",
   "address": "Số 416, ấp Phú Lợi, xã An Long, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.720632,
   "Latitude": 105.4126652
 },
 {
   "STT": 1267,
   "Name": "Quầy thuốc Ngân Hà",
   "address": "Số 418, ấp Hưng Thạnh Đông, xã Long Hưng B, huyện  Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3264445,
   "Latitude": 105.6588485
 },
 {
   "STT": 1268,
   "Name": "Quầy thuốc Kim Tuyến",
   "address": "Số 419, ấp Phú Thạnh, xã An PhúThuận, huyện ChâuThành, TỈNH GIA LAI",
   "Longtitude": 10.2169808,
   "Latitude": 105.8936224
 },
 {
   "STT": 1269,
   "Name": "Quầy thuốc Cao Thăng",
   "address": "Số 42, tổ 3, ấp 4, xã Mỹ Tân, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4885898,
   "Latitude": 105.6196744
 },
 {
   "STT": 1270,
   "Name": "Cơ Sở thuốc Đông Y MỹThường",
   "address": "Số 424, ấp Hưng Thạnh Đông, xã Long Hưng B, huyện  Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3264445,
   "Latitude": 105.6588485
 },
 {
   "STT": 1271,
   "Name": "Quầy thuốc Trần Hoàng",
   "address": "Số 427, tổ 14, ấp Hòa Long, xã Hòa An, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4622676,
   "Latitude": 105.6086621
 },
 {
   "STT": 1272,
   "Name": "Quầy thuốc Ngọc Nhị",
   "address": "Số 43, ấp Tân An, xã Tân Huề, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.6225995,
   "Latitude": 105.3528262
 },
 {
   "STT": 1273,
   "Name": "Cơ Sở thuốc Đông Y Hưng Phát Đường",
   "address": "Số 432, tổ 15,khóm 3, phường 6, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4449357,
   "Latitude": 105.6403363
 },
 {
   "STT": 1274,
   "Name": "Quầy thuốc Thanh Tuyền",
   "address": "Số 435B, khóm Tân Đông B, thị trấn Thanh Bình, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5554601,
   "Latitude": 105.491758
 },
 {
   "STT": 1275,
   "Name": "Quầy thuốc Nguyễn Hồng Quyên",
   "address": "Số 436, ấp Hưng Thạnh Đông, xã Long Hưng B, huyện  Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3264445,
   "Latitude": 105.6588485
 },
 {
   "STT": 1276,
   "Name": "Quầy thuốc Huỳnh Anh Thư",
   "address": "Số 439, tổ 14, ấp Hòa Long, xã Hòa An, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4622676,
   "Latitude": 105.6086621
 },
 {
   "STT": 1277,
   "Name": "Nhà thuốc Lê Anh Kiệt",
   "address": "Số 43A, đường Trần Thị Nhượng, phường 4, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4551222,
   "Latitude": 105.628626
 },
 {
   "STT": 1278,
   "Name": "Quầy thuốc Phương Thảo",
   "address": "Số 43B/3, ấp Định Thành, xã Định Hòa, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.1953911,
   "Latitude": 105.6705801
 },
 {
   "STT": 1279,
   "Name": "Quầy thuốc Trần Tuyết Phượng",
   "address": "Số 44, tổ 2, ấp Đông Bình, xã Hòa An, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.516667,
   "Latitude": 105.55
 },
 {
   "STT": 1280,
   "Name": "Quầy thuốc Hạnh Loan",
   "address": "Số 44/A, tổ 2, ấp Tịnh Mỹ, xã Tịnh Thới, thành phố CaoLãnh, TỈNH GIA LAI",
   "Longtitude": 10.4364387,
   "Latitude": 105.664511
 },
 {
   "STT": 1281,
   "Name": "Quầy thuốc Kim Phụng",
   "address": "Số 441, ấp Tân Thới, xã Tân Hòa, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.6225995,
   "Latitude": 105.3528262
 },
 {
   "STT": 1282,
   "Name": "Cơ Sở thuốc Đông Y An Phöc Đường",
   "address": "Số 442/TM, ấp Hưng Lợi, xã Thanh Mỹ, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.4098655,
   "Latitude": 105.8407722
 },
 {
   "STT": 1283,
   "Name": "Quầy thuốc Phi Phượng",
   "address": "Số 442/TM, ấp Hưng Lợi, xãThanh Mỹ, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.4098655,
   "Latitude": 105.8407722
 },
 {
   "STT": 1284,
   "Name": "Quầy thuốc Đinh DiễmHồng",
   "address": "Số 444, tổ 10, ấp Tịnh Châu, xã TịnhThới, thành phố CaoLãnh, TỈNH GIA LAI",
   "Longtitude": 10.4324174,
   "Latitude": 105.6488796
 },
 {
   "STT": 1285,
   "Name": "Quầy thuốc Duy Khánh",
   "address": "Số 448, ấp Tân Thuận B, xã Tân Phú, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5554601,
   "Latitude": 105.491758
 },
 {
   "STT": 1286,
   "Name": "Quầy thuốc Nguyễn Trần Phương Thảo",
   "address": "Số 44A/3, ấp Định Thành, xã Định Hòa, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.1953911,
   "Latitude": 105.6705801
 },
 {
   "STT": 1287,
   "Name": "Nhà thuốc Thanh Hằng",
   "address": "Số 45, TỈNH GIA LAI lộ 848, khóm Tân Bình, phường An Hòa, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.302416,
   "Latitude": 105.7460017
 },
 {
   "STT": 1288,
   "Name": "Quầy thuốc An Khang 2",
   "address": "Số 451, tổ 8, ấp Hòa Khánh, xã Hòa An, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4622676,
   "Latitude": 105.6086621
 },
 {
   "STT": 1289,
   "Name": "Quầy thuốc Thùy Linh",
   "address": "Số 453, ấp 3, xã Bình Hàng Tây, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.3756051,
   "Latitude": 105.7644596
 },
 {
   "STT": 1290,
   "Name": "Nhà thuốc Hoài Nhân",
   "address": "Số 455 HùngVương, khóm 2, phường 1, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2965473,
   "Latitude": 105.7644123
 },
 {
   "STT": 1291,
   "Name": "Quầy thuốc Ngọc Toàn",
   "address": "Số 455, ấp An Lợi A, xã Định Yên, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.309874,
   "Latitude": 105.5532993
 },
 {
   "STT": 1292,
   "Name": "Quầy thuốc Tân Mỹ",
   "address": "Số 456, ấp 1, xã Tân Mỹ, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.6215091,
   "Latitude": 105.5357139
 },
 {
   "STT": 1293,
   "Name": "Quầy thuốc Vạn Phöc",
   "address": "Số 456/5, ấp Tân An, xã Phong Hòa, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.1833808,
   "Latitude": 105.6940454
 },
 {
   "STT": 1294,
   "Name": "Nhà thuốc Bảo Lâm",
   "address": "Số 46 Ngô Gia Tự, khóm Tân Hòa, phường An Hòa, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2977573,
   "Latitude": 105.7439194
 },
 {
   "STT": 1295,
   "Name": "Quầy thuốc Bích Ngọc",
   "address": "Số 46 Nguyễn Huệ, khóm 1, thị trấn Sa Rài, huyện TânHồng, TỈNH GIA LAI",
   "Longtitude": 10.8802053,
   "Latitude": 105.4506933
 },
 {
   "STT": 1296,
   "Name": "Quầy thuốc Mỹ Tiên",
   "address": "Số 46, ấp An Lạc, xã An Bình, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4596591,
   "Latitude": 105.6557945
 },
 {
   "STT": 1297,
   "Name": "Quầy thuốc Trung Tín",
   "address": "Số 46, ấp Long Hội, xã Hòa Long, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2611796,
   "Latitude": 105.6588485
 },
 {
   "STT": 1298,
   "Name": "Quầy thuốc Khương Nga",
   "address": "Số 46, đường Hà Huy Tập, khóm 4, thị trấn Mỹ An, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5116754,
   "Latitude": 105.8522149
 },
 {
   "STT": 1299,
   "Name": "Quầy thuốc Kim Thoa",
   "address": "Số 460, tổ 18, ấp 1, xã Mỹ Long, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.3744301,
   "Latitude": 105.7879371
 },
 // {
 //   "STT": 1300,
 //   "Name": "Quầy thuốc Mỹ Anh",
 //   "address": "Số 461 Lý Thường Kiệt, khóm Tân Đông B, thị trấn  Thanh Bình, huyện Thanh Bình, TỈNH GIA LAI",
 //   "Longtitude": 10.5554601,
 //   "Latitude": 105.491758
 // },
 {
   "STT": 1301,
   "Name": "Quầy thuốc Thành Mai",
   "address": "Số 461B/4, ấp Long Thành A, xã Long Hậu, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2761378,
   "Latitude": 105.6133459
 },
 {
   "STT": 1302,
   "Name": "Quầy thuốc Minh Trạng",
   "address": "Số 466, ấp 6, xã Phong Mỹ, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.5318166,
   "Latitude": 105.5826123
 },
 {
   "STT": 1303,
   "Name": "Quầy thuốc Minh Phương",
   "address": "Số 469, ấp 1, xã Tân Hội Trung, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4569711,
   "Latitude": 105.7703287
 },
 {
   "STT": 1304,
   "Name": "Quầy thuốc Số 9 Sơn Trà",
   "address": "Số 46D, khóm 4, thị trấn Mỹ An, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5116754,
   "Latitude": 105.8522149
 },
 {
   "STT": 1305,
   "Name": "Quầy thuốc Thanh Nhàn",
   "address": "Số 47, ấp Phú Thọ A, xã Phú Thọ, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.6958011,
   "Latitude": 105.471249
 },
 {
   "STT": 1306,
   "Name": "Nhà thuốc Thiên Kim",
   "address": "Số 470/7, đường Nguyễn Cư Trinh, khóm 5, phường 1, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2915262,
   "Latitude": 105.7571409
 },
 // {
 //   "STT": 1307,
 //   "Name": "Quầy thuốc Phụng Loan",
 //   "address": "Số 471/B, ấp Bình Định, xã BìnhThành, huyện Thanh Bình, TỈNH GIA LAI",
 //   "Longtitude": 14.1665324,
 //   "Latitude": 108.902683
 // },
 {
   "STT": 1308,
   "Name": "Cơ Sở thuốc Đông Y Lợi Đạt Đường",
   "address": "Số 473/1, ấp Tân Lợi, xã Phong Hòa, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.1833808,
   "Latitude": 105.6940454
 },
 {
   "STT": 1309,
   "Name": "Quầy thuốc Nguyễn Bằng",
   "address": "Số 473/1, ấp Tân Lợi, xã Phong Hòa, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.1833808,
   "Latitude": 105.6940454
 },
 {
   "STT": 1310,
   "Name": "Quầy thuốc Hoàng Yến",
   "address": "Số 475, ấp 1, xã Mỹ Long, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.3744301,
   "Latitude": 105.7879371
 },
 {
   "STT": 1311,
   "Name": "Nhà thuốc Kim Hương",
   "address": "Số 475, khóm An Lợi, phường An Lộc, thị xã Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.7893213,
   "Latitude": 105.3476456
 },
 {
   "STT": 1312,
   "Name": "Quầy thuốc Như Ý",
   "address": "Số 484, ấp Long Thái, xã Long Khánh B, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8055163,
   "Latitude": 105.3283766
 },
 // {
 //   "STT": 1313,
 //   "Name": "Quầy thuốc Duy Bảo",
 //   "address": "Số 484, ấp Phú Yên, xã An Long, huyện Tam Nông, TỈNH GIA LAI",
 //   "Longtitude": 13.0881861,
 //   "Latitude": 109.0928764
 // },
 {
   "STT": 1314,
   "Name": "Quầy thuốc Chí Danh",
   "address": "Số 486, tổ 17, ấp Phú Lợi B, xã Phú Thuận B, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.7570042,
   "Latitude": 105.3423909
 },
 {
   "STT": 1315,
   "Name": "Quầy thuốc Hoàng Vũ",
   "address": "Số 487 đường Hòa Đông, xã Hòa An, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4622676,
   "Latitude": 105.6086621
 },
 {
   "STT": 1316,
   "Name": "Quầy thuốc Nguyễn Sang- Kim Cương",
   "address": "Số 489, ấp Tân Phú A, xã Tân Bình, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5790949,
   "Latitude": 105.4009508
 },
 {
   "STT": 1317,
   "Name": "Quầy thuốc Quang Trung",
   "address": "Số 48B, ấp Tân Trong, xã Tân Mỹ, huyện  Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3923418,
   "Latitude": 105.6471178
 },
 {
   "STT": 1318,
   "Name": "Quầy thuốc Thành Công",
   "address": "Số 48B, khóm Tân Đông A, thị trấn .Thanh Bình, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.557999,
   "Latitude": 105.490358
 },
 {
   "STT": 1319,
   "Name": "Nhà thuốc Xuân Thảo",
   "address": "Số 49 Trần Hưng Đạo, khóm 1, phường 2, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2932139,
   "Latitude": 105.7677271
 },
 {
   "STT": 1320,
   "Name": "Nhà thuốc Hồng Phong",
   "address": "Số 49, đường Hùng Vương, phường 2, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4566947,
   "Latitude": 105.638172
 },
 {
   "STT": 1321,
   "Name": "Quầy thuốc Yên Chi",
   "address": "Số 491, ấp Tân An, xã Tân Bình, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2185915,
   "Latitude": 105.7506635
 },
 {
   "STT": 1322,
   "Name": "Quầy thuốc Thịnh Phát",
   "address": "Số 491, khóm Bình Thạnh 1, thị trấn Lấp Vò, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3577289,
   "Latitude": 105.5210607
 },
 {
   "STT": 1323,
   "Name": "Quầy thuốc Tuyết Mai",
   "address": "Số 493/B1, chợ Phú Điền, xã Phú Điền, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.4510549,
   "Latitude": 105.8992553
 },
 {
   "STT": 1324,
   "Name": "Quầy thuốc Cao Thị Hạnh",
   "address": "Số 494, ấp Tân Mỹ, xã Tân Phú Trung, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2462649,
   "Latitude": 105.7409852
 },
 {
   "STT": 1325,
   "Name": "Quầy thuốc Ngọc Nhớ",
   "address": "Số 498, ấp 1, xã Mỹ Long, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.3744301,
   "Latitude": 105.7879371
 },
 {
   "STT": 1326,
   "Name": "Quầy thuốc Vân Khánh 2",
   "address": "Số 498, ấp Tân Thành, xã Tân Quy Tây, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.335001,
   "Latitude": 105.7057792
 },
 {
   "STT": 1327,
   "Name": "Quầy thuốc Công Thành",
   "address": "Số 49A, ấp An Lợi B, xã Định Yên, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.309874,
   "Latitude": 105.5532993
 },
 {
   "STT": 1328,
   "Name": "Nhà thuốc Hưng Vương",
   "address": "Số 5 Cách Mạng Tháng 8, phường 2, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4548868,
   "Latitude": 105.6340546
 },
 {
   "STT": 1329,
   "Name": "Quầy thuốc Thế Phi",
   "address": "Số 5 lô C, ấp 1, xã Mỹ Hòa, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5179126,
   "Latitude": 105.8809053
 },
 // {
 //   "STT": 1330,
 //   "Name": "Nhà thuốc Nguyễn An",
 //   "address": "Số 5, đường Hai Bà Trưng, khóm 2, phường 3, thành phố Sa Đéc, TỈNH GIA LAI",
 //   "Longtitude": 39.27307,
 //   "Latitude": 118.460379
 // },
 {
   "STT": 1331,
   "Name": "Cơ Sở thuốc Đông Y Thọ An Đường",
   "address": "Số 50 Hà Huy Tập, khóm 4, thị trấn Mỹ An, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5116754,
   "Latitude": 105.8522149
 },
 {
   "STT": 1332,
   "Name": "Quầy thuốc Hồng Nhớ",
   "address": "Số 50, ấp K8, xã Phú Đức, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.698599,
   "Latitude": 105.5474624
 },
 {
   "STT": 1333,
   "Name": "Cơ Sở thuốc Đông Y Vĩnh Hiệp Đường",
   "address": "Số 50, đường Tháp Mười, khóm 2, phường 2, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4542986,
   "Latitude": 105.6370055
 },
 {
   "STT": 1334,
   "Name": "Quầy thuốc Hải Sơn",
   "address": "Số 50, đường Trần Hưng Đạo, khóm 3, thị trấn Sa Rài, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8797812,
   "Latitude": 105.450975
 },
 {
   "STT": 1335,
   "Name": "Quầy thuốc Thu Hà",
   "address": "Số 500, khóm Tân Đông B, thị trấn Thanh Bình, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5554601,
   "Latitude": 105.491758
 },
 {
   "STT": 1336,
   "Name": "Quầy thuốc Hoàng Minh",
   "address": "Số 501/C2, ấp 6B,xã Trường Xuân, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.6314323,
   "Latitude": 105.7703287
 },
 {
   "STT": 1337,
   "Name": "Quầy thuốc Cộng Nga",
   "address": "Số 502/B1, chợ Phú Điền, xã Phú Điền, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.4510549,
   "Latitude": 105.8992553
 },
 {
   "STT": 1338,
   "Name": "Quầy thuốc Thành Kính",
   "address": "Số 504, ấp An Thạnh, xã Hội An Đông, huyện LấpVò, TỈNH GIA LAI",
   "Longtitude": 10.4120239,
   "Latitude": 105.5590147
 },
 {
   "STT": 1339,
   "Name": "Quầy thuốc Nhật Thanh",
   "address": "Sô 506, tổ 17, ấp Long Thái, xã Long Khánh B, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.7999562,
   "Latitude": 105.332145
 },
 {
   "STT": 1340,
   "Name": "Quầy thuốc Bảo Trang",
   "address": "Số 509, tổ 01, ấp Tân Hựu, xã Tân Nhuận Đông, huyện  Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2547739,
   "Latitude": 105.8155181
 },
 {
   "STT": 1341,
   "Name": "Quầy thuốc Hồng Nhung",
   "address": "Số 50A/6, ấp Thành Tấn, xãLong Thắng, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2428539,
   "Latitude": 105.6999122
 },
 {
   "STT": 1342,
   "Name": "Cơ Sở thuốc Đông Y TếLương Đường1",
   "address": "Số 51 Nguyễn Trãi, phường An Thạnh, thị xã Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8143532,
   "Latitude": 105.3453184
 },
 {
   "STT": 1343,
   "Name": "Quầy thuốc Gia Phát",
   "address": "Số 518, ấp An Ninh, xã Định An, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3219082,
   "Latitude": 105.5298525
 },
 {
   "STT": 1344,
   "Name": "Quầy thuốc Lệ Chi",
   "address": "Số 519, khóm Bình Thạnh 1, thị trấn Lấp Vò, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3577289,
   "Latitude": 105.5210607
 },
 {
   "STT": 1345,
   "Name": "Quầy thuốc Nhật Hà",
   "address": "Số 52, lô E, TDC2B, khóm Tân Đông B, thị trấn Thanh Bình, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5554601,
   "Latitude": 105.491758
 },
 {
   "STT": 1346,
   "Name": "Quầy thuốc Nguyễn Văn Tiện",
   "address": "Số 521, ấp 1, xã Tân Hội Trung, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4569711,
   "Latitude": 105.7703287
 },
 {
   "STT": 1347,
   "Name": "Quầy thuốc Ngọc",
   "address": "Số 523, ấp Tân Thạnh, xã HòaThành, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2914895,
   "Latitude": 105.7057792
 },
 {
   "STT": 1348,
   "Name": "Quầy thuốc Tấn Được",
   "address": "Số 523A, tổ 11, ấp Tân Chủ, xã Tân Thuận Tây, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4738847,
   "Latitude": 105.5858819
 },
 {
   "STT": 1349,
   "Name": "Công ty TNHH Thương Mại Dược Phẩm Thanh Bình GIA LAI",
   "address": "Số 53 Trần Thị Nhượng, phường 4, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4547329,
   "Latitude": 105.6288266
 },
 {
   "STT": 1350,
   "Name": "Quầy thuốc Thái Gia",
   "address": "Số 53, ấp Tân Thuận B, xã TânMỹ, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.4088585,
   "Latitude": 105.644077
 },
 {
   "STT": 1351,
   "Name": "Quầy thuốc Kim Linh",
   "address": "Số 530, tổ 7, ấp 2, xã Mỹ Long, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.3744301,
   "Latitude": 105.7879371
 },
 {
   "STT": 1352,
   "Name": "Nhà thuốc Vinh Hiển",
   "address": "Số 531/22, đường Nguyễn Văn Phát, khóm 2, phường 1, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2989907,
   "Latitude": 105.7586699
 },
 {
   "STT": 1353,
   "Name": "Nhà thuốc Dk Pharmacy",
   "address": "Số 533/19, đường Nguyễn Văn Phát, khóm 2, phường 1, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.3001052,
   "Latitude": 105.7608834
 },
 {
   "STT": 1354,
   "Name": "Quầy thuốc Hải Linh",
   "address": "Số 535, ấp An Định, xã An Bình, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4595427,
   "Latitude": 105.6565608
 },
 {
   "STT": 1355,
   "Name": "Quầy thuốc Thái Mai",
   "address": "Số 536, ấp Hưng Thạnh Đông, xã Long Hưng B, huyện  Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3264445,
   "Latitude": 105.6588485
 },
 {
   "STT": 1356,
   "Name": "Nhà thuốc Thanh Bình",
   "address": "Số 54 HùngVương, khóm 2, phường 2, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2894355,
   "Latitude": 105.7671815
 },
 {
   "STT": 1357,
   "Name": "Quầy thuốc Tùng Chi",
   "address": "Số 54, đường Quãng Khánh, xã Mỹ Trà, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4899262,
   "Latitude": 105.6385471
 },
 {
   "STT": 1358,
   "Name": "Quầy thuốc Duy Thanh",
   "address": "Số 54, khóm 3, thị trấn Tràm Chim, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.6691946,
   "Latitude": 105.5650239
 },
 {
   "STT": 1359,
   "Name": "Quầy thuốc Vy Trang",
   "address": "Số 543, ấp Tân Phú A, xã Tân Bình, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5790949,
   "Latitude": 105.4009508
 },
 {
   "STT": 1360,
   "Name": "Quầy thuốc Thu Ngân",
   "address": "Số 544B, khóm Tân Thuận, thị trấn .Thanh Bình, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5554601,
   "Latitude": 105.491758
 },
 {
   "STT": 1361,
   "Name": "Quầy thuốc Thuận Lợi",
   "address": "Số 553A, ấp An Ninh, xã Mỹ An Hưng A, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.4067257,
   "Latitude": 105.5767493
 },
 {
   "STT": 1362,
   "Name": "Nhà thuốc Minh Thành",
   "address": "Số 554A, đường Nguyễn Sinh Sắc, khóm 5, phường 1, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2904372,
   "Latitude": 105.7541289
 },
 {
   "STT": 1363,
   "Name": "Quầy thuốc Thiên Phúc",
   "address": "Số 556/1, đường ĐT 852, ấp Tân Lập, xã Tân Quy Tây, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.321395,
   "Latitude": 105.7250672
 },
 {
   "STT": 1364,
   "Name": "Quầy thuốc Hiền Hoài",
   "address": "Số 556/B, ấp Tây, xã Tân Thạnh, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5761773,
   "Latitude": 105.4595306
 },
 {
   "STT": 1365,
   "Name": "Nhà thuốc Thiên Thanh",
   "address": "Số 558, đường Nguyễn Sinh Sắc, khóm Tân Bình, phường An Hòa, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2904372,
   "Latitude": 105.7541289
 },
 {
   "STT": 1366,
   "Name": "Quầy thuốc Sơn Trà 2",
   "address": "Số 56/8/A, đường ĐT 846, thị trấn MỹAn, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5225428,
   "Latitude": 105.8402536
 },
 {
   "STT": 1367,
   "Name": "Quầy thuốc Thanh Trước",
   "address": "Số 560/1, ấp Long Thành, xã Long Hậu, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2761378,
   "Latitude": 105.6133459
 },
 {
   "STT": 1368,
   "Name": "Quầy thuốc Ngọc Tùng",
   "address": "Số 567B/1, ấp Long Thành, xã Long Hậu, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2860505,
   "Latitude": 105.6259744
 },
 {
   "STT": 1369,
   "Name": "Quầy thuốc Đắc Khánh",
   "address": "Số 568, khóm Bình Thạnh 1, thị trấn Lấp Vò, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3577289,
   "Latitude": 105.5210607
 },
 {
   "STT": 1370,
   "Name": "Quầy thuốc Phúc Thâu",
   "address": "Số 569, ấp Phú Thành, xã Tân Phú Đông, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.30076,
   "Latitude": 105.765701
 },
 {
   "STT": 1371,
   "Name": "Quầy thuốc Hồng Hưng",
   "address": "Số 570, ấp Tân Thuận B, xã Tân Phú, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5554601,
   "Latitude": 105.491758
 },
 {
   "STT": 1372,
   "Name": "Quầy thuốc Như Khánh",
   "address": "Số 571, ấp Tân Hưng, xã TânThành, huyện LaiVung, TỈNH GIA LAI",
   "Longtitude": 10.2536944,
   "Latitude": 105.6196806
 },
 {
   "STT": 1373,
   "Name": "Quầy thuốc Đồng Hiệp",
   "address": "Số 579/1, ấp An Hòa, xã Tân Bình, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.7436984,
   "Latitude": 105.3892372
 },
 {
   "STT": 1374,
   "Name": "Quầy thuốc Ngọc Mai",
   "address": "Số 58 lô A, chợ Cái Tàu Hạ, huyệnChâu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2594423,
   "Latitude": 105.8703997
 },
 {
   "STT": 1375,
   "Name": "Nhà thuốc Lợi An",
   "address": "Số 58 Ngô Gia Tự, khóm Tân Hòa, phường An Hòa, thành phốSa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.3130994,
   "Latitude": 105.7458759
 },
 // {
 //   "STT": 1376,
 //   "Name": "Quầy thuốc Thúy Duy",
 //   "address": "Số 586, ấp Long Hậu, xã Long Khánh A, huyện Hồng Ngự, TỈNH GIA LAI",
 //   "Longtitude": 10.6310514,
 //   "Latitude": 106.7057733
 // },
 {
   "STT": 1377,
   "Name": "Quầy thuốc Ý Như",
   "address": "Số 586, khóm Bình Thạnh 1, thị trấn Lấp Vò, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3577289,
   "Latitude": 105.5210607
 },
 {
   "STT": 1378,
   "Name": "Quầy thuốc Cöc Phương",
   "address": "Số 589, đường Mai Văn Khải, ấp 2, xã Mỹ Tân, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4799668,
   "Latitude": 105.6188059
 },
 {
   "STT": 1379,
   "Name": "Quầy thuốc Lâm Sơn",
   "address": "Số 590A/2, ấp Hòa Định, xã Tân Hòa, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2074033,
   "Latitude": 105.6471178
 },
 {
   "STT": 1380,
   "Name": "Quầy thuốc Lê Phương Vy",
   "address": "Số 591, tổ 3, ấp 2, xã Mỹ Tân, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4885898,
   "Latitude": 105.6196744
 },
 {
   "STT": 1381,
   "Name": "Quầy thuốc Thúy Vy",
   "address": "Số 592A/1, ấp Long Thành, xã Long Hậu, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2860505,
   "Latitude": 105.6259744
 },
 {
   "STT": 1382,
   "Name": "Quầy thuốc Bé Tuyền",
   "address": "Số 593/2, ấp Hòa Định, xã Tân Hòa, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2074033,
   "Latitude": 105.6471178
 },
 {
   "STT": 1383,
   "Name": "Quầy thuốc Huyền Anh Ii",
   "address": "Số 593/2, ấp Hòa Định, xã Tân Hòa, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2074033,
   "Latitude": 105.6471178
 },
 {
   "STT": 1384,
   "Name": "Quầy thuốc Hồng Vân",
   "address": "Số 594, khóm Bình Thạnh 1, thị trấn Lấp Vò, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3577289,
   "Latitude": 105.5210607
 },
 {
   "STT": 1385,
   "Name": "Quầy thuốc Lê Thái Phong",
   "address": "Số 594, tổ 13, ấp Tịnh Châu, xã TịnhThới, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4324174,
   "Latitude": 105.6488796
 },
 {
   "STT": 1386,
   "Name": "Quầy thuốc Duy Vân",
   "address": "Số 595, ấp 2, xã Mỹ Tân, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4885898,
   "Latitude": 105.6196744
 },
 {
   "STT": 1387,
   "Name": "Quầy thuốc Hồng Ngọc",
   "address": "Số 595A, ấp Long Thành, xã Long Hậu, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2761378,
   "Latitude": 105.6133459
 },
 {
   "STT": 1388,
   "Name": "Quầy thuốc Thái Bảo",
   "address": "Số 598, ấp Thạnh An, xã Tân Long, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5790949,
   "Latitude": 105.4009508
 },
 {
   "STT": 1389,
   "Name": "Quầy thuốc My Senl",
   "address": "Số 598, đường ĐT 852, ấp Tân Lập, xã Tân Quy Tây, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.3214484,
   "Latitude": 105.7249892
 },
 {
   "STT": 1390,
   "Name": "Quầy thuốc Duy Thức",
   "address": "Số 599, ấp AnBình, xã Định Yên, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.383333,
   "Latitude": 105.533333
 },
 {
   "STT": 1391,
   "Name": "Quầy thuốc Tám Nhanh",
   "address": "Số 599, ấp Hòa Định, xã Tân Hòa, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2074033,
   "Latitude": 105.6471178
 },
 {
   "STT": 1392,
   "Name": "Quầy thuốc Tân Bình",
   "address": "Số 6, ấp An Hòa Nhất, xã Tân Bình, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2553925,
   "Latitude": 105.776198
 },
 {
   "STT": 1393,
   "Name": "Quầy thuốc Tân Giang",
   "address": "Số 6/D Nguyễn Thị Minh Khai, khóm 4, thị trấn Mỹ An, huyện  Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5116754,
   "Latitude": 105.8522149
 },
 {
   "STT": 1394,
   "Name": "Cơ Sở thuốc Đông Y Ngưỡng NguyênĐường",
   "address": "Số 600/1, ấp Long Thành, xã Long Hậu, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2860505,
   "Latitude": 105.6259744
 },
 {
   "STT": 1395,
   "Name": "Quầy thuốc Kim Liên",
   "address": "Số 600/1, ấp Tân Lợi, xã Phong Hòa, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.1833808,
   "Latitude": 105.6940454
 },
 {
   "STT": 1396,
   "Name": "Cơ Sở thuốc Đông Y Ích Thọ Đường",
   "address": "Số 601, ấp Long Thành, xã Long Hậu, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2761378,
   "Latitude": 105.6133459
 },
 {
   "STT": 1397,
   "Name": "Quầy thuốc Phương Trang",
   "address": "Số 604, ấp 3, xã Phong Mỹ, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.5318166,
   "Latitude": 105.5826123
 },
 {
   "STT": 1398,
   "Name": "Quầy thuốc Nhật Tiến",
   "address": "Số 604, ấp AnThạnh, xã An Hiệp, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2856663,
   "Latitude": 105.8231588
 },
 {
   "STT": 1399,
   "Name": "Quầy thuốc Thanh Kiều",
   "address": "Số 604/KDC, ấp Tân Bình, xã Tân Thành, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2618983,
   "Latitude": 105.5905396
 },
 {
   "STT": 1400,
   "Name": "Cơ Sở thuốc Đông Y Vạn Hõa Đường",
   "address": "Số 605, khóm Bình Thạnh 1, thị trấn Lấp Vò, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3577289,
   "Latitude": 105.5210607
 },
 {
   "STT": 1401,
   "Name": "Quầy thuốc Duyên Thảo",
   "address": "Số 605/1, ấp Long Thành, xã Long Hậu, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2761378,
   "Latitude": 105.6133459
 },
 {
   "STT": 1402,
   "Name": "Quầy thuốc Kim Bình",
   "address": "Số 60K, khóm 1, thị trấn Lai Vung, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2954488,
   "Latitude": 105.7664771
 },
 {
   "STT": 1403,
   "Name": "Quầy thuốc Thu Diễm",
   "address": "Số 613, ấp 1, xã Tân Hội Trung, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4569711,
   "Latitude": 105.7703287
 },
 {
   "STT": 1404,
   "Name": "Nhà thuốc Hoàng Trung",
   "address": "Số 613, khóm 1, thị trấn Lai Vung, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2954488,
   "Latitude": 105.7664771
 },
 {
   "STT": 1405,
   "Name": "Quầy thuốc Anh Dũng",
   "address": "Số 614A/1, ấp Long Thành, xã Long Hậu, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2761378,
   "Latitude": 105.6133459
 },
 {
   "STT": 1406,
   "Name": "Quầy thuốc Xuân Hoa",
   "address": "Số 615, ấp K10, xã Phú Hiệp, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.7757656,
   "Latitude": 105.5122694
 },
 {
   "STT": 1407,
   "Name": "Quầy thuốc Kim Thoa",
   "address": "Số 616, ấp Tân Hòa B, xã Tân Phú, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.668875,
   "Latitude": 105.3541013
 },
 {
   "STT": 1408,
   "Name": "Quầy thuốc Xuân Thảo",
   "address": "Số 620, đường Mai Văn Khải, xã Mỹ Tân, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4799668,
   "Latitude": 105.6188059
 },
 {
   "STT": 1409,
   "Name": "Quầy thuốc Trọng Nghĩa",
   "address": "Số 621 Nguyễn Chí Thanh, khóm 2, thị trấn  Tràm Chim, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.6691946,
   "Latitude": 105.5650239
 },
 {
   "STT": 1410,
   "Name": "Quầy thuốc Ngọc Hân",
   "address": "Số 625, đường Nguyễn Hữu Kiến, xã Hòa An, thành phốCao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4748975,
   "Latitude": 105.60526
 },
 {
   "STT": 1411,
   "Name": "Quầy thuốc Tân Định",
   "address": "Số 626, ấp Phú Thọ, xã An Long, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.720632,
   "Latitude": 105.4126652
 },
 {
   "STT": 1412,
   "Name": "Quầy thuốc Thanh Thảo",
   "address": "Số 628, ấp 3, xã Bình Tấn, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.6191437,
   "Latitude": 105.5826123
 },
 {
   "STT": 1413,
   "Name": "Nhà thuốc Thái Nguyên",
   "address": "Số 62A Trần Phú, khóm 2, phường 1, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2959137,
   "Latitude": 105.7626246
 },
 {
   "STT": 1414,
   "Name": "Nhà thuốc Thủy Kiều",
   "address": "Số 63 Nguyễn Trãi, khóm 2, phường An Thạnh, thị xã Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8097454,
   "Latitude": 105.3416591
 },
 {
   "STT": 1415,
   "Name": "Quầy thuốc Lê Tuấn Anh",
   "address": "Số 63, ấp Tân Lộc A, xã Tân Dương, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.335001,
   "Latitude": 105.7057792
 },
 {
   "STT": 1416,
   "Name": "Quầy thuốc Mai Linh",
   "address": "Số 631/C, ấp Mỹ Phú, xã Phú Điền, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.4925503,
   "Latitude": 105.8670589
 },
 {
   "STT": 1417,
   "Name": "Quầy thuốc Ngọc Đẹp",
   "address": "Số 635, ấp 3, xã Bình Hàng Tây, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.3756051,
   "Latitude": 105.7644596
 },
 {
   "STT": 1418,
   "Name": "Quầy thuốc Nhật Ái",
   "address": "Số 638 Nguyễn Hữu Kiến, ấp Hòa Long, xã Hòa An, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4748975,
   "Latitude": 105.60526
 },
 {
   "STT": 1419,
   "Name": "Quầy thuốc Ngọc Bích",
   "address": "Số 638, tổ 16, ấp Phú Hòa A, xã Phú Thuận A, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.722479,
   "Latitude": 105.2940941
 },
 {
   "STT": 1420,
   "Name": "Cơ Sở thuốc Đông Y Vạn Ngọc Đường",
   "address": "Số 64, đường Lê Lợi, phường AnThạnh, thị xã Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8078407,
   "Latitude": 105.3433192
 },
 {
   "STT": 1421,
   "Name": "Quầy thuốc Phúc Nga",
   "address": "Số 64/D Nguyễn Văn Tre, chợ Tháp Mười, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5195055,
   "Latitude": 105.8454066
 },
 {
   "STT": 1422,
   "Name": "Quầy thuốc Minh Hy",
   "address": "Số 642, TỈNH GIA LAI lộ ĐT 843, ấp 2, xã Tân Thành B, huyệnTân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8697347,
   "Latitude": 105.494688
 },
 {
   "STT": 1423,
   "Name": "Quầy thuốc Thu Thủy",
   "address": "Số 644, khóm 1, thị trấn Lai Vung, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2938059,
   "Latitude": 105.6588485
 },
 {
   "STT": 1424,
   "Name": "Quầy thuốc Thanh Phong",
   "address": "Số 645A, khóm 1, thị trấn Lai Vung, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2938059,
   "Latitude": 105.6588485
 },
 {
   "STT": 1425,
   "Name": "Nhà thuốc Thu Liễu",
   "address": "Số 65, tổ 3, khóm5, phường 11, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4878813,
   "Latitude": 105.5839265
 },
 {
   "STT": 1426,
   "Name": "Quầy thuốc Mỹ Linh",
   "address": "Số 652, ấp An Ninh, xã Định An, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3219082,
   "Latitude": 105.5298525
 },
 {
   "STT": 1427,
   "Name": "Quầy thuốc Tài Thu",
   "address": "Số 654, ấp 3, xã Bình Tấn, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.6191437,
   "Latitude": 105.5826123
 },
 {
   "STT": 1428,
   "Name": "Quầy thuốc Ngọc Phương",
   "address": "Số 654/B, ấp Thượng, xã TânQuới, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.6567686,
   "Latitude": 105.3775245
 },
 // {
 //   "STT": 1429,
 //   "Name": "Quầy thuốc Trần Tuấn Kiệt",
 //   "address": "Số 655/6, ấp Tân Phú, xã Tân Phước, huyện Lai Vung, TỈNH GIA LAI",
 //   "Longtitude": 10.7900517,
 //   "Latitude": 106.6281901
 // },
 {
   "STT": 1430,
   "Name": "Quầy thuốc Huỳnh Thị Thùy Trinh",
   "address": "Số 65A, ấp An Lợi B, xã Định Yên, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.309874,
   "Latitude": 105.5532993
 },
 {
   "STT": 1431,
   "Name": "Quầy thuốc Phước Lộc",
   "address": "Số 66, ấp Hưng Thành Đông, xã Long Hưng B, huyện  Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3264445,
   "Latitude": 105.6588485
 },
 {
   "STT": 1432,
   "Name": "Quầy thuốc Thành Huyền",
   "address": "Số 66, ấp Phú Thạnh, xã PhúLong, huyện ChâuThành, TỈNH GIA LAI",
   "Longtitude": 10.2901261,
   "Latitude": 105.7517453
 },
 {
   "STT": 1433,
   "Name": "Quầy thuốc Thiên Phú",
   "address": "Số 66/5 ấp Tân An, xã Phong Hòa, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.1833808,
   "Latitude": 105.6940454
 },
 {
   "STT": 1434,
   "Name": "Quầy thuốc Thanh Tuyền",
   "address": "Số 662 tổ 3, ấp 2, xã Mỹ Tân, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4885898,
   "Latitude": 105.6196744
 },
 {
   "STT": 1435,
   "Name": "Quầy thuốc Dược Phẩm Xanh",
   "address": "Số 664, ấp Phú Thuận, xã Tân Phú Đông, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.30076,
   "Latitude": 105.765701
 },
 {
   "STT": 1436,
   "Name": "Quầy thuốc Minh Nhựt",
   "address": "Số 668A, ấp Khánh An, xã Tân Khánh Trung, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3949352,
   "Latitude": 105.6932616
 },
 {
   "STT": 1437,
   "Name": "Chi Nhánh Công ty Cp Dược Phẩm Imexpharm -Cửu Long 2",
   "address": "Số 66A Nguyễn Tất Thành, khóm 3, phường 1, thành phố SaĐéc, TỈNH GIA LAI",
   "Longtitude": 10.3080677,
   "Latitude": 105.7497592
 },
 {
   "STT": 1438,
   "Name": "Quầy thuốc Hữu An",
   "address": "Số 67/A1, ấp 1, xã Thạnh Lợi, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5179126,
   "Latitude": 105.8809053
 },
 {
   "STT": 1439,
   "Name": "Quầy thuốc Lệ Thu",
   "address": "Số 670, ấp AnBình, xã Định Yên, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.383333,
   "Latitude": 105.533333
 },
 {
   "STT": 1440,
   "Name": "Quầy thuốc Mỹ Phụng",
   "address": "Số 674/3, ấp Định Thành, xã Định Hòa, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.1953911,
   "Latitude": 105.6705801
 },
 {
   "STT": 1441,
   "Name": "Quầy thuốc Đỗ Giàu 2",
   "address": "Số 677, tổ 13, ấp Tân Hòa, xã An Hiệp, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2856663,
   "Latitude": 105.8231588
 },
 {
   "STT": 1442,
   "Name": "Cơ Sở thuốc Đông Y Vạn Đức Hõa",
   "address": "Số 69 HùngVương, phường 2, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4547856,
   "Latitude": 105.6350582
 },
 {
   "STT": 1443,
   "Name": "Nhà thuốc Xuân Hương",
   "address": "Số 69 Nguyễn Trãi, khóm 2, phường An Thạnh, thị xã Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8097454,
   "Latitude": 105.3416591
 },
 {
   "STT": 1444,
   "Name": "Quầy thuốc Kim Hồng",
   "address": "Số 690, ấp Trung, xã Tân Thạnh, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5761773,
   "Latitude": 105.4595306
 },
 {
   "STT": 1445,
   "Name": "Quầy thuốc Thế Huy",
   "address": "Số 692, ấp 1, xã Tân Mỹ, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.6215091,
   "Latitude": 105.5357139
 },
 {
   "STT": 1446,
   "Name": "Quầy thuốc Quỳnh Như",
   "address": "Số 693, khóm Tân Đông B, huyệnThanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.557999,
   "Latitude": 105.490358
 },
 {
   "STT": 1447,
   "Name": "Quầy thuốc Thiên Phúc",
   "address": "Số 694, ấp 3, xã Bình Tấn, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.6191437,
   "Latitude": 105.5826123
 },
 {
   "STT": 1448,
   "Name": "Quầy thuốc Toàn Thắng",
   "address": "Số 695, ấp Vĩnh Bình A, xã Vĩnh Thạnh, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3447983,
   "Latitude": 105.6177942
 },
 {
   "STT": 1449,
   "Name": "Nhà thuốc Bệnh ViệnTâm Trí ĐồngTháp",
   "address": "Số 700 QL30, xãMỹ Tân, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4736645,
   "Latitude": 105.6204235
 },
 {
   "STT": 1450,
   "Name": "Quầy thuốc Nguyệt Thanh",
   "address": "Số 700/KDC, ấp Tân Bình, xã Tân Thành, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.335001,
   "Latitude": 105.7057792
 },
 {
   "STT": 1451,
   "Name": "Quầy thuốc Trang Thanh",
   "address": "Số 701B, khóm Tân Đông B, thị trấn Thanh Bình, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5554601,
   "Latitude": 105.491758
 },
 {
   "STT": 1452,
   "Name": "Quầy thuốc Minh Hiển",
   "address": "Số 702, ấp Trung, xã Tân Thạnh, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5761773,
   "Latitude": 105.4595306
 },
 {
   "STT": 1453,
   "Name": "Nhà thuốc Lê Hiếu",
   "address": "Số 704 Phạm Hữu Lầu, phường 6, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4290029,
   "Latitude": 105.6342852
 },
 {
   "STT": 1454,
   "Name": "Nhà thuốc Khang Thịnh",
   "address": "Số 706A, tổ 7, ấp 1, xã Mỹ Tân, thành phốCao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4885898,
   "Latitude": 105.6196744
 },
 {
   "STT": 1455,
   "Name": "Cơ Sở thuốc Đông Y Phục Sanh Đường",
   "address": "Số 708, ấp Trung, xã Tân Thạnh, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5761773,
   "Latitude": 105.4595306
 },
 // {
 //   "STT": 1456,
 //   "Name": "Quầy thuốc Hồng Hậu",
 //   "address": "Số 70D/4, ấp Hòa Bình, xã Tân Hòa, huyện Lai Vung, TỈNH GIA LAI",
 //   "Longtitude": 20.6861265,
 //   "Latitude": 105.3131185
 // },
 // {
 //   "STT": 1457,
 //   "Name": "Quầy thuốc Hồng Hậu",
 //   "address": "Số 70D/4, ấp Hòa Bình, xã Tân Hòa, huyện Lai Vung, TỈNH GIA LAI",
 //   "Longtitude": 20.6861265,
 //   "Latitude": 105.3131185
 // },
 {
   "STT": 1458,
   "Name": "Nhà thuốc Thöy Hằng",
   "address": "Số 71 Trần Hưng Đạo, khóm 1, phường 2, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2936801,
   "Latitude": 105.7675441
 },
 {
   "STT": 1459,
   "Name": "Nhà thuốc Cẩm Sánh",
   "address": "Số 71, NguyễnTrãi, khóm 2, phường An Thạnh, thị xã Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8097454,
   "Latitude": 105.3416591
 },
 {
   "STT": 1460,
   "Name": "Nhà thuốc Đặng Minh Nhựt",
   "address": "Số 712, đường Phạm Hữu Lầu, phường 6, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4159735,
   "Latitude": 105.6445601
 },
 {
   "STT": 1461,
   "Name": "Quầy thuốc Tố Anh",
   "address": "Số 722, ấp Trung, xã Tân Quới, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.6567686,
   "Latitude": 105.3775245
 },
 {
   "STT": 1462,
   "Name": "Quầy thuốc Tố Nga",
   "address": "Số 722/C, ấp Mỹ Tây 1, xã Mỹ Quí, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5453505,
   "Latitude": 105.7468535
 },
 {
   "STT": 1463,
   "Name": "Quầy thuốc Gia Hân",
   "address": "Số 73, ấp Khánh Nghĩa, xã Tân Khánh Đông, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.355597,
   "Latitude": 105.7292491
 },
 {
   "STT": 1464,
   "Name": "Quầy thuốc Thiện Phöc",
   "address": "Số 738/4, ấp Long Thành A, xã Long Hậu, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2761378,
   "Latitude": 105.6133459
 },
 {
   "STT": 1465,
   "Name": "Quầy thuốc Gia Bảo 1",
   "address": "Số 741, ấp Bình Hòa, xã Bình Thành, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5314643,
   "Latitude": 105.5453708
 },
 {
   "STT": 1466,
   "Name": "Quầy thuốc Anh Thư",
   "address": "Số 741/B, ấp Trung, xã Tân Thạnh, huyệnThanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.6009459,
   "Latitude": 105.4009508
 },
 // {
 //   "STT": 1467,
 //   "Name": "Quầy thuốc Ánh Nguyệt",
 //   "address": "Số 745/3, ấp Tân Phú, xã Tân Phước, huyện Lai Vung, TỈNH GIA LAI",
 //   "Longtitude": 10.7900517,
 //   "Latitude": 106.6281901
 // },
 {
   "STT": 1468,
   "Name": "Quầy thuốc Khả Hân",
   "address": "Số 750/C, ấp Mỹ Tây 1, xã Mỹ Quý, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5453505,
   "Latitude": 105.7468535
 },
 {
   "STT": 1469,
   "Name": "Quầy thuốc Phương Minh Phát",
   "address": "Số 751G, ấp Long Khánh A, xã Long Hậu, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2846597,
   "Latitude": 105.6236585
 },
 {
   "STT": 1470,
   "Name": "Quầy thuốc HoàngThuyết",
   "address": "Số 754/1, ấp Long Khánh A, xã Long Hậu, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2846597,
   "Latitude": 105.6236585
 },
 {
   "STT": 1471,
   "Name": "Quầy thuốc Thanh Hồng",
   "address": "Số 757C, ấp Long Thành A, xã Long Hậu, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2761378,
   "Latitude": 105.6133459
 },
 {
   "STT": 1472,
   "Name": "Quầy thuốc Huỳnh Hoa",
   "address": "Số 75A, khóm 1, thị trấn Lai Vung, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2954488,
   "Latitude": 105.7664771
 },
 {
   "STT": 1473,
   "Name": "Nhà thuốc Phước Thọ",
   "address": "Số 76 Đốc Binh Kiều, phường 2,thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4535379,
   "Latitude": 105.6362702
 },
 {
   "STT": 1474,
   "Name": "Cơ Sở thuốc Đông Y Vạn Hưng",
   "address": "Số 760, đường Phạm Hữu Lầu, phường 6, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4159735,
   "Latitude": 105.6445601
 },
 {
   "STT": 1475,
   "Name": "Quầy thuốc Lộc Đến",
   "address": "Số 761/1, ấp Long Khánh A, xã Long Hậu, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2846597,
   "Latitude": 105.6236585
 },
 {
   "STT": 1476,
   "Name": "Quầy thuốc Phi Lụa",
   "address": "Số 765/C, ấp Mỹ Tây 1, xã Mỹ Quý, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5453505,
   "Latitude": 105.7468535
 },
 {
   "STT": 1477,
   "Name": "Quầy thuốc Thanh Thùy",
   "address": "Số 77 Lê Lợi, ấp 3, thị trấn Sa Rài, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8753437,
   "Latitude": 105.4620245
 },
 {
   "STT": 1478,
   "Name": "Nhà thuốc Thiện An",
   "address": "Số 77, đường Phạm Hữu Lầu, phường 4, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4518713,
   "Latitude": 105.6313747
 },
 {
   "STT": 1479,
   "Name": "Quầy thuốc Như Ý",
   "address": "Số 77, tổ 2, ấp Long Phước, xã Long Khánh A, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8031699,
   "Latitude": 105.2955575
 },
 {
   "STT": 1480,
   "Name": "Cơ Sở thuốc Đông Y Hõa Bình",
   "address": "Số 771, ấp Phú Thọ, xã An Long, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.720632,
   "Latitude": 105.4126652
 },
 {
   "STT": 1481,
   "Name": "Quầy thuốc Nguyễn Trần Thành Phát",
   "address": "Số 774, tổ 24, ấp Hòa Long, xã Hòa An, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4622676,
   "Latitude": 105.6086621
 },
 {
   "STT": 1482,
   "Name": "Quầy thuốc Yến Nga",
   "address": "Số 777U/1, ấp Long Khánh A, xã Long Hậu, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2846597,
   "Latitude": 105.6236585
 },
 {
   "STT": 1483,
   "Name": "Quầy thuốc Hoàng Khang",
   "address": "Số 779, ấp 1, xã Tân Mỹ, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.6215091,
   "Latitude": 105.5357139
 },
 {
   "STT": 1484,
   "Name": "Nhà thuốc Bệnh Viện Y Học CổTruyền ĐồngTháp",
   "address": "Số 78, đường 30/4, phường 1, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4606016,
   "Latitude": 105.6429871
 },
 {
   "STT": 1485,
   "Name": "Nhà thuốc Anh Thư",
   "address": "Số 78, đường Trần Văn Voi, khóm 1, phường 4, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2911921,
   "Latitude": 105.7722161
 },
 {
   "STT": 1486,
   "Name": "Quầy thuốc Hữu Dũng",
   "address": "Số 783, ấp 1, xã Phú Ninh, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.6774666,
   "Latitude": 105.4009508
 },
 {
   "STT": 1487,
   "Name": "Quầy thuốc Mãnh Hiệp",
   "address": "Số 785, ấp Trung, xã Tân Thạnh, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5761773,
   "Latitude": 105.4595306
 },
 {
   "STT": 1488,
   "Name": "Quầy thuốc Đỗ Quyên",
   "address": "Số 79, ấp Hạ, xã Tân Quới, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.6567686,
   "Latitude": 105.3775245
 },
 {
   "STT": 1489,
   "Name": "Nhà thuốc Phúc An",
   "address": "Số 79/3 Nguyễn Tất Thành, khóm 2, phường 1, thành phố SaĐéc, TỈNH GIA LAI",
   "Longtitude": 10.290493,
   "Latitude": 105.762815
 },
 {
   "STT": 1490,
   "Name": "Quầy thuốc Hiển Nhân",
   "address": "Số 798, ấp Trung, xã Tân Thạnh, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5761773,
   "Latitude": 105.4595306
 },
 {
   "STT": 1491,
   "Name": "Quầy thuốc Thanh Hương",
   "address": "Số 79D, Nguyễn Văn Tre, khóm 4, thị trấn Mỹ An, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5199101,
   "Latitude": 105.8457152
 },
 {
   "STT": 1492,
   "Name": "Quầy thuốc Vĩnh Trinh",
   "address": "Số 8, ấp 6, xã Tân Hội Trung, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4569711,
   "Latitude": 105.7703287
 },
 // {
 //   "STT": 1493,
 //   "Name": "Quầy thuốc Hồng Tươi",
 //   "address": "Số 8, đường 3/2, thị trấn Mỹ Thọ, huyệnCao Lãnh, TỈNH GIA LAI",
 //   "Longtitude": 10.759844,
 //   "Latitude": 106.6517889
 // },
 // {
 //   "STT": 1494,
 //   "Name": "Quầy thuốc Quách Thị Nga",
 //   "address": "Số 802, ấp Phú Yên, xã An Long, huyện Tam Nông, TỈNH GIA LAI",
 //   "Longtitude": 13.0881861,
 //   "Latitude": 109.0928764
 // },
 {
   "STT": 1495,
   "Name": "Quầy thuốc Bảo Lâm",
   "address": "Số 80A, ấp Phú Long, xã Tân Phú Đông, thị xã Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.30076,
   "Latitude": 105.765701
 },
 {
   "STT": 1496,
   "Name": "Quầy thuốc Lan Anh",
   "address": "Số 810, ấp Trung, xã Tân Thạnh, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5761773,
   "Latitude": 105.4595306
 },
 {
   "STT": 1497,
   "Name": "Quầy thuốc Tám",
   "address": "Số 820C/4, ấp Long Thành A, xã Long Hậu, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2761378,
   "Latitude": 105.6133459
 },
 {
   "STT": 1498,
   "Name": "Quầy thuốc Vinh Hiển",
   "address": "Số 83/D, đường Nguyễn Văn Tre, khóm 4, thị trấn Mỹ An, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5199024,
   "Latitude": 105.8457082
 },
 // {
 //   "STT": 1499,
 //   "Name": "Quầy thuốc Hiếu Duy",
 //   "address": "Số 834 A, ấp Phú Yên, xã An Long, huyện Tam Nông, TỈNH GIA LAI",
 //   "Longtitude": 13.0881861,
 //   "Latitude": 109.0928764
 // },
 {
   "STT": 1500,
   "Name": "Cơ Sở thuốc Đông Y Bà Tư",
   "address": "Số 84 Nguyễn Thái Học, phường 4, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4557186,
   "Latitude": 105.6313951
 },
 {
   "STT": 1501,
   "Name": "Quầy thuốc Tuyết Minh",
   "address": "Số 842, ấp Đông Mỹ, xã Mỹ Hội, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4148355,
   "Latitude": 105.7411521
 },
 {
   "STT": 1502,
   "Name": "Quầy thuốc Tân Giang",
   "address": "Số 85, đường Nguyễn Thị Minh Khai, khóm 4, thị trấn Mỹ An, huyệnTháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5198985,
   "Latitude": 105.8457047
 },
 {
   "STT": 1503,
   "Name": "Cơ Sở thuốc Đông Y Dũ An Hòa 2",
   "address": "Số 85, đường Nguyễn Trãi, khóm 2, phường An Thạnh, thị xã Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8083973,
   "Latitude": 105.344418
 },
 {
   "STT": 1504,
   "Name": "Quầy thuốc Hải Nam",
   "address": "Số 853, ấp 3, xã Tân Mỹ, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.6215091,
   "Latitude": 105.5357139
 },
 {
   "STT": 1505,
   "Name": "Quầy thuốc Anh Thư",
   "address": "Số 859A6/TL, ấp Tân Lợi, xã Tân Thành, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2643411,
   "Latitude": 105.5835434
 },
 {
   "STT": 1506,
   "Name": "Nhà thuốc Huỳnh Yến",
   "address": "Số 86 HùngVương, phường 2, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4550167,
   "Latitude": 105.6356424
 },
 {
   "STT": 1507,
   "Name": "Nhà thuốc Hải Đăng",
   "address": "Số 87, đường Cách Mạng Tháng Tám, phường 2, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4548522,
   "Latitude": 105.6361209
 },
 {
   "STT": 1508,
   "Name": "Quầy thuốc Anh Thống",
   "address": "Số 870, tổ 01, ấp Tân Hòa, xã Tân Nhuận Đông, huyện  Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2422003,
   "Latitude": 105.8231588
 },
 {
   "STT": 1509,
   "Name": "Quầy thuốc Mỹ Duy",
   "address": "Số 872, ấp Phú Thọ, xã An Long, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.720632,
   "Latitude": 105.4126652
 },
 {
   "STT": 1510,
   "Name": "Quầy thuốc Diểm",
   "address": "Số 874, ấp Tân Thạnh, xã TânLong, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5675894,
   "Latitude": 105.4126652
 },
 {
   "STT": 1511,
   "Name": "Quầy thuốc Kim Tùng",
   "address": "Số 88, ấp An Lợi B, xã Định Yên, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.309874,
   "Latitude": 105.5532993
 },
 {
   "STT": 1512,
   "Name": "Nhà thuốc Hồng Phöc",
   "address": "Số 88, đường Trần Phú, khóm 2, phường 1, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2956632,
   "Latitude": 105.7612864
 },
 {
   "STT": 1513,
   "Name": "Quầy thuốc Cao Phát",
   "address": "Số 88/4, ấp Đông Huề, xã Tân Khánh Đông, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.355597,
   "Latitude": 105.7292491
 },
 {
   "STT": 1514,
   "Name": "Nhà thuốc Chí Công",
   "address": "Số 880 đường Phạm Hữu Lầu, phường 6, Tp Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4265275,
   "Latitude": 105.6363931
 },
 // {
 //   "STT": 1515,
 //   "Name": "Quầy thuốc Cẩm Hằng",
 //   "address": "Số 89, ấp Tân Bình B, xã Tân Thành, huyện Lai Vung, TỈNH GIA LAI",
 //   "Longtitude": 10.8014659,
 //   "Latitude": 106.6525974
 // },
 {
   "STT": 1516,
   "Name": "Quầy thuốc Quốc Khanh",
   "address": "Số 899A/1, ấp Long Thành, xã Long Hậu, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2860505,
   "Latitude": 105.6259744
 },
 {
   "STT": 1517,
   "Name": "Quầy thuốc Kim Thoa 09",
   "address": "Số 9, tổ 12, ấp 2, xã Mỹ Ngãi, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.5034079,
   "Latitude": 105.590733
 },
 {
   "STT": 1518,
   "Name": "Quầy thuốc Nguyễn Ngọc Thanh",
   "address": "Số 9/53, tổ 8, ấp 3, xã Mỹ Trà, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4826015,
   "Latitude": 105.6426665
 },
 {
   "STT": 1519,
   "Name": "Quầy thuốc Phú Tâm",
   "address": "Số 9/D, khu phố 1, thị trấn Mỹ An, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5225428,
   "Latitude": 105.8402536
 },
 {
   "STT": 1520,
   "Name": "Quầy thuốc Hồng Nguyễn",
   "address": "Số 90, ấp An Lạc, xã An Bình, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4596591,
   "Latitude": 105.6557945
 },
 {
   "STT": 1521,
   "Name": "Quầy thuốc Tư Giá",
   "address": "Số 90, ấp Long Hòa, xã LongThuận, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.766667,
   "Latitude": 105.283333
 },
 {
   "STT": 1522,
   "Name": "Quầy thuốc Thanh An",
   "address": "Số 90/8/A, khóm 1, thị trấn Mỹ An, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5225428,
   "Latitude": 105.8402536
 },
 {
   "STT": 1523,
   "Name": "Nhà thuốc Nhân Tâm",
   "address": "Số 91 Phạm Hữu Lầu, phường 4, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4481112,
   "Latitude": 105.6301921
 },
 {
   "STT": 1524,
   "Name": "Quầy thuốc Tâm Trí",
   "address": "Số 91 QL 80, ấpAn Phú, xã An Nhơn, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2607603,
   "Latitude": 105.8621906
 },
 {
   "STT": 1525,
   "Name": "Quầy thuốc Phöc Thuấn 144",
   "address": "Số 91, ấp 1, xãThường Phước 1, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8788463,
   "Latitude": 105.2077798
 },
 {
   "STT": 1526,
   "Name": "Nhà thuốc Minh Phúc 2",
   "address": "Số 9-10, đường Âu Cơ, khóm 1, phường 2, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2910831,
   "Latitude": 105.7674802
 },
 {
   "STT": 1527,
   "Name": "Nhà thuốc Medic - 9 - 11 GIA LAI",
   "address": "Số 9-11, đường30/4, phường 1, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4582171,
   "Latitude": 105.6319774
 },
 {
   "STT": 1528,
   "Name": "Quầy thuốc Như Nhựt",
   "address": "Số 919, ấp 1, xã Phong Mỹ, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.5318166,
   "Latitude": 105.5826123
 },
 {
   "STT": 1529,
   "Name": "Quầy thuốc Đăng Khoa",
   "address": "Số 919, ấp Long Thái, xã Long Khánh B, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.7999562,
   "Latitude": 105.332145
 },
 {
   "STT": 1530,
   "Name": "Nhà thuốc Vĩnh An",
   "address": "Số 91C, khóm Sa Nhiên, phường Tân Qui Đông, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.3175053,
   "Latitude": 105.7481971
 },
 {
   "STT": 1531,
   "Name": "Nhà thuốc Việt Thắng",
   "address": "Số 92 Lý Thường Kiệt, khóm 4, phường 1, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2927372,
   "Latitude": 105.7656801
 },
 {
   "STT": 1532,
   "Name": "Quầy thuốc Nguyễn Tröc",
   "address": "Số 92, ấp Phú Hòa, xã Tân Phú Đông, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2788747,
   "Latitude": 105.7409852
 },
 {
   "STT": 1533,
   "Name": "Nhà thuốc Đỗ Dũng",
   "address": "Số 92, Nguyễn Tất Thành, khóm Tân Thuận, P. An Hòa, thị xã Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.3022722,
   "Latitude": 105.7557348
 },
 {
   "STT": 1534,
   "Name": "Quầy thuốc Kim Phượng",
   "address": "Số 924C/5, ấp Tân Quới, xã Phong Hòa, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.0962481,
   "Latitude": 105.7556564
 },
 {
   "STT": 1535,
   "Name": "Quầy thuốc Xuân Trãi",
   "address": "Số 929, ấp 1, xã Phong Mỹ, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.5333548,
   "Latitude": 105.5757339
 },
 {
   "STT": 1536,
   "Name": "Quầy thuốc Lê Sua",
   "address": "Số 92B, khóm Phú Mỹ, Thị TrấnThanh Bình, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.546175,
   "Latitude": 105.4937814
 },
 {
   "STT": 1537,
   "Name": "Quầy thuốc Lộc Nhung",
   "address": "Số 93 QL80, ấp An Hòa Nhất, xã Tân Bình, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2743775,
   "Latitude": 105.7933156
 },
 {
   "STT": 1538,
   "Name": "Nhà thuốc Ngọc Hân",
   "address": "Số 94, đường Lê Lợi, khóm 3, phường An Thạnh, thị xã Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8080663,
   "Latitude": 105.3415628
 },
 {
   "STT": 1539,
   "Name": "Nhà thuốc Nguyễn Như",
   "address": "Số 94, đường Lê Lợi, phường 2, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4552412,
   "Latitude": 105.6392586
 },
 {
   "STT": 1540,
   "Name": "Quầy thuốc Lê Anh",
   "address": "Số 950, ấp 1, xã Tân Mỹ, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.6215091,
   "Latitude": 105.5357139
 },
 {
   "STT": 1541,
   "Name": "Quầy thuốc Bi",
   "address": "Số 955, tổ 13, ấp Long Phước, xã Long Khánh A, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8060563,
   "Latitude": 105.2926308
 },
 {
   "STT": 1542,
   "Name": "Nhà thuốc Giang Thành",
   "address": "Số 956A đường Phạm Hữu Lầu, phường 6, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4304989,
   "Latitude": 105.6338385
 },
 {
   "STT": 1543,
   "Name": "Quầy thuốc Trần Hào 2",
   "address": "Số 95B, ấp Phú Hòa, xã Tân Phú Đông, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.30076,
   "Latitude": 105.765701
 },
 {
   "STT": 1544,
   "Name": "Quầy thuốc Hồng Linh",
   "address": "Số 96, ấp Tân Dinh, xã Tân Hòa, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.668875,
   "Latitude": 105.3541013
 },
 {
   "STT": 1545,
   "Name": "Quầy thuốc Thành Đạt",
   "address": "Số 97, ấp Tân Khánh, xã TânThành, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2598706,
   "Latitude": 105.5924872
 },
 {
   "STT": 1546,
   "Name": "Cơ Sở thuốc Đông Y Thiên Ân Đường",
   "address": "Số 97, đường Nguyễn Huệ, khóm 1, thị trấn Sa Rài, huyện  Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8742151,
   "Latitude": 105.4613533
 },
 {
   "STT": 1547,
   "Name": "Quầy thuốc Thöy Liễu",
   "address": "Số 97A/6, ấp Thành Tấn, xãLong Thắng, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2640559,
   "Latitude": 105.6002024
 },
 {
   "STT": 1548,
   "Name": "Nhà thuốc Hiếu Vân",
   "address": "Số 98, tổ 11, khóm2, phường 6, thành phốCao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4288288,
   "Latitude": 105.6343903
 },
 {
   "STT": 1549,
   "Name": "Quầy thuốc Duy Nam",
   "address": "Số 98/TL852, ấp Hưng Lợi Đông, xã Long Hưng B, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3333126,
   "Latitude": 105.681255
 },
 {
   "STT": 1550,
   "Name": "Nhà thuốc Duy Khoa Ii",
   "address": "Số 986 Phạm Hữu Lầu, phường 6, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.42235,
   "Latitude": 105.6404865
 },
 {
   "STT": 1551,
   "Name": "Quầy thuốc Tân Phö Đông",
   "address": "Số 98A, đường Ngã Bát, ấp PhúThuận, xã Tân Phú Đông, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2788747,
   "Latitude": 105.7409852
 },
 {
   "STT": 1552,
   "Name": "Quầy thuốc Bảo Quyên",
   "address": "Số 99, ấp Phú An, xã Phú Ninh, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.6774666,
   "Latitude": 105.4009508
 },
 {
   "STT": 1553,
   "Name": "Quầy thuốc Minh Thông",
   "address": "Số 99, tổ 3, ấp Bình Mỹ B, xãBình Thạnh, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.31316,
   "Latitude": 105.7750516
 },
 {
   "STT": 1554,
   "Name": "Cơ Sở thuốc Đông Y Mỹ Châu",
   "address": "Số 99A, ấp Long Phú A, xã PhúThành A, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.6975735,
   "Latitude": 105.4360963
 },
 {
   "STT": 1555,
   "Name": "Quầy thuốc Kim Phượng",
   "address": "Số A1/6, ấp An Lợi B, xã Định Yên, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.309874,
   "Latitude": 105.5532993
 },
 {
   "STT": 1556,
   "Name": "Nhà thuốc Minh Hùng",
   "address": "Số A30, đường Hùng Vương, khóm 1, phường 2, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2952161,
   "Latitude": 105.7659214
 },
 {
   "STT": 1557,
   "Name": "Quầy thuốc Hữu Lộc",
   "address": "Số nhà 18, tổ 16, ấp 1, xã Tân Hội Trung, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4569711,
   "Latitude": 105.7703287
 },
 {
   "STT": 1558,
   "Name": "Quầy thuốc Y Dược",
   "address": "Số nhà 263, tổ 20, ấp 3, xã Bình Hàng Tây, huyện CaoLãnh, TỈNH GIA LAI",
   "Longtitude": 10.3756051,
   "Latitude": 105.7644596
 },
 {
   "STT": 1559,
   "Name": "Cơ Sở Đông Y Vinh Phát Đường",
   "address": "Số nhà 266, ấp Mỹ Thới, xã Mỹ Xương, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4111988,
   "Latitude": 105.7057792
 },
 {
   "STT": 1560,
   "Name": "Quầy thuốc Thiên Hảo",
   "address": "Số nhà 52, Tổ 02, Ấp Long Hữu, xã Long Khánh A, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8035808,
   "Latitude": 105.314582
 },
 {
   "STT": 1561,
   "Name": "Quầy thuốc Hoàng Anh",
   "address": "Số nhà 76, ấp Phú Lợi A, xã PhúThuận B, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.7570042,
   "Latitude": 105.3423909
 },
 {
   "STT": 1562,
   "Name": "Quầy thuốc Hòa Long",
   "address": "Số nhà 801, ấp 3, xã An Hòa, huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.7436984,
   "Latitude": 105.3892372
 },
 {
   "STT": 1563,
   "Name": "Quầy thuốc Thu Thơ",
   "address": "Tân Bình, xã Tân Thành, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2482267,
   "Latitude": 105.6035329
 },
 {
   "STT": 1564,
   "Name": "Quầy thuốc Hưng Thịnh",
   "address": "Tân Dân, xã Tân Thuận Tây, Tp CaoLãnh, TỈNH GIA LAI",
   "Longtitude": 10.4641183,
   "Latitude": 105.5840847
 },
 {
   "STT": 1565,
   "Name": "Quầy thuốc Phúc Vinh",
   "address": "Tân Hòa Đông, xã Tân Mỹ, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3923418,
   "Latitude": 105.6471178
 },
 {
   "STT": 1566,
   "Name": "Quầy thuốc Hoa Tiến",
   "address": "Thạnh 1, thị trấn Lấp Vò, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3607896,
   "Latitude": 105.518867
 },
 {
   "STT": 1567,
   "Name": "Quầy thuốc Khoa Nam",
   "address": "Thành B, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8691327,
   "Latitude": 105.5064087
 },
 {
   "STT": 1568,
   "Name": "Quầy thuốc Minh Trai",
   "address": "Thành B, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8691327,
   "Latitude": 105.5064087
 },
 {
   "STT": 1569,
   "Name": "Quầy thuốc Ngọc Phiên",
   "address": "Thạnh Đông, xã Long Hưng B, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3264445,
   "Latitude": 105.6588485
 },
 {
   "STT": 1570,
   "Name": "Quầy thuốc Thanh Sơn",
   "address": "Thạnh, ấp 2A, xã Hưng Thạnh, huyện Tháp Mười,",
   "Longtitude": 10.6649367,
   "Latitude": 105.7019754
 },
 {
   "STT": 1571,
   "Name": "Quầy thuốc Thu Thùy",
   "address": "Thạnh, xã An Phú Thuận, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2169808,
   "Latitude": 105.8936224
 },
 {
   "STT": 1572,
   "Name": "Quầy thuốc Hoàn Đạt",
   "address": "Thạnh, xã Hội An Đông, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.4078862,
   "Latitude": 105.5532993
 },
 {
   "STT": 1573,
   "Name": "Quầy thuốc Xuân Lan 1",
   "address": "Thạnh, xã Mỹ An Hưng B, huyện Lấp",
   "Longtitude": 10.3831971,
   "Latitude": 105.6119301
 },
 {
   "STT": 1574,
   "Name": "Quầy thuốc Mỹ Tiên",
   "address": "Thạnh, xã Phong Hòa, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2640559,
   "Latitude": 105.6002024
 },
 {
   "STT": 1575,
   "Name": "Cơ Sở thuốc Đông Y Định Thọ Đường",
   "address": "Thạnh, xã Phú Điền, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.4098655,
   "Latitude": 105.8407722
 },
 {
   "STT": 1576,
   "Name": "Quầy thuốc Dịnh - Bé",
   "address": "Thạnh, xã Phú Điền, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.4098655,
   "Latitude": 105.8407722
 },
 {
   "STT": 1577,
   "Name": "Quầy thuốc Hồng Việt",
   "address": "Thạnh, xã Tân Phước, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2835047,
   "Latitude": 105.613997
 },
 {
   "STT": 1578,
   "Name": "Nhà thuốc Như Xuân",
   "address": "Thiên Hộ Dương, phường 4, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4563447,
   "Latitude": 105.6295038
 },
 {
   "STT": 1579,
   "Name": "Nhà thuốc Vạn Xuân",
   "address": "Thiên Hộ Dương, phường 4, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4563447,
   "Latitude": 105.6295038
 },
 {
   "STT": 1580,
   "Name": "Quầy thuốc Hồng Nhung",
   "address": "Thửa đất số 2 tờ bản đồ số 07 ấp Long Hội, xã Hòa Long, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2611796,
   "Latitude": 105.6588485
 },
 {
   "STT": 1581,
   "Name": "Quầy thuốc Phạm Huỳnh Khánh Ngân",
   "address": "Thuận Tây, xã Tân Thuận Tây, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4738847,
   "Latitude": 105.5858819
 },
 {
   "STT": 1582,
   "Name": "Cơ Sở thuốc Đông Y Vạn Hưng Đường",
   "address": "Thuận, xã Mỹ An Hưng B, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3831971,
   "Latitude": 105.6119301
 },
 {
   "STT": 1583,
   "Name": "Cơ Sở thuốc Đông Y Xuân An",
   "address": "Thuận, xã Mỹ An Hưng B, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3831971,
   "Latitude": 105.6119301
 },
 {
   "STT": 1584,
   "Name": "Quầy thuốc Duy Vân",
   "address": "Thuận, xã Mỹ An Hưng B, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3831971,
   "Latitude": 105.6119301
 },
 {
   "STT": 1585,
   "Name": "Quầy thuốc Huỳnh Giao",
   "address": "Thuận, xã Mỹ An Hưng B, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3831971,
   "Latitude": 105.6119301
 },
 {
   "STT": 1586,
   "Name": "Quầy thuốc Hữu Mạng",
   "address": "Thuận, xã Mỹ An Hưng B, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3831971,
   "Latitude": 105.6119301
 },
 {
   "STT": 1587,
   "Name": "Quầy thuốc Kim Hoàng",
   "address": "Thuận, xã Mỹ An Hưng B, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3831971,
   "Latitude": 105.6119301
 },
 {
   "STT": 1588,
   "Name": "Quầy thuốc Kim Xuân",
   "address": "Thuận, xã Mỹ An Hưng B, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3831971,
   "Latitude": 105.6119301
 },
 {
   "STT": 1589,
   "Name": "Quầy thuốc Ngọc Xuân",
   "address": "Thuận, xã Mỹ An Hưng B, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3831971,
   "Latitude": 105.6119301
 },
 {
   "STT": 1590,
   "Name": "Quầy thuốc Như Trang",
   "address": "Thuận, xã Mỹ An Hưng B, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3831971,
   "Latitude": 105.6119301
 },
 {
   "STT": 1591,
   "Name": "Quầy thuốc Trọng Nhân",
   "address": "Thuận, xã Mỹ An Hưng B, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3831971,
   "Latitude": 105.6119301
 },
 {
   "STT": 1592,
   "Name": "Quầy thuốc Kim Cúc",
   "address": "Thuận, xã Tân Hòa, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2074033,
   "Latitude": 105.6471178
 },
 {
   "STT": 1593,
   "Name": "Quầy thuốc Phú Quý",
   "address": "Thuận, xã Tân Hòa, huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2074033,
   "Latitude": 105.6471178
 },
 {
   "STT": 1594,
   "Name": "Quầy thuốc Ngọc Chọn",
   "address": "Thuận, xã Vĩnh Thạnh, huyện LấpVò, TỈNH GIA LAI",
   "Longtitude": 10.3357677,
   "Latitude": 105.6189708
 },
 {
   "STT": 1595,
   "Name": "Nhà thuốc Nguyễn Mỳ",
   "address": "Thường Kiệt, khóm 1, phường 2, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2929464,
   "Latitude": 105.7666716
 },
 {
   "STT": 1596,
   "Name": "Nhà thuốc Hưng Thịnh",
   "address": "Thường Kiệt, khóm 5, phường 1, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2929464,
   "Latitude": 105.7666716
 },
 {
   "STT": 1597,
   "Name": "Quầy thuốc Lê Mai",
   "address": "Thường Kiệt, thị trấn Thanh Bình, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5575377,
   "Latitude": 105.4863957
 },
 {
   "STT": 1598,
   "Name": "Quầy thuốc Lê Mai",
   "address": "Thường Kiệt, thị trấn Thanh Bình, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5575377,
   "Latitude": 105.4863957
 },
 {
   "STT": 1599,
   "Name": "Quầy thuốc Hà Mi",
   "address": "Thượng, xãThường Thới Hậu A, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8810796,
   "Latitude": 105.2721456
 },
 {
   "STT": 1600,
   "Name": "Quầy thuốc Đại Việt",
   "address": "Thượng, xãThường Thới Hậu A, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8810796,
   "Latitude": 105.2721456
 },
 {
   "STT": 1601,
   "Name": "Quầy thuốc Đồng Tâm",
   "address": "Thượng, xãThường Thới Hậu A, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8810796,
   "Latitude": 105.2721456
 },
 {
   "STT": 1602,
   "Name": "Quầy thuốc Lê Thị Cẩm Tö",
   "address": "Thượng, xãThường Thới Hậu A, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8810796,
   "Latitude": 105.2721456
 },
 {
   "STT": 1603,
   "Name": "Quầy thuốc Ngọc Giàu",
   "address": "Thượng, xãThường Thới Hậu A, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8810796,
   "Latitude": 105.2721456
 },
 {
   "STT": 1604,
   "Name": "Quầy thuốc Ngọc Oanh",
   "address": "Thượng, xãThường Thới Hậu A, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8810796,
   "Latitude": 105.2721456
 },
 {
   "STT": 1605,
   "Name": "Quầy thuốc Ngọc Thơm",
   "address": "Thượng, xãThường Thới Hậu A, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8810796,
   "Latitude": 105.2721456
 },
 {
   "STT": 1606,
   "Name": "Quầy thuốc Quốc Trung",
   "address": "Thượng, xãThường Thới Hậu A, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8810796,
   "Latitude": 105.2721456
 },
 {
   "STT": 1607,
   "Name": "Quầy thuốc Tấn Thi",
   "address": "Thượng, xãThường Thới Hậu A, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8810796,
   "Latitude": 105.2721456
 },
 {
   "STT": 1608,
   "Name": "Quầy thuốc Minh Thuận",
   "address": "Tỉnh lộ 5, ấp Phú Hòa, xã Tân Phú Đông, thị xã Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2788747,
   "Latitude": 105.7409852
 },
 {
   "STT": 1609,
   "Name": "Nhà thuốc Ngọc Hà",
   "address": "Tỉnh lộ 841, khóm Sở Thượng, phường An Lạc,thị xã Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8208018,
   "Latitude": 105.3283656
 },
 {
   "STT": 1610,
   "Name": "Quầy thuốc Huỳnh Nga",
   "address": "Tỉnh lộ 843, khóm Tân Đông A, thị trấn Thanh Bình, huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5626423,
   "Latitude": 105.5023955
 },
 {
   "STT": 1611,
   "Name": "Quầy thuốc Kim Hồng",
   "address": "Tỉnh lộ 854, khóm Phú Mỹ Hiệp, thị trấn Cái Tàu Hạ, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.257936,
   "Latitude": 105.870189
 },
 {
   "STT": 1612,
   "Name": "Quầy thuốc Thuận Hõa",
   "address": "Tỉnh lộ 854, khóm Phú Mỹ Hiệp, thị trấn Cái Tàu Hạ, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.257936,
   "Latitude": 105.870189
 },
 {
   "STT": 1613,
   "Name": "Quầy thuốc Ngọc Hân",
   "address": "Tỉnh lộ ĐT 842, ấp Hoàng Việt, xã Tân Phước, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8390414,
   "Latitude": 105.5591615
 },
 {
   "STT": 1614,
   "Name": "Quầy thuốc Ngọc Hằng",
   "address": "Tỉnh lộ ĐT 843, ấp 2, xã Tân Thành B, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8697347,
   "Latitude": 105.494688
 },
 {
   "STT": 1615,
   "Name": "Quầy thuốc Xuân Lan",
   "address": "TL 853, CDC ấpTân Phú, xã Tân Phú Trung, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2546901,
   "Latitude": 105.7360164
 },
 {
   "STT": 1616,
   "Name": "Nhà thuốc Sĩ Lan",
   "address": "TL853, ấp Tân Phú, xã Tân PhúTrung, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2335256,
   "Latitude": 105.7602262
 },
 {
   "STT": 1617,
   "Name": "Quầy thuốc Kim Hạnh",
   "address": "TL854, tổ 11,khóm Phú Mỹ Hiệp, thị trấn Cái Tàu Hạ, huyện ChâuThành, TỈNH GIA LAI",
   "Longtitude": 10.177211,
   "Latitude": 105.835031
 },
 {
   "STT": 1618,
   "Name": "Quầy thuốc Phước Thảo",
   "address": "Tổ 01, ấp Tân Hựu, xã Tân Nhuận Đông, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2547739,
   "Latitude": 105.8155181
 },
 {
   "STT": 1619,
   "Name": "Quầy thuốc Ngọc Mai",
   "address": "Tổ 05, ấp Tân Hòa, xã Tân Phú, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2405768,
   "Latitude": 105.7373482
 },
 {
   "STT": 1620,
   "Name": "Quầy thuốc Thu Nhi",
   "address": "Tổ 06, ấp Long Thới A, xã LongThuận, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.7710854,
   "Latitude": 105.283978
 },
 {
   "STT": 1621,
   "Name": "Quầy thuốc Võ Văn Hưởng",
   "address": "Tổ 1, ấp 1, xã Mỹ Hiệp, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4047542,
   "Latitude": 105.8116263
 },
 {
   "STT": 1622,
   "Name": "Quầy thuốc Hồng Linh",
   "address": "Tổ 1, ấp 1, xã MỹTrà, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4878315,
   "Latitude": 105.6441852
 },
 {
   "STT": 1623,
   "Name": "Quầy thuốc Ánh Khoa",
   "address": "Tổ 1, ấp 2, xã Mỹ Long, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.3635488,
   "Latitude": 105.7879371
 },
 {
   "STT": 1624,
   "Name": "Quầy thuốc Thông Hiệp",
   "address": "Tổ 1, ấp An Phú, xã An Nhơn, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2730435,
   "Latitude": 105.8583873
 },
 {
   "STT": 1625,
   "Name": "Quầy thuốc Hạnh Nhân",
   "address": "Tổ 1, ấp Bình Linh, xã Bình Thạnh, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.3203752,
   "Latitude": 105.7866496
 },
 {
   "STT": 1626,
   "Name": "Quầy thuốc Lê Thị Thu Hương",
   "address": "Tổ 1, ấp Hòa Bình, xã Hòa Tân, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.204205,
   "Latitude": 105.8204305
 },
 {
   "STT": 1627,
   "Name": "Cơ Sở thuốc Đông Y Phước ThọĐường",
   "address": "Tổ 1, ấp I, xã Mỹ Hiệp, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4047542,
   "Latitude": 105.8116263
 },
 {
   "STT": 1628,
   "Name": "Quầy thuốc Nam Anh",
   "address": "Tổ 1, ấp PhúThuận, xã Tân Phú Đông, thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2868669,
   "Latitude": 105.7633842
 },
 {
   "STT": 1629,
   "Name": "Quầy thuốc Hồng Nhật",
   "address": "Tổ 1, đường Gò Tháp, khóm 3, thị trấn Mỹ An, huyệnTháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5243845,
   "Latitude": 105.8494889
 },
 {
   "STT": 1630,
   "Name": "Quầy thuốc Trung Hiếu",
   "address": "Tổ 10, ấp 1, xã Gáo Giồng, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.6051812,
   "Latitude": 105.6369188
 },
 {
   "STT": 1631,
   "Name": "Quầy thuốc Ngọc Giàu",
   "address": "Tổ 10, ấp Long Hữu, xã Long Khánh A, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8035808,
   "Latitude": 105.314582
 },
 {
   "STT": 1632,
   "Name": "Quầy thuốc Thanh  Tùng",
   "address": "Tổ 10, ấp Phú Thạnh, xã PhúLong, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2306479,
   "Latitude": 105.8044244
 },
 {
   "STT": 1633,
   "Name": "Quầy thuốc Kim Chi",
   "address": "Tổ 10, ấp Tân Quới, xã Tân Phú Trung, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2462649,
   "Latitude": 105.7409852
 },
 {
   "STT": 1634,
   "Name": "Quầy thuốc Minh Hùng",
   "address": "Tổ 11, ấp Tân Phú, xã Tân Phú Trung, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2477844,
   "Latitude": 105.7601667
 },
 {
   "STT": 1635,
   "Name": "Quầy thuốc Phúc Trí",
   "address": "Tổ 13, ấp 1, xã Mỹ Long, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.3635488,
   "Latitude": 105.7879371
 },
 {
   "STT": 1636,
   "Name": "Quầy thuốc Chí Hùng",
   "address": "Tổ 13, ấp 4, xã Bình Hàng Trung, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4088492,
   "Latitude": 105.7527221
 },
 {
   "STT": 1637,
   "Name": "Quầy thuốc Bé Sáu",
   "address": "Tổ 13, ấp Long Thạnh, xã LongThuận, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.759573,
   "Latitude": 105.3045666
 },
 {
   "STT": 1638,
   "Name": "Quầy thuốc Huỳnh Thị Bích Thủy",
   "address": "Tổ 15, ấp An Phú, xã An Nhơn, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2730435,
   "Latitude": 105.8583873
 },
 {
   "STT": 1639,
   "Name": "Quầy thuốc Trung Hiếu",
   "address": "Tổ 16, ấp 1, xã Tân Hội Trung, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.5179126,
   "Latitude": 105.8809053
 },
 {
   "STT": 1640,
   "Name": "Quầy thuốc Vân Sơn 2",
   "address": "Tổ 16, ấp 1, xã Tân Hội Trung, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.5179126,
   "Latitude": 105.8809053
 },
 {
   "STT": 1641,
   "Name": "Quầy thuốc Thủy Tươi",
   "address": "Tổ 17, ấp Bình Mỹ B, xã Bình Thạnh, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.31316,
   "Latitude": 105.7750516
 },
 {
   "STT": 1642,
   "Name": "Quầy thuốc Thanh Mai",
   "address": "Tổ 17, ấp Long Hưng, xã LongThuận, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.7710854,
   "Latitude": 105.283978
 },
 {
   "STT": 1643,
   "Name": "Cơ Sở thuốc Đông Y Phö Hữu",
   "address": "Tổ 18, ấp 2, xã Mỹ Long, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.3744301,
   "Latitude": 105.7879371
 },
 {
   "STT": 1644,
   "Name": "Quầy thuốc Bình An",
   "address": "Tổ 18, ấp Bình Mỹ A, xã Bình Thạnh, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.3200374,
   "Latitude": 105.7879371
 },
 // {
 //   "STT": 1645,
 //   "Name": "Quầy thuốc Thúy Di",
 //   "address": "Tổ 18, ấp Hòa Bình, xã Hòa Tân, huyện Châu Thành, TỈNH GIA LAI",
 //   "Longtitude": 15.9128998,
 //   "Latitude": 79.7399875
 // },
 {
   "STT": 1646,
   "Name": "Quầy thuốc Kim Tuyến",
   "address": "Tổ 2, ấp 3, xã Bình Hàng Tây, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.3625893,
   "Latitude": 105.7606608
 },
 {
   "STT": 1647,
   "Name": "Quầy thuốc Ngô Nguyễn",
   "address": "Tổ 2, ấp Tân An, xã Tân Nhuận Đông, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2422003,
   "Latitude": 105.8231588
 },
 {
   "STT": 1648,
   "Name": "Quầy thuốc Tuyết Nhi",
   "address": "Tổ 2, ấp Tân An, xã Tân Nhuận Đông, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2422003,
   "Latitude": 105.8231588
 },
 {
   "STT": 1649,
   "Name": "Quầy thuốc Ngọc Mai",
   "address": "Tổ 2, ấp Thanh Tiến, xã Nhị Mỹ, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4641259,
   "Latitude": 105.6286054
 },
 {
   "STT": 1650,
   "Name": "Quầy thuốc Phöc Hậu",
   "address": "Tổ 23, ấp 3, xã Mỹ Long, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.3744301,
   "Latitude": 105.7879371
 },
 {
   "STT": 1651,
   "Name": "Quầy thuốc Châu Thanh",
   "address": "Tổ 23, ấp Thượng 1, xã Thường Thới Tiền, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8208572,
   "Latitude": 105.2814555
 },
 {
   "STT": 1652,
   "Name": "Quầy thuốc Kim Cương",
   "address": "Tổ 24, ấp 3, xã Bình Hàng Trung, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4088492,
   "Latitude": 105.7527221
 },
 {
   "STT": 1653,
   "Name": "Quầy thuốc Phượng Nở",
   "address": "Tổ 24, ấp 3, xã Bình Hàng Trung, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4088492,
   "Latitude": 105.7527221
 },
 {
   "STT": 1654,
   "Name": "Quầy thuốc Thu Trang",
   "address": "Tổ 25, ấp 2, xã Mỹ Long, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.3744301,
   "Latitude": 105.7879371
 },
 {
   "STT": 1655,
   "Name": "Quầy thuốc Đức Tín",
   "address": "Tổ 26, ấp 2, xã Mỹ Long, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.3744301,
   "Latitude": 105.7879371
 },
 {
   "STT": 1656,
   "Name": "Quầy thuốc Khánh Nguyên",
   "address": "Tổ 28, ấp An Định, xã An Bình, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4596591,
   "Latitude": 105.6557945
 },
 {
   "STT": 1657,
   "Name": "Quầy thuốc Vũ",
   "address": "Tổ 3, ấp 3, xã Bình Hàng Tây, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.3625893,
   "Latitude": 105.7606608
 },
 {
   "STT": 1658,
   "Name": "Quầy thuốc Bệnh Viện Phổi ĐồngTháp",
   "address": "Tổ 3, ấp 4, xã Mỹ Thọ, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4501101,
   "Latitude": 105.6970266
 },
 {
   "STT": 1659,
   "Name": "Nhà thuốc Bệnh Viện Tâm ThầnGIA LAI",
   "address": "Tổ 3, ấp Mỹ Đông 4, xã Mỹ Thọ, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4501101,
   "Latitude": 105.6970266
 },
 {
   "STT": 1660,
   "Name": "Quầy thuốc Cao Sĩ Lợi",
   "address": "Tổ 3, ấp Tân Phú, xã Tân Phú Trung, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2477844,
   "Latitude": 105.7601667
 },
 {
   "STT": 1661,
   "Name": "Quầy thuốc Trắng Thảo",
   "address": "Tổ 38, ấp An Hòa, xã An Bình A, thị xã Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.7829013,
   "Latitude": 105.3716684
 },
 {
   "STT": 1662,
   "Name": "Quầy thuốc Đăng Khôi",
   "address": "Tổ 4, ấp 1, xã Bình Hàng Trung, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4088492,
   "Latitude": 105.7527221
 },
 {
   "STT": 1663,
   "Name": "Quầy thuốc Công Khanh",
   "address": "Tổ 4, ấp 1, xã Tân Hội Trung, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4569711,
   "Latitude": 105.7703287
 },
 {
   "STT": 1664,
   "Name": "Quầy thuốc Tâm Anh",
   "address": "Tổ 5, ấp 4, xã Bình Hàng Trung, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4088492,
   "Latitude": 105.7527221
 },
 {
   "STT": 1665,
   "Name": "Quầy thuốc Đoan Trang",
   "address": "Tổ 5, ấp 5, xã Phong Mỹ, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.5318166,
   "Latitude": 105.5826123
 },
 {
   "STT": 1666,
   "Name": "Quầy thuốc Kiều Oanh 2",
   "address": "Tổ 55, ấp Phú Lợi B, xã Phú Thành B, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.7570042,
   "Latitude": 105.3423909
 },
 {
   "STT": 1667,
   "Name": "Quầy thuốc Phöc Thịnh",
   "address": "Tổ 6, ấp 4, xã Bình Hàng Trung, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4088592,
   "Latitude": 105.7509828
 },
 {
   "STT": 1668,
   "Name": "Quầy thuốc Kiều Khoa",
   "address": "Tổ 6, ấp An Nghiệp, xã An Bình, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4596806,
   "Latitude": 105.6617588
 },
 {
   "STT": 1669,
   "Name": "Quầy thuốc Kim Ngọc",
   "address": "Tổ 6, ấp Đông Mỹ, xã Mỹ Hội, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.404013,
   "Latitude": 105.7248507
 },
 {
   "STT": 1670,
   "Name": "Quầy thuốc Trường Khanh",
   "address": "Tổ 6, ấp Mỹ Đông 2, xã Mỹ, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.5211623,
   "Latitude": 105.7938069
 },
 {
   "STT": 1671,
   "Name": "Quầy thuốc Nguyễn Tâm",
   "address": "Tổ 7, ấp 3, xã Phương Trà, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.5069025,
   "Latitude": 105.654042
 },
 {
   "STT": 1672,
   "Name": "Quầy thuốc Ngọc Thanh",
   "address": "Tổ 7, ấp Hòa Dân, xã Nhị Mỹ, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4934548,
   "Latitude": 105.6974786
 },
 {
   "STT": 1673,
   "Name": "Quầy thuốc Huỳnh Đạt",
   "address": "Tổ 7, ấp Hòa Quới, xã Hòa Tân, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.1719042,
   "Latitude": 105.8172881
 },
 {
   "STT": 1674,
   "Name": "Quầy thuốc Huỳnh Duy",
   "address": "Tổ 7, ấp Tân Phú, xã An Nhơn, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2462649,
   "Latitude": 105.7409852
 },
 {
   "STT": 1675,
   "Name": "Quầy thuốc Thúy Vân",
   "address": "Tổ 7, ấp TânThuận, xã Tân Phú Trung, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2422003,
   "Latitude": 105.8231588
 },
 {
   "STT": 1676,
   "Name": "Quầy thuốc Diễm Quỳnh",
   "address": "Tổ 8, ấp 3, xã Phương Trà, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.5069025,
   "Latitude": 105.654042
 },
 {
   "STT": 1677,
   "Name": "Quầy thuốc Cẩm Duyên",
   "address": "Tổ 8, ấp An Thạnh, xã An Phú Thuận, huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2169808,
   "Latitude": 105.8936224
 },
 {
   "STT": 1678,
   "Name": "Quầy thuốc Cẩm Hằng",
   "address": "Tổ 8, ấp Lợi An, xã Thanh Mỹ, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.4098655,
   "Latitude": 105.8407722
 },
 {
   "STT": 1679,
   "Name": "Quầy thuốc Cẩm Hằng",
   "address": "Tổ 8, ấp Lợi An, xã Thanh Mỹ, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.4098655,
   "Latitude": 105.8407722
 },
 {
   "STT": 1680,
   "Name": "Quầy thuốc Quyên Quyên",
   "address": "Tổ 8, ấp Phú Lợi B, xã Phú Thuận B, huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.7570042,
   "Latitude": 105.3423909
 },
 {
   "STT": 1681,
   "Name": "Quầy thuốc Ngọc Giàu",
   "address": "Tổ 9B, ấp 4, xã Bình Hàng Trung, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4088492,
   "Latitude": 105.7527221
 },
 {
   "STT": 1682,
   "Name": "Quầy thuốc Minh Nhật",
   "address": "Trần Phú, khóm 4, thị trấn Mỹ An, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5186237,
   "Latitude": 105.8435599
 },
 {
   "STT": 1683,
   "Name": "Quầy thuốc Mỹ Hạnh",
   "address": "Trung, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4088492,
   "Latitude": 105.7527221
 },
 {
   "STT": 1684,
   "Name": "Quầy thuốc Ngọc Thöy",
   "address": "Trung, xã Bình Thạnh Trung, huyện  Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3803603,
   "Latitude": 105.5591615
 },
 {
   "STT": 1685,
   "Name": "Quầy thuốc Öt Hậu",
   "address": "Trường, xã Mỹ Hội, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4209146,
   "Latitude": 105.7292491
 },
 {
   "STT": 1686,
   "Name": "Quầy thuốc Thảo Nguyên",
   "address": "Tuyến dân cư xã Tân Hội Trung, ấp 1, xã Tân Hội Trung, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4415928,
   "Latitude": 105.6969788
 },
 {
   "STT": 1687,
   "Name": "Quầy thuốc Phương Hạnh",
   "address": "Văn Cừ, khóm 2, thị trấn Mỹ An, huyện  Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5382423,
   "Latitude": 105.838849
 },
 {
   "STT": 1688,
   "Name": "Quầy thuốc Huỳnh Yến 1",
   "address": "Văn Cừ, thị trấn Mỹ An, huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5213947,
   "Latitude": 105.8419096
 },
 {
   "STT": 1689,
   "Name": "Quầy thuốc Võ Ngọc Phượng",
   "address": "Văn Khải, tổ 13, ấp 3, xã Mỹ Ngãi, thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.5034079,
   "Latitude": 105.590733
 },
 {
   "STT": 1690,
   "Name": "Quầy thuốc Bích Vân",
   "address": "xã Bình Hàng Trung, huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4088492,
   "Latitude": 105.7527221
 },
 {
   "STT": 1691,
   "Name": "Quầy thuốc Dung Anh",
   "address": "xã Bình Thạnh Trung, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3803603,
   "Latitude": 105.5591615
 },
 {
   "STT": 1692,
   "Name": "Quầy thuốc Hồng Hà",
   "address": "xã Bình Thạnh Trung, huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3803603,
   "Latitude": 105.5591615
 },
 {
   "STT": 1693,
   "Name": "Tủ thuốcTrạm Y Tế Xã Tân Thành A",
   "address": "Ấp Thi Sơn, xã Tân Thành A, huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8858566,
   "Latitude": 105.5480851
 }
];