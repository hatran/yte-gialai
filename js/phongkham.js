var dataphongkham = [
 {
   "STT": 1,
   "Name": "Phòng khám đa khoa Thành phố Cao Lãnh",
   "address": "216 Phạm Hữu Lầu, phường 4, thành phố hường Cao Lãnh, GIA LAI",
   "Longtitude": 10.4437397,
   "Latitude": 105.6309692
 },
 {
   "STT": 2,
   "Name": "Phòng khám đa khoa Thành phố Sa Đéc",
   "address": "Đường Trần Thị Nhượng, khóm 3, phường 1, thành phố Sa Đéc, GIA LAI",
   "Longtitude": 10.3009484,
   "Latitude": 105.7529005
 },
 {
   "STT": 3,
   "Name": "Phòng khám đa khoa khu vực Vĩnh Thạnh",
   "address": "QL80, Vĩnh Thạnh, Lấp Vò, GIA LAI",
   "Longtitude": 10.33838,
   "Latitude": 105.6115883
 },
 {
   "STT": 4,
   "Name": "Phòng khám đa khoa khu vực Tháp Mười",
   "address": "Khóm 2, thị trấn Mỹ An, huyện Tháp Mười, GIA LAI",
   "Longtitude": 10.5302706,
   "Latitude": 105.8784683
 },
 {
   "STT": 5,
   "Name": "Phòng khám đa khoa khu vực thị xã Hồng Ngự",
   "address": "Đường Chu Văn An, phường An Thạnh, Hồng Ngự, GIA LAI",
   "Longtitude": 10.8075988,
   "Latitude": 105.339896
 },
 {
   "STT": 6,
   "Name": "Phòng khám đa khoa khu vực Tân Thành, Huyện Lai Vung",
   "address": "Tân Thành, huyện Lai Vung, GIA LAI",
   "Longtitude": 10.2640559,
   "Latitude": 105.6002024
 },
 {
   "STT": 7,
   "Name": "Phòng khám đa khoa Quân dân y Dinh Bà",
   "address": "Ấp Dinh Bà, xã Tân Hộ Cơ, huyện Tân Hồng, GIA LAI",
   "Longtitude": 10.9327262,
   "Latitude": 105.4419546
 },
 {
   "STT": 8,
   "Name": "Phòng khám Bác sĩ Hồ Thành",
   "address": "603 Ấp 2, thị trấn Tràm Chim, huyện Tam Nông, GIA LAI",
   "Longtitude": 10.6790239,
   "Latitude": 105.5576686
 },
 {
   "STT": 9,
   "Name": "Phòng khám Bác sĩ Hướng",
   "address": "Trần Phú, thị trấn Hồng Ngự, huyện Hồng Ngự, GIA LAI",
   "Longtitude": 10.7829013,
   "Latitude": 105.3716684
 },
 {
   "STT": 10,
   "Name": "Phòng khám Bác sĩ Huỳnh Anh Hòa",
   "address": "Khóm 4 thị trấn Mỹ An, huyện Tháp Mười, GIA LAI",
   "Longtitude": 10.5117149,
   "Latitude": 105.8521796
 },
 {
   "STT": 11,
   "Name": "Phòng khám Bác sĩ Huỳnh Hồng Châu",
   "address": "669 A Ấp Vĩnh Bình, xã Vĩnh Thạnh, huyện Lấp Vò, GIA LAI",
   "Longtitude": 10.3447983,
   "Latitude": 105.6177942
 },
 {
   "STT": 12,
   "Name": "Phòng khám Bác sĩ Huỳnh Kim Lâm",
   "address": "20 Ql30, xã An Bình, thành phố Cao Lãnh, GIA LAI",
   "Longtitude": 10.4533365,
   "Latitude": 105.6566614
 },
 {
   "STT": 13,
   "Name": "Phòng khám Bác sĩ Huỳnh Văn Hồng",
   "address": "Ấp Phú Mỹ Hiệp, thị trấn Cái Tàu Hạ, huyện Châu Thành, GIA LAI",
   "Longtitude": 10.2535915,
   "Latitude": 105.8671954
 },
 {
   "STT": 14,
   "Name": "Phòng khám Bác sĩ Kim Hoàng",
   "address": "Ấp Long Sơn Ngọc, xã Thông Bình, huyện Tân Hồng, GIA LAI",
   "Longtitude": 10.9051057,
   "Latitude": 105.5234111
 },
 {
   "STT": 15,
   "Name": "Phòng khám Bác sĩ Lâm Thị Việt Phương",
   "address": "Ngô Quyền, thị trấn Hồng Ngự, huyện Hồng Ngự, GIA LAI",
   "Longtitude": 10.8057383,
   "Latitude": 105.3397518
 },
 {
   "STT": 16,
   "Name": "Phòng khám Bác sĩ Lành",
   "address": "Ấp Phú Yên, xã An Long, huyện Tam Nông, GIA LAI",
   "Longtitude": 10.720632,
   "Latitude": 105.4126652
 },
 {
   "STT": 17,
   "Name": "Phòng khám Bác sĩ Lê Phước Dư",
   "address": "12 Ấp Long Phú A, xã Phú Thành A, huyện Tam Nông, GIA LAI",
   "Longtitude": 10.6975735,
   "Latitude": 105.4360963
 },
 {
   "STT": 18,
   "Name": "Phòng khám Bác sĩ Lê Thị Thu Vân",
   "address": "124 Đường 30 Tháng 4, phường 1, thành phố Cao Lãnh, GIA LAI",
   "Longtitude": 10.4655114,
   "Latitude": 105.6278771
 },
 {
   "STT": 19,
   "Name": "Phòng khám Bác sĩ Lê Tùng Lâm",
   "address": "Ấp 2, thị trấn Tràm Chim, huyện Tam Nông, GIA LAI",
   "Longtitude": 10.6790239,
   "Latitude": 105.5576686
 },
 {
   "STT": 20,
   "Name": "Phòng khám Bác sĩ Lê Văn Cường",
   "address": "Ấp Trung, xã Tân Thạnh, huyện Thanh Bình, GIA LAI",
   "Longtitude": 10.5717356,
   "Latitude": 105.4504695
 },
 {
   "STT": 21,
   "Name": "Phòng khám Bác sĩ Lê Văn Việt",
   "address": "Ấp An Phú, xã An Nhơn, huyện Châu Thành, GIA LAI",
   "Longtitude": 10.2607596,
   "Latitude": 105.8750068
 },
 {
   "STT": 22,
   "Name": "Phòng khám Bác sĩ Mai Phú Cường",
   "address": "245 Ấp Phú Thọ, xã An Long, huyện Tam Nông, Dồng Tháp",
   "Longtitude": 10.720632,
   "Latitude": 105.4126652
 },
 {
   "STT": 23,
   "Name": "Phòng khám Bác sĩ Mai Thị Hồng Vân",
   "address": "Ấp Mỹ Thuận, thị trấn Mỹ Thọ, huyện Cao Lãnh, Dồng Tháp",
   "Longtitude": 10.4501101,
   "Latitude": 105.6970266
 },
 {
   "STT": 24,
   "Name": "Phòng khám Bác sĩ Nghiêm",
   "address": "Ấp Phú Mỹ Hiệp, thị trấn Cái Tàu Hạ, huyện Châu Thành, GIA LAI",
   "Longtitude": 10.2535915,
   "Latitude": 105.8671954
 },
 {
   "STT": 25,
   "Name": "Phòng khám Bác sĩ Nguyễn Công Bằng",
   "address": "32 Khóm 1 Trần Hưng Đạo, phường 2, thị xã Sa Đéc, GIA LAI",
   "Longtitude": 10.2952895,
   "Latitude": 105.7669313
 },
 {
   "STT": 26,
   "Name": "Phòng khám Bác sĩ Nguyễn Hiếu Nhân",
   "address": "683/F Nguyễn Chí Thanh, Ấp 2, thị trấn Tràm Chim, huyện Tam Nông, GIA LAI",
   "Longtitude": 10.6701615,
   "Latitude": 105.5649213
 },
 {
   "STT": 27,
   "Name": "Phòng khám Bác sĩ Nguyễn Ngọc Diệp",
   "address": "A0881 Phạm Hữu Lầu, phường 6, thành phố Cao Lãnh, GIA LAI",
   "Longtitude": 10.4197235,
   "Latitude": 105.6438177
 },
 {
   "STT": 28,
   "Name": "Phòng khám Bác sĩ Nguyên Nhật Phùng",
   "address": "Ql30, thị trấn Thanh Bình, huyện Thanh Bình, GIA LAI",
   "Longtitude": 10.5566772,
   "Latitude": 105.4924415
 },
 {
   "STT": 29,
   "Name": "Phòng khám Bác sĩ Nguyễn Phước Huy",
   "address": "Lê Hồng Phong, thị trấn Hồng Ngự, huyện Hồng Ngự, GIA LAI",
   "Longtitude": 10.7829013,
   "Latitude": 105.3716684
 },
 {
   "STT": 30,
   "Name": "Phòng khám Bác sĩ Nguyễn Quốc Chánh",
   "address": "Lê Anh Xuân, phường 2, thành phố Cao Lãnh, GIA LAI",
   "Longtitude": 10.4556321,
   "Latitude": 105.6389587
 },
 {
   "STT": 31,
   "Name": "Phòng khám Bác sĩ Nguyễn Thanh Hải",
   "address": "205 Ấp Tân Bình, xã Tân Thành, huyện Lai Vung, GIA LAI",
   "Longtitude": 10.2595167,
   "Latitude": 105.5896702
 },
 {
   "STT": 32,
   "Name": "Phòng khám Bác sĩ Nguyễn Thanh Hồngnguyễn",
   "address": "Khóm 4 thị trấn Mỹ An, huyện Tháp Mười, GIA LAI",
   "Longtitude": 10.5117149,
   "Latitude": 105.8521796
 },
 {
   "STT": 33,
   "Name": "Phòng khám Bác sĩ Nguyễn Thanh Vân",
   "address": "Thị trấn Mỹ Thọ, thành phố Cao Lãnh, GIA LAI",
   "Longtitude": 10.4501101,
   "Latitude": 105.6970266
 },
 {
   "STT": 34,
   "Name": "Phòng khám Bác sĩ Nguyễn Thị Hướng",
   "address": "111a Phạm Hữu Lầu, phường 4, huyện Cao Lãnh, GIA LAI",
   "Longtitude": 10.4484172,
   "Latitude": 105.6299829
 },
 {
   "STT": 35,
   "Name": "Phòng Mạch Bác sĩ Nguyễn Thị Thủ",
   "address": "368 Ấp Tân Dinh, xã Tân Hò, huyện Thanh Bình, GIA LAI",
   "Longtitude": 10.6706207,
   "Latitude": 105.35007
 },
 {
   "STT": 36,
   "Name": "Phòng khám Bác sĩ Nguyễn Tiến Điệp",
   "address": "Trần Văn Sinh, thị trấn Hồng Ngự, huyện Hồng Ngự, GIA LAI",
   "Longtitude": 10.7979866,
   "Latitude": 105.2897042
 },
 {
   "STT": 37,
   "Name": "Phòng khám Bác sĩ Nguyễn Trung Quân",
   "address": "Thị trấn Mỹ Thọ, thành phố Cao Lãnh, GIA LAI",
   "Longtitude": 10.4501101,
   "Latitude": 105.6970266
 },
 {
   "STT": 38,
   "Name": "Phòng khám Bác sĩ Nguyễn Văn Bé",
   "address": "Ấp An Thạnh A2, thị trấn Hồng Ngự, huyện Hồng Ngự, GIA LAI",
   "Longtitude": 10.8143532,
   "Latitude": 105.3453184
 },
 {
   "STT": 39,
   "Name": "Phòng khám Bác sĩ Nguyễn Văn Chính",
   "address": "Ấp Tân Nghĩa, xã Tân Nhuận Đông, huyện Châu Thành, GIA LAI",
   "Longtitude": 10.2422003,
   "Latitude": 105.8231588
 },
 {
   "STT": 40,
   "Name": "Phòng khám Bác sĩ Nguyễn Văn Lành",
   "address": "Ấp Phú Thọ B, xã Phú Thọ, huyện Tam Nông, GIA LAI",
   "Longtitude": 10.681937,
   "Latitude": 105.4927557
 },
 {
   "STT": 41,
   "Name": "Phòng khám Bác sĩ Nguyễn Văn Tân",
   "address": "Nguyễn Huệ, Ấp 1, thị trấn Sa Rài, huyện Tân Hồng, GIA LAI",
   "Longtitude": 10.8732659,
   "Latitude": 105.4625849
 },
 {
   "STT": 42,
   "Name": "Phòng khám Bác sĩ Nguyễn Văn Thảo",
   "address": "Chợ Hòa Thành xã Hòa Thành, huyện Lai Vung, GIA LAI",
   "Longtitude": 10.2914895,
   "Latitude": 105.7057792
 },
 {
   "STT": 43,
   "Name": "Phòng khám Bác sĩ Nguyễn Văn Tráng",
   "address": "A0308 Tân Việt Hòa phường 6, huyện Cao Lãnh, GIA LAI",
   "Longtitude": 10.4323878,
   "Latitude": 105.6403431
 },
 {
   "STT": 44,
   "Name": "Phòng khám Bác sĩ Nông Thành Thông",
   "address": "798b Ấp Trung, xã Tân Thạnh, huyện Thanh Bình, GIA LAI",
   "Longtitude": 10.5717356,
   "Latitude": 105.4504695
 },
 {
   "STT": 45,
   "Name": "Phòng khám Bác sĩ Phạm Duy Thắng",
   "address": "Ql30, xã Tân Thạnh, huyện Thanh Bình, GIA LAI",
   "Longtitude": 10.5761773,
   "Latitude": 105.4595306
 },
 {
   "STT": 46,
   "Name": "Phòng khám Bác sĩ Phạm Hoàng Trung",
   "address": "87/2 Ấp Phú Mỹ, thị trấn Cái Tàu Hạ, huyện Châu Thành, GIA LAI",
   "Longtitude": 10.2535915,
   "Latitude": 105.8671954
 },
 {
   "STT": 47,
   "Name": "Phòng khám Bác sĩ Phạm Ngọc Bạch",
   "address": "1782 Ấp Hòa Thuận, xã Hòa An, huyện Cao Lãnh, GIA LAI",
   "Longtitude": 10.4659127,
   "Latitude": 105.6192603
 },
 {
   "STT": 48,
   "Name": "Phòng khám Bác sĩ Phạm Nông",
   "address": "432 Ấp Tân Đông B, thị trấn Thanh Bình, huyện Thanh Bình, GIA LAI",
   "Longtitude": 10.5554601,
   "Latitude": 105.491758
 },
 {
   "STT": 49,
   "Name": "Phòng khám Bác sĩ Phạm Thị Kim Thoa",
   "address": "210 Ấp Phú Thọ, xã An Long, huyện Tam Nông, GIA LAI",
   "Longtitude": 10.720632,
   "Latitude": 105.4126652
 },
 {
   "STT": 50,
   "Name": "Phòng khám Bác sĩ Phan Văn Bé Bảy",
   "address": "35 Lê Lợi, phường 2, thành phố Cao Lãnh, GIA LAI",
   "Longtitude": 10.452644,
   "Latitude": 105.635407
 },
 {
   "STT": 51,
   "Name": "Phòng khám Bác sĩ Phan Văn E",
   "address": "210 Ấp Bình Chánh, xã Bình Thành, huyện Thanh Bình, GIA LAI",
   "Longtitude": 10.5766484,
   "Latitude": 105.5591615
 },
 {
   "STT": 52,
   "Name": "Phòng khám Bác sĩ Phan Văn Hải",
   "address": "Thiên Hộ Dương, huyện Hồng Ngự, GIA LAI",
   "Longtitude": 10.8089476,
   "Latitude": 105.3400017
 },
 {
   "STT": 53,
   "Name": "Phòng khám Khu Vực",
   "address": "Quốc Lộ 80 thị trấn Lấp Vò huyện Lấp Vò, huyện Lấp Vò, GIA LAI",
   "Longtitude": 10.3609111,
   "Latitude": 105.5194518
 },
 {
   "STT": 54,
   "Name": "Phòng Mạch Bác sĩ Tùng",
   "address": "443 Nguyễn Chí Thanh Ấp 2 thị trấn Tràm Chim huyện Tam Nông, huyện Tam Nông, GIA LAI",
   "Longtitude": 10.6718484,
   "Latitude": 105.5698995
 },
 {
   "STT": 55,
   "Name": "Phòng Mạch Tư Khóm 4 Thị Trấn Mỹ An",
   "address": "Khóm 4 thị trấn Mỹ An, huyện Tháp Mười, GIA LAI",
   "Longtitude": 10.5117149,
   "Latitude": 105.8521796
 },
 {
   "STT": 56,
   "Name": "Phòng khám Bác sĩ Phúc",
   "address": "Hùng Vương, phường 2, huyện Cao Lãnh, GIA LAI",
   "Longtitude": 10.4576052,
   "Latitude": 105.6397472
 },
 {
   "STT": 57,
   "Name": "Phòng khám Bác sĩ Thái Thị Thu Hồng",
   "address": " Ấp Phú Hòa, thị trấn Cái Tàu Hạ, huyện Châu Thành, GIA LAI",
   "Longtitude": 10.2535915,
   "Latitude": 105.8671954
 },
 {
   "STT": 58,
   "Name": "Phòng khám Bác sĩ Trần Duy Thanh",
   "address": "325 Ấp Tân Đông B, thị trấn Thanh Bình, huyện Thanh Bình, GIA LAI",
   "Longtitude": 10.6158374,
   "Latitude": 105.4772024
 },
 {
   "STT": 59,
   "Name": "Phòng khám Bác sĩ Trần Thị Năm",
   "address": "Ấp 1, xã Tân Mỹ, huyện Thanh Bình, GIA LAI",
   "Longtitude": 10.6215091,
   "Latitude": 105.5357139
 },
 {
   "STT": 60,
   "Name": "Phòng khám Bác sĩ Võ Thị Chín",
   "address": "23 Trần Hưng Đạo, Ấp 2, thị trấn Tràm Chim, huyện Tam Nông, GIA LAI",
   "Longtitude": 10.6701615,
   "Latitude": 105.565093
 },
 {
   "STT": 61,
   "Name": "Phòng khám Bác sĩ Võ Văn Gặp",
   "address": "434 Ấp Bình Hòa, xã Bình Thành, huyện Thanh Bình, GIA LAI",
   "Longtitude": 10.5766484,
   "Latitude": 105.5591615
 }
];