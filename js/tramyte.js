var datatramyte = [
 {
   "STT": 1,
   "Name": "Trạm y tế Phường An Lộc",
   "address": "Phường An Lộc, Thị xã Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.7976262,
   "Latitude": 105.3511736
 },
 {
   "STT": 2,
   "Name": "Trạm y tế Phường An Thạnh",
   "address": "Phường An Thạnh, Thị xã Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8143532,
   "Latitude": 105.3453184
 },
 {
   "STT": 3,
   "Name": "Trạm y tế Xã Bình Thạnh",
   "address": "Xã Bình Thạnh, Thị xã Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8255238,
   "Latitude": 105.3950939
 },
 {
   "STT": 4,
   "Name": "Trạm y tế Xã Tân Hội",
   "address": "Xã Tân Hội, Thị xã Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8469276,
   "Latitude": 105.3511736
 },
 {
   "STT": 5,
   "Name": "Trạm y tế Phường An Lạc",
   "address": "Phường An Lạc, Thị xã Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8342679,
   "Latitude": 105.3306814
 },
 {
   "STT": 6,
   "Name": "Trạm y tế Xã An Bình B",
   "address": "Xã An Bình B, Thị xã Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.7966565,
   "Latitude": 105.4243804
 },
 {
   "STT": 7,
   "Name": "Trạm y tế Xã An Bình A",
   "address": "Xã An Bình A, Thị xã Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.7829013,
   "Latitude": 105.3716684
 },
 {
   "STT": 8,
   "Name": "Trạm y tế Phường 3",
   "address": "Phường 3, Thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.3063257,
   "Latitude": 105.7629924
 },
 {
   "STT": 9,
   "Name": "Trạm y tế Phường 1",
   "address": "Phường 1, Thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.30118,
   "Latitude": 105.7571236
 },
 {
   "STT": 10,
   "Name": "Trạm y tế Phường 4",
   "address": "Phường 4, Thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.294869,
   "Latitude": 105.7747307
 },
 {
   "STT": 11,
   "Name": "Trạm y tế Phường 2",
   "address": "Phường 2, Thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2812794,
   "Latitude": 105.7747307
 },
 {
   "STT": 12,
   "Name": "Trạm y tế Xã Tân Khánh Đông",
   "address": "Xã Tân Khánh Đông, Thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.355597,
   "Latitude": 105.7292491
 },
 {
   "STT": 13,
   "Name": "Trạm y tế Phường Tân Quy Đông",
   "address": "Phường Tân Quy Đông, Thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.319217,
   "Latitude": 105.7497878
 },
 {
   "STT": 14,
   "Name": "Trạm y tế Phường An Hoà",
   "address": "Phường An Hoà, Thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2977573,
   "Latitude": 105.7439194
 },
 {
   "STT": 15,
   "Name": "Trạm y tế Xã Tân Quy Tây",
   "address": "Xã Tân Quy Tây, Thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.3120792,
   "Latitude": 105.7292491
 },
 {
   "STT": 16,
   "Name": "Trạm y tế Xã Tân Phú Đông",
   "address": "Xã Tân Phú Đông, Thành phố Sa Đéc, TỈNH GIA LAI",
   "Longtitude": 10.2868669,
   "Latitude": 105.7633842
 },
 {
   "STT": 17,
   "Name": "Trạm y tế Phường 11",
   "address": "Phường 11, Thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4910581,
   "Latitude": 105.5796808
 },
 {
   "STT": 18,
   "Name": "Trạm y tế Phường 1",
   "address": "Phường 1, Thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4597303,
   "Latitude": 105.6339216
 },
 {
   "STT": 19,
   "Name": "Trạm y tế Phường 2",
   "address": "Phường 2, Thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4548522,
   "Latitude": 105.6361209
 },
 {
   "STT": 20,
   "Name": "Trạm y tế Phường 4",
   "address": "Phường 4, Thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4505598,
   "Latitude": 105.6265908
 },
 {
   "STT": 21,
   "Name": "Trạm y tế Phường 3",
   "address": "Phường 3, Thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4496815,
   "Latitude": 105.6441852
 },
 {
   "STT": 22,
   "Name": "Trạm y tế Phường 6",
   "address": "Phường 6, Thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4288288,
   "Latitude": 105.6343903
 },
 {
   "STT": 23,
   "Name": "Trạm y tế Xã Mỹ Ngãi",
   "address": "Xã Mỹ Ngãi, Thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.5034079,
   "Latitude": 105.590733
 },
 {
   "STT": 24,
   "Name": "Trạm y tế Xã Mỹ Tân",
   "address": "Xã Mỹ Tân, Thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4885898,
   "Latitude": 105.6196744
 },
 {
   "STT": 25,
   "Name": "Trạm y tế Xã Mỹ Trà",
   "address": "Xã Mỹ Trà, Thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4826015,
   "Latitude": 105.6426665
 },
 {
   "STT": 26,
   "Name": "Trạm y tế Phường Mỹ Phú",
   "address": "Phường Mỹ Phú, Thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4660294,
   "Latitude": 105.6441852
 },
 {
   "STT": 27,
   "Name": "Trạm y tế Xã Tân Thuận Tây",
   "address": "Xã Tân Thuận Tây, Thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4738847,
   "Latitude": 105.5858819
 },
 {
   "STT": 28,
   "Name": "Trạm y tế Phường Hoà Thuận",
   "address": "Phường Hoà Thuận, Thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4659127,
   "Latitude": 105.6192603
 },
 {
   "STT": 29,
   "Name": "Trạm y tế Xã Hòa An",
   "address": "Xã Hòa An, Thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4622676,
   "Latitude": 105.6086621
 },
 {
   "STT": 30,
   "Name": "Trạm y tế Xã Tân Thuận Đông",
   "address": "Xã Tân Thuận Đông, Thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4250752,
   "Latitude": 105.5893817
 },
 {
   "STT": 31,
   "Name": "Trạm y tế Xã Tịnh Thới",
   "address": "Xã Tịnh Thới, Thành phố Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4364387,
   "Latitude": 105.664511
 },
 {
   "STT": 32,
   "Name": "Trạm y tế Thị trấn Mỹ An",
   "address": "Thị trấn Mỹ An, Huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5225428,
   "Latitude": 105.8402536
 },
 {
   "STT": 33,
   "Name": "Trạm y tế Xã Thạnh Lợi",
   "address": "Xã Thạnh Lợi, Huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.7005628,
   "Latitude": 105.6999122
 },
 {
   "STT": 34,
   "Name": "Trạm y tế Xã Hưng Thạnh",
   "address": "Xã Hưng Thạnh, Huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.6568692,
   "Latitude": 105.6999122
 },
 {
   "STT": 35,
   "Name": "Trạm y tế Xã Trường Xuân",
   "address": "Xã Trường Xuân, Huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.6314323,
   "Latitude": 105.7703287
 },
 {
   "STT": 36,
   "Name": "Trạm y tế Xã Tân Kiều",
   "address": "Xã Tân Kiều, Huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5772264,
   "Latitude": 105.8701316
 },
 {
   "STT": 37,
   "Name": "Trạm y tế Xã Mỹ Hòa",
   "address": "Xã Mỹ Hòa, Huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5802329,
   "Latitude": 105.8114175
 },
 {
   "STT": 38,
   "Name": "Trạm y tế Xã Mỹ Quý",
   "address": "Xã Mỹ Quý, Huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5453505,
   "Latitude": 105.7468535
 },
 {
   "STT": 39,
   "Name": "Trạm y tế Xã Mỹ Đông",
   "address": "Xã Mỹ Đông, Huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5211623,
   "Latitude": 105.7938069
 },
 {
   "STT": 40,
   "Name": "Trạm y tế Xã Đốc Binh Kiều",
   "address": "Xã Đốc Binh Kiều, Huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.4933918,
   "Latitude": 105.9112424
 },
 {
   "STT": 41,
   "Name": "Trạm y tế Xã Mỹ An",
   "address": "Xã Mỹ An, Huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.5212651,
   "Latitude": 105.87028
 },
 {
   "STT": 42,
   "Name": "Trạm y tế Xã Phú Điền",
   "address": "Xã Phú Điền, Huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.4682585,
   "Latitude": 105.8701316
 },
 {
   "STT": 43,
   "Name": "Trạm y tế Xã Láng Biển",
   "address": "Xã Láng Biển, Huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.4936192,
   "Latitude": 105.7996769
 },
 {
   "STT": 44,
   "Name": "Trạm y tế Xã Thanh Mỹ",
   "address": "Xã Thanh Mỹ, Huyện Tháp Mười, TỈNH GIA LAI",
   "Longtitude": 10.4098655,
   "Latitude": 105.8407722
 },
 {
   "STT": 45,
   "Name": "Trạm y tế Thị trấn Thanh Bình",
   "address": "Thị trấn Thanh Bình, Huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5554601,
   "Latitude": 105.491758
 },
 {
   "STT": 46,
   "Name": "Trạm y tế Xã Tân Quới",
   "address": "Xã Tân Quới, Huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.6567686,
   "Latitude": 105.3775245
 },
 {
   "STT": 47,
   "Name": "Trạm y tế Xã Tân Hòa",
   "address": "Xã Tân Hòa, Huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.668875,
   "Latitude": 105.3541013
 },
 {
   "STT": 48,
   "Name": "Trạm y tế Xã An Phong",
   "address": "Xã An Phong, Huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.6325611,
   "Latitude": 105.4243804
 },
 {
   "STT": 49,
   "Name": "Trạm y tế Xã Phú Lợi",
   "address": "Xã Phú Lợi, Huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.6250429,
   "Latitude": 105.4653897
 },
 {
   "STT": 50,
   "Name": "Trạm y tế Xã Tân Mỹ",
   "address": "Xã Tân Mỹ, Huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.6215091,
   "Latitude": 105.5357139
 },
 {
   "STT": 51,
   "Name": "Trạm y tế Xã Bình Tấn",
   "address": "Xã Bình Tấn, Huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.6191437,
   "Latitude": 105.5826123
 },
 {
   "STT": 52,
   "Name": "Trạm y tế Xã Tân Huề",
   "address": "Xã Tân Huề, Huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.6136235,
   "Latitude": 105.3658125
 },
 {
   "STT": 53,
   "Name": "Trạm y tế Xã Tân Bình",
   "address": "Xã Tân Bình, Huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.6009459,
   "Latitude": 105.4009508
 },
 {
   "STT": 54,
   "Name": "Trạm y tế Xã Tân Thạnh",
   "address": "Xã Tân Thạnh, Huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5761773,
   "Latitude": 105.4595306
 },
 {
   "STT": 55,
   "Name": "Trạm y tế Xã Tân Phú",
   "address": "Xã Tân Phú, Huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.570921,
   "Latitude": 105.5054372
 },
 {
   "STT": 56,
   "Name": "Trạm y tế Xã Bình Thành",
   "address": "Xã Bình Thành, Huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5766484,
   "Latitude": 105.5591615
 },
 {
   "STT": 57,
   "Name": "Trạm y tế Xã Tân Long",
   "address": "Xã Tân Long, Huyện Thanh Bình, TỈNH GIA LAI",
   "Longtitude": 10.5790949,
   "Latitude": 105.4009508
 },
 {
   "STT": 58,
   "Name": "Trạm y tế Thị trấn Sa Rài",
   "address": "Thị trấn Sa Rài, Huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8753437,
   "Latitude": 105.4620245
 },
 {
   "STT": 59,
   "Name": "Trạm y tế Xã Tân Hộ Cơ",
   "address": "Xã Tân Hộ Cơ, Huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.9327262,
   "Latitude": 105.4419546
 },
 {
   "STT": 60,
   "Name": "Trạm y tế Xã Thông Bình",
   "address": "Xã Thông Bình, Huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.9354906,
   "Latitude": 105.494688
 },
 {
   "STT": 61,
   "Name": "Trạm y tế Xã Bình Phú",
   "address": "Xã Bình Phú, Huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8900758,
   "Latitude": 105.4185227
 },
 {
   "STT": 62,
   "Name": "Trạm y tế Xã Tân Thành A",
   "address": "Xã Tân Thành A, Huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8857723,
   "Latitude": 105.5481495
 },
 {
   "STT": 63,
   "Name": "Trạm y tế Xã Tân Thành B",
   "address": "Xã Tân Thành B, Huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8697347,
   "Latitude": 105.494688
 },
 {
   "STT": 64,
   "Name": "Trạm y tế Xã Tân Phước",
   "address": "Xã Tân Phước, Huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8390414,
   "Latitude": 105.5591615
 },
 {
   "STT": 65,
   "Name": "Trạm y tế Xã Tân Công Chí",
   "address": "Xã Tân Công Chí, Huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.8438495,
   "Latitude": 105.4653897
 },
 {
   "STT": 66,
   "Name": "Trạm y tế Xã An Phước",
   "address": "Xã An Phước, Huyện Tân Hồng, TỈNH GIA LAI",
   "Longtitude": 10.804624,
   "Latitude": 105.4829681
 },
 {
   "STT": 67,
   "Name": "Trạm y tế Thị trấn Tràm Chim",
   "address": "Thị trấn Tràm Chim, Huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.6691946,
   "Latitude": 105.5650239
 },
 {
   "STT": 68,
   "Name": "Trạm y tế Xã Hoà Bình",
   "address": "Xã Hoà Bình, Huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.7633938,
   "Latitude": 105.6471178
 },
 {
   "STT": 69,
   "Name": "Trạm y tế Xã Tân Công Sính",
   "address": "Xã Tân Công Sính, Huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.7272256,
   "Latitude": 105.6060661
 },
 {
   "STT": 70,
   "Name": "Trạm y tế Xã Phú Hiệp",
   "address": "Xã Phú Hiệp, Huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.7757656,
   "Latitude": 105.5122694
 },
 {
   "STT": 71,
   "Name": "Trạm y tế Xã Phú Đức",
   "address": "Xã Phú Đức, Huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.7353848,
   "Latitude": 105.5532993
 },
 {
   "STT": 72,
   "Name": "Trạm y tế Xã Phú Thành B",
   "address": "Xã Phú Thành B, Huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.7574512,
   "Latitude": 105.4419546
 },
 {
   "STT": 73,
   "Name": "Trạm y tế Xã An Hòa",
   "address": "Xã An Hòa, Huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.7436984,
   "Latitude": 105.3892372
 },
 {
   "STT": 74,
   "Name": "Trạm y tế Xã An Long",
   "address": "Xã An Long, Huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.720632,
   "Latitude": 105.4126652
 },
 {
   "STT": 75,
   "Name": "Trạm y tế Xã Phú Cường",
   "address": "Xã Phú Cường, Huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.6616491,
   "Latitude": 105.6060661
 },
 {
   "STT": 76,
   "Name": "Trạm y tế Xã Phú Ninh",
   "address": "Xã Phú Ninh, Huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.6774666,
   "Latitude": 105.4009508
 },
 {
   "STT": 77,
   "Name": "Trạm y tế Xã Phú Thọ",
   "address": "Xã Phú Thọ, Huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.6894462,
   "Latitude": 105.488828
 },
 {
   "STT": 78,
   "Name": "Trạm y tế Xã Phú Thành A",
   "address": "Xã Phú Thành A, Huyện Tam Nông, TỈNH GIA LAI",
   "Longtitude": 10.6975735,
   "Latitude": 105.4360963
 },
 {
   "STT": 79,
   "Name": "Trạm y tế Thị trấn Lấp Vò",
   "address": "Thị trấn Lấp Vò, Huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3577289,
   "Latitude": 105.5210607
 },
 {
   "STT": 80,
   "Name": "Trạm y tế Xã Mỹ An Hưng A",
   "address": "Xã Mỹ An Hưng A, Huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.4067257,
   "Latitude": 105.5767493
 },
 {
   "STT": 81,
   "Name": "Trạm y tế Xã Tân Mỹ",
   "address": "Xã Tân Mỹ, Huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3923418,
   "Latitude": 105.6471178
 },
 {
   "STT": 82,
   "Name": "Trạm y tế Xã Mỹ An Hưng B",
   "address": "Xã Mỹ An Hưng B, Huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3831971,
   "Latitude": 105.6119301
 },
 {
   "STT": 83,
   "Name": "Trạm y tế Xã Tân  Khánh Trung",
   "address": "Xã Tân  Khánh Trung, Huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3791189,
   "Latitude": 105.6940454
 },
 {
   "STT": 84,
   "Name": "Trạm y tế Xã Long Hưng A",
   "address": "Xã Long Hưng A, Huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3585135,
   "Latitude": 105.6705801
 },
 {
   "STT": 85,
   "Name": "Trạm y tế Xã Vĩnh Thạnh",
   "address": "Xã Vĩnh Thạnh, Huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3447983,
   "Latitude": 105.6177942
 },
 {
   "STT": 86,
   "Name": "Trạm y tế Xã Long Hưng B",
   "address": "Xã Long Hưng B, Huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3264445,
   "Latitude": 105.6588485
 },
 {
   "STT": 87,
   "Name": "Trạm y tế Xã Bình Thành",
   "address": "Xã Bình Thành, Huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3413789,
   "Latitude": 105.5767493
 },
 {
   "STT": 88,
   "Name": "Trạm y tế Xã Định An",
   "address": "Xã Định An, Huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3219082,
   "Latitude": 105.5298525
 },
 {
   "STT": 89,
   "Name": "Trạm y tế Xã Định Yên",
   "address": "Xã Định Yên, Huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.309874,
   "Latitude": 105.5532993
 },
 {
   "STT": 90,
   "Name": "Trạm y tế Xã Hội An Đông",
   "address": "Xã Hội An Đông, Huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.4078862,
   "Latitude": 105.5532993
 },
 {
   "STT": 91,
   "Name": "Trạm y tế Xã Bình Thạnh Trung",
   "address": "Xã Bình Thạnh Trung, Huyện Lấp Vò, TỈNH GIA LAI",
   "Longtitude": 10.3803603,
   "Latitude": 105.5591615
 },
 {
   "STT": 92,
   "Name": "Trạm y tế Thị trấn Lai Vung",
   "address": "Thị trấn Lai Vung, Huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2938059,
   "Latitude": 105.6588485
 },
 {
   "STT": 93,
   "Name": "Trạm y tế Xã Tân Dương",
   "address": "Xã Tân Dương, Huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.335001,
   "Latitude": 105.7057792
 },
 {
   "STT": 94,
   "Name": "Trạm y tế Xã Hòa Thành",
   "address": "Xã Hòa Thành, Huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2984155,
   "Latitude": 105.7118286
 },
 {
   "STT": 95,
   "Name": "Trạm y tế Xã Long Hậu",
   "address": "Xã Long Hậu, Huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2846597,
   "Latitude": 105.6236585
 },
 {
   "STT": 96,
   "Name": "Trạm y tế Xã Tân Phước",
   "address": "Xã Tân Phước, Huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2972665,
   "Latitude": 105.5884754
 },
 {
   "STT": 97,
   "Name": "Trạm y tế Xã Hòa Long",
   "address": "Xã Hòa Long, Huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2611796,
   "Latitude": 105.6588485
 },
 {
   "STT": 98,
   "Name": "Trạm y tế Xã Tân Thành",
   "address": "Xã Tân Thành, Huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2640799,
   "Latitude": 105.5837976
 },
 {
   "STT": 99,
   "Name": "Trạm y tế Xã Long Thắng",
   "address": "Xã Long Thắng, Huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2428539,
   "Latitude": 105.6999122
 },
 {
   "STT": 100,
   "Name": "Trạm y tế Xã Vĩnh Thới",
   "address": "Xã Vĩnh Thới, Huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2405852,
   "Latitude": 105.6353878
 },
 {
   "STT": 101,
   "Name": "Trạm y tế Xã Tân Hòa",
   "address": "Xã Tân Hòa, Huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.2074033,
   "Latitude": 105.6471178
 },
 {
   "STT": 102,
   "Name": "Trạm y tế Xã Định Hòa",
   "address": "Xã Định Hòa, Huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.1953911,
   "Latitude": 105.6705801
 },
 {
   "STT": 103,
   "Name": "Trạm y tế Xã Phong Hòa",
   "address": "Xã Phong Hòa, Huyện Lai Vung, TỈNH GIA LAI",
   "Longtitude": 10.1833808,
   "Latitude": 105.6940454
 },
 {
   "STT": 104,
   "Name": "Trạm y tế Xã Thường Phước 1",
   "address": "Xã Thường Phước 1, Huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8788463,
   "Latitude": 105.2077798
 },
 {
   "STT": 105,
   "Name": "Trạm y tế Xã Thường Thới Hậu A",
   "address": "Xã Thường Thới Hậu A, Huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8810796,
   "Latitude": 105.2721456
 },
 {
   "STT": 106,
   "Name": "Trạm y tế Xã Thường Thới Hậu B",
   "address": "Xã Thường Thới Hậu B, Huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8567804,
   "Latitude": 105.3189726
 },
 {
   "STT": 107,
   "Name": "Trạm y tế Xã Thường Thới Tiền",
   "address": "Xã Thường Thới Tiền, Huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.837225,
   "Latitude": 105.2721456
 },
 {
   "STT": 108,
   "Name": "Trạm y tế Xã Thường Phước 2",
   "address": "Xã Thường Phước 2, Huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8389934,
   "Latitude": 105.2370339
 },
 {
   "STT": 109,
   "Name": "Trạm y tế Xã Thường Lạc",
   "address": "Xã Thường Lạc, Huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8348603,
   "Latitude": 105.3189726
 },
 {
   "STT": 110,
   "Name": "Trạm y tế Xã Long Khánh A",
   "address": "Xã Long Khánh A, Huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.8031699,
   "Latitude": 105.2955575
 },
 {
   "STT": 111,
   "Name": "Trạm y tế Xã Long Khánh B",
   "address": "Xã Long Khánh B, Huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.7910359,
   "Latitude": 105.3189726
 },
 {
   "STT": 112,
   "Name": "Trạm y tế Xã Long Thuận",
   "address": "Xã Long Thuận, Huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.7708956,
   "Latitude": 105.2838511
 },
 {
   "STT": 113,
   "Name": "Trạm y tế Xã Phú Thuận B",
   "address": "Xã Phú Thuận B, Huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.7570042,
   "Latitude": 105.3423909
 },
 {
   "STT": 114,
   "Name": "Trạm y tế Xã Phú Thuận A",
   "address": "Xã Phú Thuận A, Huyện Hồng Ngự, TỈNH GIA LAI",
   "Longtitude": 10.7368718,
   "Latitude": 105.3072646
 },
 {
   "STT": 115,
   "Name": "Trạm y tế Thị trấn Cái Tàu Hạ",
   "address": "Thị trấn Cái Tàu Hạ, Huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2587308,
   "Latitude": 105.8730678
 },
 {
   "STT": 116,
   "Name": "Trạm y tế Xã An Hiệp",
   "address": "Xã An Hiệp, Huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2856663,
   "Latitude": 105.8231588
 },
 {
   "STT": 117,
   "Name": "Trạm y tế Xã An Nhơn",
   "address": "Xã An Nhơn, Huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2730435,
   "Latitude": 105.8583873
 },
 {
   "STT": 118,
   "Name": "Trạm y tế Xã Tân Nhuận Đông",
   "address": "Xã Tân Nhuận Đông, Huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2422003,
   "Latitude": 105.8231588
 },
 {
   "STT": 119,
   "Name": "Trạm y tế Xã Tân Bình",
   "address": "Xã Tân Bình, Huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2553925,
   "Latitude": 105.776198
 },
 {
   "STT": 120,
   "Name": "Trạm y tế Xã Tân Phú Trung",
   "address": "Xã Tân Phú Trung, Huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2462649,
   "Latitude": 105.7409852
 },
 {
   "STT": 121,
   "Name": "Trạm y tế Xã Phú Long",
   "address": "Xã Phú Long, Huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2222167,
   "Latitude": 105.7879371
 },
 {
   "STT": 122,
   "Name": "Trạm y tế Xã An Phú Thuận",
   "address": "Xã An Phú Thuận, Huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2169808,
   "Latitude": 105.8936224
 },
 {
   "STT": 123,
   "Name": "Trạm y tế Xã Phú Hựu",
   "address": "Xã Phú Hựu, Huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2404514,
   "Latitude": 105.8583873
 },
 {
   "STT": 124,
   "Name": "Trạm y tế Xã An Khánh",
   "address": "Xã An Khánh, Huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.1970145,
   "Latitude": 105.8583873
 },
 {
   "STT": 125,
   "Name": "Trạm y tế Xã Tân Phú",
   "address": "Xã Tân Phú, Huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.2130899,
   "Latitude": 105.7527221
 },
 {
   "STT": 126,
   "Name": "Trạm y tế Xã Hòa Tân",
   "address": "Xã Hòa Tân, Huyện Châu Thành, TỈNH GIA LAI",
   "Longtitude": 10.1719042,
   "Latitude": 105.8172881
 },
 {
   "STT": 127,
   "Name": "Trạm y tế Thị trấn Mỹ Thọ",
   "address": "Thị trấn Mỹ Thọ, Huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4501101,
   "Latitude": 105.6970266
 },
 {
   "STT": 128,
   "Name": "Trạm y tế Xã Gáo Giồng",
   "address": "Xã Gáo Giồng, Huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.6066717,
   "Latitude": 105.6293731
 },
 {
   "STT": 129,
   "Name": "Trạm y tế Xã Phương Thịnh",
   "address": "Xã Phương Thịnh, Huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.5983142,
   "Latitude": 105.6705801
 },
 {
   "STT": 130,
   "Name": "Trạm y tế Xã Ba Sao",
   "address": "Xã Ba Sao, Huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.5477285,
   "Latitude": 105.6999122
 },
 {
   "STT": 131,
   "Name": "Trạm y tế Xã Phong Mỹ",
   "address": "Xã Phong Mỹ, Huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.5318166,
   "Latitude": 105.5826123
 },
 {
   "STT": 132,
   "Name": "Trạm y tế Xã Tân Nghĩa",
   "address": "Xã Tân Nghĩa, Huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.5294622,
   "Latitude": 105.629523
 },
 {
   "STT": 133,
   "Name": "Trạm y tế Xã Phương Trà",
   "address": "Xã Phương Trà, Huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.5116284,
   "Latitude": 105.6588485
 },
 {
   "STT": 134,
   "Name": "Trạm y tế Xã Nhị Mỹ",
   "address": "Xã Nhị Mỹ, Huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4880535,
   "Latitude": 105.6940454
 },
 {
   "STT": 135,
   "Name": "Trạm y tế Xã Mỹ Thọ",
   "address": "Xã Mỹ Thọ, Huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4753833,
   "Latitude": 105.7292491
 },
 {
   "STT": 136,
   "Name": "Trạm y tế Xã Tân Hội Trung",
   "address": "Xã Tân Hội Trung, Huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4569711,
   "Latitude": 105.7703287
 },
 {
   "STT": 137,
   "Name": "Trạm y tế Xã An Bình",
   "address": "Xã An Bình, Huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4596806,
   "Latitude": 105.6617588
 },
 {
   "STT": 138,
   "Name": "Trạm y tế Xã Mỹ Hội",
   "address": "Xã Mỹ Hội, Huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4209146,
   "Latitude": 105.7292491
 },
 {
   "STT": 139,
   "Name": "Trạm y tế Xã Mỹ Hiệp",
   "address": "Xã Mỹ Hiệp, Huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4059017,
   "Latitude": 105.8114175
 },
 {
   "STT": 140,
   "Name": "Trạm y tế Xã Mỹ Long",
   "address": "Xã Mỹ Long, Huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.3744301,
   "Latitude": 105.7879371
 },
 {
   "STT": 141,
   "Name": "Trạm y tế Xã Bình Hàng Trung",
   "address": "Xã Bình Hàng Trung, Huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4088492,
   "Latitude": 105.7527221
 },
 {
   "STT": 142,
   "Name": "Trạm y tế Xã Mỹ Xương",
   "address": "Xã Mỹ Xương, Huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.4111988,
   "Latitude": 105.7057792
 },
 {
   "STT": 143,
   "Name": "Trạm y tế Xã Bình Hàng Tây",
   "address": "Xã Bình Hàng Tây, Huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.3756051,
   "Latitude": 105.7644596
 },
 {
   "STT": 144,
   "Name": "Trạm y tế Xã Bình Thạnh",
   "address": "Xã Bình Thạnh, Huyện Cao Lãnh, TỈNH GIA LAI",
   "Longtitude": 10.3200374,
   "Latitude": 105.7879371
 }
];