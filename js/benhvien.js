var data = [
 {
   "STT": 1,
   "Name": "Bệnh viện đa khoa tỉnh Gia Lai",
   "address": "132 Tôn Thất Tùng, Phù Đổng, Thành phố Pleiku, Gia Lai",
   "Longtitude": 13.982499,
   "Latitude": 108.027789
 },
 {
   "STT": 2,
   "Name": "Bệnh viện y học cổ truyền tỉnh Gia Lai",
   "address": "Hoa Lư, P. Hoa Lư, Thành phố Pleiku",
   "Longtitude": 13.988917,
   "Latitude": 108.026504
 },
 {
   "STT": 3,
   "Name": "Bệnh viện Điều dưỡng- PHCN",
   "address": "Ấp Bình A, Hồng Ngự, GIA LAI",
   "Longtitude": 13.980393,
   "Latitude": 108.003945
 },
 {
   "STT": 4,
   "Name": "Bệnh viện ChưPrông",
   "address": "Ia Drăng, Chư Prông, Gia Lai, Việt Nam",
   "Longtitude": 13.742112,
   "Latitude": 107.853112
 },
 {
   "STT": 5,
   "Name": "Trung Tâm Y Tế Chư sê",
   "address": "TT. Chư Sê, h. Chư Sê, Gia Lai, Việt Nam",
   "Longtitude": 13.695350,
   "Latitude": 108.080818
 },
 // {
 //   "STT": 6,
 //   "Name": "Bệnh viện Phục hồi chức năng",
 //   "address": "167 Tôn Đức Thắng, Phường 1, Thành phố Cao Lãnh, GIA LAI, Việt Nam",
 //   "Longtitude": 10.4670693,
 //   "Latitude": 105.6346658
 // },
 // {
 //   "STT": 7,
 //   "Name": "Bệnh viện Tâm thần",
 //   "address": "Tỉnh Lộ 847, Nhị Mỹ, Cao Lãnh, GIA LAI, Việt Nam",
 //   "Longtitude": 10.4677828,
 //   "Latitude": 105.7057231
 // },
 // {
 //   "STT": 8,
 //   "Name": "Bệnh viện Phổi",
 //   "address": "Tỉnh Lộ 847, Nhị Mỹ, Cao Lãnh, GIA LAI, Việt Nam",
 //   "Longtitude": 10.4686163,
 //   "Latitude": 105.7066673
 // },
 // {
 //   "STT": 9,
 //   "Name": "Bệnh viện Da liễu",
 //   "address": "396 Lê Đại Hành, Phường Mỹ Phú, Thành phố Cao Lãnh, GIA LAI, Việt Nam",
 //   "Longtitude": 10.4640062,
 //   "Latitude": 105.6363388
 // },
 // {
 //   "STT": 11,
 //   "Name": "Trung tâm Y tế huyện Tân Hồng",
 //   "address": "Số 09 Đường Trần Phú, Khóm III, Tân Hồng, GIA LAI, Việt Nam",
 //   "Longtitude": 10.8719693,
 //   "Latitude": 105.464574
 // },
 // {
 //   "STT": 12,
 //   "Name": "Trung tâm Y tế huyện Hồng Ngự",
 //   "address": "Thường Thới Tiền, Hồng Ngự, GIA LAI, Việt Nam",
 //   "Longtitude": 10.8130777,
 //   "Latitude": 105.2441539
 // },
 // {
 //   "STT": 13,
 //   "Name": "Trung tâm Y tế huyện Tam Nông",
 //   "address": "Thị trấn Tràm Chim, Tam Nông, GIA LAI, Việt Nam",
 //   "Longtitude": 10.6760501,
 //   "Latitude": 105.5612185
 // },
 // {
 //   "STT": 14,
 //   "Name": "Trung tâm Y tế huyện Thanh Bình",
 //   "address": "Thị trấn Thanh Bình, Thanh Bình, GIA LAI, Việt Nam",
 //   "Longtitude": 10.5524838,
 //   "Latitude": 105.4935604
 // },
 // {
 //   "STT": 15,
 //   "Name": "Trung tâm Y tế huyện Cao Lãnh",
 //   "address": "Thị trấn Mỹ Thọ Huyện Cao Lãnh, Thị trấn Mỹ Thọ, Cao Lãnh, GIA LAI, Việt Nam",
 //   "Longtitude": 10.4415923,
 //   "Latitude": 105.6794692
 // },
 // {
 //   "STT": 16,
 //   "Name": "Trung tâm Y tế huyện Tháp Mười",
 //   "address": "Nguyễn Văn Cừ, Thị trấn Mỹ An, Tháp Mười, GIA LAI, Việt Nam",
 //   "Longtitude": 10.5213947,
 //   "Latitude": 105.8397209
 // },
 // {
 //   "STT": 17,
 //   "Name": "Trung tâm Y tế huyện Lấp Vò",
 //   "address": "ĐT852B, Ấp Bình Hiệp A, Lấp Vò, GIA LAI, Việt Nam",
 //   "Longtitude": 10.3524604,
 //   "Latitude": 105.5360872
 // },
 // {
 //   "STT": 18,
 //   "Name": "Trung tâm Y tế huyện Lai Vung",
 //   "address": "Thị trấn Lai Vung, Lai Vung, GIA LAI, Việt Nam",
 //   "Longtitude": 10.2918495,
 //   "Latitude": 105.6448048
 // },
 // {
 //   "STT": 19,
 //   "Name": "Trung tâm Y tế huyện Châu Thành",
 //   "address": "Phú Mỹ Hiệp Thị trấn Cái Tàu Hạ Huyện Châu Thành, Phú Hựu, Châu Thành, GIA LAI, Việt Nam",
 //   "Longtitude": 10.3456502,
 //   "Latitude": 105.4881289
 // },
 // {
 //   "STT": 20,
 //   "Name": "Trung tâm Y tế thị xã Hồng Ngự",
 //   "address": "Đường Chu Văn An, Phường An Thạnh, Hồng Ngự, GIA LAI, Việt Nam",
 //   "Longtitude": 10.8073191,
 //   "Latitude": 105.3369623
 // },
 // {
 //   "STT": 21,
 //   "Name": "Trung tâm Y tế thành phố Sa Đéc",
 //   "address": "Phường 1, Sa Đéc, GIA LAI, Việt Nam",
 //   "Longtitude": 10.3050544,
 //   "Latitude": 105.7552615
 // },
 // {
 //   "STT": 22,
 //   "Name": "Trung tâm Y tế thành phố Cao Lãnh",
 //   "address": "Đường Trần Thị Nhượng, Phường 4, Thành phố Cao Lãnh, GIA LAI, Việt Nam",
 //   "Longtitude": 10.4527351,
 //   "Latitude": 105.5958545
 // }
];