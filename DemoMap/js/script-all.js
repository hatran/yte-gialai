﻿
var lat = 10.823099; 
var lon = 106.629662;
var map = new L.Map('map', {

        zoom: 18,
        minZoom: 4,
});



    // create a new tile layer
    var tileUrl = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
    layer = new L.TileLayer(tileUrl,
    {
        attribution: '',//'Maps © <a href=\"www.openstreetmap.org/copyright\">OpenStreetMap</a> contributors',
        maxZoom: 12
    });

    // add the layer to the map
    map.addLayer(layer);

    // var cuuthuong = getParameterByName('ct');
    var cuuthuong = 1;
    // var cuuhoa = getParameterByName('ch');
    var cuuhoa = 1;
    

    var xethang1 = [[10.773583, 106.705704], [10.773583, 106.705704]];
    var xethang2 = [[10.785594, 106.611873], [10.785594, 106.611873]];
    var xethang3 = [[10.792575, 106.653468], [10.792575, 106.653468]];
    var xethang4 = [[10.853824, 106.628341], [10.853824, 106.628341]];
    

    // var xebot1 = [[lat - 0.01, lon + 0.02], [lat - 0.041, lon + 0.047], [lat - 0.041, lon + 0.047], [lat - 0.01, lon + 0.02]];
    // var xebot2 = [[lat - 0.032, lon + 0.068], [lat + 0.02, lon - 0.02], [lat + 0.02, lon - 0.02], [lat - 0.032, lon + 0.068]];
    var xethang5 = [[10.823099, 106.629662], [10.823099, 106.629662]];
    var xethang6 = [[10.763755,, 106.660113], [10.763755,, 106.660113]];
    var xethang7 = [[10.861294, 106.679985], [10.861294, 106.679985]];
    var xethang8 = [[10.813894, 106.677950], [10.813894, 106.677950]];



//map.fitBounds(mymap);

    map.setView([lat, lon], 16);

    var FireIcon = L.icon({
        iconUrl: 'images/icons/icon_camera.png',
        iconSize: [36, 36], // size of the icon
    });

    var CCIcon = L.icon({
        iconUrl: 'images/icons/icon_camera.png',
        iconSize: [32, 32], // size of the icon
    });

    var CTIcon = L.icon({
        iconUrl: 'images/Ol_icon_red_example.png',
        iconSize: [32, 32], // size of the icon
    });

    var CHIcon = L.icon({
        iconUrl: 'images/jeep.png',
        iconSize: [32, 32], // size of the icon
    });

    var marker = L.marker([lat, lon], { icon: FireIcon }).addTo(map);

    if (cuuhoa == 1) {
        //========================================================================
        var marker1 = L.Marker.movingMarker(xethang1,
            [40000, 12000, 40000], { autostart: false, loop: true, icon: CCIcon }).addTo(map);

        marker1.loops = 0;
        marker1.bindPopup();
        marker1.on('mouseover', function(e) {
            //open popup;
            var popup = L.popup()
                .setLatLng(e.latlng)
                // .setContent('Xe thang 1 thuộc: ' + lstFirefighter[0].name + '<br> Còn <strong>8</strong> phút nữa hiện trường')
                .setContent('Camera <br> Techcombank - Atm')
                .openOn(map);
        });

        marker1.once('click', function () {
            marker1.start();
            marker1.closePopup();
            marker1.unbindPopup();
            marker1.on('click', function () {
                if (marker1.isRunning()) {
                    marker1.pause();
                } else {
                    marker1.start();
                }
                // document.getElementById('exampleModal4').style.display = 'block';
                openModal('myModal1');
            });
        });

        //========================================================================

        var marker2 = L.Marker.movingMarker(xethang2,
            [50000, 40000, 40000, 50000], { autostart: false, loop: true, icon: CCIcon }).addTo(map);

        marker2.loops = 0;
        marker2.bindPopup();
        marker2.on('mouseover', function(e) {
            //open popup;
            var popup = L.popup()
                .setLatLng(e.latlng)
                // .setContent('Xe thang 2 thuộc: ' + lstFirefighter[1].name + '<br> Còn <strong>30</strong> phút nữa hiện trường')
                .setContent('Camera <br> Trường tiểu học Bình Long')
                .openOn(map);
        });
        marker2.once('click', function () {
            marker2.start();
            marker2.closePopup();
            marker2.unbindPopup();
            marker2.on('click', function () {
                if (marker2.isRunning()) {
                    marker2.pause();
                } else {
                    marker2.start();
                }
                // document.getElementById('exampleModal3').style.display = 'block';
                openModal('myModal2');

            });
        });

         var marker3 = L.Marker.movingMarker(xethang3,
            [50000, 40000, 40000, 50000], { autostart: false, loop: true, icon: CCIcon }).addTo(map);

        marker3.loops = 0;
        marker3.bindPopup();
        marker3.on('mouseover', function(e) {
            //open popup;
            var popup = L.popup()
                .setLatLng(e.latlng)
                // .setContent('Xe thang 2 thuộc: ' + lstFirefighter[1].name + '<br> Còn <strong>30</strong> phút nữa hiện trường')
                .setContent('Camera <br> Ngã tư Lý Thừng Kiệt')
                .openOn(map);
        });
        marker3.once('click', function () {
            marker3.start();
            marker3.closePopup();
            marker3.unbindPopup();
            marker3.on('click', function () {
                if (marker3.isRunning()) {
                    marker3.pause();
                } else {
                    marker3.start();
                }
                // document.getElementById('exampleModal3').style.display = 'block';
                openModal('myModal2');

            });
        });

        // Camer 5
        var marker4 = L.Marker.movingMarker(xethang4,
            [50000, 40000, 40000, 50000], { autostart: false, loop: true, icon: CCIcon }).addTo(map);

        marker4.loops = 0;
        marker4.bindPopup();
        marker4.on('mouseover', function(e) {
            //open popup;
            var popup = L.popup()
                .setLatLng(e.latlng)
                // .setContent('Xe thang 2 thuộc: ' + lstFirefighter[1].name + '<br> Còn <strong>30</strong> phút nữa hiện trường')
                .setContent('Camera <br> Công viên phần mềm Quang Trung')
                .openOn(map);
        });
        marker4.once('click', function () {
            marker4.start();
            marker4.closePopup();
            marker4.unbindPopup();
            marker4.on('click', function () {
                if (marker4.isRunning()) {
                    marker4.pause();
                } else {
                    marker4.start();
                }
                // document.getElementById('exampleModal3').style.display = 'block';
                openModal('myModal2');

            });
        });


        // Camer 6
        var marker7 = L.Marker.movingMarker(xethang7,
            [50000, 40000, 40000, 50000], { autostart: false, loop: true, icon: CCIcon }).addTo(map);

        marker7.loops = 0;
        marker7.bindPopup();
        marker7.on('mouseover', function(e) {
            //open popup;
            var popup = L.popup()
                .setLatLng(e.latlng)
                // .setContent('Xe thang 2 thuộc: ' + lstFirefighter[1].name + '<br> Còn <strong>30</strong> phút nữa hiện trường')
                .setContent('Camera <br> Cầu vượt ngã tư Gia')
                .openOn(map);
        });
        marker7.once('click', function () {
            marker7.start();
            marker7.closePopup();
            marker7.unbindPopup();
            marker7.on('click', function () {
                if (marker7.isRunning()) {
                    marker7.pause();
                } else {
                    marker7.start();
                }
                // document.getElementById('exampleModal3').style.display = 'block';
                openModal('myModal2');

            });
        });
        // Camer 7
        var marker8 = L.Marker.movingMarker(xethang8,
            [50000, 40000, 40000, 50000], { autostart: false, loop: true, icon: CCIcon }).addTo(map);

        marker8.loops = 0;
        marker8.bindPopup();
        marker8.on('mouseover', function(e) {
            //open popup;
            var popup = L.popup()
                .setLatLng(e.latlng)
                // .setContent('Xe thang 2 thuộc: ' + lstFirefighter[1].name + '<br> Còn <strong>30</strong> phút nữa hiện trường')
                .setContent('Camera <br> Vòng xoay Nguyễn Thái Sơn')
                .openOn(map);
        });
        marker8.once('click', function () {
            marker8.start();
            marker8.closePopup();
            marker8.unbindPopup();
            marker8.on('click', function () {
                if (marker8.isRunning()) {
                    marker8.pause();
                } else {
                    marker8.start();
                }
                // document.getElementById('exampleModal3').style.display = 'block';
                openModal('myModal2');

            });
        });

        //=========================================================================

        // var marker3 = L.Marker.movingMarker(xebot1,
        //     [16000, 11000, 16000], { autostart: false, loop: true, icon: CCIcon }).addTo(map);
        //
        // marker3.loops = 0;
        // marker3.bindPopup('Xe bọt 1', { closeOnClick: false });
        // marker3.on('loop', function (e) {
        //     //if (e.elapsedTime < 50) {
        //     marker3.getPopup().setContent("Xe bọt 1")
        //     marker3.openPopup();
        //     setTimeout(function () {
        //         marker3.closePopup();
        //     }, 200);
        //     //}
        // });
        // marker3.once('click', function () {
        //     marker3.start();
        //     marker3.closePopup();
        //     marker3.unbindPopup();
        //     marker3.on('click', function () {
        //         if (marker3.isRunning()) {
        //             marker3.pause();
        //         } else {
        //             marker3.start();
        //         }
        //     });
        // });
        //
        // var marker4 = L.Marker.movingMarker(xebot2,
        //     [16000, 16000, 16000], { autostart: false, loop: true, icon: CCIcon }).addTo(map);
        //
        // marker4.loops = 0;
        // marker4.bindPopup('Xe bọt 2', { closeOnClick: false });
        // marker4.on('loop', function (e) {
        //     //if (e.elapsedTime < 50) {
        //     marker4.getPopup().setContent("Xe bọt 2")
        //     marker4.openPopup();
        //     setTimeout(function () {
        //         marker4.closePopup();
        //     }, 200);
        //     //}
        // });
        // marker4.once('click', function () {
        //     marker4.start();
        //     marker4.closePopup();
        //     marker4.unbindPopup();
        //     marker4.on('click', function () {
        //         if (marker4.isRunning()) {
        //             marker4.pause();
        //         } else {
        //             marker4.start();
        //         }
        //     });
        // });

         var marker5 = L.Marker.movingMarker(xethang5,
             [16000, 16000, 16000, 16000, 16000, 16000, 16000, 16000], { autostart: false, loop: true, icon: CCIcon }).addTo(map);
        
         marker5.loops = 0;
         marker5.bindPopup();
         marker5.on('mouseover', function(e) {
             //open popup;
             var popup = L.popup()
                 .setLatLng(e.latlng)
                 // .setContent('Xe thang 3 thuộc: ' + lstFirefighter[2].name + '<br> Còn <strong>5</strong> phút nữa hiện trường')
                 .setContent('Camera <br> Ngã ba Trường Trinh')
                 .openOn(map);
         });
         marker5.once('click', function () {
             marker5.start();
             marker5.closePopup();
             marker5.unbindPopup();
             marker5.on('click', function () {
                 if (marker5.isRunning()) {
                     marker5.pause();
                 } else {
                     marker5.start();
                 }
                 // document.getElementById('exampleModal2').style.display = 'block';
                 openModal('myModal3');
             });
         });
         
         var marker6 = L.Marker.movingMarker(xethang6,
                 [16000, 16000, 16000, 16000, 16000, 16000, 16000, 16000], { autostart: false, loop: true, icon: CCIcon }).addTo(map);
            
         marker6.loops = 0;
         marker6.bindPopup();
         marker6.on('mouseover', function(e) {
                 //open popup;
                 var popup = L.popup()
                     .setLatLng(e.latlng)
                     // .setContent('Xe thang 3 thuộc: ' + lstFirefighter[2].name + '<br> Còn <strong>5</strong> phút nữa hiện trường')
                     .setContent('Camera <br>Ngã tư đường Ba tháng hai')
                     .openOn(map);
             });

         marker6.once('click', function () {
        	 marker6.start();
        	 marker6.closePopup();
        	 marker6.unbindPopup();
        	 marker6.on('click', function () {
                     if (marker6.isRunning()) {
                    	 marker6.pause();
                     } else {
                    	 marker6.start();
                     }
                     // window.location = 'camera.html';
                     openModal('myModal4');
                     // document.getElementById('exampleModal1').style.display = 'block';
                 });
             });
//          function rootURL {
//     window.location = 'baocaoguive.html';
// }
         //khai bien global rootURL => link ngoài

         //Viet function change rootURL = url page sau

         // Click -> gọi function
        //
        // var marker6 = L.Marker.movingMarker(xethang6,
        //     [16000, 16000, 16000,16000,16000], { autostart: false, loop: true, icon: CCIcon }).addTo(map);
        //
        // marker6.loops = 0;
        // marker6.bindPopup('Xe bọt 2', { closeOnClick: false });
        // marker6.on('loop', function (e) {
        //     //if (e.elapsedTime < 50) {
        //     marker6.getPopup().setContent("Xe bọt 2")
        //     marker6.openPopup();
        //     setTimeout(function () {
        //         marker6.closePopup();
        //     }, 200);
        //     //}
        // });
        // marker6.once('click', function () {
        //     marker6.start();
        //     marker6.closePopup();
        //     marker6.unbindPopup();
        //     marker6.on('click', function () {
        //         if (marker6.isRunning()) {
        //             marker6.pause();
        //         } else {
        //             marker6.start();
        //         }
        //     });
        // });
    }

    // if (cuuthuong == 1) {
    //     var markerCT1 = L.Marker.movingMarker(xecuuthuong1,
    //         [32000, 10000, 32000], { autostart: false, loop: true, icon: CTIcon }).addTo(map);

    //     markerCT1.loops = 0;
    //     markerCT1.bindPopup();
    //     markerCT1.on('mouseover', function(e) {
    //         //open popup;
    //         var popup = L.popup()
    //             .setLatLng(e.latlng)
    //             // .setContent('Xe cứu thương 1 thuộc: ' + lstHospital[2].name + '<br> Còn <strong>10</strong> phút nữa hiện trường')
    //             .setContent('Xe cứu thương 1 <br> Còn <strong>10</strong> phút nữa đến hiện trường')
    //             .openOn(map);
    //     });
    //     markerCT1.once('click', function () {
    //         markerCT1.start();
    //         markerCT1.closePopup();
    //         markerCT1.unbindPopup();
    //         markerCT1.on('click', function () {
    //             if (markerCT1.isRunning()) {
    //                 markerCT1.pause();
    //             } else {
    //                 markerCT1.start();
    //             }
    //         });
    //     });

    //     var markerCT2 = L.Marker.movingMarker(xecuuthuong2,
    //         [35000, 30000, 35000], { autostart: false, loop: true, icon: CTIcon }).addTo(map);

    //     markerCT2.loops = 0;
    //     markerCT2.bindPopup();
    //     markerCT2.on('mouseover', function(e) {
    //         //open popup;
    //          var popup = L.popup()
    //             .setLatLng(e.latlng)
    //             // .setContent('Xe cứu thương 2 thuộc: ' + lstHospital[1].name + '<br> Còn <strong>10</strong> phút nữa hiện trường')
    //             .setContent('Xe cứu thương 2 <br> Còn <strong>10</strong> phút nữa đến hiện trường')
    //             .openOn(map);
    //     });

    //     markerCT2.once('click', function () {
    //         markerCT2.start();
    //         markerCT2.closePopup();
    //         markerCT2.unbindPopup();
    //         markerCT2.on('click', function () {
    //             if (markerCT2.isRunning()) {
    //                 markerCT2.pause();
    //             } else {
    //                 markerCT2.start();
    //             }
    //         });
    //     });

        // var markerCT3 = L.Marker.movingMarker(xecuuthuong3,
        //     [35000, 35000, 35000, 35000], { autostart: false, loop: true, icon: CTIcon }).addTo(map);
        //
        // markerCT3.loops = 0;
        // markerCT3.bindPopup('Xe cứu thương 3', { closeOnClick: false });
        // markerCT3.on('loop', function (e) {
        //     //if (e.elapsedTime < 50) {
        //     markerCT3.getPopup().setContent("Xe cứu thương 3")
        //     markerCT3.openPopup();
        //     setTimeout(function () {
        //         markerCT3.closePopup();
        //     }, 200);
        //     //}
        // });
        //
        // markerCT3.once('click', function () {
        //     markerCT3.start();
        //     markerCT3.closePopup();
        //     markerCT3.unbindPopup();
        //     markerCT3.on('click', function () {
        //         if (markerCT3.isRunning()) {
        //             markerCT3.pause();
        //         } else {
        //             markerCT3.start();
        //         }
        //     });
        // });
        // var markerCT4 = L.Marker.movingMarker(xecuuthuong4,
        //     [35000, 35000, 35000, 35000], { autostart: false, loop: true, icon: CTIcon }).addTo(map);
        //
        // markerCT4.loops = 0;
        // markerCT4.bindPopup('Xe cứu thương 4', { closeOnClick: false });
        // markerCT4.on('loop', function (e) {
        //     //if (e.elapsedTime < 50) {
        //     markerCT4.getPopup().setContent("Xe cứu thương 4")
        //     markerCT4.openPopup();
        //     setTimeout(function () {
        //         markerCT4.closePopup();
        //     }, 200);
        //     //}
        // });
        //
        // markerCT4.once('click', function () {
        //     markerCT4.start();
        //     markerCT4.closePopup();
        //     markerCT4.unbindPopup();
        //     markerCT4.on('click', function () {
        //         if (markerCT4.isRunning()) {
        //             markerCT4.pause();
        //         } else {
        //             markerCT4.start();
        //         }
        //     });
        // });
    // }

    // if (chihuy == 1)
    // {
    //     var markerCH = L.marker([lat - 0.02, lon - 0.02], { icon: CHIcon }).addTo(map);
    // }
 
    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, '\\$&');
        var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, ' '));
    }

    function openModal(idModal) {
        document.getElementById(idModal).style.display = "block";
    }
