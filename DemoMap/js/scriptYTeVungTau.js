﻿var lat = 10.50824
var lon = 107.18699;
var map = new L.Map('map', {
    zoom: 12
});

// create a new tile layer
var tileUrl = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
    layer = new L.TileLayer(tileUrl, {
        attribution: '', //'Maps © <a href=\"www.openstreetmap.org/copyright\">OpenStreetMap</a> contributors',
        maxZoom: 20,
        minZoom: 4
    });

map.addLayer(layer);

map.setView([lat, lon], 15);

var hospitalIcon = L.icon({
    iconUrl: 'images/pin-hopital.png',
    iconSize: [30, 30], // size of the icon
});

var FireIcon = L.icon({
    iconUrl: 'images/fire-2-32_2.gif',
    iconSize: [36, 36], // size of the icon
});

var carIcon = L.icon({
    iconUrl: 'images/ambulance.png',
    iconSize: [36, 36], // size of the icon
});

var lstHospital = data;
// console.log(lstHospital);
var color = null;
var lstColor = [{
        "color": "#003366",
    },
    {
        "color": "#0000FF"
    },
    {
        "color": "#33CC33"
    },
    {
        "color": "#990033"
    },
    {
        "color": "#FF66FF"
    },
    {
        "color": "#CD853F"
    },
    {
        "color": "#CDCD00"
    },
    {
        "color": "#8B658B"
    },
    {
        "color": "#5D478B"
    }
];

var xecuuthuong1 = [
    [lstHospital[0].lat, lstHospital[0].lon],
    [10.50760,107.19743],
    [10.50619,107.19677],
    [10.50726,107.19454],
    [10.51037,107.18808],
    [lat, lon]
];
var xecuuthuong2 = [
    [lstHospital[1].lat, lstHospital[1].lon],
    [10.50784,107.19338],
    [10.50928,107.19033],
    [10.50930,107.19037],
    [10.51038,107.18805],
    [lat, lon]
];

var xecuuthuong3 = [
    [lstHospital[2].lat, lstHospital[2].lon],
    [10.50601,107.17791],
    [10.50882,107.18151],
    [10.51241,107.18579],
    [10.51036,107.18807],
    [lat, lon]
];


var marker = L.marker([lat, lon], { icon: FireIcon }).bindPopup('<p style="font-size:20px;">Hiện Trường: <span style="color:blue">Chợ Long Tâm</span></p>').addTo(map);
marker.openPopup();
var route = null;
var maker = null;


var markerCT1 = L.Marker.movingMarker(xecuuthuong1,
    [10000, 10000, 10000, 10000, 10000], { autostart: true, loop: false, icon: carIcon }).addTo(map);

markerCT1.loops = 0;
markerCT1.bindPopup();
markerCT1.on('mouseover', function(e) {
    //open popup;
    var popup = L.popup()
        .setLatLng(e.latlng)
        .setContent('Xe cứu thương biển số 72A - 90378 thuộc: Bệnh viện đa khoa tỉnh Pleiku<br>Lái xe: Bùi Đức Toàn<br>Bác sỹ: Nông Văn Dũng<br>Điều dưỡng: Phạm Thái Dương<br>Địa chỉ: Chợ Hà Lâm<br>Bệnh nhân: Nguyên Văn Đạt <br>Còn <strong>15</strong> phút nữa hiện trường')
        .openOn(map);
});
markerCT1.start();

var markerCT2 = L.Marker.movingMarker(xecuuthuong2,
    [10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000], { autostart: true, loop: false, icon: carIcon }).addTo(map);

markerCT2.loops = 0;
markerCT2.bindPopup();
markerCT2.on('mouseover', function(e) {
    //open popup;
    var popup = L.popup()
        .setLatLng(e.latlng)
        .setContent('Xe cứu thương biển số 72A - 76904 thuộc: Bệnh viện quân y<br>Lái xe: Ngô Xuân Hải<br>Bác sỹ: Lưu T. Quỳnh Nga<br>Điều dưỡng: Phạm Thị Thương<br>Địa chỉ: Chợ Hà Lâm<br>Bệnh nhân: Nguyên Văn Long <br>Còn <strong>10</strong> phút nữa hiện trường')
        .openOn(map);
});
markerCT2.start();

var markerCT3 = L.Marker.movingMarker(xecuuthuong3,
    [10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000], { autostart: true, loop: false, icon: carIcon }).addTo(map);

markerCT3.loops = 0;
markerCT3.bindPopup();
markerCT3.on('mouseover', function(e) {
    //open popup;
    var popup = L.popup()
        .setLatLng(e.latlng)
        .setContent('Xe cứu thương biển số 72A - 76904 thuộc: Bệnh viện mắt<br>Lái xe: Ngô Xuân Hải<br>Bác sỹ: Lưu T. Quỳnh Nga<br>Điều dưỡng: Phạm Thị Thương<br>Địa chỉ: Chợ Hà Lâm<br>Bệnh nhân: Nguyên Văn Long <br>Còn <strong>10</strong> phút nữa hiện trường')
        .openOn(map);
});
markerCT3.start();

var route1 = new L.Routing.control({
    waypoints: [
        L.latLng(lstHospital[0].lat, lstHospital[0].lon),
        L.latLng(lat, lon)
    ],
    addWaypoints: false,
    lineOptions: {
        styles: [{ color: "red", opacity: 1, weight: 5 }]
    },
    createMarker: function() { return null; },
    draggableWaypoints: false,
    routeWhileDragging: true
}).addTo(map);

var route2 = new L.Routing.control({
    waypoints: [
        L.latLng(lstHospital[1].lat, lstHospital[1].lon),
        L.latLng(lat, lon)
    ],
    addWaypoints: false,
    lineOptions: {
        styles: [{ color: "blue", opacity: 1, weight: 5 }]
    },
    createMarker: function() { return null; },
    draggableWaypoints: false,
    routeWhileDragging: true
}).addTo(map);

var route3 = new L.Routing.control({
    waypoints: [
        L.latLng(lstHospital[2].lat, lstHospital[2].lon),
        L.latLng(lat, lon)
    ],
    addWaypoints: false,
    lineOptions: {
        styles: [{ color: "SaddleBrown", opacity: 1, weight: 5 }]
    },
    createMarker: function() { return null; },
    draggableWaypoints: false,
    routeWhileDragging: true
}).addTo(map);

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

function openModal(idModal) {
    document.getElementById(idModal).style.display = "block";
}