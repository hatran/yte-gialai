var data = [
 {
   "STT": 1,
   "Name": "Bệnh viện đa khoa tỉnh Kiên Giang",
   "address": "Phường Vĩnh Thanh Vân,TP Rạch Giá, Kiên Giang",
   "Longtitude": 10.0095159,
   "Latitude": 105.0832318
 },
 {
   "STT": 2,
   "Name": "Bệnh viện Y dược cổ truyền tỉnh Kiên Giang",
   "address": "Phường Vĩnh Lạc, TP Rạch Giá, Kiên Giang",
   "Longtitude": 9.9943793,
   "Latitude": 105.0943227
 },
 {
   "STT": 3,
   "Name": "Bệnh viện tư nhân Bình An",
   "address": "Nguyễn Trung Trực, Phường Vĩnh Bảo, tp. Rạch Giá, tỉnh Kiên Giang, Việt Nam",
   "Longtitude": 10.0045545,
   "Latitude": 105.0844343
 },
 {
   "STT": 4,
   "Name": "Bệnh viện huyện An Minh",
   "address": "TT. Thứ Mười Một, An Minh, tỉnh Kiên Giang, Việt Nam",
   "Longtitude": 9.7134832,
   "Latitude": 104.9647228
 },
 {
   "STT": 5,
   "Name": "Bệnh viện huyện An Biên",
   "address": "TT. Thứ Ba, tx.An Biên, tỉnh Kiên Giang, Việt Nam",
   "Longtitude": 9.817606,
   "Latitude": 105.0636672
 },
 {
   "STT": 6,
   "Name": "Bệnh viện huyện Vinh Thuận",
   "address": "Vĩnh Thuận, tỉnh Kiên Giang, Việt Nam",
   "Longtitude": 9.5106455,
   "Latitude": 105.2582522
 },
 {
   "STT": 7,
   "Name": "Bệnh viện huyện Giồng Riềng",
   "address": "TT. Giồng Riềng, Giồng Riềng, tỉnh Kiên Giang, Việt Nam",
   "Longtitude": 9.9077243,
   "Latitude": 105.3084359
 },
 {
   "STT": 8,
   "Name": "Bệnh viện huyện Gò Quao",
   "address": "TT. Gò Quao, Gò Quao, tỉnh Kiên Giang, Việt Nam",
   "Longtitude": 9.7402775,
   "Latitude": 105.2750797
 },
 {
   "STT": 9,
   "Name": "Bệnh viện huyện Châu Thành",
   "address": "TT. Minh Lương, Châu Thành, tỉnh Kiên Giang, Việt Nam",
   "Longtitude": 10.0669087,
   "Latitude": 105.4458995
 },
 {
   "STT": 10,
   "Name": "Bệnh viện huyện Tân Hiệp",
   "address": "TT. Tân Hiệp, Tân Hịêp, tỉnh Kiên Giang, Việt Nam",
   "Longtitude": 10.1132946,
   "Latitude": 105.2810252
 },
 {
   "STT": 11,
   "Name": "Bệnh viện huyện Hòn Đất",
   "address": "TT. Hòn Đất, Hòn Đất, tỉnh Kiên Giang, Việt Nam",
   "Longtitude": 10.1791336,
   "Latitude": 104.9371335
 },
 {
   "STT": 12,
   "Name": "Bệnh viện huyện Kiên Lương",
   "address": "Thị Trấn Kiên Lương, Huyện Kiên Lương, tỉnh Kiên Giang, Việt Nam",
   "Longtitude": 10.2517428,
   "Latitude": 104.5956464
 },
 {
   "STT": 13,
   "Name": "Bệnh viện thị xã Hà Tiên",
   "address": "Khu Phố 3, Hà Tiên, tỉnh Kiên Giang, Việt Nam",
   "Longtitude": 10.37292,
   "Latitude": 104.488873
 },
 {
   "STT": 14,
   "Name": "Bệnh viện Phú Quốc",
   "address": "TT. Dương Đông, Phú Quốc, tỉnh Kiên Giang, Việt Nam",
   "Longtitude": 10.1561993,
   "Latitude": 103.8023344
 }
];